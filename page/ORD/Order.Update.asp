<%@  language="VBScript" codepage="65001" %>
<!-- #include virtual = "/share/include/DbInfo.asp" -->
<!-- #include virtual = "/share/include/Injection.asp" -->
<!-- #include virtual = "/share/include/Function.asp" -->
<!-- #include virtual = "/share/include/Const.asp" -->
<!-- #include virtual = "/share/include/PageHeader.asp" -->
<!-- #include virtual = "/share/include/ContentsHeader.asp" -->

<%
	OrderCode = str_chk(Request("Oc"))

	sql = ""
	sql = sql & vbCrlf & "      select    a.order_code,     "
	sql = sql & vbCrlf & "                  a.ver,     "
	sql = sql & vbCrlf & "                  ( select  z.site  from  sales_member z with(nolock)  where  z.id = a.mem_id ) as site,     "
	sql = sql & vbCrlf & "                  a.order_type,     "
	sql = sql & vbCrlf & "                  ( select  z.t_name  from  sales_order_type z with(nolock)  where  z.t_code = a.order_type ) as order_type_nm,     "
	sql = sql & vbCrlf & "                  a.mem_id,     "
	sql = sql & vbCrlf & "                  ( select  z.name  from  sales_member z with(nolock)  where  z.id = a.mem_id ) as mem_id_nm,     "
	sql = sql & vbCrlf & "                  a.emp_id,     "
	sql = sql & vbCrlf & "                  ( select  z.name  from  sales_member z with(nolock)  where  z.id = a.emp_id ) as emp_id_nm,     "
	sql = sql & vbCrlf & "                  a.vat_type,     "
	sql = sql & vbCrlf & "                  a.cus_code,     "
	sql = sql & vbCrlf & "                  a.insu_man,     "
	sql = sql & vbCrlf & "                  a.insu_tel,     "
	sql = sql & vbCrlf & "                  a.b_zipcode,     "
	sql = sql & vbCrlf & "                  a.b_add1,     "
	sql = sql & vbCrlf & "                  a.b_add2,     "
	sql = sql & vbCrlf & "                  a.imsi_code,     "
	sql = sql & vbCrlf & "                  a.odate,     "
	sql = sql & vbCrlf & "                  a.otime,     "
	sql = sql & vbCrlf & "                  a.inhouse,     "
	sql = sql & vbCrlf & "                  a.sigong_type,     "
	sql = sql & vbCrlf & "                  a.sdate,     "
	sql = sql & vbCrlf & "                  a.sigong_day,     "
	sql = sql & vbCrlf & "                  a.out_memo,     "
	sql = sql & vbCrlf & "                  a.idx     "
	sql = sql & vbCrlf & "        from   sales_order a with(nolock)     "
	sql = sql & vbCrlf & "       where   a.order_code = '" & OrderCode & "'     "
	sql = sql & vbCrlf & "          and   a.ver = a.now_ver     "
	sql = sql & vbCrlf & "          and   a.del_chk != 'Y'     "
	'Response.Write sql
	Set rsOrder = db.execute(sql)
%>

<div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="#">오더관리</a>
        </li>
        <li class="breadcrumb-item active">오더생성</li>
    </ol>

    <!-- Example DataTables Card-->
    <div class="card mb-3">

        <div class="card-body">

            <div class="row">
                <div class="col-12 col-md-6 col-xl-4 mb-3">
                    <div class="card">
                        <h5 class="card-header">기본정보</h5>
                        <div class="card-body">
                            <div class="form-group row">
                                <label for="exampleInputEmail1" class="col-xl-3 col-form-label">오더유형</label>
                                <div class="col-xl-9">
                                    <input type="hidden" class="form-control text-center" id="OrderIdx_OrderMainForm" value="<%=rsOrder("idx")%>">
                                    <input type="hidden" class="form-control text-center" id="OrderCode_OrderMainForm" value="<%=rsOrder("order_code")%>">
                                    <input type="hidden" class="form-control text-center" name="OrderType" id="OrderType_OrderMainForm" value="<%=rsOrder("order_type")%>">
                                    <input type="text" class="form-control text-center" value="<%=rsOrder("order_type_nm")%>" readonly>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="exampleInputEmail1" class="col-xl-3 col-form-label">결재유형</label>
                                <div class="col-xl-9">
                                    <select class="form-control" name="VatType" id="VatType_OrderMainForm">
                                        <option value="A" <%if rsOrder("vat_type") = "A" then response.write "selected" end if%>>일반세금계산서</option>
                                        <option value="D" <%if rsOrder("vat_type") = "D" then response.write "selected" end if%>>계산서미발급</option>
                                        <option value="L" <%if rsOrder("vat_type") = "L" then response.write "selected" end if%>>신용카드</option>
                                        <option value="P" <%if rsOrder("vat_type") = "P" then response.write "selected" end if%>>현금영수증</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="exampleInputEmail1" class="col-xl-3 col-form-label">판매처</label>
                                <div class="col-xl-9">
                                    <div class="input-group">
                                        <input type="hidden" class="form-control text-center" id="Site_OrderMainForm" value="<%=rsOrder("site")%>">
                                        <input type="text" class="form-control text-center" aria-label="First name" name="MemId" id="MemId_OrderMainForm" value="<%=rsOrder("mem_id")%>" readonly>
                                        <input type="text" class="form-control text-center" aria-label="Last name" name="MemNm" id="MemNm_OrderMainForm" value="<%=rsOrder("mem_id_nm")%>" readonly>
                                        <div class="input-group-append">
                                            <button type="button" class="btn btn-outline-secondary" id="btnOrderMainMemId"><i class="fa fa-search" aria-hidden="true"></i><span></span></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="exampleInputEmail1" class="col-xl-3 col-form-label">담당자</label>
                                <div class="col-xl-9">
                                    <div class="input-group">
                                        <input type="text" class="form-control text-center" aria-label="First name" name="EmpId" id="EmpId_OrderMainForm" value="<%=rsOrder("emp_id")%>" readonly>
                                        <input type="text" class="form-control text-center" aria-label="Last name" name="EmpNm" id="EmpNm_OrderMainForm" value="<%=rsOrder("mem_id_nm")%>" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-xl-4 mb-3">
                    <form id="CustomerForm">
						<%
							sql = ""
							sql = sql & vbCrlf & "   select  a.name,   "
							sql = sql & vbCrlf & "             a.hp,   "
							sql = sql & vbCrlf & "             a.zipcode,   "
							sql = sql & vbCrlf & "             a.add1,   "
							sql = sql & vbCrlf & "             a.add2   "
							sql = sql & vbCrlf & "     from  sales_customer a with(nolock)     "
							sql = sql & vbCrlf & "   where  a.cus_code = '" & rsOrder("cus_code") & "'     "
							'Response.Write sql
							Set rs = db.execute(sql)
						%>
                            <input type="hidden" name="CusCode" id="CusCode_MainCustomer" value="<%=rsOrder("cus_code")%>">
                            <div class="card">
                                <h5 class="card-header">고객정보</h5>
                                <div class="card-body">
                                    <div class="form-group row">
                                        <label for="exampleInputEmail1" class="col-xl-3 col-form-label">연락처 4자리</label>
                                        <div class="col-xl-9">
                                            <div class="input-group mb-1">
                                                <input type="number" class="form-control" name="CustomerPhoneNumber" id="CustomerPhoneNumber">
                                                <div class="input-group-append">
                                                    <button type="submit" class="btn btn-outline-secondary" id="btnOrderMainCustomer"><i class="fa fa-search" aria-hidden="true"></i> <span>검색</span></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
									<%
										If Not rs.bof And Not rs.eof Then 
									%>
											<div class="bd-callout bd-callout-danger" id="CustomerInfo">
												<h5 class="font-weight-bold"><%=rs("name")%></h5>
												<a href='tel:<%=rs("hp")%>'><h6><%=rs("hp")%></h6></a>
												<h6><%=rs("add1")%></h6>
												<h6><%=rs("add2")%></h6>
                                                <h6 class='d-none'><%=rs("zipcode")%> %></h6>
											</div>
									<%
										Else
									%>
											<div class="bd-callout bd-callout-danger" id="CustomerInfo">
												<h5 class="font-weight-bold">&nbsp;</h5>
												<h6>&nbsp;</h6>
												<h6>&nbsp;</h6>
												<h6>&nbsp;</h6>
                                                <h6 class='d-none'>&nbsp;</h6>
											</div>
									<%
										End If 
									%>
                                </div>
                            </div>
						<%
							rs.close
							Set rs = Nothing
						%>
                    </form>
                </div>
                <div class="col-12 col-md-6 col-xl-4 mb-3">
                    <div class="card">
                        <h5 class="card-header">인수자정보</h5>
                        <div class="card-body">
                            <div class="form-group row">
                                <label for="exampleInputEmail1" class="col-xl-3 col-form-label">인수자(필수)</label>
                                <div class="col-xl-9">
                                    <input type="text" class="form-control" name="InsuMan" id="InsuMan_OrderMainForm" value="<%=rsOrder("insu_man")%>" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="exampleInputEmail1" class="col-xl-3 col-form-label">연락처(필수)</label>
                                <div class="col-xl-9">
                                    <input type="text" class="form-control" name="InsuTel" id="InsuTel_OrderMainForm" value="<%=rsOrder("insu_tel")%>" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="exampleInputEmail1" class="col-xl-3 col-form-label">설치주소(필수)</label>
                                <div class="col-xl-9">
                                    <div class="row">
                                        <div class="col-12 input-group mb-1">
                                            <input type="text" class="form-control " name="ZipCode" id="ZipCode_OrderMainForm" placeholder="" value="<%=rsOrder("b_zipcode")%>" required>
                                            <input type="text" class="form-control" name="Addr1" id="Addr1_OrderMainForm" placeholder="" value="<%=rsOrder("b_add1")%>" style="width: 30% !important;" required>
                                            <div class="input-group-append">
                                                <button class="btn btn-outline-secondary" type="button" id="btnDaumAddr_OrderMainForm">주소찾기</button>
                                            </div>
                                        </div>
                                        <div class="col-12 mb-1">
                                            <div id="wrap_OrderMainForm" style="display: none; border: 1px solid; width: 100%; height: 300px; margin: 5px 0; position: relative">
                                                <img src="//t1.daumcdn.net/localimg/localimages/07/postcode/320/close.png" id="btnFoldWrap_OrderMainForm" style="cursor: pointer; position: absolute; right: 0px; top: -1px; z-index: 1" onclick="foldDaumPostcode('wrap_OrderMainForm')" alt="접기 버튼">
                                            </div>
                                            <input type="text" class="form-control" name="Addr2" id="Addr2_OrderMainForm" placeholder="" value="<%=rsOrder("b_add2")%>" required>
                                        </div>
                                        <div class="col-12 mb-1">
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" id="ChkAddrCopy_OrderMainForm">
                                                <label class="form-check-label" for="ChkAddrCopy_OrderMainForm">
                                                    <small class="text-muted">고객주소 동일</small>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 mb-3">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="mb-4">견적정보</h5>
                            <div class="row justify-content-between">
                                <div class="col-sm-6 mb-3">
                                    <select class="form-control" id="SetOpt_OrderMainForm">
                                        <option value="ADD" num="0">세트추가</option>
                                        <option value="SET1" num="1" selected>세트1</option>
                                        <%
                                            sql = ""
                                            sql = sql & vbCrlf & "     select  a.set_opt,   "
                                            sql = sql & vbCrlf & "               a.rname   "
                                            sql = sql & vbCrlf & "       from  sales_imsi_set a with(nolock)   "
                                            sql = sql & vbCrlf & "     where  a.imsi_code = '" & rsOrder("imsi_code") & "'    "
                                            sql = sql & vbCrlf & "        and   a.set_opt != 'SET1'    "
                                            sql = sql & vbCrlf & " order by  idx    "
            								'Response.Write sql
                                            Set rsSet = db.execute(sql)
								            Do Until rsSet.bof Or rsSet.eof
                                                Response.Write "<option value='" & rsSet("set_opt") & "' num='" & Replace(rsSet("set_opt"), "SET", "") & "'>" & rsSet("rname") & "</option>"
                                                rsSet.movenext
                                            Loop 
                                            rsSet.close
                                            Set rsSet = Nothing
                                        %>
                                    </select>
                                </div>
                                <div class="col-sm-4 col-md-3 mb-3">
                                    <button type="button" class="btn btn-danger btn-block" id="btnAddProd_OrderMainForm"><i class="fa fa-cart-plus" aria-hidden="true"></i> <span>품목추가</span></button>
                                </div>
                            </div>
                        </div>
                        <div class="card-body" id="divProdList_OrderMainForm">
							<%
								'// 총금액
								TotPrice = 0
								TotPPrice = 0
								TotCostOri = 0
								TotPCostOri = 0

								sql = ""
								sql = sql & vbCrlf & "    select  a.set_opt,    "
								sql = sql & vbCrlf & "              a.rname    "
								sql = sql & vbCrlf & "      from  sales_imsi_set a with(nolock)    "
								sql = sql & vbCrlf & "    where  a.imsi_code = '" & rsOrder("imsi_code") & "'      "
								sql = sql & vbCrlf & "order by  a.idx     "
								'Response.Write sql
								Set rsSet = db.execute(sql)
								Do Until rsSet.bof Or rsSet.eof
									sql = ""
									sql = sql & vbCrlf & "       select  a.prod_code,    "
									sql = sql & vbCrlf & "                 a.prod_name,    "
									sql = sql & vbCrlf & "                 a.cost_ori,    "
									sql = sql & vbCrlf & "                 a.prod_price,    "
									sql = sql & vbCrlf & "                 a.pcost_ori,    "
									sql = sql & vbCrlf & "                 a.prod_pprice,    "
									sql = sql & vbCrlf & "                 a.prod_count,    "
									sql = sql & vbCrlf & "                 a.set_info,    "
									sql = sql & vbCrlf & "                 a.set_opt,    "
									sql = sql & vbCrlf & "                 a.memo,    "
									sql = sql & vbCrlf & "                 a.idx,    "
									sql = sql & vbCrlf & "                 (select  z.site from sales_member z with(nolock) where z.id = b.mem_id ) as site    "
									sql = sql & vbCrlf & "        from  sales_order_item a with(nolock),    "
									sql = sql & vbCrlf & "                 sales_order b with(nolock)    "
									sql = sql & vbCrlf & "      where  a.order_code = b.order_code    "
									sql = sql & vbCrlf & "         and  a.ver = b.ver    "
									sql = sql & vbCrlf & "         and  b.order_code = '" & rsOrder("order_code") & "'    "
									sql = sql & vbCrlf & "         and  b.ver = '" & rsOrder("ver") & "'    "
									sql = sql & vbCrlf & "         and  a.set_opt = '" & rsSet("set_opt") & "'    "
									sql = sql & vbCrlf & "  order by  a.code_index   "
									'Response.Write sql
									Set rsItem = Server.CreateObject("ADODB.RecordSet")
									rsItem.CursorType = 3
									rsItem.CursorLocation = 3
									rsItem.LockType = 3
									rsItem.Open sql, db

									TotSetPrice = 0
									TotSetPPrice = 0
									TotSetCostOri = 0
									TotSetPCostOri = 0
									Do Until rsItem.bof Or rsItem.eof 
										TotSetPrice = TotSetPrice + (CLng("0" & rsItem("prod_price")) * CLng("0" & rsItem("prod_count")))
										TotSetPPrice = TotSetPPrice + (CLng("0" & rsItem("prod_pprice")) * CLng("0" & rsItem("prod_count")))
										TotSetCostOri = TotSetCostOri + (CLng("0" & rsItem("cost_ori")) * CLng("0" & rsItem("prod_count")))
										TotSetPCostOri = TotSetPCostOri + (CLng("0" & rsItem("pcost_ori")) * CLng("0" & rsItem("prod_count")))
										rsItem.movenext
									Loop 

									TotPrice = TotPrice + TotSetPrice
									TotPPrice = TotPPrice + TotSetPPrice
									TotCostOri = TotCostOri + TotSetCostOri
									TotPCostOri = TotCostOri + TotSetPCostOri

                                    if rsSet("set_opt") = "SET1" then 
                                        SetOptBackColor = "bg-danger"
                                    else
                                        SetOptBackColor = "bg-info"
                                    end if 
							%>
									<div class="row mb-3">
										<div class="col-12">
											<div class="card" name="ProdList_OrderMainForm" setopt="<%=rsSet("set_opt")%>">
												<div class="card-header text-white <%=SetOptBackColor%> d-flex w-100 justify-content-between">
													<h6><%=rsSet("rname")%></h6>
													<h6 name='SetSCost'>￦ <%=FormatNumber(TotSetPrice, 0)%></h6>
												</div>
												<div class="card-body">
													<div class="row">
														<%
															If rsItem.RecordCount > 0 Then rsItem.movefirst
															Do Until rsItem.bof Or rsItem.eof
                                                                
                                                                '// OrderItem 셋팅
									                            sql = ""
									                            sql = sql & vbCrlf & "       select  a.status,    "
									                            sql = sql & vbCrlf & "                 a.worry_yn,    "
									                            sql = sql & vbCrlf & "                 a.prod_max_cnt,    "
									                            sql = sql & vbCrlf & "                 b.prod_img    "
									                            sql = sql & vbCrlf & "        from  sales_prod a with(nolock),    "
									                            sql = sql & vbCrlf & "                 sales_prod_info b with(nolock)    "
									                            sql = sql & vbCrlf & "      where  a.prod_code = b.prod_code    "
                                                                sql = sql & vbCrlf & "         and  a.prod_code = '" & rsItem("prod_code") & "'    "
                                                                sql = sql & vbCrlf & "         and  a.site = '" & rsItem("site") & "'    "
									                            'Response.Write sql
                                    							Set rsProdInfo = db.execute(sql)
                                                                if Not rsProdInfo.eof And Not rsProdInfo.bof then 
                                                                    OrderItem = "{"
                                                                    OrderItem = OrderItem & """ProdCode"":""" & rsItem("prod_code") & ""","
                                                                    OrderItem = OrderItem & """ProdName"":""" & rsItem("prod_name") & ""","
                                                                    OrderItem = OrderItem & """ProdCount"":""" & rsItem("prod_count") & ""","
                                                                    OrderItem = OrderItem & """ProdPrice"":""" & rsItem("prod_price") & ""","
                                                                    OrderItem = OrderItem & """SetInfo"":""" & rsItem("set_info") & ""","
                                                                    OrderItem = OrderItem & """SetOpt"":""" & rsItem("set_opt") & ""","
                                                                    OrderItem = OrderItem & """CostOri"":""" & rsItem("cost_ori") & ""","
                                                                    OrderItem = OrderItem & """ProdPPrice"":""" & rsItem("prod_pprice") & ""","
                                                                    OrderItem = OrderItem & """PCostOri"":""" & rsItem("pcost_ori") & ""","
                                                                    OrderItem = OrderItem & """ProdImg"":""" & rsProdInfo("prod_img") & ""","
                                                                    OrderItem = OrderItem & """Status"":""" & rsProdInfo("status") & ""","
                                                                    OrderItem = OrderItem & """WorryYn"":""" & rsProdInfo("worry_yn") & ""","
                                                                    OrderItem = OrderItem & """Site"":""" & rsItem("site") & ""","
                                                                    OrderItem = OrderItem & """MaxCnt"":""" & rsProdInfo("prod_max_cnt") & ""","
                                                                    OrderItem = OrderItem & """Memo"":""" & rsItem("memo") & ""","
                                                                    OrderItem = OrderItem & """Idx"":""" & rsItem("idx") & """"
                                                                    OrderItem = OrderItem & "}"
                                                                end if 
                                                                rsProdInfo.close
                                                                Set rsProdInfo = Nothing
														%>
																<div class='col-12 col-sm-6 col-lg-4 col-xl-3'>
																	<div class='bd-callout bd-callout-danger'>
                                                                        <button type='button' class='btn btn-link' name='btnItemUpdate' style='opacity: .5; font-size: 1.5rem; color: #000;'>
                                                                           <i class='fa fa-search' aria-hidden='true'></i>
                                                                        </button>
                                                                        <button type='button' class='close' aria-label='Close' name='btnItemRemove'>
                                                                           <span aria-hidden='true'>&times;</span>
                                                                        </button>
                                                                        <input type='hidden' name='selectedItem' OrderItem='<%=OrderItem%>'>
																		<strong>
																			<p><%=rsItem("prod_name")%></p>
																		</strong>
																		<strong>
																			<p><%=rsItem("prod_code")%></p>
																		</strong>
																		<small>
																			<p><%=rsItem("set_info")%></p>
																		</small>
																		<small>
																			<p class='text-right'>수량 : <%=rsItem("prod_count")%></p>
																		</small>
																		<small>
																			<p class='text-right' data-bind='ProdPrice'>단가 : <%=FormatNumber(rsItem("prod_price"), 0)%></p>
																		</small>
																		<small>
																			<p class='text-right' data-bind='ProdAmount'>금액 : <%=FormatNumber(CLng("0" & rsItem("prod_price")) * CLng("0" & rsItem("prod_count")), 0)%></p>
																		</small>
																	</div>
																</div>
														<%
																rsItem.movenext 
															Loop 
														%>
													</div>
												</div>
											</div>
										</div>
									</div>
							<%
									rsItem.close
									Set rsItem = Nothing

									rsSet.movenext
								Loop 
								rsSet.close
								Set rsSet = Nothing
							%>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 mb-3">
                    <div class="card">
                        <h5 class="card-header">소비자 금액정보</h5>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-6 mb-3">
                                    <div class="col-12">
                                        <form id="SCostDCRateForm">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">할인율</label>
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">%</span>
                                                    </div>
                                                    <input type="number" class="form-control" id="txtSCostDcRate_OrderMainForm" required>
                                                    <div class="input-group-append">
                                                        <button class="btn btn-outline-secondary" type="submit">적용</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="col-12">
                                        <form id="SCostDCAmountForm">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">최종금액</label>
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">￦</span>
                                                    </div>
                                                    <input type="number" class="form-control" id="txtSCostDcAmout_OrderMainForm" required>
                                                    <div class="input-group-append">
                                                        <button class="btn btn-outline-secondary" type="submit">적용</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="col-sm-6 mb-3">
                                    <div class="bd-callout bd-callout-danger" id="SCostInfo">
                                        <div class='d-flex w-100 justify-content-between'>
                                            <h6 class="font-weight-bold">최종금액</h6>
                                            <h6 class="font-weight-bold">￦ <%=FormatNumber(TotPrice, 0)%></h6>
                                        </div>
                                        <div class='d-flex w-100 justify-content-between'>
                                            <h6 class="font-weight-bold">할인율</h6>
                                            <%
	                                            On Error Resume Next 
                                                    TotSCostRate = Round((1 - (TotPrice / TotCostOri)) * 100, 1)
		                                            If Err.Number <> 0 Then 
			                                            TotSCostRate = 0
		                                            End If 
	                                            On Error GoTo 0
                                             %>
                                            <h6 class="font-weight-bold"><%=TotSCostRate%>%</h6>
                                        </div>
                                        <div class='d-flex w-100 justify-content-between'>
                                            <h6 class="font-weight-bold">할인금액</h6>
                                            <h6 class="font-weight-bold">￦ <%=FormatNumber(TotCostOri - TotPrice, 0)%></h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 mb-3">
                    <div class="card">
                        <h5 class="card-header">공급가 금액정보</h5>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-6 mb-3">
                                    <div class="col-12">
                                        <form id="PCostDCRateForm">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">할인율</label>
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">%</span>
                                                    </div>
                                                    <input type="number" class="form-control" id="txtPCostDcRate_OrderMainForm" required>
                                                    <div class="input-group-append">
                                                        <button class="btn btn-outline-secondary" type="submit">적용</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="col-12">
                                        <form id="PCostDCAmountForm">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">최종금액</label>
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">￦</span>
                                                    </div>
                                                    <input type="number" class="form-control" id="txtPCostDcAmout_OrderMainForm" required>
                                                    <div class="input-group-append">
                                                        <button class="btn btn-outline-secondary" type="submit">적용</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="col-sm-6 mb-3">
                                    <div class="bd-callout bd-callout-danger" id="PCostInfo">
                                        <div class='d-flex w-100 justify-content-between'>
                                            <h6 class="font-weight-bold">최종금액</h6>
                                            <h6 class="font-weight-bold">￦ <%=FormatNumber(TotPPrice, 0)%></h6>
                                        </div>
                                        <div class='d-flex w-100 justify-content-between'>
                                            <h6 class="font-weight-bold">할인율</h6>
                                            <%
	                                            On Error Resume Next 
                                                    TotPCostRate = Round((1 - (TotPPrice / TotPCostOri)) * 100, 1)
		                                            If Err.Number <> 0 Then 
			                                            TotPCostRate = 0
		                                            End If 
	                                            On Error GoTo 0
                                             %>
                                            <h6 class="font-weight-bold"><%=TotPCostRate%>%</h6>
                                        </div>
                                        <div class='d-flex w-100 justify-content-between'>
                                            <h6 class="font-weight-bold">할인금액</h6>
                                            <h6 class="font-weight-bold">￦ <%=FormatNumber(TotPCostOri - TotPPrice, 0)%></h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 col-md-6 col-xl-4 mb-3">
                    <div class="card">
                        <h5 class="card-header">배송정보</h5>
                        <div class="card-body">
                            <div class="form-group row">
                                <label for="exampleInputEmail1" class="col-xl-3 col-form-label">도착요청일</label>
                                <div class="col-xl-9">
                                    <input type="text" class="form-control text-center" name="oDate" id="oDate_OrderMainForm" value="<%=rsOrder("odate")%>" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="exampleInputEmail1" class="col-xl-3 col-form-label">도착요청시간</label>
                                <div class="col-xl-9">
                                    <select class="form-control" name="oTime" id="oTime_OrderMainForm">
                                        <option value="06" <%if rsOrder("otime") = "06" then response.write "selected" end if%>>06:00</option>
                                        <option value="07" <%if rsOrder("otime") = "07" then response.write "selected" end if%>>07:00</option>
                                        <option value="08" <%if rsOrder("otime") = "08" then response.write "selected" end if%>>08:00</option>
                                        <option value="09" <%if rsOrder("otime") = "09" then response.write "selected" end if%>>09:00</option>
                                        <option value="10" <%if rsOrder("otime") = "10" then response.write "selected" end if%>>10:00</option>
                                        <option value="11" <%if rsOrder("otime") = "11" then response.write "selected" end if%>>11:00</option>
                                        <option value="12" <%if rsOrder("otime") = "12" then response.write "selected" end if%>>12:00</option>
                                        <option value="13" <%if rsOrder("otime") = "13" then response.write "selected" end if%>>13:00</option>
                                        <option value="14" <%if rsOrder("otime") = "14" then response.write "selected" end if%>>14:00</option>
                                        <option value="15" <%if rsOrder("otime") = "15" then response.write "selected" end if%>>15:00</option>
                                        <option value="16" <%if rsOrder("otime") = "16" then response.write "selected" end if%>>16:00</option>
                                        <option value="17" <%if rsOrder("otime") = "17" then response.write "selected" end if%>>17:00</option>
                                        <option value="18" <%if rsOrder("otime") = "18" then response.write "selected" end if%>>18:00</option>
                                        <option value="19" <%if rsOrder("otime") = "19" then response.write "selected" end if%>>19:00</option>
                                        <option value="20" <%if rsOrder("otime") = "20" then response.write "selected" end if%>>20:00</option>
                                        <option value="30" <%if rsOrder("otime") = "30" then response.write "selected" end if%>>오전중</option>
                                        <option value="40" <%if rsOrder("otime") = "40" then response.write "selected" end if%>>오후중</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="exampleInputEmail1" class="col-xl-3 col-form-label">세대반입</label>
                                <div class="col-xl-9">
                                    <select class="form-control" name="Inhouse" id="Inhouse_OrderMainForm">
                                        <option value="X" <%if rsOrder("inhouse") = "X" then response.write "selected" end if%>>신청안함</option>
                                        <option value="O" <%if rsOrder("inhouse") = "O" then response.write "selected" end if%>>신청</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-xl-4 mb-3">
                    <div class="card">
                        <h5 class="card-header">시공정보</h5>
                        <div class="card-body">
                            <div class="form-group row">
                                <label for="exampleInputEmail1" class="col-xl-3 col-form-label">시공요청</label>
                                <div class="col-xl-9">
                                    <select class="form-control" name="SigongType" id="SigongType_OrderMainForm">
                                        <option value="직시공" <%if rsOrder("sigong_type") = "직시공" then response.write "selected" end if%>>영림시공</option>
                                        <option value="자체시공" <%if rsOrder("sigong_type") = "자체시공" then response.write "selected" end if%>>자체시공</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="exampleInputEmail1" class="col-xl-3 col-form-label">시공요청일</label>
                                <div class="col-xl-9">
                                    <%
                                        SDate = rsOrder("sdate")
                                        if SDate = "1900-01-01" then SDate = ""
                                    %>
                                    <input type="text" class="form-control text-center" name="sDate" id="sDate_OrderMainForm" value="<%=SDate%>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="exampleInputEmail1" class="col-xl-3 col-form-label">시공방법</label>
                                <div class="col-xl-9">
                                    <select class="form-control" name="SigongDay" id="SigongDay_OrderMainForm">
                                        <option value="3" <%if rsOrder("sigong_day") = "3" then response.write "selected" end if%>>덧방 (3일)</option>
                                        <option value="4" <%if rsOrder("sigong_day") = "4" then response.write "selected" end if%>>부분철거 (4일)</option>
                                        <option value="5" <%if rsOrder("sigong_day") = "5" then response.write "selected" end if%>>전체철거 (5일)</option>
                                        <option value="6" <%if rsOrder("sigong_day") = "6" then response.write "selected" end if%>>UBR (6일)</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-xl-4 mb-3">
                    <div class="card">
                        <h5 class="card-header">출고메모</h5>
                        <div class="card-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1" class="col-form-label">출고요청</label>
                                <textarea class="form-control" name="OutMemo" id="OutMemo_OrderMainForm" rows="8"><%=rsOrder("out_memo")%></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="card-footer">

            <form id="OrderMainForm">
                <button type="submit" class="btn btn-success"><i class="fa fa-check" aria-hidden="true"></i> <span>임시오더 수정</span></button>
            </form>

        </div>

    </div>
</div>
<!-- /.container-fluid-->
<!-- /.content-wrapper-->

<!-- modal -->
<!-- Select Customer Modal HTML -->
<div id="selCustomerModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><i class="fa fa-user-circle" aria-hidden="true"></i> <span>고객찾기</span></h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#addCustomerModal"><i class="fa fa-user-plus" aria-hidden="true"></i> <span>신규등록</span></button>
            </div>
        </div>
    </div>
</div>
<!-- Add Customer Modal HTML -->
<div id="addCustomerModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="addCusModalForm">
                <div class="modal-header">
                    <h4 class="modal-title">고객등록</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>고객명</label>
                        <input type="text" class="form-control" name="CusName" id="CusName_AddCusModal" placeholder="Name" required>
                    </div>
                    <div class="form-group">
                        <label>휴대전화</label>
                        <input type="number" class="form-control" name="CusHp" id="CusHp_AddCusModal" placeholder="HP" required>
                    </div>
                    <div class="form-group">
                        <label>주소</label>
                        <div class="input-group mb-1">
                            <input type="text" class="form-control " name="ZipCode" id="ZipCode_AddCusModal" placeholder="Zipcode" required>
                            <input type="text" class="form-control" name="Addr1" id="Addr1_AddCusModal" placeholder="Addr" style="width: 30% !important;" required>
                            <div class="input-group-append">
                                <button class="btn btn-outline-secondary" type="button" id="btnDaumAddr_AddCusModal">주소찾기</button>
                            </div>
                        </div>
                        <div id="wrap_AddCusModal" style="display: none; border: 1px solid; width: 100%; height: 300px; margin: 5px 0; position: relative">
                            <img src="//t1.daumcdn.net/localimg/localimages/07/postcode/320/close.png" id="btnFoldWrap_AddCusModal" style="cursor: pointer; position: absolute; right: 0px; top: -1px; z-index: 1" onclick="foldDaumPostcode('wrap_AddCusModal')" alt="접기 버튼">
                        </div>
                        <input type="text" class="form-control" name="Addr2" id="Addr2_AddCusModal" placeholder="Addr" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success"><i class="fa fa-check" aria-hidden="true"></i> <span>저장</span></button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-undo" aria-hidden="true"></i> <span>취소</span></button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Add Prod Modal HTML -->
<div id="addProdModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">품목등록</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <div class="accordion" id="Accordion_AddProdModal">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed btn-block" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    <i class="fa fa-cubes" aria-hidden="true"></i> <span>Package</span>
                                </button>
                            </h5>
                        </div>
                        <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#Accordion_AddProdModal">
                            <div class="card-body">
                                <div class="row" id="Category_AddProdModal">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed btn-block" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    <i class="fa fa-cube" aria-hidden="true"></i> <span>Items</span>
                                </button>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#Accordion_AddProdModal">
                            <div class="card-body">
                                <div class="row" id="ItemsCategory_AddProdModal">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <h5 class="mb-0">
                                <button class="btn btn-link collapsed btn-block" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseTwo">
                                    <i class="fa fa-search" aria-hidden="true"></i> <span>Search</span>
                                </button>
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#Accordion_AddProdModal">
                            <div class="card-body">
                                <form id="ProdSearchModalForm">
                                    <div class="row">
                                        <div class="input-group">
                                            <input type="text" class="form-control" id="ProdSearch_AddProdModal" required>
                                            <div class="input-group-append">
                                                <button type="submit" class="btn btn-outline-secondary"><i class="fa fa-search" aria-hidden="true"></i> <span>검색</span></button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div style="border-top: 1px solid #e5e5e5;"></div>
            <div class="modal-body" id="ItemList_AddProdModal">
                <div class="list-group">
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Add Prod Detail Modal HTML -->
<div id="addProdDetailModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="ProdCountForm_AddProdDetailModal">
                <input type="hidden" name="ProdCode" OrderItem="" required>
                <!--<input type="text" name="TopCode">-->
                <div class="modal-header">
                    <h4 class="modal-title">품목상세</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="row justify-content-center">
                        <div class="col-6 mb-3">
                            <img src="" class="img-thumbnail" alt="품목이미지" onerror="this.src='http://119.196.177.124:8080/new/images/no_image.gif';">
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-6">
                            <input class="form-control form-control-lg text-center" type="number" name="Count" placeholder="수량" required>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-6">
                            <input class="form-control form-control-lg text-center" type="number" name="ProdPPrice" placeholder="공급가" required>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-6">
                            <input class="form-control form-control-lg text-center" type="number" name="ProdPrice" placeholder="소비자가" required>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-6">
                            <input class="form-control form-control-lg text-center" type="number" name="Memo" placeholder="메모">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="col-6">
                        <button type="button" class="btn btn-success btn-lg btn-block" data-toggle="modal" data-target="#addProdDetailBomModal"><i class="fa fa-th-list" aria-hidden="true"></i> <span>BOM</span></button>
                    </div>
                    <div class="col-6">
                        <button type="submit" class="btn btn-primary btn-lg btn-block"><i class="fa fa-plus" aria-hidden="true"></i> <span>추가</span></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Add Prod Detail BOM Modal HTML -->
<div id="addProdDetailBomModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="ProdBomForm_AddProdDetailBomModal">
                <div class="modal-header">
                    <h4 class="modal-title">BOM</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="btn-group-toggle" data-toggle="buttons" id="BomList_AddProdDetailBomModal">
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="col-12">
                        <button type="submit" class="btn btn-primary btn-lg btn-block" id="btnProdDetailBOM_addProdDetailBomModal"><i class="fa fa-plus" aria-hidden="true"></i> <span>추가</span></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Update Prod Detail Modal HTML -->
<div id="updateProdDetailModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="ProdCountForm_UpdateProdDetailModal">
                <input type="hidden" name="ProdCode" OrderItem="" required>
                <!--<input type="text" name="TopCode">-->
                <div class="modal-header">
                    <h4 class="modal-title">품목상세</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="row justify-content-center">
                        <div class="col-6 mb-3">
                            <img src="" class="img-thumbnail" alt="품목이미지" onerror="this.src='http://119.196.177.124:8080/new/images/no_image.gif';">
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-6">
                            <input class="form-control form-control-lg text-center" type="number" name="Count" placeholder="수량" required>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="col-6">
                        <button type="submit" class="btn btn-primary btn-lg btn-block"><i class="fa fa-plus" aria-hidden="true"></i> <span>수정</span></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Select MemId Modal HTML -->
<div id="selMemIdModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="MemIdSearchModalForm">
                <div class="modal-header">
                    <h4 class="modal-title"><i class="fa fa-user-circle" aria-hidden="true"></i> <span>판매처</span></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="row" id="divSiteButton_SelMemIdModal">
                    </div>
                </div>
                <div style="border-top: 1px solid #e5e5e5;"></div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="input-group">
                                <input type="text" class="form-control" id="MemIdSearch_SelMemIdModal">
                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-outline-secondary"><i class="fa fa-search" aria-hidden="true"></i> <span>검색</span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <div style="border-top: 1px solid #e5e5e5;"></div>
            <div class="modal-body" id="MemIdList_SelMemIdModal">
                <div class="list-group">
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.modal -->

<%
	rsOrder.close
	Set rsOrder = Nothing
%>

<!-- #include virtual = "/share/include/ContentsAddon.asp" -->
<!-- #include virtual = "/share/include/ContentsFooter.asp" -->
<!-- #include virtual = "/share/include/JavaScript.asp" -->

<script type="text/javascript">
<!--
    $(document).ready(function () {

        $('#oDate_OrderMainForm').datepicker({
            format: "yyyy-mm-dd",
            todayBtn: "linked",
            language: "kr",
            multidate: false,
            daysOfWeekDisabled: "0",
            daysOfWeekHighlighted: "0",
            autoclose: true,
            todayHighlight: false,
            toggleActive: true
        });

        $('#sDate_OrderMainForm').datepicker({
            format: "yyyy-mm-dd",
            todayBtn: "linked",
            language: "kr",
            multidate: false,
            daysOfWeekDisabled: "0",
            daysOfWeekHighlighted: "0",
            startDate: '<%=Date() + 1%>',
            autoclose: true,
            todayHighlight: false,
            toggleActive: true
        });

        $("#CustomerForm").ajaxForm({
            url: "/data/ORD/Order.Customer.Json.asp",
            type: "POST",
            dataType: "json",
            beforeSubmit: function (data, form, option) {
                //validation체크
                //막기위해서는 return false를 잡아주면됨

                var $CustomerPhoneNumber = $("#CustomerPhoneNumber");
                if ($CustomerPhoneNumber.val() == "") {
                    alert("전화번호 뒷번호를 입력하세요.");
                    $CustomerPhoneNumber.focus();
                    return false;
                }

                var info = "";
                info += "<h5 class='font-weight-bold'>&nbsp;</h5>";
                info += "<h6>&nbsp;</h6>";
                info += "<h6>&nbsp;</h6>";
                info += "<h6>&nbsp;</h6>";
                $("#CustomerInfo").html(info);
                $("#CusCode_MainCustomer").val('');

                return true;
            },
            beforeSend: function () {
                $(".fa").addClass("fa-spinner fa-spin");
            },
            complete: function () {
                $(".fa").removeClass("fa-spinner fa-spin");
            },
            success: function (response, status) {

                var info = "";
                if (response.length == 0) {
                    info += "<div class='bd-callout bd-callout-danger'>"
                    info += "	<h5 class='font-weight-bold'>등록된 고객이 없습니다.</h5>"
                    info += "	<h6>&nbsp;</h6>"
                    info += "	<h6>&nbsp;</h6>"
                    info += "	<h6>&nbsp;</h6>"
                    info += "	<h6 class='d-none'>&nbsp;</h6>"
                    info += "</div>"
                }
                else {
                    $.each(response, function (i) {
                        info += "<div class='bd-callout bd-callout-danger' name='CusList_selCustomerModel' idx='" + this.idx + "'>"
                        info += "	<h5 class='font-weight-bold'>" + this.name + "</h5>"
                        info += "	<h6>" + this.hp + "</h6>"
                        info += "	<h6>" + this.add1 + "</h6>"
                        info += "	<h6>" + this.add2 + "</h6>"
                        info += "	<h6 class='d-none'>" + this.zipcode + "</h6>"
                        info += "</div>"
                    });
                }

                var body = $("#selCustomerModal").find(".modal-body");
                body.html(body.html() + info);

                $("#selCustomerModal").modal('show');

            },
            error: function (e) {
                alert("오류 발생!");
            }
        });

        $("#addCusModalForm").ajaxForm({
            url: "/data/ORD/Order.Customer.Write.Proc.asp",
            type: "POST",
            dataType: "text",
            beforeSend: function () {
                $(".fa").addClass("fa-spinner fa-spin");
            },
            complete: function () {
                $(".fa").removeClass("fa-spinner fa-spin");
            },
            beforeSubmit: function (data, form, option) {
                //validation체크
                //막기위해서는 return false를 잡아주면됨
                return true;
            },
            success: function (response, status) {
                //성공후 서버에서 받은 데이터 처리

                var datas = response.split("|");
                if (datas[0] == "OK") {
                    alert("저장 되었습니다.");
                    $("#CusCode_MainCustomer").val(datas[1]);
                    $("#addCustomerModal").modal('hide');
                    $("#selCustomerModal").modal('hide');

                    BindCustomerInfo(datas[1]);
                } else {
                    alert("에러가 발생 하였습니다.");
                }
            },
            error: function () {
                alert("오류 발생!");
            }
        });

        $("#ProdSearchModalForm").ajaxForm({
            url: "/data/ORD/Order.Prod.Items.Json.asp",
            type: "POST",
            data: {
                ProdInfo: function () { return $("#ProdSearch_AddProdModal").val() },
                UseSite: function () { return $("#Site_OrderMainForm").val() }
            },
            dataType: "json",
            beforeSend: function () {
                $(".fa").addClass("fa-spinner fa-spin");
            },
            complete: function () {
                $(".fa").removeClass("fa-spinner fa-spin");
            },
            beforeSubmit: function (data, form, option) {
                //validation체크
                //막기위해서는 return false를 잡아주면됨
                return true;
            },
            success: function (data, status) {
                //성공후 서버에서 받은 데이터 처리
                $("#Accordion_AddProdModal").find(".collapse").collapse('hide');
                BindProdList(data);
            },
            error: function () {
                alert("품목 리스트 조회 중 오류 발생!");
            }
        });

        $("#ProdBomForm_AddProdDetailBomModal").ajaxForm({
            beforeSend: function () {
                $(".fa").addClass("fa-spinner fa-spin");
            },
            complete: function () {
                $(".fa").removeClass("fa-spinner fa-spin");
            },
            beforeSubmit: function (data, form, option) {
                //validation체크
                //막기위해서는 return false를 잡아주면됨

                // 품목 삽입
                var Items = new Array();
                $.each($("#addProdDetailBomModal").find("input:checkbox:checked"), function (i) {
                    var Item = JSON.parse($(this).attr("OrderItem"));
                    Item.SetOpt = $("#SetOpt_OrderMainForm").val();
                    Items.push(Item);
                });
                var RtnHtml = AddProd(Items);

                var SetOpt = $("#SetOpt_OrderMainForm").val();
                $("div[name=ProdList_OrderMainForm][SetOpt=" + SetOpt + "]").find(".card-body").find(".row").append(RtnHtml);

                Items = null;

                // 금액 계산
                CalCostInfo();

                // 팝업 닫기
                $("#addProdDetailBomModal").modal('hide');
                $("#addProdDetailModal").modal('hide');

                return false;
            }
        });

        $("#ProdCountForm_AddProdDetailModal").ajaxForm({
            beforeSend: function () {
                $(".fa").addClass("fa-spinner fa-spin");
            },
            complete: function () {
                $(".fa").removeClass("fa-spinner fa-spin");
            },
            beforeSubmit: function (data, form, option) {
                //validation체크
                //막기위해서는 return false를 잡아주면됨

                // 품목 삽입
                var Items = new Array();
                var oItem = JSON.parse($("#addProdDetailModal").find("input[name=ProdCode]").attr("OrderItem"));
                oItem.ProdCount = $("#addProdDetailModal").find("input[name=Count]").val();
                oItem.ProdPPrice = $("#addProdDetailModal").find("input[name=ProdPPrice]").val();
                oItem.ProdPrice = $("#addProdDetailModal").find("input[name=ProdPrice]").val();
                oItem.Memo = $("#addProdDetailModal").find("input[name=Memo]").val();
                oItem.SetOpt = $("#SetOpt_OrderMainForm").val();
                Items.push(oItem);
                var RtnHtml = AddProd(Items);

                var SetOpt = $("#SetOpt_OrderMainForm").val();
                $("div[name=ProdList_OrderMainForm][SetOpt=" + SetOpt + "]").find(".card-body").find(".row").append(RtnHtml);

                Items = null;

                // 금액 계산
                CalCostInfo();

                // 팝업 닫기
                $("#addProdDetailModal").modal('hide');

                return false;
            }
        });

        $("#ProdCountForm_UpdateProdDetailModal").ajaxForm({
            beforeSend: function () {
                $(".fa").addClass("fa-spinner fa-spin");
            },
            complete: function () {
                $(".fa").removeClass("fa-spinner fa-spin");
            },
            beforeSubmit: function (data, form, option) {
                //validation체크
                //막기위해서는 return false를 잡아주면됨

                // 품목 삽입
                var Items = new Array();
                var Item = JSON.parse($("#updateProdDetailModal").find("input[name=ProdCode]").attr("OrderItem"));
                Item.ProdCount = $("#updateProdDetailModal").find("input[name=Count]").val();
                Item.SetOpt = $("#SetOpt_OrderMainForm").val();
                Items.push(Item);
                var RtnHtml = AddProd(Items);

                var TargetObj = $("div[name=ProdList_OrderMainForm]").find("[data-select=yes]").parent().parent();
                $(TargetObj).before(RtnHtml).remove();

                Items = null;

                // 금액 계산
                CalCostInfo();

                // 팝업 닫기
                $("#updateProdDetailModal").modal('hide');

                return false;
            }
        });

        $("#SCostDCRateForm").ajaxForm({
            beforeSend: function () {
                $(".fa").addClass("fa-spinner fa-spin");
            },
            complete: function () {
                $(".fa").removeClass("fa-spinner fa-spin");
            },
            beforeSubmit: function (data, form, option) {
                //validation체크
                //막기위해서는 return false를 잡아주면됨
                var DcRate = $(form).find("#txtSCostDcRate_OrderMainForm").val();
                var Items = $("input[name=selectedItem]");
                $.each(Items, function (i) {
                    var Item = JSON.parse($(this).attr("OrderItem"));
                    if (Item.SetInfo != "대표코드") {
                        var CostOri = parseInt(Item.CostOri);
                        var ProdPrice = CostOri * ((100 - DcRate) / 100);
                        ProdPrice = parseInt(ProdPrice / 10) * 10;
                        Item.ProdPrice = ProdPrice;
                        $(this).attr("OrderItem", JSON.stringify(Item));
                        $(this).parent().find("p[data-bind=ProdPrice]").text("단가 : " + addCommas(Item.ProdPrice));
                        $(this).parent().find("p[data-bind=ProdAmount]").text("금액 : " + addCommas(parseInt(Item.ProdCount) * parseInt(Item.ProdPrice)));
                    }
                });

                CalCostInfo();

                return false;
            }
        });

        $("#SCostDCAmountForm").ajaxForm({
            beforeSend: function () {
                $(".fa").addClass("fa-spinner fa-spin");
            },
            complete: function () {
                $(".fa").removeClass("fa-spinner fa-spin");
            },
            beforeSubmit: function (data, form, option) {
                //validation체크
                //막기위해서는 return false를 잡아주면됨
                var DCAmount = $(form).find("#txtSCostDcAmout_OrderMainForm").val();
                var TotSCostOri = 0;        // 원 총금액
                var TotDCSCostOri = 0;    // 할인율로 계산된 총금액
                var DiffCost = 0;              // 요청한 금액과 할인율을 적용한 값의 차액

                var Items = $("input[name=selectedItem]");
                $.each(Items, function (i) {
                    var Item = JSON.parse($(this).attr("OrderItem"));
                    TotSCostOri += parseInt(Item.CostOri) * parseInt(Item.ProdCount);
                });

                // 할인율
                var DcRate = parseFloat(DCAmount / TotSCostOri);

                // 품목별 할인
                $.each(Items, function (i) {
                    var Item = JSON.parse($(this).attr("OrderItem"));
                    if (Item.SetInfo != "대표코드") {
                        var CostOri = parseInt(Item.CostOri);
                        var ProdPrice = CostOri * DcRate;
                        ProdPrice = parseInt(ProdPrice / 10) * 10;
                        Item.ProdPrice = ProdPrice;
                        TotDCSCostOri += ProdPrice * parseInt(Item.ProdCount);
                        $(this).attr("OrderItem", JSON.stringify(Item));
                        $(this).parent().find("p[data-bind=ProdPrice]").text("단가 : " + addCommas(Item.ProdPrice));
                        $(this).parent().find("p[data-bind=ProdAmount]").text("금액 : " + addCommas(parseInt(Item.ProdCount) * parseInt(Item.ProdPrice)));
                    } else {
                        TotDCSCostOri += parseInt(Item.ProdPrice) * parseInt(Item.ProdCount);
                    }
                });

                // 할인후 자투리금액 처리
                DiffCost = DCAmount - TotDCSCostOri;
                if (DiffCost != 0) {
                    $.each(Items, function (i) {
                        var Item = JSON.parse($(this).attr("OrderItem"));
                        var ProdPrice = parseInt(Item.ProdPrice);
                        var DCProdPrice = 0;

                        if (Item.SetInfo != "대표코드") {
                            if (Item.ProdCount == 1) {
                                if ((ProdPrice + DiffCost) > 10) {
                                    DCProdPrice = ProdPrice + DiffCost;
                                }
                            }
                            if (DCProdPrice == 0 && (Items.length - 1) == i) {
                                DCProdPrice = ProdPrice + parseInt(parseFloat(DiffCost) / parseFloat(Item.ProdCount));
                            }
                        }

                        if (DCProdPrice != 0) {
                            Item.ProdPrice = DCProdPrice;
                            $(this).attr("OrderItem", JSON.stringify(Item));
                            $(this).parent().find("p[data-bind=ProdPrice]").text("단가 : " + addCommas(Item.ProdPrice));
                            $(this).parent().find("p[data-bind=ProdAmount]").text("금액 : " + addCommas(parseInt(Item.ProdCount) * parseInt(Item.ProdPrice)));
                            return false;
                        }
                    });
                }

                CalCostInfo();

                return false;
            }
        });

        $("#PCostDCRateForm").ajaxForm({
            beforeSend: function () {
                $(".fa").addClass("fa-spinner fa-spin");
            },
            complete: function () {
                $(".fa").removeClass("fa-spinner fa-spin");
            },
            beforeSubmit: function (data, form, option) {
                //validation체크
                //막기위해서는 return false를 잡아주면됨
                var DcRate = $(form).find("#txtPCostDcRate_OrderMainForm").val();
                var Items = $("input[name=selectedItem]");
                $.each(Items, function (i) {
                    var Item = JSON.parse($(this).attr("OrderItem"));
                    if (Item.SetInfo != "대표코드") {
                        var PCostOri = parseInt(Item.PCostOri);
                        var ProdPPrice = PCostOri * ((100 - DcRate) / 100);
                        ProdPPrice = parseInt(ProdPPrice / 10) * 10;
                        Item.ProdPPrice = ProdPPrice;
                        $(this).attr("OrderItem", JSON.stringify(Item));
                    }
                });

                CalCostInfo();

                return false;
            }
        });

        $("#PCostDCAmountForm").ajaxForm({
            beforeSend: function () {
                $(".fa").addClass("fa-spinner fa-spin");
            },
            complete: function () {
                $(".fa").removeClass("fa-spinner fa-spin");
            },
            beforeSubmit: function (data, form, option) {
                //validation체크
                //막기위해서는 return false를 잡아주면됨
                var DCAmount = $(form).find("#txtPCostDcAmout_OrderMainForm").val();
                var TotPCostOri = 0;        // 원 총금액
                var TotDCPCostOri = 0;    // 할인율로 계산된 총금액
                var DiffCost = 0;              // 요청한 금액과 할인율을 적용한 값의 차액

                var Items = $("input[name=selectedItem]");
                $.each(Items, function (i) {
                    var Item = JSON.parse($(this).attr("OrderItem"));
                    TotPCostOri += parseInt(Item.PCostOri) * parseInt(Item.ProdCount);
                });

                // 할인율
                var DcRate = parseFloat(DCAmount / TotPCostOri);

                // 품목별 할인
                $.each(Items, function (i) {
                    var Item = JSON.parse($(this).attr("OrderItem"));
                    if (Item.SetInfo != "대표코드") {
                        var PCostOri = parseInt(Item.PCostOri);
                        var ProdPPrice = PCostOri * DcRate;
                        ProdPPrice = parseInt(ProdPPrice / 10) * 10;
                        Item.ProdPPrice = ProdPPrice;
                        TotDCPCostOri += ProdPPrice * parseInt(Item.ProdCount);
                        $(this).attr("OrderItem", JSON.stringify(Item));
                    } else {
                        TotDCPCostOri += parseInt(Item.ProdPPrice) * parseInt(Item.ProdCount);
                    }
                });

                // 할인후 자투리금액 처리
                DiffCost = DCAmount - TotDCPCostOri;
                if (DiffCost != 0) {
                    $.each(Items, function (i) {
                        var Item = JSON.parse($(this).attr("OrderItem"));
                        var ProdPPrice = parseInt(Item.ProdPPrice);
                        var DCProdPPrice = 0;

                        if (Item.SetInfo != "대표코드") {
                            if (Item.ProdCount == 1) {
                                if ((ProdPPrice + DiffCost) > 10) {
                                    DCProdPPrice = ProdPPrice + DiffCost;
                                }
                            }
                            if (DCProdPPrice == 0 && (Items.length - 1) == i) {
                                DCProdPPrice = ProdPPrice + parseInt(parseFloat(DiffCost) / parseFloat(Item.ProdCount));
                            }
                        }

                        if (DCProdPPrice != 0) {
                            Item.ProdPPrice = DCProdPPrice;
                            $(this).attr("OrderItem", JSON.stringify(Item));
                            return false;
                        }
                    });
                }

                CalCostInfo();

                return false;
            }
        });

        $("#OrderMainForm").ajaxForm({
            url: "/data/ORD/Order.Order.Update.Proc.asp",
            type: "POST",
            data: {
                OrderData: function () {
                    var OrderData = new Object();
                    OrderData.Idx = $("#OrderIdx_OrderMainForm").val();
                    OrderData.OrderCode = $("#OrderCode_OrderMainForm").val();
                    OrderData.OrderType = $("#OrderType_OrderMainForm").val();
                    OrderData.VatType = $("#VatType_OrderMainForm").val();
                    OrderData.MemId = $("#MemId_OrderMainForm").val();
                    OrderData.EmpId = $("#EmpId_OrderMainForm").val();
                    OrderData.CusCode = $("#CusCode_MainCustomer").val();
                    OrderData.InsuMan = $("#InsuMan_OrderMainForm").val();
                    OrderData.InsuTel = $("#InsuTel_OrderMainForm").val();
                    OrderData.ZipCode = $("#ZipCode_OrderMainForm").val();
                    OrderData.Addr1 = $("#Addr1_OrderMainForm").val();
                    OrderData.Addr2 = $("#Addr2_OrderMainForm").val();

                    var Items = new Array();
                    $.each($("input[name=selectedItem]"), function(i) {
                        Items.push($(this).attr("OrderItem"));
                    });
                    OrderData.ItemList = Items;

                    OrderData.ODate = $("#oDate_OrderMainForm").val();
                    OrderData.OTime = $("#oTime_OrderMainForm").val();
                    OrderData.Inhouse = $("#Inhouse_OrderMainForm").val();
                    OrderData.SigongType = $("#SigongType_OrderMainForm").val();
                    OrderData.SDate = $("#sDate_OrderMainForm").val();
                    OrderData.SigongDay = $("#SigongDay_OrderMainForm").val();
                    OrderData.OutMemo = $("#OutMemo_OrderMainForm").val();
                    OrderData.ImsiCode = '<%=Request.cookies("is")%>';

                    return JSON.stringify(OrderData);
                }
            },
            dataType: "text",
            beforeSend: function () {
                $(".fa").addClass("fa-spinner fa-spin");
            },
            complete: function () {
                $(".fa").removeClass("fa-spinner fa-spin");
            },
            beforeSubmit: function (data, form, option) {
                //validation체크
                //막기위해서는 return false를 잡아주면됨
                var oDate = $("#oDate_OrderMainForm").val();
                if (oDate == "") {
                    alert("도착요청일을 입력하세요.");
                    $("#oDate_OrderMainForm").focus();
                    return false;
                }

                return true;
            },
            success: function (response, status) {
                var rtnData = response.split("|");
                if (rtnData[0] == "OK") {
                    alert("저장 되었습니다.");
                    location.href = "/page/ORD/Order.View.asp?Oc=" + rtnData[1];
                } else {
                    alert(rtnData[1]);
                    return;
                }
            },
            error: function (e) {
                alert("오류 발생!");
            }
        });

        $("#MemIdSearchModalForm").ajaxForm({
            url: "/data/ORD/Order.MemId.Json.asp",
            type: "POST",
            data: {
                MemInfo: function () { return $("#MemIdSearch_SelMemIdModal").val() }
            },
            dataType: "json",
            beforeSend: function () {
                $(".fa").addClass("fa-spinner fa-spin");
            },
            complete: function () {
                $(".fa").removeClass("fa-spinner fa-spin");
            },
            beforeSubmit: function (data, form, option) {
                //validation체크
                //막기위해서는 return false를 잡아주면됨
                
                return true;
            },
            success: function (response, status) {
                var info = "";
                $.each(response, function (i) {
                    info += "<div class='bd-callout bd-callout-danger' name='MemList_selMemIdModel' id='" + this.id + "' site='" + this.site + "'>"
                    info += "	<h5 class='font-weight-bold'>" + this.name + "</h5>"
                    info += "	<h6>" + this.hp + "</h6>"
                    info += "	<h6>" + this.add1 + " " + this.add2 + "</h6>"
                    info += "</div>"
                });

                var body = $("#selMemIdModal").find("#MemIdList_SelMemIdModal").find(".list-group");
                body.html(info);
            },
            error: function (e) {
                alert("오류 발생!");
            }
        });

        $(document).on("click", "#btnDaumAddr_AddCusModal", function (e) {
            execDaumPostcode_Inline("wrap_AddCusModal", "ZipCode_AddCusModal", "Addr1_AddCusModal", "Addr2_AddCusModal");
        });

        $(document).on("click", "#btnDaumAddr_OrderMainForm", function (e) {
            execDaumPostcode_Inline("wrap_OrderMainForm", "ZipCode_OrderMainForm", "Addr1_OrderMainForm", "Addr2_OrderMainForm");
        });

        $(document).on("mouseover", "div[name=CusList_selCustomerModel]", function (e) {
            $(this).css("background-color", "#cafff2").css("cursor", "pointer");
        }).on("mouseout", "div[name=CusList_selCustomerModel]", function (e) {
            $(this).css("background-color", "").css("cursor", "");
        }).on("click", "div[name=CusList_selCustomerModel]", function (e) {
            var idx = $(this).attr("idx");
            $("#CusCode_MainCustomer").val(idx);
            $("#selCustomerModal").modal('hide');

            BindCustomerInfo(idx);
        });

        $(document).on("click", "#ChkAddrCopy_OrderMainForm", function (e) {
            if (this.checked) {
                var zipcode = $($("#CustomerInfo").find("h6")[3]).text();
                var addr1 = $($("#CustomerInfo").find("h6")[1]).text();
                var addr2 = $($("#CustomerInfo").find("h6")[2]).text();
                $("#ZipCode_OrderMainForm").val(zipcode);
                $("#Addr1_OrderMainForm").val(addr1);
                $("#Addr2_OrderMainForm").val(addr2);
            }
        });

        $(document).on("click", "#btnAddProd_OrderMainForm", function (e) {
            // IMSI 테이블에 세트1 추가
            var ImsiCode = '<%=Request.cookies("is")%>';
            var SetOpt = $("#SetOpt_OrderMainForm").val();
            var RName = $("#SetOpt_OrderMainForm").find("option:selected").text();
            MakeSetOpt(ImsiCode, SetOpt, RName);

            $("#addProdModal").modal('show');
        });

        $(document).on("change", "#SetOpt_OrderMainForm", function (e) {
            if ($(this).val() == "ADD") {
                var n = parseInt($(this).find("option:last").attr("num"));
                $(this).append("<option value='SET" + (n + 1) + "' num='" + (n + 1) + "'>세트" + (n + 1) + "</option>");

                $(this).find("option:last").attr("selected", "selected");

                var ImsiCode = '<%=Request.cookies("is")%>';
                var SetOpt = $(this).val();
                var RName = $(this).find("option:selected").text();
                MakeSetOpt(ImsiCode, SetOpt, RName);
                //<div class='d-flex w-100 justify-content-between'>
                var objSet = $("#divProdList_OrderMainForm").find("div[name=ProdList_OrderMainForm][SetOpt=" + SetOpt + "]");
                if (objSet.length == 0) {
                    var info = "";
                    info += "<div class='row'>";
                    info += "   <div class='col-12'>";
                    info += "       <div class='card' name='ProdList_OrderMainForm' SetOpt='" + SetOpt + "'>";
                    info += "           <div class='card-header text-white bg-info d-flex w-100 justify-content-between'>";
                    info += "               <h6>" + RName + "</h6>";
                    info += "               <h6 name='SetSCost'>￦ 0</h6>";
                    info += "           </div>";
                    info += "           <div class='card-body'>";
                    info += "               <div class='row'>";
                    info += "               </div>";
                    info += "           </div>";
                    info += "       </div>";
                    info += "   </div>";
                    info += "</div>";

                    $("#divProdList_OrderMainForm").append(info);
                }
            }

            // 선택된 세트 색상 변경
            $("#divProdList_OrderMainForm").find("div[name=ProdList_OrderMainForm]").find(".bg-danger").addClass("bg-info").removeClass("bg-danger");
            $("#divProdList_OrderMainForm").find("div[name=ProdList_OrderMainForm][SetOpt=" + $(this).val() + "]").find(".bg-info").addClass("bg-danger").removeClass("bg-info");
        });

        $(document).on("click", "button[name=btnProdOpt_AddProdModal]", function (e) {
            $.ajax({
                url: "/data/ORD/Order.Prod.Items.Json.asp",
                type: "POST",
                data: {
                    ProdOpt: $(this).attr("prod_opt"),
                    UseSite: function () { return $("#Site_OrderMainForm").val() }
                },
                dataType: "json",
                beforeSend: function () {
                    $(".fa").addClass("fa-spinner fa-spin");
                },
                complete: function () {
                    $(".fa").removeClass("fa-spinner fa-spin");
                },
                beforeSubmit: function (data, form, option) {
                    //validation체크
                    //막기위해서는 return false를 잡아주면됨
                    return true;
                },
                success: function (data, status) {
                    //성공후 서버에서 받은 데이터 처리
                    $("#Accordion_AddProdModal").find(".collapse").collapse('hide');
                    BindProdList(data);
                },
                error: function () {
                    alert("품목 리스트 조회 중 오류 발생!");
                }
            });
        });

        $(document).on("click", "button[name=btnProdPackage_AddProdModal]", function (e) {
            $.ajax({
                url: "/data/ORD/Order.Prod.PackageItems.Json.asp",
                type: "POST",
                data: {
                    SetCode: $(this).attr("set_code"),
                    UseSite: function () { return $("#Site_OrderMainForm").val() }
                },
                dataType: "json",
                beforeSend: function () {
                    $(".fa").addClass("fa-spinner fa-spin");
                },
                complete: function () {
                    $(".fa").removeClass("fa-spinner fa-spin");
                },
                beforeSubmit: function (data, form, option) {
                    //validation체크
                    //막기위해서는 return false를 잡아주면됨
                    return true;
                },
                success: function (data, status) {
                    //성공후 서버에서 받은 데이터 처리
                    $("#Accordion_AddProdModal").find(".collapse").collapse('hide');
                    BindProdList(data);
                },
                error: function () {
                    alert("품목 리스트 조회 중 오류 발생!");
                }
            });
        });

        $(document).on("click", "button[name=btnItemRemove]", function (e) {
            $(this).parent().parent().remove();

            // 금액 계산
            CalCostInfo();
        });

        $(document).on("click", "button[name=btnItemUpdate]", function (e) {
            // 품목 리스트 선택
            $(this).attr("data-select", "yes");
            $("#updateProdDetailModal").modal('show');
        });

        $(document).on("click", "#oDate_OrderMainForm", function (e) {
            $('#oDate_OrderMainForm').datepicker('setDatesDisabled', HoliDays());
            $('#oDate_OrderMainForm').datepicker('setStartDate', StartDate());
        });

        $(document).on("click", "#btnOrderMainMemId", function (e) {
            $("#selMemIdModal").modal("show");
        });

        $(document).on("mouseover", "div[name=MemList_selMemIdModel]", function (e) {
            $(this).css("background-color", "#cafff2").css("cursor", "pointer");
        }).on("mouseout", "div[name=MemList_selMemIdModel]", function (e) {
            $(this).css("background-color", "").css("cursor", "");
        }).on("click", "div[name=MemList_selMemIdModel]", function (e) {
            var id = $(this).attr("id");
            var name = $(this).find("h5.font-weight-bold").text();
            $("#MemId_OrderMainForm").val(id);
            $("#MemNm_OrderMainForm").val(name);
            $("#selMemIdModal").modal('hide');
        });

        $(document).on("shown.bs.modal", "#addCustomerModal", function (e) {
            $("#CusName_AddCusModal").focus();
        });

        $(document).on("hidden.bs.modal", "#addCustomerModal", function (e) {
            $("#addCusModalForm")[0].reset();
        });

        $(document).on("hidden.bs.modal", "#selCustomerModal", function (e) {
            $("#selCustomerModal").find(".modal-body").html("");
        });

        $(document).on("shown.bs.modal", "#addProdModal", function (e) {
            $.ajax({
                type: "POST",
                url: "/data/ORD/Order.Category.Package.Json.asp",
                data: {
                    UseSite: function () { return $("#Site_OrderMainForm").val() },
                    OrderType: function () { return $("#OrderType_OrderMainForm").val() }
                },
                dataType: 'json',
                cache: false,
                //async: false,
                beforeSend: function () {
                    $(".fa").addClass("fa-spinner fa-spin");
                },
                complete: function () {
                    $(".fa").removeClass("fa-spinner fa-spin");
                },
                success: function (data) {
                    var info = "";
                    $.each(data, function (i) {
                        info += "<div class='col-6 col-md-4 mb-3'>"
                        info += "	<div class='dropdown' name='StandardCategory_AddProdModal' Idx='" + this.idx + "'>"
                        info += "		<button class='btn btn-outline-danger dropdown-toggle btn-block' type='button' id='dropdownMenu" + this.idx + "' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>"
                        info += "			" + this.title
                        info += "		</button>"
                        info += "		<div class='dropdown-menu' aria-labelledby='dropdownMenu" + this.idx + "'>"
                        info += "		</div>"
                        info += "	</div>						"
                        info += "</div>"
                    });

                    $("#Category_AddProdModal").html(info);
                },
                error: function (ex) {
                    alert("패키지 카테고리 조회 중 오류 발생\n" + ex.responseText);
                }
            });

            $.ajax({
                type: "POST",
                url: "/data/ORD/Order.Category.Items.Json.asp",
                data: {
                    UseSite: function () { return $("#Site_OrderMainForm").val() },
                    OrderType: function () { return $("#OrderType_OrderMainForm").val() }
                },
                dataType: 'json',
                cache: false,
                //async: false,
                beforeSend: function () {
                    $(".fa").addClass("fa-spinner fa-spin");
                },
                complete: function () {
                    $(".fa").removeClass("fa-spinner fa-spin");
                },
                success: function (data) {
                    var info = "";
                    $.each(data, function (i) {
                        info += "<div class='col-6 col-sm-4 mb-3'>"
                        info += "	<button type='button' class='btn btn-outline-danger btn-block' name='btnProdOpt_AddProdModal' prod_opt='" + this.title + "'>" + this.title + "</button>"
                        info += "</div>"
                    });

                    $("#ItemsCategory_AddProdModal").html(info);
                },
                error: function (ex) {
                    alert("단품 카테고리 조회 중 오류 발생\n" + ex.responseText);
                }
            });
        });

        $(document).on("show.bs.modal", "#addProdDetailModal", function (e) {
            var oItem = JSON.parse($(e.relatedTarget).attr("OrderItem"));

            $("#addProdDetailModal").find("img.img-thumbnail").attr("src", "http://119.196.177.124:8080/updata/prod/" + oItem.ProdImg);
            $("#addProdDetailModal").find("input[name=Count]").val(oItem.ProdCount);
            $("#addProdDetailModal").find("input[name=ProdPPrice]").val(oItem.ProdPPrice);
            $("#addProdDetailModal").find("input[name=ProdPrice]").val(oItem.ProdPrice);
            $("#addProdDetailModal").find("input[name=Memo]").val(oItem.Memo);
            $("#addProdDetailModal").find("input[name=ProdCode]").val(oItem.ProdCode);
            $("#addProdDetailModal").find("input[name=ProdCode]").attr("OrderItem", JSON.stringify(oItem));
        }).on("hidden.bs.modal", "#addCustomerModal", function (e) {
            $("#ProdCountForm_AddProdDetailModal")[0].reset();
        });

        $(document).on("shown.bs.dropdown", "div[name=StandardCategory_AddProdModal]", function (e) {
            var _obj = this;
            $.ajax({
                type: "POST",
                url: "/data/ORD/Order.Category.PackageItems.Json.asp",
                data: {
                    UseSite: function () { return $("#Site_OrderMainForm").val() },
                    OrderType: function () { return $("#OrderType_OrderMainForm").val() },
                    CategoryIdx: function () { return $(_obj).attr("Idx") }
                },
                dataType: 'json',
                cache: false,
                //async: false,
                beforeSend: function () {
                    $(".fa").addClass("fa-spinner fa-spin");
                },
                complete: function () {
                    $(".fa").removeClass("fa-spinner fa-spin");
                },
                success: function (data) {
                    var info = "";
                    $.each(data, function (i) {
                        info += "<button class='dropdown-item' type='button' name='btnProdPackage_AddProdModal' set_code='" + this.set_code + "'>" + this.title + "</button>"
                    });

                    $(_obj).find(".dropdown-menu").html(info);
                },
                error: function (ex) {
                    alert("오류 발생\n" + ex.responseText);
                }
            });
        });

        $(document).on("show.bs.modal", "#addProdDetailBomModal", function (e) {
            var _obj = this;
            $.ajax({
                type: "POST",
                url: "/data/ORD/Order.Prod.Bom.Json.asp",
                data: {
                    ProdCode: $("#ProdCountForm_AddProdDetailModal").find("input[name=ProdCode]").val(),
                    UseSite: function () { return $("#Site_OrderMainForm").val() }
                },
                dataType: 'json',
                cache: false,
                //async: false,
                beforeSend: function () {
                    $(".fa").addClass("fa-spinner fa-spin");
                },
                complete: function () {
                    $(".fa").removeClass("fa-spinner fa-spin");
                },
                success: function (data) {
                    var info = "";
                    $.each(data, function (i) {
                        var oItem = new OrderItem();
                        oItem.ProdCode = this.prod_code;
                        oItem.ProdName = this.prod_name;
                        oItem.ProdCount = this.cnt;
                        oItem.ProdPrice = this.scost_p;
                        oItem.SetInfo = this.code_opt;
                        oItem.CostOri = this.scost_p;
                        oItem.ProdPPrice = this.pcost_p;
                        oItem.PCostOri = this.pcost_p;
                        oItem.ProdImg = this.prod_img;
                        oItem.Status = this.status;
                        oItem.WorryYn = this.worry_yn;
                        oItem.Site = this.site;
                        oItem.MaxCnt = this.prod_max_cnt;
                        oItem.Memo = "";
                        oItem.Idx = "";

                        var active = (oItem.SetInfo == "종속" ? "active" : "");
                        var checked = (oItem.SetInfo == "종속" ? "checked" : "");
                        info += "<label class='col-12 btn btn-info text-left mb-1 " + active + "'>";
                        info += "   <input type='checkbox' " + checked + " autocomplete='off' name='chkProdBom_AddProdDetailBomModal' OrderItem='" + JSON.stringify(oItem) + "'>";
                        info += "   <div class='d-flex w-100 justify-content-between'>";
                        info += "		<small>[" + oItem.SetInfo + "] " + oItem.ProdName + "</small>"
                        info += "		<small> 수량 : " + oItem.ProdCount + "</small>"
                        info += "	</div>"
                        info += "</label>";
                    });

                    $(_obj).find("#BomList_AddProdDetailBomModal").html(info);
                },
                error: function (ex) {
                    alert("BOM 조회 중 오류 발생\n" + ex.responseText);
                }
            });
        });

        $(document).on("show.bs.modal", "#updateProdDetailModal", function (e) {
            var obj = $("div[name=ProdList_OrderMainForm]").find("[data-select=yes]").parent().find("input[name=selectedItem]");
            var oItem = JSON.parse($(obj).attr("OrderItem"));

            $("#updateProdDetailModal").find("img.img-thumbnail").attr("src", "http://119.196.177.124:8080/updata/prod/" + oItem.ProdImg);
            $("#updateProdDetailModal").find("input[name=Count]").val(oItem.ProdCount);
            $("#updateProdDetailModal").find("input[name=ProdCode]").val(oItem.ProdCode);
            $("#updateProdDetailModal").find("input[name=ProdCode]").attr("OrderItem", JSON.stringify(oItem));

            // 세트 선택
            $("#SetOpt_OrderMainForm").find("option[value=" + oItem.SetOpt + "]").prop("selected", true);
            // 선택된 세트 색상 변경
            $("#divProdList_OrderMainForm").find("div[name=ProdList_OrderMainForm]").find(".bg-danger").addClass("bg-info").removeClass("bg-danger");
            $("#divProdList_OrderMainForm").find("div[name=ProdList_OrderMainForm][SetOpt=" + oItem.SetOpt + "]").find(".bg-info").addClass("bg-danger").removeClass("bg-info");

        }).on("hidden.bs.modal", "#updateProdDetailModal", function (e) {
            // 품목 리스트 선택해제
            $("div[name=ProdList_OrderMainForm]").find("[data-select=yes]").attr("data-select", "");
        });

        $(document).on("show.bs.modal", "#selMemIdModal", function (e) {
            $.ajax({
                type: "POST",
                url: "/data/ORD/Order.Site.Json.asp",
                dataType: 'json',
                cache: false,
                async: false,
                beforeSend: function () {
                    $(".fa").addClass("fa-spinner fa-spin");
                },
                complete: function () {
                    $(".fa").removeClass("fa-spinner fa-spin");
                },
                success: function (data) {
                    var info = "";
                    info += "<div class='col-12 text-center'>";
                    info += "	<div class='btn-group-toggle' data-toggle='buttons'>";
                    $.each(data, function (i) {
                        if (this.site == '<%=request.cookies("msite")%>') {
                            info += "	    <label class='btn btn-secondary active mb-1'>";
                            info += "	        <input type='radio' name='Site' autocomplete='off' value='" + this.site + "' checked>" + this.sname;
                            info += "	    </label>";
                        } else {
                            info += "	    <label class='btn btn-secondary mb-1'>";
                            info += "	        <input type='radio' name='Site' autocomplete='off' value='" + this.site + "'>" + this.sname;
                            info += "	    </label>";
                        }
                    });
                    info += "	</div>";
                    info += "</div>";

                    $("#divSiteButton_SelMemIdModal").html(info);
                },
                error: function (ex) {
                    alert("사이트 조회 중 오류 발생\n" + ex.responseText);
                }
            });
        }).on("shown.bs.modal", "#selMemIdModal", function (e) {
            $('#MemIdSearch_SelMemIdModal').trigger('focus');
        }).on("hidden.bs.modal", "#selMemIdModal", function (e) {
            var body = $("#selMemIdModal").find("#MemIdList_SelMemIdModal").find(".list-group");
            body.html("");
        });

        BindCustomerInfo = function (idx) {
            $.ajax({
                type: "POST",
                url: "/data/ORD/Order.Customer.Json.asp",
                data: {
                    CustomerIdx: idx
                },
                dataType: 'json',
                cache: false,
                //async: false,
                beforeSend: function () {
                    $(".fa").addClass("fa-spinner fa-spin");
                },
                complete: function () {
                    $(".fa").removeClass("fa-spinner fa-spin");
                },
                success: function (data) {
                    var info = "";
                    $.each(data, function (i) {
                        info += "<h5 class='font-weight-bold'>" + this.name + "</h5>"
                        info += "<a href='tel:" + this.hp + "'><h6>" + this.hp + "</h6></a>"
                        info += "<h6>" + this.add1 + "</h6>"
                        info += "<h6>" + this.add2 + "</h6>"
                        info += "<h6 class='d-none'>" + this.zipcode + "</h6>"
                    });

                    $("#CustomerInfo").html(info);
                },
                error: function (ex) {
                    alert("오류 발생\n" + ex.responseText);
                }
            });
        }

        MakeSetOpt = function (ImsiCode, SetOpt, RName) {
            $.ajax({
                type: "POST",
                url: "/data/ORD/Order.ImsiCode.Write.Proc.asp",
                data: {
                    ImsiCode: ImsiCode,
                    SetOpt: SetOpt,
                    RName: RName,
                    PkgType: 0
                },
                dataType: 'text',
                cache: false,
                //async: false,
                beforeSend: function () {
                    $(".fa").addClass("fa-spinner fa-spin");
                },
                complete: function () {
                    $(".fa").removeClass("fa-spinner fa-spin");
                },
                success: function (data) {
                    return;
                },
                error: function (ex) {
                    alert("세트생성 중 오류 발생\n" + ex.responseText);
                }
            });
        }

        BindProdList = function (json) {
            var info = "";
            $.each(json, function (i) {

                var oItem = new OrderItem();
                oItem.Set(this);

                var stat = "";
                if (oItem.Status != "ON") {
                    stat = "list-group-item-danger";
                }
                else if (oItem.WorryYn == "Y") {
                    stat = "list-group-item-warning"
                }
                info += "<a href='#addProdDetailModal' class='list-group-item list-group-item-action " + stat + "' data-toggle='modal' OrderItem='" + JSON.stringify(oItem) + "'>"
                info += "	<h6 class='mb-1'><strong>" + oItem.ProdName + "</strong></h6>"
                info += "	<div class='d-flex w-100 justify-content-between'>"
                info += "		<small>" + oItem.SetInfo + "</small>"
                info += "		<small>￦ " + addCommas(oItem.ProdPrice) + "</small>"
                info += "	</div>"
                info += "</a>"
            });

            $("#ItemList_AddProdModal").find(".list-group").html(info);
        }

        AddProd = function (models) {
            var SetOpt = $("#SetOpt_OrderMainForm").val();
            var info = "";
            $.each(models, function (i) {
                var oItem = new OrderItem();
                oItem.Set(this);

                info += "<div class='col-12 col-sm-6 col-lg-4 col-xl-3'>";
                info += "    <div class='bd-callout bd-callout-danger'>";
                info += "        <button type='button' class='btn btn-link' name='btnItemUpdate' style='opacity: .5; font-size: 1.5rem; color: #000;'>";
                info += "           <i class='fa fa-search' aria-hidden='true'></i>";
                info += "        </button>";
                info += "        <button type='button' class='close' aria-label='Close' name='btnItemRemove'>";
                info += "           <span aria-hidden='true'>&times;</span>";
                info += "        </button>";
                info += "        <input type='hidden' name='selectedItem' OrderItem='" + JSON.stringify(oItem) + "'> ";
                info += "        <strong>";
                info += "            <p>" + oItem.ProdName + "</p>";
                info += "        </strong>";
                info += "        <strong>";
                info += "            <p>" + oItem.ProdCode + "</p>";
                info += "        </strong>";
                info += "        <small>";
                info += "            <p>" + oItem.SetInfo + "</p>";
                info += "        </small>";
                info += "        <small>";
                info += "            <p class='text-right'>수량 : " + oItem.ProdCount + "</p>";
                info += "        </small>";
                info += "        <small>";
                info += "            <p class='text-right' data-bind='ProdPrice'>단가 : " + addCommas(oItem.ProdPrice) + "</p>";
                info += "        </small>";
                info += "        <small>";
                info += "            <p class='text-right' data-bind='ProdAmount'>금액 : " + addCommas(parseInt(oItem.ProdCount) * parseInt(oItem.ProdPrice)) + "</p>";
                info += "        </small>";
                info += "    </div>";
                info += "</div>";

                oItem = null;
            });

            return info;
        }

        CalCostInfo = function () {
            // 세트별 및 총금액 계산
            var objSets = $("div[name=ProdList_OrderMainForm]");
            var TotSCost = 0;
            var TotPCost = 0;
            var TotSCostOri = 0;
            var TotPCostOri = 0;

            $.each(objSets, function (i) {
                var SCost = 0;
                var PCost = 0;
                var SCostOri = 0;
                var PCostOri = 0;

                $.each($(this).find("input[name=selectedItem]"), function (j) {
                    var oItem = JSON.parse($(this).attr("OrderItem"));
                    SCost += parseInt(oItem.ProdPrice) * parseInt(oItem.ProdCount);
                    PCost += parseInt(oItem.ProdPPrice) * parseInt(oItem.ProdCount);
                    SCostOri += parseInt(oItem.CostOri) * parseInt(oItem.ProdCount);
                    PCostOri += parseInt(oItem.PCostOri) * parseInt(oItem.ProdCount);
                });

                // 세트별 소비자 금액
                $("div[name=ProdList_OrderMainForm][SetOpt=" + $(this).attr("SetOpt") + "]").find("h6[name=SetSCost]").text("￦ " + addCommas(SCost));

                TotSCost += SCost;
                TotPCost += PCost;
                TotSCostOri += SCostOri;
                TotPCostOri += PCostOri;
            });

            // 전체 소비자 금액 정보
            var SCostRate = parseFloat(100 - ((TotSCost / TotSCostOri) * 100));
            SCostRate = (Math.round(SCostRate * 10)) / 10;
            if (isNaN(SCostRate)) {
                SCostRate = 0;
            }

            var SCostDC = TotSCostOri - TotSCost;
            var info = "";
            info += ""
            info += "<div class='d-flex w-100 justify-content-between'>";
            info += "    <h6 class='font-weight-bold'>최종금액</h6>";
            info += "    <h6 class='font-weight-bold'>￦ " + addCommas(TotSCost) + "</h6>";
            info += "</div>";
            info += "<div class='d-flex w-100 justify-content-between'>";
            info += "    <h6 class='font-weight-bold'>할인율</h6>";
            info += "    <h6 class='font-weight-bold'>" + SCostRate + "%</h6>";
            info += "</div>";
            info += "<div class='d-flex w-100 justify-content-between'>";
            info += "    <h6 class='font-weight-bold'>할인금액</h6>";
            info += "    <h6 class='font-weight-bold'>￦ " + addCommas(SCostDC) + "</h6>";
            info += "</div>";
            $("#SCostInfo").html(info);

            // 전체 공급가 금액 정보
            var PCostRate = parseFloat(100 - ((TotPCost / TotPCostOri) * 100));
            PCostRate = (Math.round(PCostRate * 10)) / 10;
            if (isNaN(PCostRate)) {
                PCostRate = 0;
            }

            var PCostDC = TotPCostOri - TotPCost;
            var info = "";
            info += ""
            info += "<div class='d-flex w-100 justify-content-between'>";
            info += "    <h6 class='font-weight-bold'>최종금액</h6>";
            info += "    <h6 class='font-weight-bold'>￦ " + addCommas(TotPCost) + "</h6>";
            info += "</div>";
            info += "<div class='d-flex w-100 justify-content-between'>";
            info += "    <h6 class='font-weight-bold'>할인율</h6>";
            info += "    <h6 class='font-weight-bold'>" + PCostRate + "%</h6>";
            info += "</div>";
            info += "<div class='d-flex w-100 justify-content-between'>";
            info += "    <h6 class='font-weight-bold'>할인금액</h6>";
            info += "    <h6 class='font-weight-bold'>￦ " + addCommas(PCostDC) + "</h6>";
            info += "</div>";
            $("#PCostInfo").html(info);
        }

        HoliDays = function () {
            var _HoliDays = new Array();

            $.ajax({
                type: "POST",
                url: "/data/ORD/Order.HoliDay.Json.asp",
                data: {
                },
                dataType: 'json',
                cache: false,
                async: false,
                beforeSend: function () {
                    $(".fa").addClass("fa-spinner fa-spin");
                },
                complete: function () {
                    $(".fa").removeClass("fa-spinner fa-spin");
                },
                success: function (data) {
                    $.each(data, function (i) {
                        var month = (this.hmonth.toString().length == 1) ? "0" + this.hmonth.toString() : this.hmonth.toString();
                        var day = (this.hday.toString().length == 1) ? "0" + this.hday.toString() : this.hday.toString();
                        var date = new Date().getFullYear().toString() + "-" + month + "-" + day;

                        _HoliDays.push(date);
                    });
                },
                error: function (ex) {
                    alert("오류 발생\n" + ex.responseText);
                }
            });

            return _HoliDays;
        }

        StartDate = function () {
            var startDt = "";

            var ProdCodes = new Array();
            $.each($("input[name=selectedItem]"), function (i) {
                var Item = JSON.parse($(this).attr("OrderItem"));
                var Prod = new Object();
                Prod.ProdCode = Item.ProdCode;
                ProdCodes.push(Prod);
            });

            $.ajax({
                type: "POST",
                url: "/data/ORD/Order.StartDate.Json.asp",
                data: {
                    ProdCodes: JSON.stringify(ProdCodes)
                },
                dataType: 'text',
                cache: false,
                async: false,
                success: function (text) {
                    startDt = text;
                },
                error: function (ex) {
                    alert("오류 발생\n" + ex.responseText);
                }
            });

            return startDt;
        }

    });

    function OrderItem() {
        this.ProdCode = "";
        this.ProdName = "";
        this.ProdCount = "";
        this.ProdPrice = "";
        this.SetInfo = "";
        this.SetOpt = "";
        this.CostOri = "";
        this.ProdPPrice = "";
        this.PkgYn = "";
        this.PkgSetCode = "";
        this.PCostOri = "";
        this.RelIdx = "";
        this.ProdImg = "";
        this.Status = "";
        this.WorryYn = "";
        this.Site = "";
        this.TopCode = "";
        this.MaxCnt = "";
        this.Memo = "";
        this.Idx = "";
    }

    OrderItem.prototype.Set = function(model) {
        this.ProdCode = model.prod_code || model.ProdCode || "";
        this.ProdName = model.prod_name || model.ProdName || "";
        this.ProdCount = model.cnt || model.ProdCount || "";
        this.ProdPrice = model.scost_p || model.ProdPrice || "";
        this.SetInfo = model.code_opt || model.SetInfo || "";
        this.SetOpt = model.set_opt || model.SetOpt || "";
        this.CostOri = model.scost_p || model.CostOri || "";
        this.ProdPPrice = model.pcost_p || model.ProdPPrice || "";
        this.PkgYn = model.pkg_yn || model.PkgYn || "";
        this.PkgSetCode = model.pkg_set_code || model.PkgSetCode || "";
        this.PCostOri = model.pcost_p || model.PCostOri || "";
        this.RelIdx = model.rel_idx || model.RelIdx || "";
        this.ProdImg = model.prod_img || model.ProdImg || "";
        this.Status = model.status || model.Status || "";
        this.WorryYn = model.worry_yn || model.WorryYn || "";
        this.Site = model.site || model.Site || "";
        this.TopCode = model.topcode || model.TopCode || "";
        this.MaxCnt = model.prod_max_cnt || model.MaxCnt || "";
        this.Memo = model.memo || model.Memo || "";
        this.Idx = model.idx || model.Idx || "";
    };

//-->
</script>

<!-- #include virtual = "/share/include/PageFooter.asp" -->

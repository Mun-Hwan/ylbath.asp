<%@Language="VBScript" CODEPAGE="65001"%>
<!-- #include virtual = "/share/include/DbInfo.asp" -->
<!-- #include virtual = "/share/include/Injection.asp" -->
<!-- #include virtual = "/share/include/Function.asp" -->
<!-- #include virtual = "/share/include/Const.asp" -->
<!-- #include virtual = "/share/include/PageHeader.asp" -->
<!-- #include virtual = "/share/include/ContentsHeader.asp" -->

<%
        '// 기본날짜 셋팅
        stMonth = Month(Date())
        if Len(stMonth) = 1 then stMonth = "0" & stMonth
        stDt = Year(Date()) & "-" & stMonth & "-" & "01"
        
        '// 시작월의 마지막 날
        edDt = DateAdd("m", 1, stDt)
        edDt = DateAdd("d", -1, edDt)

        '// Paging 기준값
        PageSize = 10           '// 페이지당 보여줄 데이타수
        BlockSize = 5             '// 페이지 그룹 범위       1 2 3 5 6 7 8 9 10
%>

    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">오더관리</a>
        </li>
        <li class="breadcrumb-item active">오더조회</li>
      </ol>

	  <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
			<form id="Search_Form">
                <input type="hidden" class="form-control" name="Page" value="1">
                <input type="hidden" class="form-control" name="PageSize" value="<%=PageSize%>">
				<div class="row">
					<div class="col-xl-3 col-sm-6 mb-3">
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text">도착요청일</span>
							</div>
							<input type="text" class="form-control text-center" id="startDate" value="<%=stDt%>" required>
							<input type="text" class="form-control text-center" id="endDate" value="<%=edDt%>" required>
						</div>
					</div>
					<div class="col-xl-3 col-sm-6 mb-3">
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text">오더번호</span>
							</div>
							<input type="text" class="form-control" id="Sc_OrderCode">
						</div>
					</div>
					<div class="col-xl-3 col-sm-6 mb-3">
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text">담당자</span>
							</div>
							<select class="form-control" id="Sc_MemId">
								<option></option>
								<option value="심현아">심현아</option>
								<option value="조재선">조재선</option>
								<option value="박은희">박은희</option>
								<option value="조현나">조현나</option>
								<option value="차문환">차문환</option>
							</select>
						</div>
					</div>
					<div class="col-xl-3 col-sm-6 mb-3">
						<button type="submit" class="btn btn-secondary mary btn-block"><i class="fa fa-search" aria-hidden="true"></i> <span>조회</span></button>
					</div>
				</div>
			</form>
		</div>
        <div class="card-body" id="OrderList_MainOrderForm">
            <div class="list-group">

            </div>
        </div>
        <div class="card-footer" id="Paging_MainOrderForm">
		</div>
      </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->

	<!-- modal -->
	<!-- Delete Modal HTML -->
	<div id="delNoticeModal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<form id="delModalForm">
					<div class="modal-header">						
						<h4 class="modal-title"><i class="fa fa-trash" aria-hidden="true"></i> <span>삭제</span></h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">					
						<p>삭제 하시겠습니까?</p>
						<p class="text-warning"><small>삭제시 복구 불가능 합니다.</small></p>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-undo" aria-hidden="true"></i> <span>취소</span></button>
						<button type="submit" class="btn btn-danger"><i class="fa fa-times" aria-hidden="true"></i> <span>삭제</span></button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- /.modal -->

<!-- #include virtual = "/share/include/ContentsAddon.asp" -->
<!-- #include virtual = "/share/include/ContentsFooter.asp" -->
<!-- #include virtual = "/share/include/JavaScript.asp" -->

<script type="text/javascript">
<!--
	$(document).ready(function() {

        $('#startDate').datepicker({
			format: "yyyy-mm-dd",
			todayBtn: "linked",
			language: "kr",
			multidate: false,
			//daysOfWeekDisabled: "0",
			daysOfWeekHighlighted: "0,6",
			autoclose: true,
			todayHighlight: true,
			datesDisabled: ['07/06/2018', '07/21/2018'],
			toggleActive: true
        });

        $('#endDate').datepicker({
			format: "yyyy-mm-dd",
			todayBtn: "linked",
			language: "kr",
			multidate: false,
			//daysOfWeekDisabled: "0",
			daysOfWeekHighlighted: "0,6",
			autoclose: true,
			todayHighlight: true,
			datesDisabled: ['07/06/2018', '07/21/2018'],
			toggleActive: true
        });

		$("#Search_Form").ajaxForm({
			url: "/data/ORD/Order.List.Json.asp",
            data: {
                startDate : function() { return $("#startDate").val() }, 
                endDate: function () { return $("#endDate").val() },
                OrderCode: function () { return $("#Sc_OrderCode").val() },
                MemId: function () { return $("#Sc_MemId").val() }
            },
            type: "POST",
			dataType: "json",
			beforeSend:function(){
                $(".fa").addClass("fa-spinner fa-spin");
			},
			complete:function(){
				$(".fa").removeClass("fa-spinner fa-spin");
			},
            beforeSubmit: function (data,form,option) {
                //validation체크 
                //막기위해서는 return false를 잡아주면됨
                return true;
            },
            success: function(data, status){
                var info = "";
                var maxCnt = 0;
                $.each(data, function () {
                    maxCnt = this.max_cnt;
                    var stat = "";
                    if (this.order_opt == "order") {
                        stat = " list-group-item-warning";
                    }
                    info += "  <a href='/page/ORD/Order.View.asp?Oc=" + this.order_code + "' class='list-group-item list-group-item-action" + stat + "'>";
                    info += "    <div class='row'>";
                    info += "       <div class='col-6 col-xl-2'>";
                    info += "           <small class='text-muted'>오더번호</small>";
                    info += "           <p class='mb-0'>" + this.order_code + "</p>";
                    info += "       </div>";
                    info += "       <div class='col-6 col-xl-2'>";
                    info += "           <small class='text-muted'>도착요청일</small>";
                    info += "           <p class='mb-0'>" + this.odate + "</p>";
                    info += "       </div>";
                    info += "       <div class='col-6 col-xl-2'>";
                    info += "           <small class='text-muted'>판매처</small>";
                    info += "           <p class='mb-0'>" + this.mem_name + "</p>";
                    info += "       </div>";
                    info += "       <div class='col-6 col-xl-2'>";
                    info += "           <small class='text-muted'>고객명</small>";
                    info += "           <p class='mb-0'>" + this.cus_name + "</p>";
                    info += "       </div>";
                    info += "       <div class='col-6 col-xl-2'>";
                    info += "           <small class='text-muted'>소비자가</small>";
                    info += "           <p class='mb-0'>" + addCommas(this.scost_p) + "</p>";
                    info += "       </div>";
                    info += "       <div class='col-6 col-xl-2'>";
                    info += "           <small class='text-muted'>공급가</small>";
                    info += "           <p class='mb-0'>" + addCommas(this.pcost_p) + "</p>";
                    info += "       </div>";
                    info += "    </div>";
                    info += "    <div class='row'>";
                    info += "       <div class='col-12 col-xl-10'>";
                    info += "           <small class='text-muted'>설치주소</small>";
                    info += "           <p class='mb-0'>" + this.b_add1 + " " + this.b_add2 + "</p>";
                    info += "       </div>";
                    info += "       <div class='col-6 col-xl-2'>";
                    info += "           <small class='text-muted'>오더유형</small>";
                    info += "           <p class='mb-0'>" + this.order_type_nm + "</p>";
                    info += "       </div>";
                    info += "       <div class='col-6 col-xl-2'>";
                    info += "           <small class='text-muted'>담당자</small>";
                    info += "           <p class='mb-0'>" + this.emp_name + "</p>";
                    info += "       </div>";
                    info += "    </div>";
                    info += "  </a>";
                });

                $("#OrderList_MainOrderForm").find(".list-group").html(info);
                var NowPage = $("#Search_Form").find("input[name=Page]").val();
                var page_viewList = Paging(maxCnt, '<%=PageSize%>', '<%=BlockSize%>', NowPage);
                $("#Paging_MainOrderForm").html(page_viewList);
            },
            error: function(){
				alert("오류 발생!");
            }                               
        });

		$("#delModalForm").ajaxForm({
			url: "/data/NTC/BusiLog.Delete.Proc.asp",
			type: "POST",
			data: {
				param1: function() { return $("#idx_AddModal").val() }
			},
			dataType: "text",
			beforeSend:function(){
                $(".fa").addClass("fa-spinner fa-spin");
			},
			complete:function(){
                $(".fa").removeClass("fa-spinner fa-spin");
			},
            beforeSubmit: function (data,form,option) {
                //validation체크 
                //막기위해서는 return false를 잡아주면됨
                return true;
            },
            success: function(response, status){
                //성공후 서버에서 받은 데이터 처리
				if (response == "OK")
				{
					alert("삭제 되었습니다.");
					$('#dataTable').DataTable().ajax.reload(null, false);
					$("#delNoticeModal").modal('hide');
					$("#addNoticeModal").modal('hide');
				}
            },
            error: function(){
				alert("오류 발생!");
            }                               
        });

		$("#dataTable").attr("style", "cursor: pointer;");

		$(document).on("mouseover", "#dataTable > tbody > tr", function (e) {
			$(this).css("background-color", "#cafff2");
		});

		$(document).on("mouseout", "#dataTable > tbody > tr", function (e) {
			$(this).css("background-color", "");
		});

		$(document).on("click", "#dataTable > tbody > tr", function (e) {
			var cellIndex = e.target.cellIndex;
			if (cellIndex == 0)
				return;

			var data = _dataTable.row(this).data();
			$("#addModalForm").find("#idx_AddModal").val(data.Idx);
			$("#addNoticeModal").modal('show');
		});

		$(document).on("click", "#btnDaumAddr_AddModal", function (e) {
			execDaumPostcode_Inline("wrap_AddModal", null, "addr1_AddModal", "addr2_AddModal");
        });

		$(document).on("click", "nav  a[name=pageGoto]", function (e) {
            var page = $(this).attr("target-page");
            $("#Search_Form").find("input[name=Page]").val(page);
            $("#Search_Form").submit();
        });

		$('#addNoticeModal').on('shown.bs.modal', function (e) {
			var idx = $("#addModalForm").find("#idx_AddModal").val();
			if (idx == "")
			{
				$("#addNoticeModal").find("input[name=VisitTime]").focus();
			}
			else 
			{
				$.ajax({
					type: "POST",
					url: "/data/NTC/BusiLog.BusiLog.Json.asp",
					data: {
						param1: idx
					},
					dataType: 'json',
					cache: false,
					//async: false,
					beforeSend:function(){
					   $(".fa").addClass("fa-spinner fa-spin");
					},
					complete:function(){
						$(".fa").removeClass("fa-spinner fa-spin");
					},
					success: function (data) {
						$.each(data, function(i) {
							
							$("#addModalForm").find("select[name=Mem_Id]").val(this.Mem_Id);
							$("#addModalForm").find("input[name=BusiDate]").val(this.BusiDate);
							$("#addModalForm").find("input[name=VisitTime]").val(this.VisitTime);
							$("#addModalForm").find("input[name=ReturnVisit]").val(this.ReturnVisit);
							$("#addModalForm").find("input[name=ProdType]").val(this.ProdType);
							$("#addModalForm").find("input[name=Shop_Id]").val(this.Shop_Id);
							$("#addModalForm").find("input[name=Addr1]").val(this.Addr1);
							$("#addModalForm").find("input[name=Addr2]").val(this.Addr2);
							$("#addModalForm").find("textarea[name=Remark]").val(this.Remark);

						});
					},
					error: function (ex) {
						alert("오류 발생\n" + ex.responseText);
					}
				});
			}
		});
		
		$('#addNoticeModal').on('hidden.bs.modal', function (e) {
			$("#addModalForm")[0].reset();
			$("#BusiDate_AddModal").val("<%=Date()%>");
			$("#idx_AddModal").val("");
		});
		
	});

//-->
</script>

<!-- #include virtual = "/share/include/PageFooter.asp" -->

<%@  language="VBScript" codepage="65001" %>
<!-- #include virtual = "/share/include/DbInfo.asp" -->
<!-- #include virtual = "/share/include/Injection.asp" -->
<!-- #include virtual = "/share/include/Function.asp" -->
<!-- #include virtual = "/share/include/Const.asp" -->
<!-- #include virtual = "/share/include/PageHeader.asp" -->
<!-- #include virtual = "/share/include/ContentsHeader.asp" -->

<%
	OrderCode = str_chk(Request("Oc"))

	sql = ""
	sql = sql & vbCrlf & "      select    a.order_code,     "
	sql = sql & vbCrlf & "                  a.ver,     "
	sql = sql & vbCrlf & "                  ( select  z.t_name  from  sales_order_type z with(nolock)  where  z.t_code = a.order_type ) as order_type_nm,     "
	sql = sql & vbCrlf & "                  a.mem_id,     "
	sql = sql & vbCrlf & "                  ( select  z.name  from  sales_member z with(nolock)  where  z.id = a.mem_id ) as mem_id_nm,     "
	sql = sql & vbCrlf & "                  a.emp_id,     "
	sql = sql & vbCrlf & "                  ( select  z.name  from  sales_member z with(nolock)  where  z.id = a.emp_id ) as emp_id_nm,     "
	sql = sql & vbCrlf & "                  a.vat_type,     "
	sql = sql & vbCrlf & "                  a.cus_code,     "
	sql = sql & vbCrlf & "                  a.insu_man,     "
	sql = sql & vbCrlf & "                  a.insu_tel,     "
	sql = sql & vbCrlf & "                  a.b_zipcode,     "
	sql = sql & vbCrlf & "                  a.b_add1,     "
	sql = sql & vbCrlf & "                  a.b_add2,     "
	sql = sql & vbCrlf & "                  a.imsi_code,     "
	sql = sql & vbCrlf & "                  a.odate,     "
	sql = sql & vbCrlf & "                  a.otime,     "
	sql = sql & vbCrlf & "                  a.inhouse,     "
	sql = sql & vbCrlf & "                  a.sigong_type,     "
	sql = sql & vbCrlf & "                  a.sdate,     "
	sql = sql & vbCrlf & "                  a.sigong_day,     "
	sql = sql & vbCrlf & "                  a.out_memo     "
	sql = sql & vbCrlf & "        from   sales_order a with(nolock)     "
	sql = sql & vbCrlf & "       where   a.order_code = '" & OrderCode & "'     "
	sql = sql & vbCrlf & "          and   a.ver = a.now_ver     "
	sql = sql & vbCrlf & "          and   a.del_chk != 'Y'     "
	'Response.Write sql
	Set rsOrder = db.execute(sql)
%>

<div class="container-fluid">
    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="#">오더관리</a>
        </li>
        <li class="breadcrumb-item active">오더상세</li>
    </ol>

    <!-- Example DataTables Card-->
    <div class="card mb-3">

        <div class="card-body">

            <div class="row">
                <div class="col-12 col-md-6 col-xl-4 mb-3">
                    <div class="card">
                        <h5 class="card-header">기본정보</h5>
                        <div class="card-body">
                            <div class="form-group row">
                                <label for="exampleInputEmail1" class="col-xl-3 col-form-label">오더유형</label>
                                <div class="col-xl-9">
                                    <input type="text" class="form-control text-center" value="<%=rsOrder("order_type_nm")%>" readonly>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="exampleInputEmail1" class="col-xl-3 col-form-label">결재유형</label>
                                <div class="col-xl-9">
									<%
										VatType = ""
										Select Case rsOrder("vat_type")
											Case "A" : VatType = "일반세금계산서"
											Case "D" : VatType = "계산서미발급"
											Case "L" : VatType = "신용카드"
											Case "P" : VatType = "현금영수증"
										End Select 
									%>
                                    <input type="text" class="form-control text-center" value="<%=VatType%>" readonly>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="exampleInputEmail1" class="col-xl-3 col-form-label">판매처</label>
                                <div class="col-xl-9">
                                    <div class="input-group">
                                        <input type="text" class="form-control text-center" aria-label="First name" name="MemId" id="MemId_OrderMainForm" value="<%=rsOrder("mem_id")%>" readonly>
                                        <input type="text" class="form-control text-center" aria-label="Last name" name="MemNm" id="MemNm_OrderMainForm" value="<%=rsOrder("mem_id_nm")%>" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="exampleInputEmail1" class="col-xl-3 col-form-label">담당자</label>
                                <div class="col-xl-9">
                                    <div class="input-group">
                                        <input type="text" class="form-control text-center" aria-label="First name" name="EmpId" id="EmpId_OrderMainForm" value="<%=rsOrder("emp_id")%>" readonly>
                                        <input type="text" class="form-control text-center" aria-label="Last name" name="EmpNm" id="EmpNm_OrderMainForm" value="<%=rsOrder("emp_id_nm")%>" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-xl-4 mb-3">
                    <form id="CustomerForm">
						<%
							sql = ""
							sql = sql & vbCrlf & "   select  a.name,   "
							sql = sql & vbCrlf & "             a.hp,   "
							sql = sql & vbCrlf & "             a.add1,   "
							sql = sql & vbCrlf & "             a.add2   "
							sql = sql & vbCrlf & "     from  sales_customer a with(nolock)     "
							sql = sql & vbCrlf & "   where  a.cus_code = '" & rsOrder("cus_code") & "'     "
							'Response.Write sql
							Set rs = db.execute(sql)
						%>
							<div class="card">
								<h5 class="card-header">고객정보</h5>
								<div class="card-body">
									<%
										If Not rs.bof And Not rs.eof Then 
									%>
											<div class="bd-callout bd-callout-danger">
												<h5 class="font-weight-bold"><%=rs("name")%></h5>
												<a href='tel:<%=rs("hp")%>'><h6><%=rs("hp")%></h6></a>
												<h6><%=rs("add1")%></h6>
												<h6><%=rs("add2")%></h6>
											</div>
									<%
										Else
									%>
											<div class="bd-callout bd-callout-danger">
												<h5 class="font-weight-bold">&nbsp;</h5>
												<h6>&nbsp;</h6>
												<h6>&nbsp;</h6>
												<h6>&nbsp;</h6>
											</div>
									<%
										End If 
									%>
								</div>
							</div>
						<%
							rs.close
							Set rs = Nothing
						%>
                    </form>
                </div>
                <div class="col-12 col-md-6 col-xl-4 mb-3">
                    <div class="card">
                        <h5 class="card-header">인수자정보</h5>
                        <div class="card-body">
                            <div class="form-group row">
                                <label for="exampleInputEmail1" class="col-xl-3 col-form-label">인수자(필수)</label>
                                <div class="col-xl-9">
                                    <input type="text" class="form-control text-center" value="<%=rsOrder("insu_man")%>" readonly>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="exampleInputEmail1" class="col-xl-3 col-form-label">연락처(필수)</label>
                                <div class="col-xl-9">
									 <div class="input-group">
										<input type="text" class="form-control text-center" value="<%=rsOrder("insu_tel")%>" readonly>
										<div class="input-group-append">
											<a class="btn btn-outline-secondary" href="tel:<%=rsOrder("insu_tel")%>"><i class="fa fa-phone" aria-hidden="true"></i></a>
										</div>
									</div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="exampleInputEmail1" class="col-xl-3 col-form-label">설치주소(필수)</label>
                                <div class="col-xl-9">
                                    <div class="row">
                                        <div class="col-12 input-group mb-1">
                                            <input type="text" class="form-control text-center" value="<%=rsOrder("b_zipcode")%>" readonly>
                                            <input type="text" class="form-control text-center"  value="<%=rsOrder("b_add1")%>" style="width: 50% !important;" readonly>
                                        </div>
                                        <div class="col-12 mb-1">
                                            <input type="text" class="form-control text-center" value="<%=rsOrder("b_add2")%>" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 mb-3">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="mb-4">견적정보</h5>
                        </div>
                        <div class="card-body">
							<%
								'// 총금액
								TotPrice = 0
								TotPPrice = 0
								TotCostOri = 0
								TotPCostOri = 0

								sql = ""
								sql = sql & vbCrlf & "    select  a.set_opt,    "
								sql = sql & vbCrlf & "              a.rname    "
								sql = sql & vbCrlf & "      from  sales_imsi_set a with(nolock)    "
								sql = sql & vbCrlf & "    where  a.imsi_code = '" & rsOrder("imsi_code") & "'      "
								sql = sql & vbCrlf & "order by  a.idx     "
								'Response.Write sql
								Set rsSet = db.execute(sql)
								Do Until rsSet.bof Or rsSet.eof
									sql = ""
									sql = sql & vbCrlf & "       select  a.prod_code,    "
									sql = sql & vbCrlf & "                 a.prod_name,    "
									sql = sql & vbCrlf & "                 a.cost_ori,    "
									sql = sql & vbCrlf & "                 a.prod_price,    "
									sql = sql & vbCrlf & "                 a.pcost_ori,    "
									sql = sql & vbCrlf & "                 a.prod_pprice,    "
									sql = sql & vbCrlf & "                 a.prod_count,    "
									sql = sql & vbCrlf & "                 a.set_info    "
									sql = sql & vbCrlf & "        from  sales_order_item a with(nolock)    "
									sql = sql & vbCrlf & "      where  a.order_code = '" & rsOrder("order_code") & "'    "
									sql = sql & vbCrlf & "         and  a.ver = '" & rsOrder("ver") & "'    "
									sql = sql & vbCrlf & "         and  a.set_opt = '" & rsSet("set_opt") & "'    "
									sql = sql & vbCrlf & "  order by  a.code_index   "
									'Response.Write sql
									Set rsItem = Server.CreateObject("ADODB.RecordSet")
									rsItem.CursorType = 3
									rsItem.CursorLocation = 3
									rsItem.LockType = 3
									rsItem.Open sql, db

									TotSetPrice = 0
									TotSetPPrice = 0
									TotSetCostOri = 0
									TotSetPCostOri = 0
									Do Until rsItem.bof Or rsItem.eof 
										TotSetPrice = TotSetPrice + (CLng("0" & rsItem("prod_price")) * CLng("0" & rsItem("prod_count")))
										TotSetPPrice = TotSetPPrice + (CLng("0" & rsItem("prod_pprice")) * CLng("0" & rsItem("prod_count")))
										TotSetCostOri = TotSetCostOri + (CLng("0" & rsItem("cost_ori")) * CLng("0" & rsItem("prod_count")))
										TotSetPCostOri = TotSetPCostOri + (CLng("0" & rsItem("pcost_ori")) * CLng("0" & rsItem("prod_count")))
										rsItem.movenext
									Loop 

									TotPrice = TotPrice + TotSetPrice
									TotPPrice = TotPPrice + TotSetPPrice
									TotCostOri = TotCostOri + TotSetCostOri
									TotPCostOri = TotCostOri + TotSetPCostOri
							%>
									<div class="row mb-3">
										<div class="col-12">
											<div class="card">
												<div class="card-header text-white bg-info d-flex w-100 justify-content-between">
													<h6><%=rsSet("rname")%></h6>
													<h6>￦ <%=FormatNumber(TotSetPrice, 0)%></h6>
												</div>
												<div class="card-body">
													<div class="row">
														<%
															If rsItem.RecordCount > 0 Then rsItem.movefirst
															Do Until rsItem.bof Or rsItem.eof
														%>
																<div class='col-12 col-sm-6 col-lg-4 col-xl-3'>
																	<div class='bd-callout bd-callout-danger'>
																		<strong>
																			<p><%=rsItem("prod_name")%></p>
																		</strong>
																		<strong>
																			<p><%=rsItem("prod_code")%></p>
																		</strong>
																		<small>
																			<p><%=rsItem("set_info")%></p>
																		</small>
																		<small>
																			<p class='text-right'>수량 : <%=rsItem("prod_count")%></p>
																		</small>
																		<small>
																			<p class='text-right' data-bind='ProdPrice'>단가 : <%=FormatNumber(rsItem("prod_price"), 0)%></p>
																		</small>
																		<small>
																			<p class='text-right' data-bind='ProdAmount'>금액 : <%=FormatNumber(CLng("0" & rsItem("prod_price")) * CLng("0" & rsItem("prod_count")), 0)%></p>
																		</small>
																	</div>
																</div>
														<%
																rsItem.movenext 
															Loop 
														%>
													</div>
												</div>
											</div>
										</div>
									</div>
							<%
									rsItem.close
									Set rsItem = Nothing

									rsSet.movenext
								Loop 
								rsSet.close
								Set rsSet = Nothing
							%>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 mb-3">
                    <div class="card">
                        <h5 class="card-header">금액정보</h5>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-6 mb-3">
                                    <div class="bd-callout bd-callout-danger">
										<div class="d-flex w-100 mb-3">
											<h5>소비자가</h5>
										</div>
                                        <div class='d-flex w-100 justify-content-between'>
                                            <h6 class="font-weight-bold">최종금액</h6>
                                            <h6 class="font-weight-bold">￦ <%=FormatNumber(TotPrice, 0)%></h6>
                                        </div>
                                        <div class='d-flex w-100 justify-content-between'>
                                            <h6 class="font-weight-bold">할인율</h6>
                                            <%
	                                            On Error Resume Next 
                                                    TotSCostRate = Round((1 - (TotPrice / TotCostOri)) * 100, 1)
		                                            If Err.Number <> 0 Then 
			                                            TotSCostRate = 0
		                                            End If 
	                                            On Error GoTo 0
                                             %>
                                            <h6 class="font-weight-bold"><%=TotSCostRate%>%</h6>
                                        </div>
                                        <div class='d-flex w-100 justify-content-between'>
                                            <h6 class="font-weight-bold">할인금액</h6>
                                            <h6 class="font-weight-bold">￦ <%=FormatNumber(TotCostOri - TotPrice, 0)%></h6>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 mb-3">
                                    <div class="bd-callout bd-callout-danger">
										<div class="d-flex w-100 mb-3">
											<h5>공급가</h5>
										</div>
                                        <div class='d-flex w-100 justify-content-between'>
                                            <h6 class="font-weight-bold">최종금액</h6>
                                            <h6 class="font-weight-bold">￦ <%=FormatNumber(TotPPrice, 0)%></h6>
                                        </div>
                                        <div class='d-flex w-100 justify-content-between'>
                                            <h6 class="font-weight-bold">할인율</h6>
                                            <%
	                                            On Error Resume Next 
                                                    TotPCostRate = Round((1 - (TotPPrice / TotPCostOri)) * 100, 1)
		                                            If Err.Number <> 0 Then 
			                                            TotPCostRate = 0
		                                            End If 
	                                            On Error GoTo 0
                                             %>
                                            <h6 class="font-weight-bold"><%=TotPCostRate%>%</h6>
                                        </div>
                                        <div class='d-flex w-100 justify-content-between'>
                                            <h6 class="font-weight-bold">할인금액</h6>
                                            <h6 class="font-weight-bold">￦ <%=FormatNumber(TotPCostOri - TotPPrice, 0)%></h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 col-md-6 col-xl-4 mb-3">
                    <div class="card">
                        <h5 class="card-header">배송정보</h5>
                        <div class="card-body">
                            <div class="form-group row">
                                <label for="exampleInputEmail1" class="col-xl-3 col-form-label">도착요청일</label>
                                <div class="col-xl-9">
                                    <input type="text" class="form-control text-center" value="<%=rsOrder("odate")%>" readonly>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="exampleInputEmail1" class="col-xl-3 col-form-label">도착요청시간</label>
                                <div class="col-xl-9">
									<%
										oTime = ""
										Select Case rsOrder("otime")
											Case "06" : oTime = "06:00"
											Case "07" : oTime = "07:00"
											Case "08" : oTime = "08:00"
											Case "09" : oTime = "09:00"
											Case "10" : oTime = "10:00"
											Case "11" : oTime = "11:00"
											Case "12" : oTime = "12:00"
											Case "13" : oTime = "13:00"
											Case "14" : oTime = "14:00"
											Case "15" : oTime = "15:00"
											Case "16" : oTime = "16:00"
											Case "17" : oTime = "17:00"
											Case "18" : oTime = "18:00"
											Case "19" : oTime = "19:00"
											Case "20" : oTime = "20:00"
											Case "30" : oTime = "오전중"
											Case "40" : oTime = "오후중"
										End Select 
									%>
									<input type="text" class="form-control text-center" value="<%=oTime%>" readonly>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="exampleInputEmail1" class="col-xl-3 col-form-label">세대반입</label>
                                <div class="col-xl-9">
									<%
										Inhouse = ""
										Select Case rsOrder("inhouse")
											Case "X" : Inhouse = "신청안함"
											Case "O" : Inhouse = "신청"
										End Select 
									%>
									<input type="text" class="form-control text-center" value="<%=Inhouse%>" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-xl-4 mb-3">
                    <div class="card">
                        <h5 class="card-header">시공정보</h5>
                        <div class="card-body">
                            <div class="form-group row">
                                <label for="exampleInputEmail1" class="col-xl-3 col-form-label">시공요청</label>
                                <div class="col-xl-9">
									<%
										SigongType = ""
										Select Case rsOrder("sigong_type")
											Case "직시공" : SigongType = "영림시공"
											Case "자체시공" : SigongType = "자체시공"
										End Select 
									%>
									<input type="text" class="form-control text-center" value="<%=SigongType%>" readonly>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="exampleInputEmail1" class="col-xl-3 col-form-label">시공요청일</label>
                                <div class="col-xl-9">
                                    <%
                                        SDate = rsOrder("sdate")
                                        if SDate = "1900-01-01" then SDate = ""
                                    %>
                                    <input type="text" class="form-control text-center" value="<%=SDate%>" readonly>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="exampleInputEmail1" class="col-xl-3 col-form-label">시공방법</label>
                                <div class="col-xl-9">
									<%
										SigongDay = ""
										Select Case rsOrder("sigong_day")
											Case "3" : SigongDay = "덧방 (3일)"
											Case "4" : SigongDay = "부분철거 (4일)"
											Case "5" : SigongDay = "전체철거 (5일)"
											Case "6" : SigongDay = "UBR (6일)"
										End Select 
									%>
									<input type="text" class="form-control text-center" value="<%=SigongDay%>" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-xl-4 mb-3">
                    <div class="card">
                        <h5 class="card-header">출고메모</h5>
                        <div class="card-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1" class="col-form-label">출고요청</label>
                                <textarea class="form-control" name="OutMemo" id="OutMemo_OrderMainForm" rows="8" readonly><%=rsOrder("out_memo")%></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="card-footer">
			
            <div class="accordion" id="Accordion_OrderMainForm">
                <div class="card">
                    <div class="card-header" id="headingOne">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed btn-block" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                <i class="fa fa-folder-open" aria-hidden="true"></i> <span>오더 수정/확정</span>
                            </button>
                        </h5>
                    </div>
                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#Accordion_OrderMainForm">
                        <div class="card-body">
                            <form id="OrderUpdate_OrderMainForm">
				                <a class="btn btn-info" href="/page/ORD/Order.Update.asp?Oc=<%=rsOrder("order_code")%>"><i class="fa fa-wrench" aria-hidden="true"></i> <span>오더 수정</span></a>
				                <button type="submit" class="btn btn-success"><i class="fa fa-lock" aria-hidden="true"></i> <span>오더 확정</span></button>
			                </form>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed btn-block" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <i class="fa fa-download" aria-hidden="true"></i> <span>견적서 다운</span>
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#Accordion_OrderMainForm">
                        <div class="card-body">
				            <a class="btn btn-info btn-sm" href="/data/ORD/Order.Estiamte.Price.Excel.asp?Oc=<%=rsOrder("order_code")%>"><i class="fa fa-file-excel-o" aria-hidden="true"></i> <span>소비자 견적서</span></a>
				            <a class="btn btn-secondary btn-sm" href="/data/ORD/Order.Estiamte.PPrice.Excel.asp?Oc=<%=rsOrder("order_code")%>"><i class="fa fa-file-excel-o" aria-hidden="true"></i> <span>공급자 견적서</span></a>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed btn-block" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseTwo">
                                <i class="fa fa-align-justify" aria-hidden="true"></i> <span>기타</span>
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#Accordion_OrderMainForm">
                        <div class="card-body">
                            <form id="OrderDelete_OrderMainForm">
				                <button type="submit" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i> <span>오더 삭제</span></button>
			                </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>
<!-- /.container-fluid-->
<!-- /.content-wrapper-->

<%
	rsOrder.close
	Set rsOrder = Nothing
%>

<!-- #include virtual = "/share/include/ContentsAddon.asp" -->
<!-- #include virtual = "/share/include/ContentsFooter.asp" -->
<!-- #include virtual = "/share/include/JavaScript.asp" -->

<script type="text/javascript">
<!--
    $(document).ready(function () {
		
        $("#OrderUpdate_OrderMainForm").ajaxForm({
            url: "/data/ORD/Order.Lock.Proc.asp",
            type: "POST",
            dataType: "json",
            beforeSubmit: function (data, form, option) {
                //validation체크
                //막기위해서는 return false를 잡아주면됨

                return true;
            },
            beforeSend: function () {
                $(".fa").addClass("fa-spinner fa-spin");
            },
            complete: function () {
                $(".fa").removeClass("fa-spinner fa-spin");
            },
            success: function (response, status) {
				
            },
            error: function (e) {
                alert("오류 발생!");
            }
        });

	});
//-->
</script>

<!-- #include virtual = "/share/include/PageFooter.asp" -->

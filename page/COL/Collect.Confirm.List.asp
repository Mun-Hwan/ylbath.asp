<%@Language="VBScript" CODEPAGE="65001"%>
<!-- #include virtual = "/share/include/DbInfo.asp" -->
<!-- #include virtual = "/share/include/Injection.asp" -->
<!-- #include virtual = "/share/include/Function.asp" -->
<!-- #include virtual = "/share/include/Const.asp" -->
<!-- #include virtual = "/share/include/PageHeader.asp" -->
<!-- #include virtual = "/share/include/ContentsHeader.asp" -->

<%
        '// Paging 기준값
        PageSize = 10           '// 페이지당 보여줄 데이타수
        BlockSize = 5             '// 페이지 그룹 범위       1 2 3 5 6 7 8 9 10
%>

    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">여신/수금</a>
        </li>
        <li class="breadcrumb-item active">수금확정</li>
      </ol>

	  <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
			<form id="Search_Form">
                <input type="hidden" class="form-control" name="Page" value="1">
                <input type="hidden" class="form-control" name="PageSize" value="<%=PageSize%>">
				<div class="row justify-content-between">
					<div class="col-xl-3 col-sm-6 mb-3">
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text">판매처</span>
							</div>
                            <input type="text" class="form-control text-center" aria-label="First name" name="MemId" id="MemId_CollectAmountMainForm" value="">
                            <input type="text" class="form-control text-center" aria-label="Last name" name="MemNm" id="MemNm_CollectAmountMainForm" value="">
                            <div class="input-group-append">
                                <button type="button" class="btn btn-outline-secondary" id="btnCollectAmountMainMemId"><i class="fa fa-search" aria-hidden="true"></i><span></span></button>
                            </div>
						</div>
					</div>
					<div class="col-xl-3 col-sm-6 mb-3">
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text">확정여부</span>
							</div>
							<select class="form-control" name="ConfirmYn" id="Sc_ConfirmYn">
								<option></option>
								<option value="N" selected>미확정</option>
								<option value="Y">확정</option>
							</select>
						</div>
					</div>
                    <div class="col-xl-3 col-sm-6 mb-3">
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text">메모</span>
							</div>
							<input type="text" class="form-control" name="Memo" id="Sc_Memo">
						</div>
                     </div>
					<div class="col-xl-3 col-sm-6">
						<button type="submit" class="btn btn-secondary mary btn-block" onclick="$('#Search_Form').find('input[name=Page]').val(1);"><i class="fa fa-search" aria-hidden="true"></i> <span>조회</span></button>
					</div>
				</div>
			</form>
		</div>
        <div class="card-body" id="AmountList_MainAmountForm">
            <div class="list-group">
            </div>
        </div>
        <div class="card-footer" id="Paging_MainAmountForm">
		</div>
      </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->

	<!-- modal -->
    <!-- Select MemId Modal HTML -->
    <div id="selMemIdModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <form id="MemIdSearchModalForm">
                    <div class="modal-header">
                        <h4 class="modal-title"><i class="fa fa-user-circle" aria-hidden="true"></i> <span>판매처</span></h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body">
                        <div class="row" id="divSiteButton_SelMemIdModal">
                        </div>
                    </div>
                    <div style="border-top: 1px solid #e5e5e5;"></div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-12">
                                <div class="input-group">
                                    <input type="text" class="form-control" id="MemIdSearch_SelMemIdModal">
                                    <div class="input-group-append">
                                        <button type="submit" class="btn btn-outline-secondary"><i class="fa fa-search" aria-hidden="true"></i> <span>검색</span></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <div style="border-top: 1px solid #e5e5e5;"></div>
                <div class="modal-body" id="MemIdList_SelMemIdModal">
                    <div class="list-group">
                    </div>
                </div>
            </div>
        </div>
    </div>

	<!-- Update Modal HTML -->
	<div id="UpdateAmountModal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<form id="updateModalForm">
                    <input type="hidden" class="form-control" name="Idx" id="Idx_UpdateAmountModal">
                    <input type="hidden" class="form-control" name="ConfirmYn" id="ConfirmYn_UpdateAmountModal">
					<div class="modal-header">						
						<h4 class="modal-title"><i class="fa fa-check-circle" aria-hidden="true"></i> <span>수금확정</span></h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">
                        <div class="row" style="height: 100px;">
                            <div class="col-6 d-flex align-items-stretch justify-content-between">
                                <button type="button" id="btnConfirm_UpdateAmountModal" class="btn btn-success btn-block"><i class="fa fa-check" aria-hidden="true"></i> <span>확정</span></button>
                            </div>
                            <div class="col-6 d-flex align-items-stretch justify-content-between">
                                <button type="button" id="btnCancel_UpdateAmountModal" class="btn btn-secondary btn-block"><i class="fa fa-undo" aria-hidden="true"></i> <span>취소</span></button>
                            </div>
                        </div>
					</div>
				</form>
			</div>
		</div>
	</div>
    <!-- /.modal -->

<!-- #include virtual = "/share/include/ContentsAddon.asp" -->
<!-- #include virtual = "/share/include/ContentsFooter.asp" -->
<!-- #include virtual = "/share/include/JavaScript.asp" -->

<script type="text/javascript">
<!--
	$(document).ready(function() {

		$("#Search_Form").ajaxForm({
			url: "/data/COL/Collect.Confirm.List.Json.asp",
            data: {},
            type: "POST",
			dataType: "json",
			beforeSend:function(){
                $(".fa").addClass("fa-spinner fa-spin");
			},
			complete:function(){
				$(".fa").removeClass("fa-spinner fa-spin");
			},
            beforeSubmit: function (data,form,option) {
                //validation체크 
                //막기위해서는 return false를 잡아주면됨
                return true;
            },
            success: function(data, status){
                var info = "";
                var maxCnt = 0;

                $.each(data, function () {
                    var stat = "";
                    if (this.confirm_yn == "Y")
                        stat = " list-group-item-success";

                    maxCnt = this.max_cnt;
                    info += "  <a href='#UpdateAmountModal' data-toggle='modal' class='list-group-item list-group-item-action" + stat + "' AmountInfo='" + JSON.stringify(this) + "'>";
                    info += "    <div class='row mb-3 justify-content-between'>";
                    info += "       <div class='col-6 col-xl-2'>";
                    info += "           <small class='text-muted'>판매처</small>";
                    info += "           <p class='mb-0'>" + this.mem_nm + "</p>";
                    info += "       </div>";
                    info += "       <div class='col-6 col-xl-2'>";
                    info += "           <small class='text-muted'>수금일</small>";
                    info += "           <p class='mb-0'>" + this.yes_date + "</p>";
                    info += "       </div>";
                    info += "       <div class='col-6 col-xl-2'>";
                    info += "           <small class='text-muted'>수금액</small>";
                    info += "           <p class='mb-0'>" + addCommas(this.yes_cost) + "</p>";
                    info += "       </div>";
                    info += "       <div class='col-6 col-xl-2'>";
                    info += "           <small class='text-muted'>등록일</small>";
                    info += "           <p class='mb-0'>" + this.wdate + "</p>";
                    info += "       </div>";
                    info += "       <div class='col-6 col-xl-2'>";
                    info += "           <small class='text-muted'>등록자</small>";
                    info += "           <p class='mb-0'>" + this.insert_nm + "</p>";
                    info += "       </div>";
                    info += "    </div>";
                    info += "    <div class='row mb-3 justify-content-between'>";
                    info += "       <div class='col-12 col-xl-4'>";
                    info += "           <small class='text-muted'>비고</small>";
                    info += "           <p class='mb-0'>" + this.memo + "</p>";
                    info += "       </div>";
                    info += "    </div>";
                    info += "  </a>";
                });

                $("#AmountList_MainAmountForm").find(".list-group").html(info);
                var NowPage = $("#Search_Form").find("input[name=Page]").val();
                var page_viewList = Paging(maxCnt, '<%=PageSize%>', '<%=BlockSize%>', NowPage);
                $("#Paging_MainAmountForm").html(page_viewList);
            },
            error: function(){
				alert("오류 발생!");
            }                               
        });

        $("#MemIdSearchModalForm").ajaxForm({
            url: "/data/ORD/Order.MemId.Json.asp",
            type: "POST",
            data: {
                MemInfo: function () { return $("#MemIdSearch_SelMemIdModal").val() }
            },
            dataType: "json",
            beforeSend: function () {
                $(".fa").addClass("fa-spinner fa-spin");
            },
            complete: function () {
                $(".fa").removeClass("fa-spinner fa-spin");
            },
            beforeSubmit: function (data, form, option) {
                //validation체크
                //막기위해서는 return false를 잡아주면됨
                
                return true;
            },
            success: function (response, status) {
                var info = "";
                $.each(response, function (i) {
                    info += "<div class='bd-callout bd-callout-danger' name='AmountList_selMemIdModel' id='" + this.id + "' site='" + this.site + "'>"
                    info += "	<h5 class='font-weight-bold'>" + this.name + "</h5>"
                    info += "	<h6>" + this.hp + "</h6>"
                    info += "	<h6>" + this.add1 + " " + this.add2 + "</h6>"
                    info += "</div>"
                });

                var body = $("#selMemIdModal").find("#MemIdList_SelMemIdModal").find(".list-group");
                body.html(info);
            },
            error: function (e) {
                alert("오류 발생!");
            }
        });

		$("#updateModalForm").ajaxForm({
			url: "/data/COL/Collect.Confirm.Update.Proc.asp",
			type: "POST",
			dataType: "text",
			beforeSend:function(){
				$(".fa").addClass("fa-spinner fa-spin");
			},
			complete:function(){
				$(".fa").removeClass("fa-spinner fa-spin");
			},
            beforeSubmit: function (data,form,option) {
                //validation체크 
                //막기위해서는 return false를 잡아주면됨
                return true;
            },
            success: function(data, status){
                //성공후 서버에서 받은 데이터 처리
                var Datas = data.split("|");
                if (Datas[0] == "OK") {
                    alert("처리 되었습니다.");
                    $("#Search_Form").submit();
                    $("#UpdateAmountModal").modal('hide');
                }
                else {
                    alert(Datas[1]);
                    return;
                }
            },
            error: function(){
				alert("오류 발생!");
            }                               
        });

		$(document).on("click", "nav  a[name=pageGoto]", function (e) {
            var page = $(this).attr("target-page");
            $("#Search_Form").find("input[name=Page]").val(page);
            $("#Search_Form").submit();
        });

        $(document).on("click", "#btnCollectAmountMainMemId", function (e) {
            $("#selMemIdModal").modal("show");
        });

        $(document).on("click", "#btnConfirm_UpdateAmountModal", function (e) {
            $("#UpdateAmountModal").find("#ConfirmYn_UpdateAmountModal").val("Y");
            $("#updateModalForm").submit();
        });

        $(document).on("click", "#btnCancel_UpdateAmountModal", function (e) {
            $("#UpdateAmountModal").find("#ConfirmYn_UpdateAmountModal").val("N");
            $("#updateModalForm").submit();
        });

        $(document).on("mouseover", "div[name=AmountList_selMemIdModel]", function (e) {
            $(this).css("background-color", "#cafff2").css("cursor", "pointer");
        }).on("mouseout", "div[name=AmountList_selMemIdModel]", function (e) {
            $(this).css("background-color", "").css("cursor", "");
        }).on("click", "div[name=AmountList_selMemIdModel]", function (e) {
            var id = $(this).attr("id");
            var name = $(this).find("h5.font-weight-bold").text();
            $("#MemId_CollectAmountMainForm").val(id);
            $("#MemNm_CollectAmountMainForm").val(name);
            $("#selMemIdModal").modal('hide');
        });

        $(document).on("show.bs.modal", "#selMemIdModal", function (e) {
            $.ajax({
                type: "POST",
                url: "/data/ORD/Order.Site.Json.asp",
                dataType: 'json',
                cache: false,
                async: false,
                beforeSend: function () {
                    $(".fa").addClass("fa-spinner fa-spin");
                },
                complete: function () {
                    $(".fa").removeClass("fa-spinner fa-spin");
                },
                success: function (data) {
                    var info = "";
                    info += "<div class='col-12 text-center'>";
                    info += "	<div class='btn-group-toggle' data-toggle='buttons'>";
                    $.each(data, function (i) {
                        if (this.site == '<%=request.cookies("msite")%>') {
                            info += "	    <label class='btn btn-secondary active mb-1'>";
                            info += "	        <input type='radio' name='Site' autocomplete='off' value='" + this.site + "' checked>" + this.sname;
                            info += "	    </label>";
                        } else {
                            info += "	    <label class='btn btn-secondary mb-1'>";
                            info += "	        <input type='radio' name='Site' autocomplete='off' value='" + this.site + "'>" + this.sname;
                            info += "	    </label>";
                        }
                    });
                    info += "	</div>";
                    info += "</div>";

                    $("#divSiteButton_SelMemIdModal").html(info);
                },
                error: function (ex) {
                    alert("사이트 조회 중 오류 발생\n" + ex.responseText);
                }
            });
        }).on("shown.bs.modal", "#selMemIdModal", function (e) {
            $('#MemIdSearch_SelMemIdModal').trigger('focus');
        }).on("hidden.bs.modal", "#selMemIdModal", function (e) {
            var body = $("#selMemIdModal").find("#MemIdList_SelMemIdModal").find(".list-group");
            body.html("");
        });

        $(document).on("show.bs.modal", "#UpdateAmountModal", function (e) {
            if ($(e.relatedTarget).attr("AmountInfo")) {
                var Item = JSON.parse($(e.relatedTarget).attr("AmountInfo"));
                $("#UpdateAmountModal").find("#Idx_UpdateAmountModal").val(Item.idx);
            }
		}).on("hidden.bs.modal", "#UpdateAmountModal", function (e) {
			$("#addModalForm")[0].reset();
		});

	});

//-->
</script>

<!-- #include virtual = "/share/include/PageFooter.asp" -->

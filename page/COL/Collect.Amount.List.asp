<%@Language="VBScript" CODEPAGE="65001"%>
<!-- #include virtual = "/share/include/DbInfo.asp" -->
<!-- #include virtual = "/share/include/Injection.asp" -->
<!-- #include virtual = "/share/include/Function.asp" -->
<!-- #include virtual = "/share/include/Const.asp" -->
<!-- #include virtual = "/share/include/PageHeader.asp" -->
<!-- #include virtual = "/share/include/ContentsHeader.asp" -->

<%
        '// Paging 기준값
        PageSize = 10           '// 페이지당 보여줄 데이타수
        BlockSize = 5             '// 페이지 그룹 범위       1 2 3 5 6 7 8 9 10

        '// 판매처 기본 셋팅
        If request.cookies("msite") <> "younglim" Then
            MemId = request.cookies("id")
            MemNm = request.cookies("name")
        End If
%>

    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">여신/수금</a>
        </li>
        <li class="breadcrumb-item active">수금등록</li>
      </ol>

	  <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
			<form id="Search_Form">
                <input type="hidden" class="form-control" name="Page" value="1">
                <input type="hidden" class="form-control" name="PageSize" value="<%=PageSize%>">
				<div class="row justify-content-between">
					<div class="col-xl-3 col-sm-6 mb-3">
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text">판매처</span>
							</div>
                            <input type="text" class="form-control text-center" aria-label="First name" name="MemId" id="MemId_CollectAmountMainForm" value="<%=MemId%>" readonly>
                            <input type="text" class="form-control text-center" aria-label="Last name" name="MemNm" id="MemNm_CollectAmountMainForm" value="<%=MemNm%>" readonly>
                            <% If request.cookies("msite") = "younglim" Then %>
                                <div class="input-group-append">
                                    <button type="button" class="btn btn-outline-secondary" id="btnCollectAmountMainMemId"><i class="fa fa-search" aria-hidden="true"></i><span></span></button>
                                </div>
                            <% End If %>
						</div>
					</div>
					<div class="col-xl-3 col-sm-6">
						<button type="submit" class="btn btn-secondary mary btn-block" onclick="$('#Search_Form').find('input[name=Page]').val(1);"><i class="fa fa-search" aria-hidden="true"></i> <span>조회</span></button>
					</div>
				</div>
			</form>
		</div>
        <div class="card-body" id="AmountList_MainAmountForm">
            <div class="list-group">
            </div>
        </div>
        <div class="card-footer" id="Paging_MainAmountForm">
		</div>
        <div class="card-footer" id="Button_MainAmountForm">
			<button class="btn btn-primary" id="btnAddAmount_MainAmountForm"><i class="fa fa-plus" aria-hidden="true"></i> <span>추가</span></button>
		</div>
      </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->

	<!-- modal -->
	<!-- Add Modal HTML -->
	<div id="addAmountModal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<form id="addModalForm">
					<input type="hidden" name="idx" id="Idx_AddModal">
					<div class="modal-header">
						<h4 class="modal-title">수금정보 등록</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-12">
								<div class="form-group">
									<label>수금일</label>
									<input type="text" class="form-control" name="yes_date" id="YesDate_AddModal" value="<%=Date()%>" required>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-12">
								<div class="form-group">
									<label>수금액</label>
									<input type="number" class="form-control" name="yes_cost" id="YesCost_AddModal" required>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-12">
								<div class="form-group">
									<label>비고</label>
									<input type="text" class="form-control" name="memo" id="Memo_AddModal" required>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="submit" class="btn btn-success"><i class="fa fa-check" aria-hidden="true"></i> <span>저장</span></button>
						<button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-undo" aria-hidden="true"></i> <span>취소</span></button>
						<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#delAmountModal" id="btnDel_UpdateModal"><i class="fa fa-times" aria-hidden="true"></i> <span>삭제</span></button>
					</div>
				</form>
			</div>
		</div>
	</div>

    <!-- Select MemId Modal HTML -->
    <div id="selMemIdModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <form id="MemIdSearchModalForm">
                    <div class="modal-header">
                        <h4 class="modal-title"><i class="fa fa-user-circle" aria-hidden="true"></i> <span>판매처</span></h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body">
                        <div class="row" id="divSiteButton_SelMemIdModal">
                        </div>
                    </div>
                    <div style="border-top: 1px solid #e5e5e5;"></div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-12">
                                <div class="input-group">
                                    <input type="text" class="form-control" id="MemIdSearch_SelMemIdModal">
                                    <div class="input-group-append">
                                        <button type="submit" class="btn btn-outline-secondary"><i class="fa fa-search" aria-hidden="true"></i> <span>검색</span></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <div style="border-top: 1px solid #e5e5e5;"></div>
                <div class="modal-body" id="MemIdList_SelMemIdModal">
                    <div class="list-group">
                    </div>
                </div>
            </div>
        </div>
    </div>

	<!-- Delete Modal HTML -->
	<div id="delAmountModal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<form id="delModalForm">
					<div class="modal-header">						
						<h4 class="modal-title"><i class="fa fa-trash" aria-hidden="true"></i> <span>삭제</span></h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">					
						<p>삭제 하시겠습니까?</p>
						<p class="text-warning"><small>삭제시 복구 불가능 합니다.</small></p>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-undo" aria-hidden="true"></i> <span>취소</span></button>
						<button type="submit" class="btn btn-danger"><i class="fa fa-times" aria-hidden="true"></i> <span>삭제</span></button>
					</div>
				</form>
			</div>
		</div>
	</div>
    <!-- /.modal -->

<!-- #include virtual = "/share/include/ContentsAddon.asp" -->
<!-- #include virtual = "/share/include/ContentsFooter.asp" -->
<!-- #include virtual = "/share/include/JavaScript.asp" -->

<script type="text/javascript">
<!--
	$(document).ready(function() {

        $('#YesDate_AddModal').datepicker({
			format: "yyyy-mm-dd",
			todayBtn: "linked",
			language: "kr",
			multidate: false,
			//daysOfWeekDisabled: "0",
			daysOfWeekHighlighted: "0,6",
			autoclose: true,
			todayHighlight: true,
			toggleActive: true
        }).on("hide", function(e) {
			$('.modal-backdrop').css({ 'z-index': 1041 } );
		});;

		$("#Search_Form").ajaxForm({
			url: "/data/COL/Collect.Amount.List.Json.asp",
            data: {},
            type: "POST",
			dataType: "json",
			beforeSend:function(){
                $(".fa").addClass("fa-spinner fa-spin");
			},
			complete:function(){
				$(".fa").removeClass("fa-spinner fa-spin");
			},
            beforeSubmit: function (data,form,option) {
                //validation체크 
                //막기위해서는 return false를 잡아주면됨
                return true;
            },
            success: function(data, status){
                var info = "";
                var maxCnt = 0;

                $.each(data, function () {
                    var stat = "";
                    if (this.confirm_yn == "Y")
                        stat = " list-group-item-success";

                    maxCnt = this.max_cnt;
                    info += "  <a href='#addAmountModal' data-toggle='modal' class='list-group-item list-group-item-action" + stat + "' AmountInfo='" + JSON.stringify(this) + "'>";
                    info += "    <div class='row mb-3'>";
                    info += "       <div class='col-6 col-xl-2'>";
                    info += "           <small class='text-muted'>수금일</small>";
                    info += "           <p class='mb-0'>" + this.yes_date + "</p>";
                    info += "       </div>";
                    info += "       <div class='col-6 col-xl-2'>";
                    info += "           <small class='text-muted'>수금액</small>";
                    info += "           <p class='mb-0'>" + addCommas(this.yes_cost) + "</p>";
                    info += "       </div>";
                    info += "       <div class='col-12 col-xl-4'>";
                    info += "           <small class='text-muted'>비고</small>";
                    info += "           <p class='mb-0'>" + this.memo + "</p>";
                    info += "       </div>";
                    info += "       <div class='col-6 col-xl-2'>";
                    info += "           <small class='text-muted'>등록일</small>";
                    info += "           <p class='mb-0'>" + this.wdate + "</p>";
                    info += "       </div>";
                    info += "       <div class='col-6 col-xl-2'>";
                    info += "           <small class='text-muted'>등록자</small>";
                    info += "           <p class='mb-0'>" + this.insert_nm + "</p>";
                    info += "       </div>";
                    info += "    </div>";
                    info += "  </a>";
                });

                $("#AmountList_MainAmountForm").find(".list-group").html(info);
                var NowPage = $("#Search_Form").find("input[name=Page]").val();
                var page_viewList = Paging(maxCnt, '<%=PageSize%>', '<%=BlockSize%>', NowPage);
                $("#Paging_MainAmountForm").html(page_viewList);
            },
            error: function(){
				alert("오류 발생!");
            }                               
        });

		$("#addModalForm").ajaxForm({
			url: "/data/COL/Collect.Amount.Update.Proc.asp",
            type: "POST",
            data: {
                mem_id: function () { return $("#MemId_CollectAmountMainForm").val() }
            },
			dataType: "text",
			beforeSend:function(){
				$(".fa").addClass("fa-spinner fa-spin");
			},
			complete:function(){
				$(".fa").removeClass("fa-spinner fa-spin");
			},
            beforeSubmit: function (data,form,option) {
                //validation체크 
                //막기위해서는 return false를 잡아주면됨
                return true;
            },
            success: function(response, status){
                //성공후 서버에서 받은 데이터 처리
				if (response == "OK")
				{
					alert("처리 되었습니다.");
                    $("#Search_Form").submit();
					$("#addAmountModal").modal('hide');
				}
            },
            error: function(){
				alert("오류 발생!");
            }                               
        });

        $("#MemIdSearchModalForm").ajaxForm({
            url: "/data/ORD/Order.MemId.Json.asp",
            type: "POST",
            data: {
                MemInfo: function () { return $("#MemIdSearch_SelMemIdModal").val() }
            },
            dataType: "json",
            beforeSend: function () {
                $(".fa").addClass("fa-spinner fa-spin");
            },
            complete: function () {
                $(".fa").removeClass("fa-spinner fa-spin");
            },
            beforeSubmit: function (data, form, option) {
                //validation체크
                //막기위해서는 return false를 잡아주면됨
                
                return true;
            },
            success: function (response, status) {
                var info = "";
                $.each(response, function (i) {
                    info += "<div class='bd-callout bd-callout-danger' name='AmountList_selMemIdModel' id='" + this.id + "' site='" + this.site + "'>"
                    info += "	<h5 class='font-weight-bold'>" + this.name + "</h5>"
                    info += "	<h6>" + this.hp + "</h6>"
                    info += "	<h6>" + this.add1 + " " + this.add2 + "</h6>"
                    info += "</div>"
                });

                var body = $("#selMemIdModal").find("#MemIdList_SelMemIdModal").find(".list-group");
                body.html(info);
            },
            error: function (e) {
                alert("오류 발생!");
            }
        });

		$("#delModalForm").ajaxForm({
			url: "/data/COL/Collect.Amount.Delete.Proc.asp",
			type: "POST",
			data: {
				idx: function() { return $("#Idx_AddModal").val() }
			},
			dataType: "text",
			beforeSend:function(){
				$(".fa").addClass("fa-spinner fa-spin");
			},
			complete:function(){
				$(".fa").removeClass("fa-spinner fa-spin");
			},
            beforeSubmit: function (data,form,option) {
                //validation체크 
                //막기위해서는 return false를 잡아주면됨
                return true;
            },
            success: function(response, status){
                //성공후 서버에서 받은 데이터 처리
				if (response == "OK")
				{
					alert("삭제 되었습니다.");
                    $("#Search_Form").submit();
					$("#delAmountModal").modal('hide');
					$("#addAmountModal").modal('hide');
				}
            },
            error: function(){
				alert("오류 발생!");
            }                               
        });

		$(document).on("click", "nav  a[name=pageGoto]", function (e) {
            var page = $(this).attr("target-page");
            $("#Search_Form").find("input[name=Page]").val(page);
            $("#Search_Form").submit();
        });

        $(document).on("click", "#btnCollectAmountMainMemId", function (e) {
            $("#selMemIdModal").modal("show");
        });

        $(document).on("click", "#btnAddAmount_MainAmountForm", function (e) {
            if ($("#MemId_CollectAmountMainForm").val() == "") {
                alert("판매처를 선택하세요.");
                $("#selMemIdModal").modal("show");
            } else {
                $("#addAmountModal").modal("show");
            }
        });

        $(document).on("mouseover", "div[name=AmountList_selMemIdModel]", function (e) {
            $(this).css("background-color", "#cafff2").css("cursor", "pointer");
        }).on("mouseout", "div[name=AmountList_selMemIdModel]", function (e) {
            $(this).css("background-color", "").css("cursor", "");
        }).on("click", "div[name=AmountList_selMemIdModel]", function (e) {
            var id = $(this).attr("id");
            var name = $(this).find("h5.font-weight-bold").text();
            $("#MemId_CollectAmountMainForm").val(id);
            $("#MemNm_CollectAmountMainForm").val(name);
            $("#selMemIdModal").modal('hide');
        });

        $(document).on("show.bs.modal", "#addAmountModal", function (e) {
            if ($(e.relatedTarget).attr("AmountInfo")) {
                var Item = JSON.parse($(e.relatedTarget).attr("AmountInfo"));
                if (Item.confirm_yn == 'Y') {
                    $("#addAmountModal").find("button:submit").hide();
                    $("#addAmountModal").find("#btnDel_UpdateModal").hide();
                    $("#addAmountModal").find("input").prop("disabled", true);
                }

                $("#addAmountModal").find("#Idx_AddModal").val(Item.idx);
                $("#addAmountModal").find("#YesDate_AddModal").val(Item.yes_date);
                $("#addAmountModal").find("#YesCost_AddModal").val(Item.yes_cost);
                $("#addAmountModal").find("#Memo_AddModal").val(Item.memo);
            }
		}).on("hidden.bs.modal", "#addAmountModal", function (e) {
			$("#addModalForm")[0].reset();
			$("#YesDate_AddModal").val("<%=Date()%>");
            $("#Idx_AddModal").val("");
            $("#addAmountModal").find("button:submit").show();
            $("#addAmountModal").find("#btnDel_UpdateModal").show();
            $("#addAmountModal").find("input").prop("disabled", false);
		});

        $(document).on("show.bs.modal", "#selMemIdModal", function (e) {
            $.ajax({
                type: "POST",
                url: "/data/ORD/Order.Site.Json.asp",
                dataType: 'json',
                cache: false,
                async: false,
                beforeSend: function () {
                    $(".fa").addClass("fa-spinner fa-spin");
                },
                complete: function () {
                    $(".fa").removeClass("fa-spinner fa-spin");
                },
                success: function (data) {
                    var info = "";
                    info += "<div class='col-12 text-center'>";
                    info += "	<div class='btn-group-toggle' data-toggle='buttons'>";
                    $.each(data, function (i) {
                        if (this.site == '<%=request.cookies("msite")%>') {
                            info += "	    <label class='btn btn-secondary active mb-1'>";
                            info += "	        <input type='radio' name='Site' autocomplete='off' value='" + this.site + "' checked>" + this.sname;
                            info += "	    </label>";
                        } else {
                            info += "	    <label class='btn btn-secondary mb-1'>";
                            info += "	        <input type='radio' name='Site' autocomplete='off' value='" + this.site + "'>" + this.sname;
                            info += "	    </label>";
                        }
                    });
                    info += "	</div>";
                    info += "</div>";

                    $("#divSiteButton_SelMemIdModal").html(info);
                },
                error: function (ex) {
                    alert("사이트 조회 중 오류 발생\n" + ex.responseText);
                }
            });
        }).on("shown.bs.modal", "#selMemIdModal", function (e) {
            $('#MemIdSearch_SelMemIdModal').trigger('focus');
        }).on("hidden.bs.modal", "#selMemIdModal", function (e) {
            var body = $("#selMemIdModal").find("#MemIdList_SelMemIdModal").find(".list-group");
            body.html("");
        });

	});

//-->
</script>

<!-- #include virtual = "/share/include/PageFooter.asp" -->

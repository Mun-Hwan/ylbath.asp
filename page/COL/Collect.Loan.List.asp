<%@Language="VBScript" CODEPAGE="65001"%>
<!-- #include virtual = "/share/include/DbInfo.asp" -->
<!-- #include virtual = "/share/include/Injection.asp" -->
<!-- #include virtual = "/share/include/Function.asp" -->
<!-- #include virtual = "/share/include/Const.asp" -->
<!-- #include virtual = "/share/include/PageHeader.asp" -->
<!-- #include virtual = "/share/include/ContentsHeader.asp" -->

<%
        '// Paging 기준값
        PageSize = 10           '// 페이지당 보여줄 데이타수
        BlockSize = 5             '// 페이지 그룹 범위       1 2 3 5 6 7 8 9 10
%>

    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">여신/수금</a>
        </li>
        <li class="breadcrumb-item active">여신관리</li>
      </ol>

	  <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
			<form id="Search_Form">
                <input type="hidden" class="form-control" name="Page" value="1">
                <input type="hidden" class="form-control" name="PageSize" value="<%=PageSize%>">
				<div class="row justify-content-between">
					<div class="col-xl-3 col-sm-6 mb-3">
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text">판매처 정보</span>
							</div>
							<input type="text" class="form-control" name="LoanInfo">
						</div>
					</div>
					<div class="col-xl-3 col-sm-6">
						<button type="submit" class="btn btn-secondary mary btn-block" onclick="$('#Search_Form').find('input[name=Page]').val(1);"><i class="fa fa-search" aria-hidden="true"></i> <span>조회</span></button>
					</div>
				</div>
			</form>
		</div>
        <div class="card-body" id="MemList_MainLoanForm">
            <div class="list-group">
            </div>
        </div>
        <div class="card-footer" id="Paging_MainLoanForm">
		</div>
      </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->

	<!-- modal -->
	<!-- Update Modal HTML -->
	<div id="updateLoanModal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<form id="updateModalForm">
					<input type="hidden" name="mem_id" id="MemId_UpdateModal">
					<div class="modal-header">
						<h4 class="modal-title">여신정보 수정</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-12">
								<div class="form-group">
									<label>여신한도</label>
									<input type="number" class="form-control" name="full_ys" id="FullYs_UpdateModal" value="" required>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="submit" class="btn btn-success"><i class="fa fa-check" aria-hidden="true"></i> <span>저장</span></button>
						<button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-undo" aria-hidden="true"></i> <span>취소</span></button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- /.modal -->

<!-- #include virtual = "/share/include/ContentsAddon.asp" -->
<!-- #include virtual = "/share/include/ContentsFooter.asp" -->
<!-- #include virtual = "/share/include/JavaScript.asp" -->

<script type="text/javascript">
<!--
	$(document).ready(function() {

		$("#Search_Form").ajaxForm({
			url: "/data/COL/Collect.Loan.List.Json.asp",
            data: {},
            type: "POST",
			dataType: "json",
			beforeSend:function(){
                $(".fa").addClass("fa-spinner fa-spin");
			},
			complete:function(){
				$(".fa").removeClass("fa-spinner fa-spin");
			},
            beforeSubmit: function (data,form,option) {
                //validation체크 
                //막기위해서는 return false를 잡아주면됨
                return true;
            },
            success: function(data, status){
                var info = "";
                var maxCnt = 0;
                $.each(data, function () {
                    maxCnt = this.max_cnt;
                    info += "  <a href='#updateLoanModal' data-toggle='modal' class='list-group-item list-group-item-action' LoanInfo='" + JSON.stringify(this) + "'>";
                    info += "    <div class='row mb-3'>";
                    info += "       <div class='col-6 col-xl-2'>";
                    info += "           <small class='text-muted'>판매처명</small>";
                    info += "           <p class='mb-0'>" + this.name + "</p>";
                    info += "       </div>";
                    info += "       <div class='col-6 col-xl-2'>";
                    info += "           <small class='text-muted'>연락처</small>";
                    info += "           <p class='mb-0'>" + this.hp + "</p>";
                    info += "       </div>";
                    info += "       <div class='col-6 col-xl-2'>";
                    info += "           <small class='text-muted'>우편번호</small>";
                    info += "           <p class='mb-0'>" + this.zipcode + "</p>";
                    info += "       </div>";
                    info += "       <div class='col-6 col-xl-2'>";
                    info += "           <small class='text-muted'>기본주소</small>";
                    info += "           <p class='mb-0'>" + this.add1 + "</p>";
                    info += "       </div>";
                    info += "       <div class='col-12 col-xl-4'>";
                    info += "           <small class='text-muted'>상세주소</small>";
                    info += "           <p class='mb-0'>" + this.add2 + "</p>";
                    info += "       </div>";
                    info += "    </div>";
                    info += "    <div class='row'>";
                    info += "       <div class='col-6 col-xl-2'>";
                    info += "           <small class='text-muted'>아이디</small>";
                    info += "           <p class='mb-0'>" + this.id + "</p>";
                    info += "       </div>";
                    info += "       <div class='col-6 col-xl-2'>";
                    info += "           <small class='text-muted'></small>";
                    info += "           <p class='mb-0'></p>";
                    info += "       </div>";
                    info += "       <div class='col-6 col-xl-2'>";
                    info += "           <small class='text-muted'><strong>여신한도</strong></small>";
                    info += "           <p class='mb-0'><strong>" + addCommas(this.full_ys) + "</strong></p>";
                    info += "       </div>";
                    info += "       <div class='col-6 col-xl-2'>";
                    info += "           <small class='text-muted'><strong>사용여신</strong></small>";
                    info += "           <p class='mb-0'><strong>" + addCommas(this.use_ys) + "</strong></p>";
                    info += "       </div>";
                    info += "       <div class='col-6 col-xl-2'>";
                    info += "           <small class='text-muted'><strong>잔여여신</strong></small>";
                    info += "           <p class='mb-0'><strong>" + addCommas(this.ok_ys) + "</strong></p>";
                    info += "       </div>";
                    info += "    </div>";
                    info += "  </a>";
                });

                $("#MemList_MainLoanForm").find(".list-group").html(info);
                var NowPage = $("#Search_Form").find("input[name=Page]").val();
                var page_viewList = Paging(maxCnt, '<%=PageSize%>', '<%=BlockSize%>', NowPage);
                $("#Paging_MainLoanForm").html(page_viewList);
            },
            error: function(){
				alert("오류 발생!");
            }                               
        });

		$("#updateModalForm").ajaxForm({
			url: "/data/COL/Collect.Loan.Update.Proc.asp",
			type: "POST",
			dataType: "text",
			beforeSend:function(){
				$(".fa").addClass("fa-spinner fa-spin");
			},
			complete:function(){
				$(".fa").removeClass("fa-spinner fa-spin");
			},
            beforeSubmit: function (data,form,option) {
                //validation체크 
                //막기위해서는 return false를 잡아주면됨
                return true;
            },
            success: function(response, status){
                //성공후 서버에서 받은 데이터 처리
				if (response == "OK")
				{
					alert("저장 되었습니다.");
                    $("#Search_Form").submit();
					$("#updateLoanModal").modal('hide');
				}
            },
            error: function(){
				alert("오류 발생!");
            }                               
        });

		$(document).on("click", "nav  a[name=pageGoto]", function (e) {
            var page = $(this).attr("target-page");
            $("#Search_Form").find("input[name=Page]").val(page);
            $("#Search_Form").submit();
        });

        $(document).on("show.bs.modal", "#updateLoanModal", function (e) {
            var Item = JSON.parse($(e.relatedTarget).attr("LoanInfo"));

            if (Item.mem_id == "")
			{
                alert("잘못된 접근입니다.");
                return;
			}
			else 
            {
                $("#updateLoanModal").find("#MemId_UpdateModal").val(Item.id);
                $("#updateLoanModal").find("#FullYs_UpdateModal").val(Item.full_ys);
			}
		}).on("hidden.bs.modal", "#updateLoanModal", function (e) {
			$("#updateModalForm")[0].reset();
		});

	});

//-->
</script>

<!-- #include virtual = "/share/include/PageFooter.asp" -->

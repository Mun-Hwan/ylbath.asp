<%@Language="VBScript" CODEPAGE="65001"%>
<!-- #include virtual = "/share/include/DbInfo.asp" -->
<!-- #include virtual = "/share/include/Injection.asp" -->
<!-- #include virtual = "/share/include/Function.asp" -->
<!-- #include virtual = "/share/include/Const.asp" -->
<!-- #include virtual = "/share/include/PageHeader.asp" -->
<!-- #include virtual = "/share/include/ContentsHeader.asp" -->

    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">공지사항</a>
        </li>
        <li class="breadcrumb-item active">주간업무</li>
      </ol>

	  <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
			<form>
				<div class="row">
					<div class="col-xl-3 col-sm-6 mb-3">
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text">요청일</span>
							</div>
							<input type="text" class="form-control text-center" id="startDate" value="<%=Date()%>" required>
							<input type="text" class="form-control text-center" id="endDate" value="<%=Date()%>" required>
						</div>
					</div>
					<div class="col-xl-3 col-sm-6 mb-3">
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text">담당자</span>
							</div>
							<select class="form-control" id="Sc_MemId" name="MemId">
								<option></option>
								<option value="심현아">심현아</option>
								<option value="조재선">조재선</option>
								<option value="박은희">박은희</option>
								<option value="조현나">조현나</option>
								<option value="차문환">차문환</option>
							</select>
						</div>
					</div>
					<div class="col-xl-3 col-sm-6 mb-3 d-none d-sm-block">
					</div>
					<div class="col-xl-3 col-sm-6 mb-3">
						<button type="button" id="btnRetrieve" class="btn btn-secondary mary btn-block"><i class="fa fa-search" aria-hidden="true"></i> <span>조회</span></button>
					</div>
				</div>
			</form>
		</div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered display responsive" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th class="text-center all">NO</th>
                  <th class="text-center all">담당자</th>
                  <th class="text-center all">방문<br>일자</th>
                  <th class="text-center all">방문<br>시간</th>
                  <th class="text-center not-desktop">현장<br>정보</th>
                  <th class="text-center desktop">방문<br>여부</th>
                  <th class="text-center tablet-l desktop">대리점명</th>
                  <th class="text-center tablet-l desktop">예상품목</th>
                  <th class="text-center desktop">기본주소</th>
                  <th class="text-center desktop">상세주소</th>
                  <th class="text-center none">비고</th>
                  <th class="text-center never">IDX</th>
                </tr>
              </thead>
              <tfoot></tfoot>
            </table>
          </div>
        </div>
        <div class="card-footer">
			<a href="#addNoticeModal" class="btn btn-primary" data-toggle="modal"><i class="fa fa-plus" aria-hidden="true"></i> <span>추가</span></a>
		</div>
      </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->

	<!-- modal -->
	<!-- Add Modal HTML -->
	<div id="addNoticeModal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<form id="addModalForm">
					<input type="hidden" name="idx" id="idx_AddModal">
					<div class="modal-header">
						<h4 class="modal-title">공지글 작성</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-6">
								<div class="form-group">
									<label>방문일자</label>
									<input type="text" class="form-control" name="BusiDate" id="BusiDate_AddModal" value="<%=Date()%>" required>
								</div>
							</div>
							<div class="col-6">
								<div class="form-group">
									<label>방문시간</label>
									<input type="text" class="form-control" name="VisitTime" required>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-6">
								<div class="form-group">
									<label>신규/재방문</label>
									<input type="text" class="form-control" name="ReturnVisit" required>
								</div>
							</div>
							<div class="col-6">
								<div class="form-group">
									<label>대리점명</label>
									<input type="text" class="form-control" name="Shop_Id">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-6">
								<div class="form-group">
									<label>예상품목</label>
									<input type="text" class="form-control" name="ProdType">
								</div>
							</div>
							<div class="col-6">
								<div class="form-group">
									<label>담당자</label>
									<select class="form-control" name="Mem_Id">
										<option></option>
										<option value="심현아">심현아</option>
										<option value="조재선">조재선</option>
										<option value="박은희">박은희</option>
										<option value="조현나">조현나</option>
										<option value="차문환">차문환</option>
									</select>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label>현장정보</label>
							<div class="input-group mb-1">
								<input type="text" class="form-control" name="Addr1" id="addr1_AddModal" required>
								<div class="input-group-append">
									<button class="btn btn-outline-secondary" type="button" id="btnDaumAddr_AddModal">주소찾기</button>
								</div>
							</div>
							<div id="wrap_AddModal" style="display:none;border:1px solid;width:100%;height:300px;margin:5px 0;position:relative">
								<img src="//t1.daumcdn.net/localimg/localimages/07/postcode/320/close.png" id="btnFoldWrap" style="cursor:pointer;position:absolute;right:0px;top:-1px;z-index:1" onclick="foldDaumPostcode('wrap_AddModal')" alt="접기 버튼">
							</div>
							<input type="text" class="form-control" name="Addr2" id="addr2_AddModal" required>
						</div>
						<div class="form-group">
							<label>비고</label>
							<textarea class="form-control" name="Remark" rows="3"></textarea>
						</div>
					</div>
					<div class="modal-footer">
						<button type="submit" class="btn btn-success"><i class="fa fa-check" aria-hidden="true"></i> <span>저장</span></button>
						<button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-undo" aria-hidden="true"></i> <span>취소</span></button>
						<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#delNoticeModal" id="btnDel_AddModal"><i class="fa fa-times" aria-hidden="true"></i> <span>삭제</span></button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- Delete Modal HTML -->
	<div id="delNoticeModal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<form id="delModalForm">
					<div class="modal-header">						
						<h4 class="modal-title"><i class="fa fa-trash" aria-hidden="true"></i> <span>삭제</span></h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">					
						<p>삭제 하시겠습니까?</p>
						<p class="text-warning"><small>삭제시 복구 불가능 합니다.</small></p>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-undo" aria-hidden="true"></i> <span>취소</span></button>
						<button type="submit" class="btn btn-danger"><i class="fa fa-times" aria-hidden="true"></i> <span>삭제</span></button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- /.modal -->

<!-- #include virtual = "/share/include/ContentsAddon.asp" -->
<!-- #include virtual = "/share/include/ContentsFooter.asp" -->
<!-- #include virtual = "/share/include/JavaScript.asp" -->

<script type="text/javascript">
<!--
	$(document).ready(function() {
		var _dataTable;

		_dataTable = $('#dataTable').DataTable({
			ajax: {
				url: '/data/NTC/BusiLog.List.Json.asp',
				type: "POST",
				data: {
					param1: function() { return $("#startDate").val() },
					param2: function() { return $("#endDate").val() },
					param3: function() { return $("#Sc_MemId").val() }
				},
				dataSrc: '',
				beforeSend:function(){
					$(".fa").addClass("fa-spinner fa-spin");
				},
				complete:function(){
					$(".fa").removeClass("fa-spinner fa-spin");
				},
				error: function() {
					alert("오류 발생!");
				}
			},
			destroy : true,
			aaSorting: [],					/* Disable initial sort */
			columns : [
				{
					data : "rownum", render: function ( d, type, row ) {
						return d;
					}
				}, {
					data : "Mem_Id", render: function ( d, type, row ) {
						return d;
					}
				}, {
					data : "BusiDate", render: function ( d, type, row ) {
						return d;
					}
				}, {
					data : "VisitTime", render: function ( d, type, row ) {
						return d;
					}
				}, {
					data : "ShortAddr", render: function ( d, type, row ) {
						return d;
					}
				}, {
					data : "ReturnVisit", render: function ( d, type, row ) {
						return d;
					},
					width: "5%"
				}, {
					data : "Shop_Id", render: function ( d, type, row ) {
						return d;
					}
				}, {
					data : "ProdType", render: function ( d, type, row ) {
						return d;
					},
					width: "10%"
				}, {
					data : "Addr1", render: function ( d, type, row ) {
						return d;
					},
					width: "30%"
				}, {
					data : "Addr2", render: function ( d, type, row ) {
						return d;
					},
					width: "10%"
				}, {
					data : "Remark", render: function ( d, type, row ) {
						return d;
					}
				}, {
					data : "Idx", render: function ( d, type, row ) {
						return d;
					}
				}
			],
			columnDefs: [
				{
					targets: '_all',
					className: 'text-center'
				}
			]
		});

        $('#startDate').datepicker({
			format: "yyyy-mm-dd",
			todayBtn: "linked",
			language: "kr",
			multidate: false,
			//daysOfWeekDisabled: "0",
			daysOfWeekHighlighted: "0,6",
			autoclose: true,
			todayHighlight: true,
			datesDisabled: ['07/06/2018', '07/21/2018'],
			toggleActive: true
        });

        $('#endDate').datepicker({
			format: "yyyy-mm-dd",
			todayBtn: "linked",
			language: "kr",
			multidate: false,
			//daysOfWeekDisabled: "0",
			daysOfWeekHighlighted: "0,6",
			autoclose: true,
			todayHighlight: true,
			datesDisabled: ['07/06/2018', '07/21/2018'],
			toggleActive: true
        });

        $('#BusiDate_AddModal').datepicker({
			format: "yyyy-mm-dd",
			todayBtn: "linked",
			language: "kr",
			multidate: false,
			//daysOfWeekDisabled: "0",
			daysOfWeekHighlighted: "0,6",
			autoclose: true,
			todayHighlight: true,
			datesDisabled: ['07/06/2018', '07/21/2018'],
			toggleActive: true
        }).on("hide", function(e) {
			$('.modal-backdrop').css({ 'z-index': 1041 } );
		});;

		$("#btnRetrieve").on("click", function(e) {
			_dataTable.ajax.reload(null, false);
		});

		$("#addModalForm").ajaxForm({
			url: "/data/NTC/BusiLog.Write.Proc.asp",
			type: "POST",
			dataType: "text",
			beforeSend:function(){
				$(".modal-footer > button").prop("disabled", true);
				$(".modal-footer > button").find("i")
					.addClass("fa-spinner fa-spin");
			},
			complete:function(){
				$(".modal-footer > button").prop("disabled", false);
				$(".modal-footer > button").find("i")
					.removeClass("fa-spinner fa-spin");
			},
            beforeSubmit: function (data,form,option) {
                //validation체크 
                //막기위해서는 return false를 잡아주면됨
                return true;
            },
            success: function(response, status){
                //성공후 서버에서 받은 데이터 처리
				if (response == "OK")
				{
					alert("저장 되었습니다.");
					$('#dataTable').DataTable().ajax.reload(null, false);
					$("#addNoticeModal").modal('hide');
				} else {
					alert("에러가 발생 하였습니다.");
				}
            },
            error: function(){
				alert("오류 발생!");
            }                               
        });

		$("#delModalForm").ajaxForm({
			url: "/data/NTC/BusiLog.Delete.Proc.asp",
			type: "POST",
			data: {
				param1: function() { return $("#idx_AddModal").val() }
			},
			dataType: "text",
			beforeSend:function(){
				$(".modal-footer > button").prop("disabled", true);
				$(".modal-footer > button").find("i")
					.addClass("fa-spinner fa-spin");
			},
			complete:function(){
				$(".modal-footer > button").prop("disabled", false);
				$(".modal-footer > button").find("i")
					.removeClass("fa-spinner fa-spin");
			},
            beforeSubmit: function (data,form,option) {
                //validation체크 
                //막기위해서는 return false를 잡아주면됨
                return true;
            },
            success: function(response, status){
                //성공후 서버에서 받은 데이터 처리
				if (response == "OK")
				{
					alert("삭제 되었습니다.");
					$('#dataTable').DataTable().ajax.reload(null, false);
					$("#delNoticeModal").modal('hide');
					$("#addNoticeModal").modal('hide');
				}
            },
            error: function(){
				alert("오류 발생!");
            }                               
        });

		$("#dataTable").attr("style", "cursor: pointer;");

		$(document).on("mouseover", "#dataTable > tbody > tr", function (e) {
			$(this).css("background-color", "#cafff2");
		});

		$(document).on("mouseout", "#dataTable > tbody > tr", function (e) {
			$(this).css("background-color", "");
		});

		$(document).on("click", "#dataTable > tbody > tr", function (e) {
			var cellIndex = e.target.cellIndex;
			if (cellIndex == 0)
				return;

			var data = _dataTable.row(this).data();
			$("#addModalForm").find("#idx_AddModal").val(data.Idx);
			$("#addNoticeModal").modal('show');
		});

		$(document).on("click", "#btnDaumAddr_AddModal", function (e) {
			execDaumPostcode_Inline("wrap_AddModal", null, "addr1_AddModal", "addr2_AddModal");
		});

		$('#addNoticeModal').on('shown.bs.modal', function (e) {
			var idx = $("#addModalForm").find("#idx_AddModal").val();
			if (idx == "")
			{
				$("#addNoticeModal").find("input[name=VisitTime]").focus();
			}
			else 
			{
				$.ajax({
					type: "POST",
					url: "/data/NTC/BusiLog.BusiLog.Json.asp",
					data: {
						param1: idx
					},
					dataType: 'json',
					cache: false,
					//async: false,
					beforeSend:function(){
						$(".modal-footer > button").prop("disabled", true);
						$(".modal-footer > button").find("i")
							.addClass("fa-spinner fa-spin");
					},
					complete:function(){
						$(".modal-footer > button").prop("disabled", false);
						$(".modal-footer > button").find("i")
							.removeClass("fa-spinner fa-spin");
					},
					success: function (data) {
						$.each(data, function(i) {
							
							$("#addModalForm").find("select[name=Mem_Id]").val(this.Mem_Id);
							$("#addModalForm").find("input[name=BusiDate]").val(this.BusiDate);
							$("#addModalForm").find("input[name=VisitTime]").val(this.VisitTime);
							$("#addModalForm").find("input[name=ReturnVisit]").val(this.ReturnVisit);
							$("#addModalForm").find("input[name=ProdType]").val(this.ProdType);
							$("#addModalForm").find("input[name=Shop_Id]").val(this.Shop_Id);
							$("#addModalForm").find("input[name=Addr1]").val(this.Addr1);
							$("#addModalForm").find("input[name=Addr2]").val(this.Addr2);
							$("#addModalForm").find("textarea[name=Remark]").val(this.Remark);

						});
					},
					error: function (ex) {
						alert("오류 발생\n" + ex.responseText);
					}
				});
			}
		});
		
		$('#addNoticeModal').on('hidden.bs.modal', function (e) {
			$("#addModalForm")[0].reset();
			$("#BusiDate_AddModal").val("<%=Date()%>");
			$("#idx_AddModal").val("");
		});
		
	});

//-->
</script>

<!-- #include virtual = "/share/include/PageFooter.asp" -->

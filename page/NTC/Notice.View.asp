<%@Language="VBScript" CODEPAGE="65001"%>
<!-- #include virtual = "/share/include/DbInfo.asp" -->
<!-- #include virtual = "/share/include/Injection.asp" -->
<!-- #include virtual = "/share/include/Function.asp" -->
<!-- #include virtual = "/share/include/Const.asp" -->
<!-- #include virtual = "/share/include/PageHeader.asp" -->
<!-- #include virtual = "/share/include/ContentsHeader.asp" -->

    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">공지사항</a>
        </li>
        <li class="breadcrumb-item active">공지사항</li>
      </ol>

	  <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
			<div class="row">
				<div class="col-xl-3 col-sm-6 mb-3">
					<div class="input-group">
						<div class="input-group-prepend">
							<span class="input-group-text">유형</span>
						</div>
						<select class="form-control" id="NoticeGubun">
							<option></option>
							<option value="SALE">영업공지</option>
							<option value="CONS">시공공지</option>
							<option value="LOGIC">물류공지</option>
							<option value="AS">A/S공지</option>
							<option value="STOCK">재고공지</option>
						</select>
					</div>
				</div>
				<div class="col-xl-3 mb-3 d-none d-xl-block">
				</div>
				<div class="col-xl-3 mb-3 d-none d-xl-block">
				</div>
				<div class="col-xl-3 col-sm-6 mb-3">
					<button type="button" id="btnRetrieve" class="btn btn-secondary mary btn-block"><i class="fa fa-search" aria-hidden="true"></i> <span>조회</span></button>
				</div>
			</div>
		</div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered display responsive nowrap" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th class="text-center all">NO</th>
                  <th class="text-center min-phone-l">제목</th>
                  <th class="text-center min-tablet">유형</th>
                  <th class="text-center min-tablet">작성일</th>
                  <th class="text-center min-tablet">작성자</th>
                  <th class="text-center none">내용</th>
                  <th class="text-center never">조회수</th>
                  <th class="text-center never">IDX</th>
                </tr>
              </thead>
              <tfoot></tfoot>
            </table>
          </div>
        </div>
        <div class="card-footer">
			<a href="#addNoticeModal" class="btn btn-primary" data-toggle="modal"><i class="fa fa-plus" aria-hidden="true"></i> <span>추가</span></a>
		</div>
      </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->

	<!-- modal -->
	<!-- Add Modal HTML -->
	<div id="addNoticeModal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<form id="addModalForm">
					<input type="hidden" name="idx" id="idx_AddModal" value="">
					<div class="modal-header">
						<h4 class="modal-title">공지글 작성</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">					
						<div class="form-group">
							<label>유형</label>
							<select class="form-control" name="bn" required>
								<option></option>
								<option value="SALE">영업공지</option>
								<option value="CONS">시공공지</option>
								<option value="LOGIC">물류공지</option>
								<option value="AS">A/S공지</option>
								<option value="STOCK">재고공지</option>
							</select>
						</div>
						<div class="form-group">
							<label>제목</label>
							<input type="text" class="form-control" name="title" required>
						</div>
						<div class="form-group">
							<label>내용</label>
							<textarea class="form-control" name="contents" rows="5" required></textarea>
						</div>
						<div class="form-group">
							<label>파일</label>
							<input type="file" class="form-control" name="file">
						</div>
						<div class="form-group">
							<label>첨부된 파일</label>
							<div class="alert alert-primary text-bold" role="alert" id="fileDiv"></div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="submit" class="btn btn-success"><i class="fa fa-check" aria-hidden="true"></i> <span>저장</span></button>
						<button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-undo" aria-hidden="true"></i> <span>취소</span></button>
						<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#delNoticeModal" id="btnDel_AddModal"><i class="fa fa-times" aria-hidden="true"></i> <span>삭제</span></button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- Delete Modal HTML -->
	<div id="delNoticeModal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<form id="delModalForm">
					<div class="modal-header">						
						<h4 class="modal-title"><i class="fa fa-trash" aria-hidden="true"></i> <span>삭제</span></h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">					
						<p>삭제 하시겠습니까?</p>
						<p class="text-warning"><small>삭제시 복구 불가능 합니다.</small></p>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-undo" aria-hidden="true"></i> <span>취소</span></button>
						<button type="submit" class="btn btn-danger"><i class="fa fa-times" aria-hidden="true"></i> <span>삭제</span></button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- /.modal -->

<!-- #include virtual = "/share/include/ContentsAddon.asp" -->
<!-- #include virtual = "/share/include/ContentsFooter.asp" -->
<!-- #include virtual = "/share/include/JavaScript.asp" -->

<script type="text/javascript">
<!--
	$(document).ready(function() {
		var _dataTable;

		_dataTable = $('#dataTable').DataTable({
			ajax: {
				url: '/data/NTC/Notice.List.Json.asp',
				type: "POST",
				data: {
					param1: function() { return $("#NoticeGubun").val() }
				},
				dataSrc: '',
				beforeSend:function(){
					$(".fa").addClass("fa-spinner fa-spin");
				},
				complete:function(){
					$(".fa").removeClass("fa-spinner fa-spin");
				},
			},
			destroy : true,
			aaSorting: [],					/* Disable initial sort */
			columns : [
				{
					data : "rownum", render: function ( d, type, row ) {
						var str = d;
						if (d == 0)
							str = "!";

						return TextBold(str, row);
					}
				}, {
					data : "title", render: function ( d, type, row ) {
						return TextBold(d, row);
					}
				}, {
					data : "bn", render: function ( d, type, row ) {
						var strBnName = GetBnName(d);
						return TextBold(strBnName, row);
					}
				}, {
					data : "wdate", render: function ( d, type, row ) {
						return TextBold(d, row);
					}
				}, {
					data : "name", render: function ( d, type, row ) {
						return TextBold(d, row);
					}
				}, {
					data : "contents", render: function ( d, type, row ) {
						var strContents = TextBold(d, row);
						return "<pre>" + strContents + "</pre>";
					}
				}, {
					data : "readcnt", render: function ( d, type, row ) {
						return TextBold(d, row);
					}
				}, {
					data : "idx", render: function ( d, type, row ) {
						return TextBold(d, row);
					}
				}
			],
			columnDefs: [
				{
					targets: '_all',
					className: 'text-center'
				}
				//{
					//targets: [ 6 ],
					//visible: false,
					//searchable: false
				//}
			]
		});

		$("#btnRetrieve").on("click", function(e) {
			_dataTable.ajax.reload(null, false);
		});

		$("#addModalForm").ajaxForm({
			url: "/data/NTC/Notice.Write.Proc.asp",
			type: "POST",
			enctype: "multipart/form-data",
			dataType: "text",
			beforeSend:function(){
				$(".modal-footer > button").prop("disabled", true);
				$(".modal-footer > button").find("i")
					.addClass("fa-spinner fa-spin");
			},
			complete:function(){
				$(".modal-footer > button").prop("disabled", false);
				$(".modal-footer > button").find("i")
					.removeClass("fa-spinner fa-spin");
			},
            beforeSubmit: function (data,form,option) {
                //validation체크 
                //막기위해서는 return false를 잡아주면됨
                return true;
            },
            success: function(response, status){
                //성공후 서버에서 받은 데이터 처리
				if (response == "OK")
				{
					alert("저장 되었습니다.");
					$('#dataTable').DataTable().ajax.reload(null, false);
					$("#addNoticeModal").modal('hide');
				}
            },
            error: function(){
				alert("오류 발생!");
            }                               
        });

		$("#delModalForm").ajaxForm({
			url: "/data/NTC/Notice.Delete.Proc.asp",
			type: "POST",
			data: {
				param1: function() { return $("#idx_AddModal").val() }
			},
			dataType: "text",
			beforeSend:function(){
				$(".modal-footer > button").prop("disabled", true);
				$(".modal-footer > button").find("i")
					.addClass("fa-spinner fa-spin");
			},
			complete:function(){
				$(".modal-footer > button").prop("disabled", false);
				$(".modal-footer > button").find("i")
					.removeClass("fa-spinner fa-spin");
			},
            beforeSubmit: function (data,form,option) {
                //validation체크 
                //막기위해서는 return false를 잡아주면됨
                return true;
            },
            success: function(response, status){
                //성공후 서버에서 받은 데이터 처리
				if (response == "OK")
				{
					alert("삭제 되었습니다.");
					$('#dataTable').DataTable().ajax.reload(null, false);
					$("#delNoticeModal").modal('hide');
					$("#addNoticeModal").modal('hide');
				}
            },
            error: function(){
				alert("오류 발생!");
            }                               
        });

		$("#dataTable").attr("style", "cursor: pointer;");

		$(document).on("mouseover", "#dataTable > tbody > tr", function (e) {
			$(this).css("background-color", "#cafff2");
		});

		$(document).on("mouseout", "#dataTable > tbody > tr", function (e) {
			$(this).css("background-color", "");
		});

		$(document).on("click", "#dataTable > tbody > tr", function (e) {
			var cellIndex = e.target.cellIndex;
			if (cellIndex == 0)
				return;

			var data = _dataTable.row(this).data();
			$("#addModalForm").find("#idx_AddModal").val(data.idx);
			$("#addNoticeModal").modal('show');
		});

		$('#addNoticeModal').on('shown.bs.modal', function (e) {
			var idx = $("#addModalForm").find("#idx_AddModal").val();
			if (idx == "")
			{
				var bn = $("#NoticeGubun").val();
				$("#addModalForm").find("select[name=bn]").val(bn);
			}
			else 
			{
				$.ajax({
					type: "POST",
					url: "/data/NTC/Notice.NBoard.Json.asp",
					data: {
						param1: idx
					},
					dataType: 'json',
					cache: false,
					//async: false,
					beforeSend:function(){
						$(".modal-footer > button").prop("disabled", true);
						$(".modal-footer > button").find("i")
							.addClass("fa-spinner fa-spin");
					},
					complete:function(){
						$(".modal-footer > button").prop("disabled", false);
						$(".modal-footer > button").find("i")
							.removeClass("fa-spinner fa-spin");
					},
					success: function (data) {
						$.each(data, function(i) {
							
							$("#addModalForm").find("select[name=bn]").val(this.bn);
							$("#addModalForm").find("input[name=title]").val(this.title);
							$("#addModalForm").find("textarea[name=contents]").val(this.contents);

							var filename = "";
							if (this.filename == "")
							{
								filename = "첨부된 파일이 없습니다.";
							}
							else
							{
								filename = "<a href='/share/include/Download.asp?p=\\updata\\notice&f=" + this.filename + "'><strong>" + this.filename + "</strong></a>";
							}

							$("#addModalForm").find("#fileDiv").html(filename);
						});
					},
					error: function (ex) {
						alert("오류 발생\n" + ex.responseText);
					}
				});
			}
		});
		
		$('#addNoticeModal').on('hidden.bs.modal', function (e) {
			$("#addModalForm")[0].reset();
			$("#idx_AddModal").val("");
			$("#addModalForm").find("#fileDiv").html("");
		});
		
	});

	function TextBold(str, row) {
		var strReturn = "";

		switch (row.rownum)
		{
			case 0:
				strReturn = "<b>" + str +  "</b>";
				break;
			default :
				strReturn = str;
				break;
		}

		return strReturn;
	}

	function GetBnName(str) {
		switch (str)
		{
			case "SALE": return "영업공지";
			case "CONS": return "시공공지";
			case "LOGIC": return "물류공지";
			case "AS": return "A/S공지";
			case "STOCK": return "재고공지";
			case "INFORM": return "업무연락";
			default: return str;		
		}
	}

//-->
</script>

<!-- #include virtual = "/share/include/PageFooter.asp" -->

<%@Language="VBScript" CODEPAGE="65001"%>
<!-- #include virtual = "/share/include/DbInfo.asp" -->
<!-- #include virtual = "/share/include/Injection.asp" -->
<!-- #include virtual = "/share/include/Function.asp" -->
<!-- #include virtual = "/share/include/Const.asp" -->
<!-- #include virtual = "/share/include/PageHeader.asp" -->
<!-- #include virtual = "/share/include/ContentsHeader.asp" -->

	<%

		syear 	= trim(request("syear"))
		smonth 	= trim(request("smonth"))
		sWeek 		= trim(request("sWeek"))

		' 시작일 세팅
		if sYear = "" then
			sYear = PrintMonWeek(Date(), "{u}")
		end if
		if sMonth = "" then
			sMonth = PrintMonWeek(Date(), "{n}")
		end if
		if Len(sMonth) = 1 then
			sMonth = "0" & sMonth
		end if
		if sWeek = "" then
			sWeek = PrintMonWeek(Date(), "{w}")
		end If

		'// sWeek 이전주 알아오기
		sDate_Prev = GetMonDay(sYear & "-" & sMonth & "-" & sWeek)
		sDate_Prev = DateAdd("d", -7, CDate(sDate_Prev))
		sYear_P = PrintMonWeek(sDate_Prev, "{u}")
		sMonth_P = PrintMonWeek(sDate_Prev, "{n}")
		sWeek_P = PrintMonWeek(sDate_Prev, "{w}")
		'Response.Write "sDate_Prev : " & sDate_Prev & "<br>"
		'Response.Write "sMonth_P : " & sMonth_P & "<br>"
		'Response.Write "sWeek_P : " & sWeek_P & "<br>"

		'// sWeek 다음주 알아오기
		sDate_Next = GetMonDay(sYear & "-" & sMonth & "-" & sWeek)
		sDate_Next = DateAdd("d", 7, CDate(sDate_Next))
		sYear_N = PrintMonWeek(sDate_Next, "{u}")
		sMonth_N = PrintMonWeek(sDate_Next, "{n}")
		sWeek_N = PrintMonWeek(sDate_Next, "{w}")
		'Response.Write "sDate_Next : " & sDate_Next & "<br>"
		'Response.Write "sMonth_N : " & sMonth_N & "<br>"
		'Response.Write "sWeek_N : " & sWeek_N & "<br>"

	%>

    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">공지사항</a>
        </li>
        <li class="breadcrumb-item active">주간업무</li>
      </ol>

	  <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
			<div class="row align-items-center">
				<div class="col-4 col-sm-4 text-right">
					<a href="?syear=<%=sYear_P%>&smonth=<%=sMonth_P%>&sweek=<%=sWeek_P%>"><i class="fa fa-chevron-circle-left fa-5x" aria-hidden="true"></i><span></span></a>
				</div>
				<div class="col-4 col-sm-4">
					<h3>
						<div class="row">
							<div class="col-md-12 col-lg-6 d-none d-sm-block text-center">
								<%=syear%>년
							</div>
							<div class="col-md-12 col-lg-6 text-center">
								<%=smonth%>월 <%=sWeek%>주차
							</div>
						</div>
					</h3>
				</div>
				<div class="col-4 col-sm-4 text-left">
					<a href="?syear=<%=sYear_N%>&smonth=<%=sMonth_N%>&sweek=<%=sWeek_N%>"><i class="fa fa-chevron-circle-right fa-5x" aria-hidden="true"></i><span></span></a>
				</div>
			</div>
			<div class="row">
				<div class="col text-center">
					<h5>
						<%
							nDate = GetMonDay(sYear & "-" & sMonth & "-" & sWeek)
							sDate = DateAdd("d" , -4, nDate)
							eDate = DateAdd("d" , 2, nDate)
						%>
						( <%=sDate%> ∼ <%=eDate%> )
					</h5>
				</div>
			</div>
		</div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered display responsive" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th class="text-center all">NO</th>
                  <th class="text-center all">담당자</th>
                  <th class="text-center all">방문<br>시간</th>
                  <th class="text-center not-desktop">현장<br>정보</th>
                  <th class="text-center tablet-l desktop">신규/재방문</th>
                  <th class="text-center tablet-l desktop">대리점명</th>
                  <th class="text-center tablet-l desktop">예상품목</th>
                  <th class="text-center desktop">기본주소</th>
                  <th class="text-center desktop">상세주소</th>
                  <th class="text-center none">비고</th>
                  <th class="text-center never">IDX</th>
                </tr>
              </thead>
              <tfoot></tfoot>
            </table>
          </div>
        </div>
        <div class="card-footer">
			<a href="#addNoticeModal" class="btn btn-primary" data-toggle="modal"><i class="fa fa-plus" aria-hidden="true"></i> <span>추가</span></a>
		</div>
      </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->

	<!-- modal -->
	<!-- Add Modal HTML -->
	<div id="addNoticeModal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<form id="addModalForm">
					<input type="hidden" name="idx" id="idx_AddModal">
					<input type="hidden" name="wyear" value="<%=syear%>">
					<input type="hidden" name="wmonth" value="<%=smonth%>">
					<input type="hidden" name="wweek" value="<%=sWeek%>">
					<div class="modal-header">
						<h4 class="modal-title">공지글 작성</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-6">
								<div class="form-group">
									<label>영업담당자</label>
									<select class="form-control" name="mem_id">
										<option></option>
										<option value="차문환">차문환</option>
										<option value="홍길동">홍길동</option>
										<option value="김철수">김철수</option>
									</select>
								</div>
							</div>
							<div class="col-6">
								<div class="form-group">
									<label>방문시간</label>
									<input type="text" class="form-control" name="visit_time" required>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-6">
								<div class="form-group">
									<label>신규/재방문</label>
									<input type="text" class="form-control" name="return_yn" required>
								</div>
							</div>
							<div class="col-6">
								<div class="form-group">
									<label>예상품목</label>
									<input type="text" class="form-control" name="prod">
								</div>
							</div>
						</div>
						<div class="form-group">
							<label>대리점명</label>
							<input type="text" class="form-control" name="shop_id" required>
						</div>
						<div class="form-group">
							<label>현장정보</label>
							<div class="input-group mb-1">
								<input type="text" class="form-control" name="addr1" id="addr1_AddModal" required>
								<div class="input-group-append">
									<button class="btn btn-outline-secondary" type="button" id="btnDaumAddr_AddModal">주소찾기</button>
								</div>
							</div>
							<div id="wrap_AddModal" style="display:none;border:1px solid;width:100%;height:300px;margin:5px 0;position:relative">
								<img src="//t1.daumcdn.net/localimg/localimages/07/postcode/320/close.png" id="btnFoldWrap" style="cursor:pointer;position:absolute;right:0px;top:-1px;z-index:1" onclick="foldDaumPostcode('wrap_AddModal')" alt="접기 버튼">
							</div>
							<input type="text" class="form-control" name="addr2" id="addr2_AddModal" required>
						</div>
						<div class="form-group">
							<label>비고</label>
							<textarea class="form-control" name="remark" rows="5"></textarea>
						</div>
					</div>
					<div class="modal-footer">
						<button type="submit" class="btn btn-success"><i class="fa fa-check" aria-hidden="true"></i> <span>저장</span></button>
						<button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-undo" aria-hidden="true"></i> <span>취소</span></button>
						<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#delNoticeModal" id="btnDel_AddModal"><i class="fa fa-times" aria-hidden="true"></i> <span>삭제</span></button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- Delete Modal HTML -->
	<div id="delNoticeModal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<form id="delModalForm">
					<div class="modal-header">						
						<h4 class="modal-title"><i class="fa fa-trash" aria-hidden="true"></i> <span>삭제</span></h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">					
						<p>삭제 하시겠습니까?</p>
						<p class="text-warning"><small>삭제시 복구 불가능 합니다.</small></p>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-undo" aria-hidden="true"></i> <span>취소</span></button>
						<button type="submit" class="btn btn-danger"><i class="fa fa-times" aria-hidden="true"></i> <span>삭제</span></button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- /.modal -->

<!-- #include virtual = "/share/include/ContentsAddon.asp" -->
<!-- #include virtual = "/share/include/ContentsFooter.asp" -->
<!-- #include virtual = "/share/include/JavaScript.asp" -->

<script type="text/javascript">
<!--
	$(document).ready(function() {
		var _dataTable;

		_dataTable = $('#dataTable').DataTable({
			ajax: {
				url: '/data/NTC/BusiLog.List.Json.asp',
				type: "POST",
				data: {
					param1: "<%=syear%>",
					param2: "<%=smonth%>",
					param3: "<%=sWeek%>"
				},
				dataSrc: '',
				beforeSend:function(){
					$(".fa").addClass("fa-spinner fa-spin");
				},
				complete:function(){
					$(".fa").removeClass("fa-spinner fa-spin");
				},
			},
			destroy : true,
			aaSorting: [],					/* Disable initial sort */
			columns : [
				{
					data : "rownum", render: function ( d, type, row ) {
						return d;
					}
				}, {
					data : "mem_id", render: function ( d, type, row ) {
						return d;
					}
				}, {
					data : "visit_time", render: function ( d, type, row ) {
						return d;
					}
				}, {
					data : "shortaddr", render: function ( d, type, row ) {
						return d;
					}
				}, {
					data : "return_yn", render: function ( d, type, row ) {
						return d;
					}
				}, {
					data : "shop_id", render: function ( d, type, row ) {
						return d;
					}
				}, {
					data : "prod", render: function ( d, type, row ) {
						return d;
					}
				}, {
					data : "addr1", render: function ( d, type, row ) {
						return d;
					},
					width: "30%"
				}, {
					data : "addr2", render: function ( d, type, row ) {
						return d;
					},
					width: "10%"
				}, {
					data : "remark", render: function ( d, type, row ) {
						return d;
					}
				}, {
					data : "idx", render: function ( d, type, row ) {
						return d;
					}
				}
			],
			columnDefs: [
				{
					targets: '_all',
					className: 'text-center'
				}
			]
		});

		$("#addModalForm").ajaxForm({
			url: "/data/NTC/BusiLog.Write.Proc.asp",
			type: "POST",
			dataType: "text",
			beforeSend:function(){
				$(".modal-footer > button").prop("disabled", true);
				$(".modal-footer > button").find("i")
					.addClass("fa-spinner fa-spin");
			},
			complete:function(){
				$(".modal-footer > button").prop("disabled", false);
				$(".modal-footer > button").find("i")
					.removeClass("fa-spinner fa-spin");
			},
            beforeSubmit: function (data,form,option) {
                //validation체크 
                //막기위해서는 return false를 잡아주면됨
                return true;
            },
            success: function(response, status){
                //성공후 서버에서 받은 데이터 처리
				if (response == "OK")
				{
					alert("저장 되었습니다.");
					$('#dataTable').DataTable().ajax.reload(null, false);
					$("#addNoticeModal").modal('hide');
				}
            },
            error: function(){
				alert("오류 발생!");
            }                               
        });

		$("#delModalForm").ajaxForm({
			url: "/data/NTC/BusiLog.Delete.Proc.asp",
			type: "POST",
			data: {
				param1: function() { return $("#idx_AddModal").val() }
			},
			dataType: "text",
			beforeSend:function(){
				$(".modal-footer > button").prop("disabled", true);
				$(".modal-footer > button").find("i")
					.addClass("fa-spinner fa-spin");
			},
			complete:function(){
				$(".modal-footer > button").prop("disabled", false);
				$(".modal-footer > button").find("i")
					.removeClass("fa-spinner fa-spin");
			},
            beforeSubmit: function (data,form,option) {
                //validation체크 
                //막기위해서는 return false를 잡아주면됨
                return true;
            },
            success: function(response, status){
                //성공후 서버에서 받은 데이터 처리
				if (response == "OK")
				{
					alert("삭제 되었습니다.");
					$('#dataTable').DataTable().ajax.reload(null, false);
					$("#delNoticeModal").modal('hide');
					$("#addNoticeModal").modal('hide');
				}
            },
            error: function(){
				alert("오류 발생!");
            }                               
        });

		$("#dataTable").attr("style", "cursor: pointer;");

		$(document).on("mouseover", "#dataTable > tbody > tr", function (e) {
			$(this).css("background-color", "#cafff2");
		});

		$(document).on("mouseout", "#dataTable > tbody > tr", function (e) {
			$(this).css("background-color", "");
		});

		$(document).on("click", "#dataTable > tbody > tr", function (e) {
			var cellIndex = e.target.cellIndex;
			if (cellIndex == 0)
				return;

			var data = _dataTable.row(this).data();
			$("#addModalForm").find("#idx_AddModal").val(data.idx);
			$("#addNoticeModal").modal('show');
		});

		$(document).on("click", "#btnDaumAddr_AddModal", function (e) {
			execDaumPostcode_Inline("wrap_AddModal", null, "addr1_AddModal", "addr2_AddModal");
		});

		$('#addNoticeModal').on('shown.bs.modal', function (e) {
			var idx = $("#addModalForm").find("#idx_AddModal").val();
			if (idx == "")
			{
				var bn = $("#NoticeGubun").val();
				$("#addModalForm").find("select[name=bn]").val(bn);
			}
			else 
			{
				$.ajax({
					type: "POST",
					url: "/data/NTC/BusiLog.WeeklyRpt.Json.asp",
					data: {
						param1: idx
					},
					dataType: 'json',
					cache: false,
					//async: false,
					beforeSend:function(){
						$(".modal-footer > button").prop("disabled", true);
						$(".modal-footer > button").find("i")
							.addClass("fa-spinner fa-spin");
					},
					complete:function(){
						$(".modal-footer > button").prop("disabled", false);
						$(".modal-footer > button").find("i")
							.removeClass("fa-spinner fa-spin");
					},
					success: function (data) {
						$.each(data, function(i) {
							
							$("#addModalForm").find("select[name=mem_id]").val(this.mem_id);
							$("#addModalForm").find("input[name=visit_time]").val(this.visit_time);
							$("#addModalForm").find("input[name=return_yn]").val(this.return_yn);
							$("#addModalForm").find("input[name=prod]").val(this.prod);
							$("#addModalForm").find("input[name=shop_id]").val(this.shop_id);
							$("#addModalForm").find("input[name=addr1]").val(this.addr1);
							$("#addModalForm").find("input[name=addr2]").val(this.addr2);
							$("#addModalForm").find("textarea[name=remark]").val(this.remark);

						});
					},
					error: function (ex) {
						alert("오류 발생\n" + ex.responseText);
					}
				});
			}
		});
		
		$('#addNoticeModal').on('hidden.bs.modal', function (e) {
			$("#addModalForm")[0].reset();
			$("#idx_AddModal").val("");
		});
		
	});

//-->
</script>

<!-- #include virtual = "/share/include/PageFooter.asp" -->

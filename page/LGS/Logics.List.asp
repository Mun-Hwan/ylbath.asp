<%@Language="VBScript" CODEPAGE="65001"%>
<!-- #include virtual = "/share/include/DbInfo.asp" -->
<!-- #include virtual = "/share/include/Injection.asp" -->
<!-- #include virtual = "/share/include/Function.asp" -->
<!-- #include virtual = "/share/include/Const.asp" -->
<!-- #include virtual = "/share/include/PageHeader.asp" -->
<!-- #include virtual = "/share/include/ContentsHeader.asp" -->

<%
        '// 기본날짜 셋팅
        stDt = Date()
        edDt = Date()

        '// Paging 기준값
        PageSize = 10           '// 페이지당 보여줄 데이타수
        BlockSize = 5             '// 페이지 그룹 범위       1 2 3 5 6 7 8 9 10
%>

    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">물류관리</a>
        </li>
        <li class="breadcrumb-item active">출고요청서출력</li>
      </ol>

	  <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
			<form id="Search_Form">
                <input type="hidden" class="form-control" name="Page" value="1">
                <input type="hidden" class="form-control" name="PageSize" value="<%=PageSize%>">
				<div class="row">
                    <div class="col-12 mb-3">
                        <div class="btn-group btn-group-toggle" data-toggle="buttons">
                            <label class="btn btn-secondary active">
                                <input type="checkbox" name="OutStatus" autocomplete="off" value="" checked>&nbsp;&nbsp;&nbsp;전체&nbsp;&nbsp;&nbsp;
                            </label>
                            <label class="btn btn-secondary">
                                <input type="checkbox" name="OutStatus" autocomplete="off" value="rdy">출고준비
                            </label>
                            <label class="btn btn-secondary">
                                <input type="checkbox" name="OutStatus" autocomplete="off" value="pac">포장완료
                            </label>
                            <label class="btn btn-secondary">
                                <input type="checkbox" name="OutStatus" autocomplete="off" value="yes">&nbsp;&nbsp;&nbsp;출고&nbsp;&nbsp;&nbsp;
                            </label>
                            <label class="btn btn-secondary">
                                <input type="checkbox" name="OutStatus" autocomplete="off" value="no">&nbsp;&nbsp;미출고&nbsp;&nbsp;
                            </label>
                            <label class="btn btn-secondary">
                                <input type="checkbox" name="OutStatus" autocomplete="off" value="re">&nbsp;&nbsp;재확인&nbsp;&nbsp;
                            </label>
                        </div>
                     </div>
                    <div class="col-xl-3 col-sm-6 mb-3">
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text">오더상태</span>
							</div>
							<select class="form-control" name="OrderOpt">
								<option value=""></option>
								<option value="order" selected>확정오더</option>
								<option value="frder">임시오더</option>
							</select>
						</div>
                     </div>
                    <div class="col-xl-3 col-sm-6 mb-3">
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text">오더번호</span>
							</div>
							<input type="text" class="form-control" name="OrderCode">
						</div>
                     </div>
                    <div class="col-xl-3 col-sm-6 mb-3">
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text">담당자</span>
							</div>
							<select class="form-control" name="EmpId">
								<option value=""></option>
                                <%
                                    sql = ""
                                    sql = sql & vbCrlf & "      select  a.id,  a.name       "
                                    sql = sql & vbCrlf & "       from  sales_member a with(nolock)       "
                                    sql = sql & vbCrlf & "      where  a.site = 'younglim'       "
                                    sql = sql & vbCrlf & "         and  a.del_chk != 'Y'       "
                                    sql = sql & vbCrlf & " order by  a.name       "
                                    'response.Write sql
									Set rs = db.execute(sql)
									If Not rs.eof Then
										Do Until rs.eof
											MemId = Trim(rs("id"))
											MemName = Trim(rs("name"))
                                %>
                                            <option value="<%=MemId%>"><%=MemName%></option>
                                <%
											rs.movenext
										Loop
									End If
									rs.close
									Set rs = Nothing
                                %>
							</select>
						</div>
                     </div>
                    <div class="col-xl-3 col-sm-6 mb-3">
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text">오더유형</span>
							</div>
							<select class="form-control" name="OrderType">
								<option value=""></option>
                                <%
                                    sql = ""
                                    sql = sql & vbCrlf & "      select  a.t_code,  a.t_name       "
                                    sql = sql & vbCrlf & "       from  sales_order_type a with(nolock)       "
                                    sql = sql & vbCrlf & "      where  a.status = 'Y'       "
                                    sql = sql & vbCrlf & " order by  a.sort       "
                                    'response.Write sql
									Set rs = db.execute(sql)
									If Not rs.eof Then
										Do Until rs.eof
											TCode = Trim(rs("t_code"))
											TName = Trim(rs("t_name"))
                                %>
                                            <option value="<%=TCode%>"><%=TName%></option>
                                <%
											rs.movenext
										Loop
									End If
									rs.close
									Set rs = Nothing
                                %>
							</select>
						</div>
                     </div>
                </div>
				<div class="row">
                    <div class="col-xl-3 col-sm-6 mb-3">
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text">도착요청일</span>
							</div>
							<input type="text" class="form-control text-center" id="startDate" value="<%=stDt%>" required>
							<input type="text" class="form-control text-center" id="endDate" value="<%=edDt%>" required>
						</div>
                     </div>
                    <div class="col-xl-3 col-sm-6 mb-3">
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text">품목</span>
							</div>
							<input type="text" class="form-control" name="ProdInfo">
						</div>
                     </div>
                    <div class="col-xl-3 col-sm-6 mb-3">
                     </div>
                    <div class="col-xl-3 col-sm-6 mb-3">
						<button type="submit" class="btn btn-secondary mary btn-block"><i class="fa fa-search" aria-hidden="true"></i> <span>조회</span></button>
                     </div>
                </div>
			</form>

		</div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-hover table-bordered" style="font-size: 8pt;" id="TransTable">
                    <thead>
                        <tr>
						    <th scope="col">오더번호</th>
						    <th scope="col">오더상태</th>
						    <th scope="col">오더유형</th>
						    <th scope="col">현장</th>
						    <th scope="col">공급가액<br>VAT(-)</th>
						    <th scope="col">배송비<br>VAT(+)</th>
						    <th scope="col">도착시간</th>
						    <th scope="col">대리점명</th>
						    <th scope="col">출고일</th>
						    <th scope="col">상태변경</th>
						    <th scope="col">요청서출력</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                 </table>
            </div>

        </div>
        <div class="card-footer" id="Paging_MainTransForm">
		</div>
      </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->


<!-- #include virtual = "/share/include/ContentsAddon.asp" -->
<!-- #include virtual = "/share/include/ContentsFooter.asp" -->
<!-- #include virtual = "/share/include/JavaScript.asp" -->

<script type="text/javascript">
<!--
	$(document).ready(function() {

        $('#startDate').datepicker({
			format: "yyyy-mm-dd",
			todayBtn: "linked",
			language: "kr",
			multidate: false,
			//daysOfWeekDisabled: "0",
			daysOfWeekHighlighted: "0,6",
			autoclose: true,
			todayHighlight: true,
			datesDisabled: ['07/06/2018', '07/21/2018'],
			toggleActive: true
        });

        $('#endDate').datepicker({
			format: "yyyy-mm-dd",
			todayBtn: "linked",
			language: "kr",
			multidate: false,
			//daysOfWeekDisabled: "0",
			daysOfWeekHighlighted: "0,6",
			autoclose: true,
			todayHighlight: true,
			datesDisabled: ['07/06/2018', '07/21/2018'],
			toggleActive: true
        });

		$("#Search_Form").ajaxForm({
			url: "/data/LGS/Logics.List.Json.asp",
            data: {
                startDate : function() { return $("#startDate").val() }, 
                endDate: function () { return $("#endDate").val() }
            },
            type: "POST",
			dataType: "json",
			beforeSend:function(){
                $(".fa").addClass("fa-spinner fa-spin");
			},
			complete:function(){
				$(".fa").removeClass("fa-spinner fa-spin");
			},
            beforeSubmit: function (data,form,option) {
                //validation체크 
                //막기위해서는 return false를 잡아주면됨
                return true;
            },
            success: function(data, status){
                var info = "";
                var maxCnt = 0;
                $.each(data, function () {
                    maxCnt = this.max_cnt;

                    var OrderOptNm = "";
                    switch (this.order_opt) {
                        case "frder": OrderOptNm = "임시오더"; break;
                        case "order": OrderOptNm = "확정오더"; break;
                        default:
                    }
                    var oTimeNm = "";
                    switch (this.otime) {
                        case "06": oTimeNm = " 06:00"; break;
                        case "07": oTimeNm = " 07:00"; break;
                        case "08": oTimeNm = " 08:00"; break;
                        case "09": oTimeNm = " 09:00"; break;
                        case "10": oTimeNm = " 10:00"; break;
                        case "11": oTimeNm = " 11:00"; break;
                        case "12": oTimeNm = " 12:00"; break;
                        case "13": oTimeNm = " 13:00"; break;
                        case "14": oTimeNm = " 14:00"; break;
                        case "15": oTimeNm = " 15:00"; break;
                        case "16": oTimeNm = " 16:00"; break;
                        case "17": oTimeNm = " 17:00"; break;
                        case "18": oTimeNm = " 18:00"; break;
                        case "19": oTimeNm = " 19:00"; break;
                        case "20": oTimeNm = " 20:00"; break;
                        case "30": oTimeNm = " 오전중"; break;
                        case "40": oTimeNm = " 오후중"; break;
                        default:
                    }
                    var stat = "";
                    var OutStatusNm = "";
                    switch (this.out_status) {
                        case "rdy": OutStatusNm = "출고준비"; stat = " table-success"; break;
                        case "pac": OutStatusNm = "포장완료"; stat = " table-warning"; break;
                        case "yes": OutStatusNm = "출고"; stat = " table-primary"; break;
                        case "no": OutStatusNm = "미출고"; break;
                        case "re": OutStatusNm = "재확인"; stat = " table-secondary"; break;
                        default:
                    }
                    info += "  <tr class='" + stat + "'>";
					info += "     <td>" + this.order_code + "</td>";
					info += "     <td>" + OrderOptNm + "</td>";
					info += "     <td>" + this.order_type_nm + "</td>";
					info += "     <td>" + this.b_add1 + " " + this.b_add2 + "</td>";
					info += "     <td class='text-right'>" + addCommas(this.order_pamt) + "</td>";
					info += "     <td class='text-right'>" + addCommas(this.order_delivery) + "</td>";
					info += "     <td>" + oTimeNm + "</td>";
					info += "     <td>" + this.mem_name + "</td>";
					info += "     <td>" + this.odate + "</td>";
					info += "     <td><button type='button' class='btn btn-info btn-sm' name='btnOutStatus' ordercode='" + this.order_code + "' status='" + this.out_status + "'><i class='fa fa-truck' aria-hidden='true'></i> <span>" + OutStatusNm + "</span></button></td>";
                    info += "     <td><a class='btn btn-info btn-sm' href='/data/LGS/Logics.Out.Specification.Excel.asp?Oc=" + this.order_code + "'><i class='fa fa-file-excel-o' aria-hidden='true'></i> <span>요청서</span></a></td>";
                    info += "  </tr>";
                });

                $("#TransTable").find("tbody").html(info);
                var NowPage = $("#Search_Form").find("input[name=Page]").val();
                var page_viewList = Paging(maxCnt, '<%=PageSize%>', '<%=BlockSize%>', NowPage);
                $("#Paging_MainTransForm").html(page_viewList);
            },
            error: function(){
				alert("오류 발생!");
            }                               
        });

        $(document).on("click", "button[name=btnOutStatus]", function (e) {
            var $this = $(this);
            var status = $(this).attr("status");
            $.ajax({
                url: "/data/LGS/Logics.OutStatus.Update.Proc.asp",
                type: "POST",
                data: {
                    OrderCode: $(this).attr("ordercode"),
                    OutStatus: $(this).attr("status")
                },
                dataType: "text",
                beforeSend: function () {
                    $(".fa").addClass("fa-spinner fa-spin");
                },
                complete: function () {
                    $(".fa").removeClass("fa-spinner fa-spin");
                },
                beforeSubmit: function (data, form, option) {
                    //validation체크
                    //막기위해서는 return false를 잡아주면됨
                    return true;
                },
                success: function (data, status) {
                    //성공후 서버에서 받은 데이터 처리
                    var dataNm = "";
                    var stat = "";
                    switch (data) {
                        case "rdy": dataNm = "출고준비"; stat = " table-success"; break;
                        case "pac": dataNm = "포장완료"; stat = " table-warning"; break;
                        case "yes": dataNm = "출고"; stat = " table-primary"; break;
                        case "no": dataNm = "미출고"; break;
                        case "re": dataNm = "재확인"; stat = " table-secondary"; break;
                        default:
                    }
                    $this.attr("status", data);
                    $this.find("span").text(dataNm);
                    $this.parent().parent().removeClass().addClass(stat);
                },
                error: function () {
                    alert("물류 상태 변경 중 오류 발생!");
                }
            });
        });

        $(document).on("click", "button[name=btnOutPrint]", function (e) {
            var $this = $(this);
            var status = $(this).attr("status");
            $.ajax({
                url: "/data/LGS/Logics.OutStatus.Update.Proc.asp",
                type: "POST",
                data: {
                    OrderCode: $(this).attr("ordercode"),
                    OutStatus: $(this).attr("status")
                },
                dataType: "text",
                beforeSend: function () {
                    $(".fa").addClass("fa-spinner fa-spin");
                },
                complete: function () {
                    $(".fa").removeClass("fa-spinner fa-spin");
                },
                beforeSubmit: function (data, form, option) {
                    //validation체크
                    //막기위해서는 return false를 잡아주면됨
                    return true;
                },
                success: function (data, status) {
                    //성공후 서버에서 받은 데이터 처리
                    var dataNm = "";
                    var stat = "";
                    switch (data) {
                        case "rdy": dataNm = "출고준비"; stat = " table-success"; break;
                        case "pac": dataNm = "포장완료"; stat = " table-warning"; break;
                        case "yes": dataNm = "출고"; stat = " table-primary"; break;
                        case "no": dataNm = "미출고"; break;
                        case "re": dataNm = "재확인"; stat = " table-secondary"; break;
                        default:
                    }
                    $this.attr("status", data);
                    $this.find("span").text(dataNm);
                    $this.parent().parent().removeClass().addClass(stat);
                },
                error: function () {
                    alert("물류 상태 변경 중 오류 발생!");
                }
            });
        });

		$(document).on("click", "nav a[name=pageGoto]", function (e) {
            var page = $(this).attr("target-page");
            $("#Search_Form").find("input[name=Page]").val(page);
            $("#Search_Form").submit();
        });

        $(document).on("change", "input[name=OutStatus]", function (e) {
            switch ($(this).val()) {
                case "":
                    if (this.checked) {
                        $("input[name=OutStatus]:not([value=''])").prop("checked", false)
                            .parent().removeClass("active");
                    }
                    break;
                case "rdy":
                case "pac":
                case "yes":
                case "no":
                case "re":
                    if (this.checked) {
                        $("input[name=OutStatus][value='']").prop("checked", false)
                            .parent().removeClass("active");
                    }
                    break;
                default:
            }
        });

    });

//-->
</script>

<!-- #include virtual = "/share/include/PageFooter.asp" -->

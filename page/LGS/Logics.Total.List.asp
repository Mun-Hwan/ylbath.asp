<%@Language="VBScript" CODEPAGE="65001"%>
<!-- #include virtual = "/share/include/DbInfo.asp" -->
<!-- #include virtual = "/share/include/Injection.asp" -->
<!-- #include virtual = "/share/include/Function.asp" -->
<!-- #include virtual = "/share/include/Const.asp" -->
<!-- #include virtual = "/share/include/PageHeader.asp" -->
<!-- #include virtual = "/share/include/ContentsHeader.asp" -->

<%
        '// 기본날짜 셋팅
        stDt = Date()
        edDt = Date()

        '// Paging 기준값
        PageSize = 10           '// 페이지당 보여줄 데이타수
        BlockSize = 5             '// 페이지 그룹 범위       1 2 3 5 6 7 8 9 10
%>

    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">물류관리</a>
        </li>
        <li class="breadcrumb-item active">출고집계현황</li>
      </ol>

	  <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
			<form id="Search_Form">
				<div class="row">
                    <div class="col-xl-3 col-sm-6 mb-3">
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text">오더상태</span>
							</div>
							<select class="form-control" name="OrderOpt">
								<option value=""></option>
								<option value="order" selected>확정오더</option>
								<option value="frder">임시오더</option>
							</select>
						</div>
                     </div>
                    <div class="col-xl-3 col-sm-6 mb-3">
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text">오더번호</span>
							</div>
							<input type="text" class="form-control" name="OrderCode">
						</div>
                     </div>
                    <div class="col-xl-3 col-sm-6 mb-3">
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text">담당자</span>
							</div>
							<select class="form-control" name="EmpId">
								<option value=""></option>
                                <%
                                    sql = ""
                                    sql = sql & vbCrlf & "      select  a.id,  a.name       "
                                    sql = sql & vbCrlf & "       from  sales_member a with(nolock)       "
                                    sql = sql & vbCrlf & "      where  a.site = 'younglim'       "
                                    sql = sql & vbCrlf & "         and  a.del_chk != 'Y'       "
                                    sql = sql & vbCrlf & " order by  a.name       "
                                    'response.Write sql
									Set rs = db.execute(sql)
									If Not rs.eof Then
										Do Until rs.eof
											MemId = Trim(rs("id"))
											MemName = Trim(rs("name"))
                                %>
                                            <option value="<%=MemId%>"><%=MemName%></option>
                                <%
											rs.movenext
										Loop
									End If
									rs.close
									Set rs = Nothing
                                %>
							</select>
						</div>
                     </div>
                    <div class="col-xl-3 col-sm-6 mb-3">
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text">오더유형</span>
							</div>
							<select class="form-control" name="OrderType">
								<option value=""></option>
                                <%
                                    sql = ""
                                    sql = sql & vbCrlf & "      select  a.t_code,  a.t_name       "
                                    sql = sql & vbCrlf & "       from  sales_order_type a with(nolock)       "
                                    sql = sql & vbCrlf & "      where  a.status = 'Y'       "
                                    sql = sql & vbCrlf & " order by  a.sort       "
                                    'response.Write sql
									Set rs = db.execute(sql)
									If Not rs.eof Then
										Do Until rs.eof
											TCode = Trim(rs("t_code"))
											TName = Trim(rs("t_name"))
                                %>
                                            <option value="<%=TCode%>"><%=TName%></option>
                                <%
											rs.movenext
										Loop
									End If
									rs.close
									Set rs = Nothing
                                %>
							</select>
						</div>
                     </div>
                </div>
				<div class="row">
                    <div class="col-xl-3 col-sm-6 mb-3">
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text">도착요청일</span>
							</div>
							<input type="text" class="form-control text-center" id="startDate" value="<%=stDt%>" required>
							<input type="text" class="form-control text-center" id="endDate" value="<%=edDt%>" required>
						</div>
                     </div>
                    <div class="col-xl-3 col-sm-6 mb-3">
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text">품목</span>
							</div>
							<input type="text" class="form-control" name="ProdInfo">
						</div>
                     </div>
                    <div class="col-xl-3 col-sm-6 mb-3">
                     </div>
                    <div class="col-xl-3 col-sm-6 mb-3">
						<button type="submit" class="btn btn-secondary mary btn-block"><i class="fa fa-search" aria-hidden="true"></i> <span>조회</span></button>
                     </div>
                </div>
			</form>

		</div>
        <div class="card-body">
          <div class="table-responsive">
          </div>
        </div>

        <div class="card-footer">
            <button type="button" id="btnExcel" class='btn btn-info'><i class='fa fa-file-excel-o' aria-hidden='true'></i> <span>EXCEL</span></button>
		</div>
      </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->


<!-- #include virtual = "/share/include/ContentsAddon.asp" -->
<!-- #include virtual = "/share/include/ContentsFooter.asp" -->
<!-- #include virtual = "/share/include/JavaScript.asp" -->


<script type="text/javascript">
<!--
	$(document).ready(function() {
		var _dataTable;

        $('#startDate').datepicker({
			format: "yyyy-mm-dd",
			todayBtn: "linked",
			language: "kr",
			multidate: false,
			//daysOfWeekDisabled: "0",
			daysOfWeekHighlighted: "0,6",
			autoclose: true,
			todayHighlight: true,
			datesDisabled: ['07/06/2018', '07/21/2018'],
			toggleActive: true
        });

        $('#endDate').datepicker({
			format: "yyyy-mm-dd",
			todayBtn: "linked",
			language: "kr",
			multidate: false,
			//daysOfWeekDisabled: "0",
			daysOfWeekHighlighted: "0,6",
			autoclose: true,
			todayHighlight: true,
			datesDisabled: ['07/06/2018', '07/21/2018'],
			toggleActive: true
        });

		$("#Search_Form").ajaxForm({
			url: "/data/LGS/Logics.Total.List.Html.asp",
            data: {
                startDate : function() { return $("#startDate").val() }, 
                endDate: function () { return $("#endDate").val() }
            },
            type: "POST",
			dataType: "html",
			beforeSend:function(){
                $(".fa").addClass("fa-spinner fa-spin");
			},
			complete:function(){
				$(".fa").removeClass("fa-spinner fa-spin");
			},
            beforeSubmit: function (data,form,option) {
                //validation체크 
                //막기위해서는 return false를 잡아주면됨
                return true;
            },
            success: function(html, status){
                $(".card-body").find(".table-responsive").html(html);
                _dataTable = $('#dataTable').DataTable({
                    "aLengthMenu": [[10, 50, 100, -1], [10, 50, 100, "All"]],
                    "iDisplayLength": 50
                   });
            },
            error: function(){
				alert("오류 발생!");
            }                               
        });

        $("#btnExcel").on("click", function (e) {
            var params = $("#Search_Form").serialize();
            params += "&startDate=" + $("#startDate").val();
            params += "&endDate=" + $("#endDate").val();

            location.href = "/data/LGS/Logics.Total.Excel.asp?" + params; 
		});

    });

//-->
</script>

<!-- #include virtual = "/share/include/PageFooter.asp" -->

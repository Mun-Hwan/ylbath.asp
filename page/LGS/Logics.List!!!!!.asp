<!-- #include virtual = "/new/include/dbinfo2.asp" -->
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script type="text/javascript">
function view_cal(f, y, m, d) {
	url = "/new/include/view_cal.asp?f=" + f + "&y=" + y + "&m=" + m + "&d=" + d;
	opt_str = "toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, width=210, height=240, left=" + (event.screenX - 150) + ", top=" + (event.screenY - 120);
	window.open (url, 'view_cal', opt_str);
}
function print_opt_click() {
	switch ( getCheckedValue(pForm.print_opt) ) {
		case "date" :
			document.getElementById("order_code").disabled = true;
			document.getElementById("osb").disabled = true;

			document.getElementById("syear").disabled = false;
			document.getElementById("smonth").disabled = false;
			document.getElementById("sday").disabled = false;

			document.getElementById("eyear").disabled = false;
			document.getElementById("emonth").disabled = false;
			document.getElementById("eday").disabled = false;

			break;
		case "order" :
			document.getElementById("order_code").disabled = false;
			document.getElementById("osb").disabled = false;

			document.getElementById("syear").disabled = true;
			document.getElementById("smonth").disabled = true;
			document.getElementById("sday").disabled = true;

			document.getElementById("eyear").disabled = true;
			document.getElementById("emonth").disabled = true;
			document.getElementById("eday").disabled = true;

			break;
	}
}
function pgo() {
	if ( pForm.print_opt[0].checked ) {
		opt1 = "sk";
		opt2 = "date";
		opt3 = pForm.sgyear1.value + "-" + pForm.sgmonth1.value + "-" + pForm.sgday1.value;
	} else if ( pForm.print_opt[1].checked ) {
		opt1 = "sk";
		opt2 = "order";
		opt3 = pForm.order_code.value;
	}
	url = "/new/page/print.asp?opt1=" + opt1 + "&opt2=" + opt2 + "&opt3=" + opt3;

	window.open(url, 'pgo', 'toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=no, width=820, height=600, left=0, top=0;');
}

function change_stat() {
	if ( document.sForm.allbox.checked == true ) {
		document.sForm.allbox.checked = true;
		checkall('sForm', 'chk_idx', true);
	} else {
		document.sForm.allbox.checked = false;
		checkall('sForm', 'chk_idx', false);
	}
}

function checkall(formname,checkname,thestate) {
	var el_collection = eval("document.forms." + formname + "." + checkname);

	if ( el_collection != null ) {
		if ( el_collection.length ) {
			for ( c=0 ; c<el_collection.length ; c++ )
				if ( ! el_collection[c].disabled ) {
					el_collection[c].checked = thestate;
				}
		} else {
			if ( ! el_collection.disabled ) {
				el_collection.checked = thestate;
			}
		}
	}
}

// 선택인쇄 ( 출고요청서 )
function item_print(opt1) {
	var el_collection = sForm.chk_idx;

	if ( typeof(el_collection) != "undefined" ) 
	{
		ichk = "";
		if ( el_collection.length ) 
		{
			for ( i=0 ; i<=el_collection.length-1 ; i++ ) 
			{
				if ( el_collection[i].checked == true ) 
				{
					ichk = ichk + el_collection[i].value + ",";
				}
			}
		} 
		else 
		{
			if ( el_collection.checked == true )
				ichk = el_collection.value + ",";
		}

		if ( ichk != "" ) {
			url = "/new/page/print.asp?opt1=" + opt1 + "&opt2=" + ichk;
			window.open(url, 'pgo', 'toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=no, width=820, height=600, left=0, top=0;');
			//var PntUrl = "http://218.237.119.204:8091/";
			//var PntPg = "ConsignmentNote.jsp";
			//var PntParam = "";
			//PntParam += "?opt2=" + ichk;
			//var popup = window.open(PntUrl + PntPg + PntParam, "PrintPage", "top=100, left=200, width=1200, height=700, menubar=false, tollbar=falsle, location=false, status=false");
		} else {
			alert ('데이터를 선택해주세요.');
		}
	}
}
function item_print2(opt1, opt2) {
	url = "/new/page/print.asp?opt1=" + opt1 + "&opt2=" + opt2;
	window.open(url, 'pgo', 'toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=no, width=820, height=600, left=0, top=0;');
	//var PntUrl = "http://218.237.119.204:8091/";
	//var PntPg = "ConsignmentNote.jsp";
	//var PntParam = "";
	//PntParam += "?opt2=" + opt2;
	//var popup = window.open(PntUrl + PntPg + PntParam, "PrintPage", "top=100, left=200, width=1200, height=700, menubar=false, tollbar=falsle, location=false, status=false");
}

// 선택 출고완료
function item_out(opt1) {
	var el_collection = sForm.chk_idx;

	if ( typeof(el_collection) != "undefined" ) 
	{
		ichk = "";
		if ( el_collection.length ) 
		{
			for ( i=0 ; i<=el_collection.length-1 ; i++ ) 
			{
				if ( el_collection[i].checked == true ) 
				{
					ichk = ichk + el_collection[i].value + ",";
				}
			}
		} 
		else 
		{
			if ( el_collection.checked == true )
				ichk = el_collection.value + ",";
		}

		if ( ichk != "" ) {
			if ( confirm ('상태변경 하시겠습니까?') )
			{
				url = "/new/page/out_sta.asp?opt1=" + opt1 + "&opt2=" + ichk;

				//window.open(url, 'pgo', 'toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=no, width=820, height=600, left=0, top=0;');
				document.getElementById("jFrame").src = url;
			}
		} else {
			alert ('상태변경할 데이터를 선택해주세요.');
		}
	}
}
function item_out2(opt1, opt2) {
	if ( confirm ('상태변경 하시겠습니까?') )
	{
		url = "/new/page/out_sta.asp?opt1=" + opt1 + "&opt2=" + opt2;

		//window.open(url, 'pgo', 'toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=no, width=820, height=600, left=0, top=0;');
		document.getElementById("jFrame").src = url;
	}
}
function order_search3() {
	window.open('/new/page/order_search3.asp', 'order_search3', 'toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=no, width=620, height=500, left=0, top=0;');
}
function frm_chk() {
	loadding(true, 200);
	return true;
}

function os_chk() {
	if ( document.getElementsByName("out_status")[0].checked ) {
		document.getElementsByName("out_status")[1].checked = false;
		document.getElementsByName("out_status")[2].checked = false;
		document.getElementsByName("out_status")[3].checked = false;
		document.getElementsByName("out_status")[4].checked = false;
		document.getElementsByName("out_status")[5].checked = false;
	}
}
function os_chk2() {
	if ( document.getElementsByName("out_status")[0].checked ) {
		document.getElementsByName("out_status")[0].checked = false;
	}
}

function chk_pack(num) {
	var chkbox = document.getElementById("pack_yn" + num);
	var inputbox = document.getElementById("pack_order_code" + num);
	if (chkbox.checked) {
		inputbox.disabled = false;
	} else {
		inputbox.value = "";
		inputbox.disabled = true;
	}
}

function fnSave(pIdx) {
	$.ajax({
		type: "POST",
		url: "/new/page/T07_M0702_OK.asp",
		data: {
			"oc" : $("#order_code" + pIdx).val(),
			"ready_date" : $("#ready_date" + pIdx).val(),
			"trans_type1" : $("#trans_type1" + pIdx).val(),
			"trans_type1_lock" : ($("#trans_type1_lock" + pIdx).is(":checked") ? "Y":"N"),
			"trans_type3" : $("#trans_type3" + pIdx).val(),
			"inhouse" : $("input[name=inhouse" + pIdx + "]:checked").val(),
			"inhouse_std_amt" : $("#inhouse_std_amt" + pIdx).val(),
			"inhouse_std_amt_lock" : ($("#inhouse_std_amt_lock" + pIdx).is(":checked") ? "Y":"N"),
			"amt_type" : $("input[name=amt_type" + pIdx + "]:checked").val(),
			"trans_code" : $("#trans_code" + pIdx + " option:selected").val(),
			"pack_yn" : ($("#pack_yn" + pIdx).is(":checked") ? "Y":"N"),
			"pack_order_code" : $("#pack_order_code" + pIdx).val(),
			"remark" : $("#remark" + pIdx).val(),
			"odate_v" : $("#odate_v" + pIdx).val(),
			"otime_v" : $("#otime_v" + pIdx).val(),
			"req_car_type" : $("#req_car_type" + pIdx).val(),
			"trans_emer" : ($("#trans_emer" + pIdx).is(":checked") ? "Y":"N"),
			"trans_shuttle" : ($("#trans_shuttle" + pIdx).is(":checked") ? "Y":"N")
		},
		dataType: "text",
		async: true,
		success: function (text) {
			//alert('저장 되었습니다.');
			//$("#btn_Save" + pIdx).val("저장완료");
			$("#trans_type1" + pIdx).val(text);
		},
		failure: function (text) {
			alert('실패 하였습니다.\n\n다시 조회해 주세요.');
		}
	});
}

function out_status_history() {
	url = "/new/page/T07_M0702_Out_Status.asp";
	opt_str = "toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, width=600, height=600, left=" + (event.screenX - 400) + ", top=" + (event.screenY - 240);
	window.open (url, 'view_out', opt_str);
}
function etc_show(pObj, pTxt) {
	if (pTxt != "") {
		var pop = $("<div name='pop' style='font-family: Verdana, Dotum, Gulim; font-size: 11px; letter-spacing:-1px; width:200px; height: 100px; border: solid 3px  #f1f1f1; display: none;'>" + pTxt + "</div>");
		pop.css({
			position : "absolute",
			top : $(pObj).offset().top+$(pObj).outerHeight()+2,
			left : $(pObj).offset().left,
			background : "#fff"
		});
		
		$("body").append(pop);
		pop.show();
	} // if (pTxt != "") {
}

function etc_hide() {
	$("div[name='pop']").hide();
}

function gogo(opt ,pIdx) {
	switch ( opt ) {
		case "e" :
			var sIdx = "";
			if (pIdx == "CHK") {
				// 체크된 체크박스 구하기
				$("input:checkbox[name=chk_idx]:checked").each(function(i) {
					sIdx += ", " + $(this).val();
				});
			} else {
				sIdx = ", " + pIdx;
			} // if (pIdx == "CHK")
			
			if (sIdx != "") {
				jForm.action = "/new/page/T07_M0702_Excel.asp";
				jForm.t.value = $("select[name=sh_trans_code] option:selected").text();
				jForm.i.value = sIdx;
				jForm.submit();
			} else {
				alert("데이터를 선택해주세요.");
			} //if (sIdx != "")
			break;

		case "t" :
			var sIdx = "";
			if (pIdx == "CHK") {
				// 체크된 체크박스 구하기
				$("input:checkbox[name=chk_idx]:checked").each(function(i) {
					sIdx += ", " + $(this).val();
				});
			} else {
				sIdx = ", " + pIdx;
			} // if (pIdx == "CHK")
			
			if (sIdx != "") {
				jForm.action = "/new/page/T07_M0702_Excel2.asp";
				jForm.i.value = sIdx;
				jForm.submit();
			} else {
				alert("데이터를 선택해주세요.");
			} //if (sIdx != "")
			break;

		default : 
	}
}

function fnBatch_readdt() {
	var $ready_dt = $("input:text[name=batch_ready_date]");
	//if ($ready_dt.val() == "") {
		//alert("출고 준비일을 입력 하세요.");
		//return;
	//} // if ($("input:text[name=batch_ready_date]").val() == "")

	if ($("input:checkbox[name='chk_idx']:checked").length == 0) {
		alert("선택 된 오더가 없습니다.");
		return;
	} else {
		var aIdx = "";
		$.each($("input:checkbox[name='chk_idx']:checked"), function(i) {
			aIdx += ", " + $(this).val();
		});
		
		$.post(  "/new/page/T07_M0702_ReadyDT.asp",
					{   i : aIdx ,
						v : $ready_dt.val()  }
		).done( function() {
			$.each($("input:checkbox[name='chk_idx']:checked"), function(i) {
				$(this).parent().parent().prev().find("input:text[name=ready_date]").val($ready_dt.val());
			});
			alert("저장 되었습니다.");
		});
	} // if ($("input:checkbox[name='chk_idx']:checked").length == 0)
}

$(function() {
	$("input[name='ready_date']").datepicker({
		changeYear: true,
		changeMonth: true,
		dateFormat:"yy-mm-dd",
		showMonthAfterYear:true,
		dayNamesMin: ['일','월','화','수','목','금','토'],
		monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월']
	});

	$("input[name='batch_ready_date']").datepicker({
		changeYear: true,
		changeMonth: true,
		dateFormat:"yy-mm-dd",
		showMonthAfterYear:true,
		dayNamesMin: ['일','월','화','수','목','금','토'],
		monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월']
	});

	//$("input[name='ready_date']").change( function(){
		//url = "/new/page/T07_M0702_ReadyDT.asp?dt=" + $(this).val() + "&idx=" + $(this).attr("idx");
		//document.getElementById("jFrame").src = url;
	//});
});
</script>
<style type="text/css">
	.top-bottom-line {
		border-top:1px solid #afafaf;
		border-bottom:1px solid #afafaf;
	}
	.top-line {
		border-top:1px solid #afafaf;
	}
	.bottom-line {
		border-bottom:1px solid #afafaf;
	}
	.listTable, input, select, textarea {
		font-family: 맑은 고딕, "Malgun Gothic";
		font-size: 8pt;
	}
</style>


<%
order_code = trim(request("order_code"))
act = trim(request("act"))
out_status = trim(request("out_status"))
If out_status = "" Then
	out_status = "all"
End If 

syear = trim(request("syear"))
smonth = trim(request("smonth"))
sday = trim(request("sday"))

' 시작일 세팅
if sYear = "" then
	sYear = Year(date)
end if
if sMonth = "" then
	sMonth = Month(date)
end if
if Len(sMonth) = 1 then
	sMonth = "0" & sMonth
end if
if sDay = "" then
	sDay = Day(date)
end if
if Len(sDay) = 1 then
	sDay = "0" & sDay
end if
sDate = sYear & "-" & sMonth & "-" & sDay

eyear = trim(request("eyear"))
emonth = trim(request("emonth"))
eday = trim(request("eday"))

' 종료일 세팅
ed = DateAdd("d", date, 4)
if eYear = "" then
	eYear = Year(ed)
end if
if eMonth = "" then
	eMonth = Month(ed)
end if
if Len(eMonth) = 1 then
	eMonth = "0" & eMonth
end if
if eDay = "" then
	eDay = Day(ed)
end if
if Len(eDay) = 1 then
	eDay = "0" & eDay
end if
eDate = eYear & "-" & eMonth & "-" & eDay
eDate = DateAdd("d", cDate(eDate) , 1)             '// 조회하는 날의 하루를 더함.

order_sta = request("order_sta")
'If order_sta = "" Then
'	order_sta = "order"
'End If 

order_type = request("order_type")
selKeyWordType = request("selKeyWordType")
txtKeyWord = request("txtKeyWord")
groupcode = request("groupcode")

rdoOType = request("rdoOType")
If rdoOType = "" Then 
	rdoOType = "O"
End If 

trans_yn = request("trans_yn")

sh_trans_code = request("sh_trans_code")

If act = "search" Then
	search_str = " a.del_chk='N' and a.ver=a.now_ver "
	
	If IsDate(sdate) And IsDate(edate) Then
		If rdoOType = "O" Then 
			search_str = search_str & " and a.odate >= '" & sdate & "' "
			search_str = search_str & " and a.odate < '" & edate & "' "
		Else
			search_str = search_str & " and a.ready_date >= '" & sdate & "' "
			search_str = search_str & " and a.ready_date < '" & edate & "' "
		End If 
	End If 

	If order_code <> "" Then
		search_str = search_str & " and a.order_code like '%" & order_code & "%' "
	End If 
	If order_sta <> "" Then
		search_str = search_str & " and a.order_opt='" & order_sta & "' "
	End If 
	If order_type <> "" Then
		search_str = search_str & " and a.order_type='" & order_type & "' "
	End If 

	If txtKeyWord <> "" Then
		Select Case selKeyWordType
			Case "PC"
				search_str = search_str & " and a.order_code in (SELECT ORDER_CODE FROM SALES_ORDER_ITEM WITH (NOLOCK) WHERE PROD_CODE in (select prod_code from sales_prod_info where prod_code like '%" & txtKeyWord & "%' ) and SALES_ORDER_ITEM.ver = a.now_ver)  "
			Case "PN"
				search_str = search_str & " and a.order_code in (SELECT ORDER_CODE FROM SALES_ORDER_ITEM WITH (NOLOCK) WHERE PROD_CODE in (select prod_code from sales_prod_info where prod_name like '%"  & txtKeyWord & "%' ) and SALES_ORDER_ITEM.ver = a.now_ver)  "
		End Select 
	End If 
	
	If InStr(out_status, "all") Then
	Else 
		search_str = search_str & " and ( 1=0 "
		If InStr(out_status, "yes") Then
			search_str = search_str & " or a.out_status='yes' "
		End If
		If InStr(out_status, "re") Then
			search_str = search_str & " or a.out_status='re' "
		End If
		If InStr(out_status, "no") Then
			search_str = search_str & " or isnull(a.out_status, '')='' "
		End If
		If InStr(out_status, "rdy") Then
			search_str = search_str & "or a.out_status='rdy' "
		End If
		If InStr(out_status, "pac") Then
			search_str = search_str & "or a.out_status='pac' "
		End If
		search_str = search_str & " ) "
	End If

	If groupcode <> "" Then
		search_str = search_str & " and a.mem_id in ( select id from sales_member where group_code='" & groupcode & "' ) "
	End If 
	
	If trans_yn <> "" Then
		If trans_yn = "Y" Then 
			trans_search_str = trans_search_str & " tb.idx is not null "
		Else
			trans_search_str = trans_search_str & " tb.idx is null "
		End If 		
	Else
		trans_search_str = trans_search_str & " 'abc' = 'abc' "
	End If 

	If sh_trans_code <> "" Then
		trans_search_str = trans_search_str & " and  tb.trans_code = '" & sh_trans_code & "' "
	End If 

	order_gb = request("order_gb")
	If order_gb <> "" Then 
		search_str = search_str & " and a.order_gb =  '" & order_gb & "' "
	End If 

	wh_empno = request("wh_empno")
	If wh_empno <> "" Then 
		search_str = search_str & " and c.wh_empno =  '" & wh_empno & "' "
	End If 
	
End If 


pagesize = 50
page = request("page")
If page < "1" Then
	page = 1
End If

bs = ( page - 1 ) * pagesize
startNum = ((page - 1) * pagesize) + 1

'If search_str <> "" Then
	''sql = " select count(*) from sales_order where order_opt='order' and " & search_str & " "
	'sql = " select count(*) from sales_order a with(nolock) left outer join sales_trans b with(nolock) on a.order_code = b.order_code  where " & search_str & " "
'Else
	''sql = " select count(*) from sales_order where order_opt='order' "
	'sql = " select count(*) from sales_order "
'End If
If search_str <> "" then
	sql = ""
	sql = sql & vbCrlf & "                  select  count(*)		"
	sql = sql & vbCrlf & "                    from  (		"
	sql = sql & vbCrlf & "                           select  a.order_code			"
	sql = sql & vbCrlf & "                             from  sales_order a with(nolock)	,	"
	sql = sql & vbCrlf & "                                      (  select  za.id,							"
	sql = sql & vbCrlf & "                                                za.name,							"
	sql = sql & vbCrlf & "                                                za.group_empno,							"
	sql = sql & vbCrlf & "                                                zb.wh_empno,							"
	sql = sql & vbCrlf & "                                                zb.wh_empnm							"
	sql = sql & vbCrlf & "                                          from  sales_member za with(nolock)							"
	sql = sql & vbCrlf & "                                    left outer join  sales_delivery_wh_res zb with(nolock) on za.group_empno = zb.group_empno							"
	sql = sql & vbCrlf & "                                         where  za.del_chk <> 'Y' ) c							"
	sql = sql & vbCrlf & "                            where  a.mem_id = c.id		"
	sql = sql & vbCrlf & "                                and  " & search_str
	sql = sql & vbCrlf & "                           ) ta		"
	sql = sql & vbCrlf & "         left outer join  sales_trans tb with(nolock) on ta.order_code = tb.order_code			"
	sql = sql & vbCrlf & "                  where  " & trans_search_str	
	'response.write sql
	Set rs = db.execute (sql)
	recordcount = Int(rs(0))
Else
	recordcount = 0
end If

pageCount = Int((recordcount-1) / pagesize) + 1

of = trim(request("of"))
If of = "" Then
	of = "idx"
End If 
os = trim(request("os"))
If os = "" Then
	os = "desc"
End If 

%>
					<form class="listForm" name="pForm" method="get" action="/new/bathplan_onc_system.asp" onsubmit="return frm_chk();">
					<input type="hidden" name="t" value="<%=t%>">
					<input type="hidden" name="m" value="<%=m%>">
					<input type="hidden" name="s" value="<%=s%>">
					<input type="hidden" name="act" value="search">
					<input type="hidden" name="of" value="<%=of%>">
					<input type="hidden" name="os" value="<%=os%>">

						<table class="nmlTable" style="margin-bottom:5px;">
							<!--<tr>
								<th width="20%">구분</th>
								<td width="80%">
									<input type="radio" name="print_opt" value="date" class="radio" onClick="print_opt_click();" <%If print_opt = "date" Then response.write "checked" End If %>> 일자별
									<input type="radio" name="print_opt" value="order" class="radio" onClick="print_opt_click();" <%If print_opt = "order" Then response.write "checked" End If %>> 오더별
								</td>
							</tr>-->
							<tr>
								<th width="15%" style="font-size:8pt;">출고상태</th>
								<td width="35%" style="font-size:8pt;">
									<label><input type="checkbox" name="out_status" value="all" class="radio" <%If InStr(out_status, "all") > 0 Then response.write "checked" End If %> onclick="os_chk();" > 전체&nbsp;&nbsp;</label>
									<label><input type="checkbox" name="out_status" value="rdy" class="radio" <%If InStr(out_status, "rdy") > 0 Then response.write "checked" End If %> onclick="os_chk2();" > 출고준비&nbsp;&nbsp;</label>
									<label><input type="checkbox" name="out_status" value="pac" class="radio" <%If InStr(out_status, "pac") > 0 Then response.write "checked" End If %> onclick="os_chk2();" > 포장완료&nbsp;&nbsp;</label>
									<label><input type="checkbox" name="out_status" value="yes" class="radio" <%If InStr(out_status, "yes") > 0 Then response.write "checked" End If %> onclick="os_chk2();" > 출고&nbsp;&nbsp;</label>
									<label><input type="checkbox" name="out_status" value="no" class="radio" <%If InStr(out_status, "no") > 0 Then response.write "checked" End If %> onclick="os_chk2();" > 미출고&nbsp;&nbsp;</label>
									<label><input type="checkbox" name="out_status" value="re" class="radio" <%If InStr(out_status, "re") > 0 Then response.write "checked" End If %> onclick="os_chk2();" > 재확인</label>
								</td>
								<th width="15%" style="font-size:8pt;">오더상태</th>
								<td width="35%">
									<select name="order_sta" id="order_sta">
										<option value="">선택</option>
										<option value="order" <%If order_sta = "order" Then response.write "selected" End If %>>확정오더</option>
										<option value="frder" <%If order_sta = "frder" Then response.write "selected" End If %>>임시오더</option>
									</select>
								</td>
							</tr>
							<tr>
								<th width="15%" style="font-size:8pt;">오더번호</th>
								<td width="35%"><input type="text" name="order_code" id="order_code" size="20" value="<%=order_code%>" <%'If print_opt = "date" Then response.write "disabled" End If %> /> <!--<input class="list" type="button" value="검 색" onClick="order_search3();" <%'If print_opt = "date" Then response.write "disabled" End If %> id="osb" />--></td>
								<th width="15%" style="font-size:8pt;">오더유형</th>
								<td width="35%"><select name="order_type" id="order_type" >
									<option value="">선택</option>
									
<%
sql = " select t_code, t_name from sales_order_type where status='Y' and charIndex('" & request.cookies("msite") & "', site) > 0 order by sort asc "
Set rs = db.execute (sql)
If Not rs.eof Then
	Do Until rs.eof
		t_code = Trim(rs(0))
		t_name = Trim(rs(1))
%>
										<option value="<%=t_code%>" <%If order_type = t_code Then response.write "selected" End If %>><%=t_name%></option>
<%
		rs.movenext
	Loop
End If
rs.close
Set rs = Nothing
%>
									
								</select></td>
							</tr>
							<tr>
								<th width="15%" style="font-size:8pt;">품목검색</th>
								<td width="35%"><select name="selKeyWordType" id="selKeyWordType">
									<!--<option value="">선택</option>-->
									<!--<option value="all" <%If search = "all" Then response.write "selected" End if%>>전체</option>-->
									<option value="PN" <% If selKeyWordType = "PN" Then Response.write "selected" End if%>>제품명</option><!-- PN : Prod Name 제품명 -->
									<option value="PC" <% If selKeyWordType = "PC" Then Response.write "selected" End if%>>제품코드</option><!-- PC : Prod Code 제품코드 -->

									</select>
									<input type="text" name="txtKeyWord" id="txtKeyWord" value="<%= txtKeyWord%>" style="ime-mode:inactive;" >
									&nbsp;
									<!--<input class="list" type="button" value="검 색" onClick="javaScript:goSearch();" /> -->
									&nbsp;
								<th width="15%" style="font-size:8pt;">영업그룹</th>
								<td width="35%"><select name="groupcode" id="groupcode">
										<option value="">선택</option>
<%
sql = " select distinct group_code, group_name from sales_member where group_code <> '' and del_chk='N' order by group_name "
'response.write sql
Set rs = db.execute (sql)
If Not rs.eof Then
	num = 1
	Do Until rs.eof
		SALES_GRP = Trim(rs(0))
		SALES_GRP_NM = Trim(rs(1))

%>
										<option value="<%=SALES_GRP%>" <%If groupcode = SALES_GRP Then response.write "selected" End If %> ><%=SALES_GRP_NM%></option>
<%
		num = num + 1
		rs.movenext
	Loop
End If
rs.close
Set rs = Nothing
%>
									</select></td>
							</tr>
							<tr>
								<th width="15%" style="font-size:8pt;">기준일</th>
								<td width="35%">
									<input type="text" name="syear" id="syear" maxlength="4" class="input01" value="<%=syear%>" style="width:40px; text-align:center;">년
									<input type="text" name="smonth" id="smonth" maxlength="2" class="input01" value="<%=smonth%>" style="width:20px; text-align:center;">월
									<input type="text" name="sday" id="sday" maxlength="2" class="input01" value="<%=sday%>" style="width:20px; text-align:center;">일 
									<span id="ddd"><img src="/new/images/calendar2.gif" border="0" onClick="javascript:view_cal('pForm', 'syear', 'smonth', 'sday');" style="cursor:hand" width="15" height="15"></span> - 
									<input type="text" name="eyear" id="eyear" maxlength="4" class="input01" value="<%=eyear%>" style="width:40px; text-align:center;">년
									<input type="text" name="emonth" id="emonth" maxlength="2" class="input01" value="<%=emonth%>" style="width:20px; text-align:center;">월
									<input type="text" name="eday" id="eday" maxlength="2" class="input01" value="<%=eday%>" style="width:20px; text-align:center;">일 
									<span id="ddd"><img src="/new/images/calendar2.gif" border="0" onClick="javascript:view_cal('pForm', 'eyear', 'emonth', 'eday');" style="cursor:hand" width="15" height="15"></span>
								</td>
								<th width="15%" style="font-size:8pt;">배송등록</th>
								<td width="35%">
									<select name="trans_yn" id="trans_yn" >
										<option value="">전체</option>
										<option value="Y" <%If trans_yn = "Y" Then response.write "selected" End If %>>등록</option>
										<option value="N" <%If trans_yn = "N" Then response.write "selected" End If %>>미등록</option>
									</select>				
								</td>
							</tr>
							<tr>
								<th width="15%" style="font-size:8pt;">기준유형</th>
								<td width="35%">
									<label><input type="radio" name="rdoOType" value="O" <%If rdoOType = "O" Then Response.Write "checked=checked"%> /> 출고일</label>&nbsp;&nbsp;&nbsp;
									<label><input type="radio" name="rdoOType" value="R" <%If rdoOType = "R" Then Response.Write "checked=checked"%> /> 출고준비일</label>
								</td>
								<th width="15%" style="font-size:8pt;">배송업체</th>
								<td width="35%">
									<select name="sh_trans_code">
										<option value="">전체</option>
										<%
											sql = ""
											sql = sql & vbCrlf & "  	select  1 as sort,		"
											sql = sql & vbCrlf & "              id,		"
											sql = sql & vbCrlf & "              com_name		"
											sql = sql & vbCrlf & "        from  sales_member with(nolock)		"
											sql = sql & vbCrlf & "       where  site = 'bshop'		"
											sql = sql & vbCrlf & "         and  del_chk <> 'Y'		"
											sql = sql & vbCrlf & "   union all		"
											sql = sql & vbCrlf & "  	select  2 as sort,		"
											sql = sql & vbCrlf & "              '경남CDC',			"
											sql = sql & vbCrlf & "              '경남CDC'		"
											sql = sql & vbCrlf & "   union all		"
											sql = sql & vbCrlf & "  	select  3 as sort,		"
											sql = sql & vbCrlf & "              '경북CDC',			"
											sql = sql & vbCrlf & "              '경북CDC'		"
											sql = sql & vbCrlf & "   union all		"
											sql = sql & vbCrlf & "  	select  4 as sort,		"
											sql = sql & vbCrlf & "              '기타업체',			"
											sql = sql & vbCrlf & "              '기타업체'		"
											sql = sql & vbCrlf & "   union all		"
											sql = sql & vbCrlf & "  	select  5 as sort,		"
											sql = sql & vbCrlf & "              '방문수령',			"
											sql = sql & vbCrlf & "              '방문수령'		"
											sql = sql & vbCrlf & "    order by  sort,		"
											sql = sql & vbCrlf & "              com_name		"
											'Response.Write sql
											Set rs2 = db.execute (sql)
											Do Until rs2.bof Or rs2.eof
										%>
												<option value="<%=rs2("id")%>" <%If sh_trans_code = rs2("id") Then response.write "selected" End If %>><%=rs2("com_name")%></option>
										<%
												rs2.movenext
											Loop 
											rs2.close
											Set rs2 = Nothing
										%>
									</select>
								</td>
							</tr>
							<tr>
								<th width="15%" style="font-size:8pt;">브랜드</th>
								<td width="35%">
									<select name="order_gb" id="order_gb" style="width: 100px;">
										<option value="">전체</option>
										<option value="bath" <%If order_gb = "bath" Then response.write "selected" End if%>>바스</option>
										<!-- <option value="door" <%If order_gb = "door" Then response.write "selected" End if%>>도어</option> -->
										<!-- <option value="floor" <%If order_gb = "floor" Then response.write "selected" End if%>>마루</option> -->
									</select>
								</td>
								</td>
								<th width="15%" style="font-size:8pt;">물류담당자</th>
								<td width="35%">
									<select name="wh_empno" id="wh_empno" style="width: 100px;">
										<option value="">전체</option>
										<option value="21410181" <%If wh_empno = "21410181" Then response.write "selected" End if%>>허영민</option>
										<option value="21610491" <%If wh_empno = "21610491" Then response.write "selected" End if%>>류동우</option>
									</select>
								</td>
							</tr>
						</table>
						<input class="list" type="button" value="목록" onClick="javascript:document.location.href='/new/bathplan_onc_system.asp?t=T07&m=M0702'" />
						<input class="list" type="submit" value="검색" />
					</form>	

<%If act = "search" then%>
					<form name="sForm">	
						<p align="right">
							<input class="list" type="button" value="상태변경 이력조회" onClick="out_status_history();" />&nbsp;&nbsp;&nbsp;
							<input class="list" type="button" value="선택출고" onClick="item_out('sta');" />&nbsp;&nbsp;&nbsp;
							<input class="list" type="button" value="선택인쇄(출고요청서)" onClick="item_print('out');" />&nbsp;&nbsp;&nbsp;
							<input class="list" type="button" value="선택인쇄(지방권배차)" onClick="gogo('e', 'CHK');" />&nbsp;&nbsp;&nbsp;
							<!-- <input class="list" type="button" value="선택인쇄(배송오더표지)" onClick="gogo('t', 'CHK');" />&nbsp;&nbsp;&nbsp; -->
							<span style="display:inline-block; height : 20px; border: 1px solid #a1a5ad; padding: 0 4px 0 4px; font: bold 11px arial; background: #dddddd;">출고준비일</span><input type="text" name="batch_ready_date" class="input01" style="width:70px; text-align:center;" />
							<input class="list" type="button" value="일괄적용" onClick="fnBatch_readdt();" />&nbsp;&nbsp;&nbsp;
						</p>
						<table summery="" class="listTable" style="table-layout:fixed; word-break:break-all;">
							<tr>
								<th width="2%" rowspan="2"><input type="checkbox" name="allbox" onclick="javascript:change_stat();" class="radio" ></th>
								<th width="8%" style="font-size:8pt;">오더번호</th>
								<th width="8%" style="font-size:8pt;">오더상태</th>
								<th width="8%" style="font-size:8pt;">오더유형</th>
								<th width="18%" style="font-size:8pt;" colspan="2">현장</th>
								<th width="8%" style="font-size:8pt;">공급가액<br>VAT(-)</th>
								<th width="8%" style="font-size:8pt;">배송비</th>
								<th width="8%" style="font-size:8pt;">도착시간</th>
								<th width="8%" style="font-size:8pt;">대리점명</th>
								<th width="8%" style="font-size:8pt;">출고일</th>
								<th width="8%" style="font-size:8pt;">상태변경</th>
								<th width="8%" style="font-size:8pt;">출고준비일</th>
								<th width="8%" style="font-size:8pt;">비고</th>
							</tr>
							<tr>
								<th style="font-size:8pt;" colspan="3">출고제목(자동생성)</th>
								<th width="9%" style="font-size:8pt;">세대반입</th>
								<th width="8%" style="font-size:8pt;">세대반입비</th>
								<th width="9%" style="font-size:8pt;">운임(선,착)</th>
								<th width="8%" style="font-size:8pt;">배송업체</th>
								<th width="8%" style="font-size:8pt;">차량요청</th>
								<th width="8%" style="font-size:8pt;">영업<br>담당자</th>
								<th width="8%" style="font-size:8pt;">고객명</th>
								<th width="8%" style="font-size:8pt;">오더<br>생성일</th>
								<th width="8%" style="font-size:8pt;">최종<br>수정일</th>
								<th width="8%" style="font-size:8pt;">합적오더</th>
							</tr>
<%
If search_str <> "" then
	sql = ""
	sql = sql & vbCrlf & "     select  top 50 *		"
	sql = sql & vbCrlf & "       from  (		"
	sql = sql & vbCrlf & "                  select  row_number() over (order by ta.idx desc) as rownum,		"
	sql = sql & vbCrlf & "                          ta.idx,		"
	sql = sql & vbCrlf & "                          ta.order_code,		"
	sql = sql & vbCrlf & "                          ta.cont_code,			"
	sql = sql & vbCrlf & "                          ta.cus_code,		"
	sql = sql & vbCrlf & "                          ta.shop_code,			"
	sql = sql & vbCrlf & "                          ta.mem_id,		"
	sql = sql & vbCrlf & "                          ta.wdate,		"
	sql = sql & vbCrlf & "                          ta.order_opt,		"
	sql = sql & vbCrlf & "                          ta.b_zipcode,		"
	sql = sql & vbCrlf & "                          ta.b_add1,			"
	sql = sql & vbCrlf & "                          ta.b_add2,			"
	sql = sql & vbCrlf & "                          ta.order_type,		"
	sql = sql & vbCrlf & "                          ta.as_type,		"
	sql = sql & vbCrlf & "                          ta.as_why,			"
	sql = sql & vbCrlf & "                          ta.as_etc,		"
	sql = sql & vbCrlf & "                          ta.free_type,		"
	sql = sql & vbCrlf & "                          ta.ka_no,		"
	sql = sql & vbCrlf & "                          ta.free_etc,		"
	sql = sql & vbCrlf & "                          convert(varchar(10), ta.odate,120) as odate,		"
	sql = sql & vbCrlf & "                          ta.otime,		"
	sql = sql & vbCrlf & "                          ta.sdate,		"
	sql = sql & vbCrlf & "                          ta.edate,		"
	sql = sql & vbCrlf & "                          ta.sigong_type,		"
	sql = sql & vbCrlf & "                          ta.imsi_code,			"
	sql = sql & vbCrlf & "                          ta.dn_no,		"
	sql = sql & vbCrlf & "                          ta.bp_cd,		"
	sql = sql & vbCrlf & "                          ta.out_status,		"
	sql = sql & vbCrlf & "                          ( select top 1 name from sales_customer with(nolock) where cus_code = ta.cus_code ) as cname,		"
	sql = sql & vbCrlf & "                          ( select top 1 com_company from sales_member with (nolock) where id = ta.mem_id ) as ccom,			"
	sql = sql & vbCrlf & "                          ( select top 1 group_name from sales_member with (nolock) where id = ta.mem_id ) as group_name,			"
	sql = sql & vbCrlf & "                          ( select wdate from sales_order with (nolock) where order_code = ta.order_code and ver = 0 ) as first_wdate,			"
	sql = sql & vbCrlf & "                          ta.now_ver,		"
	sql = sql & vbCrlf & "                          ta.ready_date,		"
	sql = sql & vbCrlf & "                          ta.req_car_type,		"
	sql = sql & vbCrlf & "                          ta.out_memo,		"
	sql = sql & vbCrlf & "                          ta.wh_empnm,		"
	sql = sql & vbCrlf & "                          tb.idx as trans_idx,			"
	sql = sql & vbCrlf & "                          tb.trans_type1,		"
	sql = sql & vbCrlf & "                          tb.trans_type1_lock,		"
	sql = sql & vbCrlf & "                          tb.trans_type2,		"
	sql = sql & vbCrlf & "                          tb.trans_type3,		"
	sql = sql & vbCrlf & "                          tb.remark,			"
	sql = sql & vbCrlf & "                          tb.trans_code,		"
	sql = sql & vbCrlf & "                          isnull(tb.pack_yn,'n') as pack_yn,			"
	sql = sql & vbCrlf & "                          tb.pack_order_code,		"
	sql = sql & vbCrlf & "                          isnull(tb.inhouse, ta.inhouse) as inhouse,		"
	sql = sql & vbCrlf & "                          (case when isnull(tb.inhouse_std_amt_lock, 'N') = 'Y' then isnull(tb.inhouse_std_amt, 0) else isnull(ta.inhouse_std_amt,0) end) as inhouse_std_amt,	"
	sql = sql & vbCrlf & "                          isnull(tb.inhouse_std_amt_lock,'N') as inhouse_std_amt_lock,		"
	sql = sql & vbCrlf & "                          tb.amt_type,		"
	sql = sql & vbCrlf & "                          ta.order_pamt,		"
	sql = sql & vbCrlf & "                          ta.order_delivery		"
	sql = sql & vbCrlf & "                    from  (		"
	sql = sql & vbCrlf & "                           select  a.idx,		"
	sql = sql & vbCrlf & "                                   a.order_code,			"
	sql = sql & vbCrlf & "                                   a.cont_code,		"
	sql = sql & vbCrlf & "                                   a.cus_code,		"
	sql = sql & vbCrlf & "                                   a.shop_code,		"
	sql = sql & vbCrlf & "                                   a.mem_id,			"
	sql = sql & vbCrlf & "                                   a.wdate,		"
	sql = sql & vbCrlf & "                                   a.order_opt,		"
	sql = sql & vbCrlf & "                                   a.b_zipcode,		"
	sql = sql & vbCrlf & "                                   a.b_add1,		"
	sql = sql & vbCrlf & "                                   a.b_add2,		"
	sql = sql & vbCrlf & "                                   a.order_type,			"
	sql = sql & vbCrlf & "                                   a.as_type,		"
	sql = sql & vbCrlf & "                                   a.as_why,		"
	sql = sql & vbCrlf & "                                   a.as_etc,		"
	sql = sql & vbCrlf & "                                   a.free_type,		"
	sql = sql & vbCrlf & "                                   a.ka_no,			"
	sql = sql & vbCrlf & "                                   a.free_etc,			"
	sql = sql & vbCrlf & "                                   a.odate,		"
	sql = sql & vbCrlf & "                                   a.otime,			"
	sql = sql & vbCrlf & "                                   a.sdate,			"
	sql = sql & vbCrlf & "                                   a.edate,			"
	sql = sql & vbCrlf & "                                   a.sigong_type,		"
	sql = sql & vbCrlf & "                                   a.imsi_code,		"
	sql = sql & vbCrlf & "                                   a.dn_no,			"
	sql = sql & vbCrlf & "                                   a.bp_cd,			"
	sql = sql & vbCrlf & "                                   a.out_status,		"
	sql = sql & vbCrlf & "                                   a.now_ver,		"
	sql = sql & vbCrlf & "                                   a.ready_date,		"
	sql = sql & vbCrlf & "                                   a.inhouse,		"
	sql = sql & vbCrlf & "                                   a.inhouse_std_amt,		"
	sql = sql & vbCrlf & "                                   a.req_car_type,		"
	sql = sql & vbCrlf & "                                   a.out_memo,		"
	sql = sql & vbCrlf & "   								  sum(case when a.order_type = 'i33' or a.order_type = 'i41' then isnull(round((case when prod_code not like '%SAIBRCC%' and prod_code not in ('SAIOCPC0020', 'SAIOCPC0899') then b.prod_pprice else 0 end) * b.prod_count / 1.1, 0), 0) * -1		"
	sql = sql & vbCrlf & "   																						                                  else isnull(round((case when prod_code not like '%SAIBRCC%' and prod_code not in ('SAIOCPC0020', 'SAIOCPC0899') then b.prod_pprice else 0 end) * b.prod_count / 1.1, 0), 0) end) as order_pamt,  /* 품목공급가 */		"
	sql = sql & vbCrlf & "   								  sum(case when a.order_type = 'i33' or a.order_type = 'i41' then isnull(round((case when prod_code in ('SAIOCPC0020', 'SAIOCPC0899') then prod_price else 0 end) * b.prod_count, 0), 0) * -1		"
	sql = sql & vbCrlf & "   																						                                  else isnull(round((case when prod_code in ('SAIOCPC0020', 'SAIOCPC0899') then prod_price else 0 end) * b.prod_count, 0), 0) end) as order_delivery,   /* 배송비 */		"
	sql = sql & vbCrlf & "                                   c.wh_empnm		"
	sql = sql & vbCrlf & "                             from  sales_order a with(nolock),		"
	sql = sql & vbCrlf & "                                      sales_order_item b with(nolock),		"
	sql = sql & vbCrlf & "                                      (  select  za.id,							"
	sql = sql & vbCrlf & "                                                za.name,							"
	sql = sql & vbCrlf & "                                                za.group_empno,							"
	sql = sql & vbCrlf & "                                                zb.wh_empno,							"
	sql = sql & vbCrlf & "                                                zb.wh_empnm							"
	sql = sql & vbCrlf & "                                          from  sales_member za with(nolock)							"
	sql = sql & vbCrlf & "                                    left outer join  sales_delivery_wh_res zb with(nolock) on za.group_empno = zb.group_empno  ) c							"
	'sql = sql & vbCrlf & "                                    left outer join  sales_delivery_wh_res zb with(nolock) on za.group_empno = zb.group_empno							"
	'sql = sql & vbCrlf & "                                         where  za.del_chk <> 'Y' ) c							"
	sql = sql & vbCrlf & "                            where  a.order_code = b.order_code		"
	sql = sql & vbCrlf & "                                and  a.ver = b.ver		"
	sql = sql & vbCrlf & "                                and  a.mem_id = c.id		"
	sql = sql & vbCrlf & "                                and  " & search_str
	sql = sql & vbCrlf & "                         group by  a.idx,		"
	sql = sql & vbCrlf & "                                   a.order_code,			"
	sql = sql & vbCrlf & "                                   a.cont_code,		"
	sql = sql & vbCrlf & "                                   a.cus_code,		"
	sql = sql & vbCrlf & "                                   a.shop_code,		"
	sql = sql & vbCrlf & "                                   a.mem_id,			"
	sql = sql & vbCrlf & "                                   a.wdate,		"
	sql = sql & vbCrlf & "                                   a.order_opt,		"
	sql = sql & vbCrlf & "                                   a.b_zipcode,		"
	sql = sql & vbCrlf & "                                   a.b_add1,		"
	sql = sql & vbCrlf & "                                   a.b_add2,		"
	sql = sql & vbCrlf & "                                   a.order_type,			"
	sql = sql & vbCrlf & "                                   a.as_type,		"
	sql = sql & vbCrlf & "                                   a.as_why,		"
	sql = sql & vbCrlf & "                                   a.as_etc,		"
	sql = sql & vbCrlf & "                                   a.free_type,		"
	sql = sql & vbCrlf & "                                   a.ka_no,			"
	sql = sql & vbCrlf & "                                   a.free_etc,			"
	sql = sql & vbCrlf & "                                   a.odate,		"
	sql = sql & vbCrlf & "                                   a.otime,			"
	sql = sql & vbCrlf & "                                   a.sdate,			"
	sql = sql & vbCrlf & "                                   a.edate,			"
	sql = sql & vbCrlf & "                                   a.sigong_type,		"
	sql = sql & vbCrlf & "                                   a.imsi_code,		"
	sql = sql & vbCrlf & "                                   a.dn_no,			"
	sql = sql & vbCrlf & "                                   a.bp_cd,			"
	sql = sql & vbCrlf & "                                   a.out_status,		"
	sql = sql & vbCrlf & "                                   a.cus_code,		"
	sql = sql & vbCrlf & "                                   a.mem_id,		"
	sql = sql & vbCrlf & "                                   a.now_ver,		"
	sql = sql & vbCrlf & "                                   a.ready_date,		"
	sql = sql & vbCrlf & "                                   a.inhouse,		"
	sql = sql & vbCrlf & "                                   a.inhouse_std_amt,		"
	sql = sql & vbCrlf & "                                   a.req_car_type,		"
	sql = sql & vbCrlf & "                                   a.out_memo,	"
	sql = sql & vbCrlf & "                                   c.wh_empnm		"
	sql = sql & vbCrlf & "                           ) ta		"
	sql = sql & vbCrlf & "         left outer join  sales_trans tb with(nolock) on ta.order_code = tb.order_code			"
	sql = sql & vbCrlf & "                  where  " & trans_search_str	
	sql = sql & vbCrlf & "             ) tta		"
	sql = sql & vbCrlf & "       where  tta.rownum >=  " & startNum
	sql = sql & vbCrlf & "    order by  tta.rownum		"
end If
'response.write sql
Set rs = db.execute (sql)
If Not rs.eof Then
	num = recordcount - bs
	rownum = 1
	Do Until rs.eof 
		idx = Trim(rs("idx"))
		v_order_code = Trim(rs("order_code"))
		cont_code = Trim(rs("cont_code"))
		cus_code = Trim(rs("cus_code"))
		shop_code = Trim(rs("shop_code"))
		mem_id = Trim(rs("mem_id"))
		wdate = Trim(rs("wdate"))
		order_opt = Trim(rs("order_opt"))
		b_zipcode = Trim(rs("b_zipcode"))
		b_add1 = Trim(rs("b_add1"))
		b_add2 = Trim(rs("b_add2"))
		b_order_type = Trim(rs("order_type"))
		as_type = Trim(rs("as_type"))
		as_why = Trim(rs("as_why"))
		as_etc = Trim(rs("as_etc"))
		free_type = Trim(rs("free_type"))
		ka_no = Trim(rs("ka_no"))
		free_etc = Trim(rs("free_etc"))
		odate_v = Trim(rs("odate"))
		If odate_v = "1900-01-01" Then
			odate_v = ""
		End If
		
		otime = Trim(rs("otime"))
		Select Case otime
			Case "06"
				otime_v = "AM 06:00"
			Case "07"
				otime_v =  "AM 07:00"
			Case "08"
				otime_v =  "AM 08:00"
			Case "09"
				otime_v =  "AM 09:00"
			Case "10"
				otime_v =  "AM 10:00"
			Case "11"
				otime_v =  "AM 11:00"
			Case "12"
				otime_v =  "PM 12:00"
			Case "13"
				otime_v =  "PM 13:00"
			Case "14"
				otime_v =  "PM 14:00"
			Case "15"
				otime_v =  "PM 15:00"
			Case "16"
				otime_v =  "PM 16:00"
			Case "17"
				otime_v =  "PM 17:00"
			Case "18"
				otime_v =  "PM 18:00"
			Case "19"
				otime_v =  "PM 19:00"
			Case "20"
				otime_v =  "PM 20:00"
			Case "30"
				otime_v =  "오전중"
			Case "40"
				otime_v =  "오후중"
			Case Else 
				otime_v = otime
		End Select 

		sdate = Trim(rs("sdate"))
		If sdate = "1900-01-01" Then
			sdate = ""
		End If
		edate = Trim(rs("edate"))
		If edate = "1900-01-01" Then
			edate = ""
		End If
		sigong_type = Trim(rs("sigong_type"))
		imsi_code = Trim(rs("imsi_code"))
		DN_NO = Trim(rs("dn_no"))
		BP_CD = Trim(rs("bp_cd"))
		rs_out_status = Trim(rs("out_status"))
		cname = Trim(rs("cname"))
'		scom_company = Trim(rs(27))
		'sk_cost = Trim(rs(28))
		ccom = Trim(rs("ccom"))
		group_name = Trim(rs("group_name"))
		first_wdate = Trim(rs("first_wdate"))
		now_ver = Trim(rs("now_ver"))
		inhouse_std_amt = Trim(rs("inhouse_std_amt"))
		inhouse_std_amt_lock = Trim(rs("inhouse_std_amt_lock"))
		req_car_type  = Trim(rs("req_car_type"))
		out_memo  = Trim(rs("out_memo"))
		wh_empnm  = Trim(rs("wh_empnm"))

		trans_idx = Trim(rs("trans_idx"))
		trans_type1 = Trim(rs("trans_type1"))
		trans_type1_lock = Trim(rs("trans_type1_lock"))
		trans_type2 = Trim(rs("trans_type2"))
		trans_type3 = Trim(rs("trans_type3"))
		remark = Trim(rs("remark"))
		trans_code = Trim(rs("trans_code"))
		pack_yn = Trim(rs("pack_yn"))
		pack_order_code = Trim(rs("pack_order_code"))
		inhouse = Trim(rs("inhouse"))
		amt_type = Trim(rs("amt_type"))
		ready_date = Trim(rs("ready_date"))
		If ready_date = "1900-01-01" Then
			ready_date = ""
		End If

		order_pamt = Trim(rs("order_pamt"))
		order_delivery = Trim(rs("order_delivery"))


		sql = " select t_name from sales_order_type with(nolock) where t_code = '" & b_order_type & "' "
		Set rs2 = db.execute (sql)
		If Not rs2.eof Then
			Select Case b_order_type 
				Case "I3C", "I3AA", "I5C", "I5AA", "I7C", "I7AA"
					order_type_str = "<font color='blue'>" & rs2("t_name") & "</font>"
				Case Else 
					order_type_str = rs2("t_name")
			End Select 
		Else 
			order_type_str = b_order_type
		End If
		rs2.close
		Set rs2 = Nothing

		order_code_str = v_order_code
		Select Case order_opt
			Case "plan"
				order_opt_str = "견적"
			Case "frder"
				order_opt_str = "<font color='red'>임시오더</font>"
			Case "order"
				order_opt_str = "<b>확정오더</b>"
				'If DN_NO <> "" Then
					'sql = " select SHIP_TO_PARTY, SALES_GRP, SALES_ORG, MOV_TYPE, DLVY_DT, PROMISE_DT, COST_CD, SHIP_TO_PLCE, STP_INFO_NO from S_DN_HDR where DN_NO='" & DN_NO & "' "
					'Set rs2 = db2.execute (sql)
					'If Not rs2.eof Then
						'SHIP_TO_PARTY = Trim(rs2(0))
						'SALES_GRP = Trim(rs2(1))
						'SALES_ORG = Trim(rs2(2))
						'MOV_TYPE = Trim(rs2(3))
						'DLVY_DT = Trim(rs2(4))
						'PROMISE_DT = Trim(rs2(5))
						'COST_CD = Trim(rs2(6))
						'SHIP_TO_PLCE = Trim(rs2(7))
						'STP_INFO_NO = Trim(rs2(8))
					'End If
					'rs2.close
					'Set rs2 = Nothing
				'End If 
				''If DN_NO <> "" Then 
				''	order_code_str = DN_NO
				''End If 
			Case else
				order_opt_str = ""
		End Select

			'If BP_CD <> "" Then
				'sql = " select BP_RGST_NO, BP_FULL_NM, REPRE_NM, REPRE_RGST_NO, ZIP_CD, ADDR1, ADDR2, IND_TYPE, IND_CLASS, TEL_NO1, TEL_NO2, FAX_NO from B_BIZ_PARTNER where BP_CD='" & BP_CD & "' "
				'Set rs2 = db2.execute (sql)
				'If Not rs2.eof Then
					'BP_RGST_NO = Trim(rs2(0))
					'BP_FULL_NM = Trim(rs2(1))
					'REPRE_NM = Trim(rs2(2))
					'REPRE_RGST_NO = Trim(rs2(3))
					'ZIP_CD = Trim(rs2(4))
					'ADDR1 = Trim(rs2(5))
					'ADDR2 = Trim(rs2(6))
					'IND_TYPE = Trim(rs2(7))
					'IND_CLASS = Trim(rs2(8))
					'TEL_NO1 = Trim(rs2(9))
					'TEL_NO2 = Trim(rs2(10))
					'FAX_NO = Trim(rs2(11))

					'cname = BP_FULL_NM		'// 고객명
					'company = BP_FULL_NM
					'zipcode = ZIP_CD
					'add1 = ADDR1
					'add2 = ADDR2
					'hp = TEL_NO1
					'tel = TEL_NO2
					'jumin_no = BP_RGST_NO
					'tae = IND_CLASS
					'jong = IND_TYPE
					'com_jumin = REPRE_RGST_NO

				'End If
				'rs2.close
				'Set rs2 = Nothing
				'cus_code = BP_CD
			'End If 


			Select Case rs_out_status
				Case "yes"
					out_color = "style=""background:#E4F7BA;"""
				Case "re"
					out_color = "style=""background:#FFE7AA;"""
				Case "rdy"
					out_color = "style=""background:#90e00e;"""
				Case "pac"
					out_color = "style=""background:#33cc00;"""
				Case Else
					out_color = ""
			End Select 
%>
						<tr <%=out_color%> class="top-line">
							<td><%=rownum%></td>
							<td style="font-size:8pt;"><%=order_code_str%></td>
							<td style="font-size:8pt;"><%=order_opt_str%></td>
							<td style="font-size:8pt;"><%=order_type_str%></td>
							<td style="text-align:left; font-size:8pt;" colspan="2">&nbsp;<%=Left(b_add1&b_add2,20)%></td>
							<td style="text-align:right; font-size:8pt; padding-right:5px;"><%If IsNumeric(order_pamt) Then response.write FormatNumber(order_pamt, 0) Else response.write order_pamt End If %></td>
							<td style="text-align:right; font-size:8pt; padding-right:5px;"><%If IsNumeric(order_delivery) Then response.write FormatNumber(order_delivery, 0) Else response.write order_delivery End If %></td>
							<td style="font-size:8pt;"><%=otime_v%><input type="hidden" name="otime_v<%=idx%>" id="otime_v<%=idx%>" value="<%=otime_v%>"></td>
							<td style="font-size:8pt;"><%=ccom%></td>
							<td style="font-size:8pt;"><%=odate_v%><input type="hidden" name="odate_v<%=idx%>" id="odate_v<%=idx%>" value="<%=odate_v%>"></td>
							<td>
								<%Select Case rs_out_status %>
									<%Case "yes"%>
										<input type="button" class="list" value="출고" onClick="item_out2('re', '<%=idx%>');" >
									<%Case "re"%>
										<input type="button" class="list" value="재확인" onClick="item_out2('no', '<%=idx%>');" >
									<%Case "rdy"%>
										<input type="button" class="list" value="출고준비" onClick="item_out2('pac', '<%=idx%>');" >
									<%Case "pac"%>
										<input type="button" class="list" value="포장완료" onClick="item_out2('sta', '<%=idx%>');" >
									<%Case else%>
										<input type="button" class="list" value="미출고" onClick="item_out2('rd', '<%=idx%>');" >
								<%End Select %>
							</td>
							<td style="font-size:8pt;">
								<input type="text" idx="<%=idx%>" name="ready_date" id="ready_date<%=idx%>" value="<%=ready_date%>" class="input01" style="width:70px; text-align:center;" onChange="fnSave('<%=idx%>');" />
							</td>
							<td>								
								<input type="button" class="list" value="출고요청서" onClick="item_print2('out', '<%=idx%>');" >
							</td>
						</tr>
						<tr <%=out_color%>>
							<td rowspan="2">
								<input type="hidden" name="idx<%=idx%>" id="idx<%=idx%>" value="<%=idx%>">
								<input type="hidden" name="order_code<%=idx%>" id="order_code<%=idx%>" value="<%=order_code_str%>">
								<input type="checkbox" name="chk_idx" value="<%=idx%>" class="radio" >
							</td>
							<td style="font-size:8pt;" colspan="3">
								<input type="text" name="trans_type1<%=idx%>" id="trans_type1<%=idx%>" value="<%=trans_type1%>" style="width:200px;" onChange="javascript:$(this).next().prop('checked', true); fnSave('<%=idx%>');" />
								<input type="checkbox" name="trans_type1_lock<%=idx%>" id="trans_type1_lock<%=idx%>" value="Y" <%If trans_type1_lock = "Y" then Response.Write "checked" End If %> onChange="fnSave('<%=idx%>');" />
							</td>
							<td style="font-size:8pt;">
								<label><input type="radio" name="inhouse<%=idx%>" value="O" <%If inhouse = "O" then Response.Write "checked" End If %> onChange="fnSave('<%=idx%>');" /> O</label>
								&nbsp;&nbsp;
								<label><input type="radio" name="inhouse<%=idx%>" value="X" <%If inhouse = "X" then Response.Write "checked" End If %> onChange="fnSave('<%=idx%>');" /> X</label>
							</td>
							<td style="font-size:8pt;">
								<input type="text" name="inhouse_std_amt<%=idx%>" id="inhouse_std_amt<%=idx%>" value="<%=inhouse_std_amt%>" style="width:60px; text-align:center;" onChange="javascript:$(this).next().prop('checked', true); fnSave('<%=idx%>');" />
								<input type="checkbox" name="inhouse_std_amt_lock<%=idx%>" id="inhouse_std_amt_lock<%=idx%>" value="Y" <%If inhouse_std_amt_lock = "Y" then Response.Write "checked" End If %> onChange="fnSave('<%=idx%>');" />
							</td>
							<td style="font-size:8pt;">
								<label><input type="radio" name="amt_type<%=idx%>" value="선불" <%If amt_type = "선불" then Response.Write "checked" End If %> onChange="fnSave('<%=idx%>');" /> 선불</label><br>
								<label><input type="radio" name="amt_type<%=idx%>" value="착불" <%If amt_type = "착불" then Response.Write "checked" End If %> onChange="fnSave('<%=idx%>');" /> 착불</label>
							</td>
							<td style="font-size:8pt;">
								<select name="trans_code<%=idx%>" id="trans_code<%=idx%>" onChange="fnSave('<%=idx%>');" >
									<option value=""></option>
									<%
										sql = ""
										sql = sql & vbCrlf & "  	select  1 as sort,		"
										sql = sql & vbCrlf & "              id,		"
										sql = sql & vbCrlf & "              com_name		"
										sql = sql & vbCrlf & "        from  sales_member with(nolock)		"
										sql = sql & vbCrlf & "       where  site = 'bshop'		"
										sql = sql & vbCrlf & "         and  del_chk <> 'Y'		"
										sql = sql & vbCrlf & "   union all		"
										sql = sql & vbCrlf & "  	select  2 as sort,		"
										sql = sql & vbCrlf & "              '경남CDC',			"
										sql = sql & vbCrlf & "              '경남CDC'		"
										sql = sql & vbCrlf & "   union all		"
										sql = sql & vbCrlf & "  	select  3 as sort,		"
										sql = sql & vbCrlf & "              '경북CDC',			"
										sql = sql & vbCrlf & "              '경북CDC'		"
										sql = sql & vbCrlf & "   union all		"
										sql = sql & vbCrlf & "  	select  4 as sort,		"
										sql = sql & vbCrlf & "              '기타업체',			"
										sql = sql & vbCrlf & "              '기타업체'		"
										sql = sql & vbCrlf & "   union all		"
										sql = sql & vbCrlf & "  	select  5 as sort,		"
										sql = sql & vbCrlf & "              '방문수령',			"
										sql = sql & vbCrlf & "              '방문수령'		"
										sql = sql & vbCrlf & "    order by  sort,		"
										sql = sql & vbCrlf & "              com_name		"
										'Response.Write sql
										Set rs2 = db.execute (sql)
										Do Until rs2.bof Or rs2.eof
									%>
											<option value="<%=rs2("id")%>" <%If trans_code = rs2("id") Then response.write "selected" End If %>><%=rs2("com_name")%></option>
									<%
											rs2.movenext
										Loop 
										rs2.close
										Set rs2 = Nothing
									%>
								</select>
							</td>
							<td style="font-size:8pt;">
								<input type="hidden" name="req_car_type<%=idx%>" id="req_car_type<%=idx%>" value="<%=req_car_type%>" />
								<%=req_car_type%>
							</td>
							<td style="font-size:8pt;"><%=group_name%></td>
							<td style="font-size:8pt;"><%=cname%></td>
							<td style="font-size:8pt;"><%=first_wdate%></td>
							<td style="font-size:8pt;"><%=wdate%></td>
							<td style="font-size:8pt;">
								<label><input type="checkbox" name="pack_yn<%=idx%>" id="pack_yn<%=idx%>" value="Y" onclick="javascript:chk_pack('<%=idx%>');" <%If pack_yn = "Y" then Response.Write "checked" End If %>  onChange="fnSave('<%=idx%>');" /> 합적출고</label><br>
								<input type="text" name="pack_order_code<%=idx%>" id="pack_order_code<%=idx%>" value="<%=pack_order_code%>" style="width:70px;" <%If pack_yn <> "Y" then Response.Write "disabled" End If %>  onChange="fnSave('<%=idx%>');" />
							</td>
						</tr>
						<tr <%=out_color%> class="bottom-line">
							<td style="font-size:8pt;" colspan="2">
								<label><input type="checkbox" name="trans_emer<%=idx%>" id="trans_emer<%=idx%>" value="Y" <%If trans_emer = "Y" then Response.Write "checked" End If %> onChange="fnSave('<%=idx%>');" /> 긴급</label>
								&nbsp;&nbsp;&nbsp;&nbsp;
								<label><input type="checkbox" name="trans_shuttle<%=idx%>" id="trans_shuttle<%=idx%>" value="Y" <%If trans_shuttle = "Y" then Response.Write "checked" End If %> onChange="fnSave('<%=idx%>');" /> 왕복</label>
							</td>
							<td style="font-size:8pt;" colspan="3">
								<input type="text" name="out_memo<%=idx%>" id="out_memo<%=idx%>" value="<%=out_memo%>" style="width:220px;"  onmouseover = "etc_show(this, '<%=Replace("" & out_memo, Chr(13)&Chr(10), "<br>")%>');" onmouseout = "etc_hide();"/>
							</td>
							<td style="font-size:8pt;" colspan="7">
								<input type="text" name="remark<%=idx%>" id="remark<%=idx%>" value="<%=remark%>" style="width:532px;" onChange="fnSave('<%=idx%>');" />
							</td>
							<td style="font-size:8pt;">
								<b><%=wh_empnm%></b>
							</td>
						</tr>

<%
		rownum = rownum + 1
		rs.movenext
	Loop
End If
rs.close
Set rs = Nothing
%>
						</table>
						<table align="center">
							<tr>
								<td colspan="10" align="center">
								<% 
								gopage = "bathplan_onc_system.asp?act=" & act & "&t=" & t & "&m=" & m & "&print_opt=" & print_opt & "&order_code=" & order_code & "&syear=" & syear & "&smonth=" & smonth & "&sday=" & sday & "&eyear=" & eyear & "&emonth=" & emonth & "&eday=" & eday & "&out_status=" & out_status & "&order_sta=" & order_sta & "&order_type=" & order_type & "&selKeyWordType=" & selKeyWordType & "&txtKeyWord=" & txtKeyWord & "&groupcode=" & groupcode & "&trans_yn=" & trans_yn & "&rdoOType=" & rdoOType & "&sh_trans_code=" & sh_trans_code & "&wh_empno=" & wh_empno & "&"
								if search <> "" then
									GotoPageInSearchResult page, pageCount, search, searchstring, gopage
								else
									GoToPageDirectly page, pageCount, gopage
								end If
								%>
								</td>
							</tr>
						</table>
					</form>
					<form name="jForm" >
						<input type="hidden" name="i" value="">
						<input type="hidden" name="t" value="">
					</form>

<%End if%>
<% If Instr(Request.ServerVariables("SERVER_NAME"), "test.bathplan.com") >= 1 Then %>
	<iframe id="jFrame" name="jFrame" src="" width="100%" height="200" style="display:block;"></iframe>
<% Else %>
	<iframe id="jFrame" name="jFrame" src="" width="100%" height="200" style="display:none;"></iframe>
<% End If %>


<%@Language="VBScript" CODEPAGE="65001"%>
<!-- #include virtual = "/share/include/DbInfo.asp" -->
<!-- #include virtual = "/share/include/Injection.asp" -->
<!-- #include virtual = "/share/include/Function.asp" -->
<!-- #include virtual = "/share/include/Const.asp" -->
<!-- #include virtual = "/share/include/PageHeader.asp" -->
<!-- #include virtual = "/share/include/ContentsHeader.asp" -->

<%
        '// Paging 기준값
        PageSize = 10           '// 페이지당 보여줄 데이타수
        BlockSize = 5             '// 페이지 그룹 범위       1 2 3 5 6 7 8 9 10
%>

    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">고객관리</a>
        </li>
        <li class="breadcrumb-item active">고객조회</li>
      </ol>

	  <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
			<form id="Search_Form">
                <input type="hidden" class="form-control" name="Page" value="1">
                <input type="hidden" class="form-control" name="PageSize" value="<%=PageSize%>">
				<div class="row justify-content-between">
					<div class="col-xl-3 col-sm-6 mb-3">
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text">고객정보</span>
							</div>
							<input type="text" class="form-control" name="CusInfo">
						</div>
					</div>
					<div class="col-xl-3 col-sm-6">
						<button type="submit" class="btn btn-secondary mary btn-block" onclick="$('#Search_Form').find('input[name=Page]').val(1);"><i class="fa fa-search" aria-hidden="true"></i> <span>조회</span></button>
					</div>
				</div>
			</form>
		</div>
        <div class="card-body" id="CusList_MainCustomerForm">
            <div class="list-group">

            </div>
        </div>
        <div class="card-footer" id="Paging_MainCustomerForm">
		</div>
      </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->

	<!-- modal -->
	<!-- Update Modal HTML -->
	<div id="updateCustomerModal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<form id="updateModalForm">
					<input type="hidden" name="idx" id="idx_UpdateModal">
					<div class="modal-header">
						<h4 class="modal-title">고객정보 수정</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="col-6">
								<div class="form-group">
									<label>고객명</label>
									<input type="text" class="form-control" name="name" id="name_UpdateModal" value="" required>
								</div>
							</div>
							<div class="col-6">
								<div class="form-group">
									<label>연락처</label>
									<input type="text" class="form-control" name="hp" id="hp_UpdateModal" required>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label>현장정보</label>
							<div class="input-group mb-1">
								<input type="hidden" class="form-control" name="zipcode" id="zipcode_UpdateModal">
								<input type="text" class="form-control" name="add1" id="add1_UpdateModal" required>
								<div class="input-group-append">
									<button class="btn btn-outline-secondary" type="button" id="btnDaumAddr_UpdateModal">주소찾기</button>
								</div>
							</div>
							<div id="wrap_UpdateModal" style="display:none;border:1px solid;width:100%;height:300px;margin:5px 0;position:relative">
								<img src="//t1.daumcdn.net/localimg/localimages/07/postcode/320/close.png" id="btnFoldWrap" style="cursor:pointer;position:absolute;right:0px;top:-1px;z-index:1" onclick="foldDaumPostcode('wrap_UpdateModal')" alt="접기 버튼">
							</div>
							<input type="text" class="form-control" name="add2" id="add2_UpdateModal" required>
						</div>
					</div>
					<div class="modal-footer">
						<button type="submit" class="btn btn-success"><i class="fa fa-check" aria-hidden="true"></i> <span>저장</span></button>
						<button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-undo" aria-hidden="true"></i> <span>취소</span></button>
						<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#delCustomerModal" id="btnDel_UpdateModal"><i class="fa fa-times" aria-hidden="true"></i> <span>삭제</span></button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- Delete Modal HTML -->
	<div id="delCustomerModal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<form id="delModalForm">
					<div class="modal-header">						
						<h4 class="modal-title"><i class="fa fa-trash" aria-hidden="true"></i> <span>삭제</span></h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">					
						<p>삭제 하시겠습니까?</p>
						<p class="text-warning"><small>삭제시 복구 불가능 합니다.</small></p>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-undo" aria-hidden="true"></i> <span>취소</span></button>
						<button type="submit" class="btn btn-danger"><i class="fa fa-times" aria-hidden="true"></i> <span>삭제</span></button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- /.modal -->

<!-- #include virtual = "/share/include/ContentsAddon.asp" -->
<!-- #include virtual = "/share/include/ContentsFooter.asp" -->
<!-- #include virtual = "/share/include/JavaScript.asp" -->

<script type="text/javascript">
<!--
	$(document).ready(function() {

		$("#Search_Form").ajaxForm({
			url: "/data/CUS/Customer.List.Json.asp",
            data: {},
            type: "POST",
			dataType: "json",
			beforeSend:function(){
                $(".fa").addClass("fa-spinner fa-spin");
			},
			complete:function(){
				$(".fa").removeClass("fa-spinner fa-spin");
			},
            beforeSubmit: function (data,form,option) {
                //validation체크 
                //막기위해서는 return false를 잡아주면됨
                return true;
            },
            success: function(data, status){
                var info = "";
                var maxCnt = 0;
                $.each(data, function () {
                    maxCnt = this.max_cnt;
                    info += "  <a href='#updateCustomerModal' data-toggle='modal' class='list-group-item list-group-item-action' cusInfo='" + JSON.stringify(this) + "'>";
                    info += "    <div class='row'>";
                    info += "       <div class='col-6 col-xl-2'>";
                    info += "           <small class='text-muted'>고객명</small>";
                    info += "           <p class='mb-0'>" + this.name + "</p>";
                    info += "       </div>";
                    info += "       <div class='col-6 col-xl-2'>";
                    info += "           <small class='text-muted'>연락처</small>";
                    info += "           <p class='mb-0'>" + this.hp + "</p>";
                    info += "       </div>";
                    info += "       <div class='col-6 col-xl-2'>";
                    info += "           <small class='text-muted'>우편번호</small>";
                    info += "           <p class='mb-0'>" + this.zipcode + "</p>";
                    info += "       </div>";
                    info += "       <div class='col-6 col-xl-2'>";
                    info += "           <small class='text-muted'>기본주소</small>";
                    info += "           <p class='mb-0'>" + this.add1 + "</p>";
                    info += "       </div>";
                    info += "       <div class='col-12 col-xl-4'>";
                    info += "           <small class='text-muted'>상세주소</small>";
                    info += "           <p class='mb-0'>" + this.add2 + "</p>";
                    info += "       </div>";
                    info += "    </div>";
                    info += "  </a>";
                });

                $("#CusList_MainCustomerForm").find(".list-group").html(info);
                var NowPage = $("#Search_Form").find("input[name=Page]").val();
                var page_viewList = Paging(maxCnt, '<%=PageSize%>', '<%=BlockSize%>', NowPage);
                $("#Paging_MainCustomerForm").html(page_viewList);
            },
            error: function(){
				alert("오류 발생!");
            }                               
        });

		$("#updateModalForm").ajaxForm({
			url: "/data/CUS/Customer.Update.Proc.asp",
			type: "POST",
			dataType: "text",
			beforeSend:function(){
				$(".fa").addClass("fa-spinner fa-spin");
			},
			complete:function(){
				$(".fa").removeClass("fa-spinner fa-spin");
			},
            beforeSubmit: function (data,form,option) {
                //validation체크 
                //막기위해서는 return false를 잡아주면됨
                return true;
            },
            success: function(response, status){
                //성공후 서버에서 받은 데이터 처리
				if (response == "OK")
				{
					alert("저장 되었습니다.");
                    $("#Search_Form").submit();
					$("#updateCustomerModal").modal('hide');
				}
            },
            error: function(){
				alert("오류 발생!");
            }                               
        });

		$("#delModalForm").ajaxForm({
			url: "/data/CUS/Customer.Delete.Proc.asp",
			type: "POST",
			data: {
				idx: function() { return $("#idx_UpdateModal").val() }
			},
			dataType: "text",
			beforeSend:function(){
				$(".fa").addClass("fa-spinner fa-spin");
			},
			complete:function(){
				$(".fa").removeClass("fa-spinner fa-spin");
			},
            beforeSubmit: function (data,form,option) {
                //validation체크 
                //막기위해서는 return false를 잡아주면됨
                return true;
            },
            success: function(response, status){
                //성공후 서버에서 받은 데이터 처리
				if (response == "OK")
				{
					alert("삭제 되었습니다.");
                    $("#Search_Form").submit();
					$("#delCustomerModal").modal('hide');
					$("#updateCustomerModal").modal('hide');
				}
            },
            error: function(){
				alert("오류 발생!");
            }                               
        });

		$(document).on("click", "#btnDaumAddr_UpdateModal", function (e) {
			execDaumPostcode_Inline("wrap_UpdateModal", "zipcode_UpdateModal", "add1_UpdateModal", "add2_UpdateModal");
        });

		$(document).on("click", "nav  a[name=pageGoto]", function (e) {
            var page = $(this).attr("target-page");
            $("#Search_Form").find("input[name=Page]").val(page);
            $("#Search_Form").submit();
        });

        $(document).on("show.bs.modal", "#updateCustomerModal", function (e) {
            var Item = JSON.parse($(e.relatedTarget).attr("cusInfo"));

            if (Item.idx == "")
			{
                alert("잘못된 접근입니다.");
                return;
			}
			else 
            {
                $("#updateCustomerModal").find("#idx_UpdateModal").val(Item.idx);
                $("#updateCustomerModal").find("#name_UpdateModal").val(Item.name);
                $("#updateCustomerModal").find("#hp_UpdateModal").val(Item.hp);
                $("#updateCustomerModal").find("#zipcode_UpdateModal").val(Item.zipcode);
                $("#updateCustomerModal").find("#add1_UpdateModal").val(Item.add1);
                $("#updateCustomerModal").find("#add2_UpdateModal").val(Item.add2);
			}
		}).on("hidden.bs.modal", "#updateCustomerModal", function (e) {
			$("#updateModalForm")[0].reset();
		});

	});

//-->
</script>

<!-- #include virtual = "/share/include/PageFooter.asp" -->

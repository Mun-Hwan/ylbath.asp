<%
	function str_chk(str)
		str = trim(str)

		str = replace(str, "'", "''")
		str = replace(str, ";", "")
		str = replace(str, "--", "")
		str = replace(str, "%", "")
		str = replace(str, "^", "")
		str = replace(str, ",", "")
		str = replace(str, "[]", "")
		'str = replace(str, "@", "")
		'str = replace(str, "+", "")
		'str = replace(str, "@@", "")
		str = replace(str, "PRINT", "")
		str = replace(str, "SET", "")
		str = replace(str, "UNION", "")
		str = replace(str, "AND", "")
		str = replace(str, "INSERT", "")
		str = replace(str, "DELETE", "")
		str = replace(str, "javascript", "xxx")
		str = replace(str, "<script", "<xxx")
		str = replace(str, "<object", "<xxx")
		str = replace(str, "<applet", "<xxx")
		str = replace(str, "<iframe", "<xxx")
		str = replace(str, "<xml", "<xxx")
		str = replace(str, "<form", "<xxx")
		str = replace(str, "<embed", "<xxx")
		str = replace(str, "<", "[")
		str = replace(str, ">", "]")

		str_chk = str
	End Function

	' 파일 저장시 같은 이름의 파일이 있을경우..
	' 파일이름 뒤에 숫자 붙이는 함수
	Function SaveFile(bo_path, FilePath)
		dim fs
		Set fs = CreateObject("Scripting.FileSystemObject")

		dim FileName	: FileName	= Mid(FilePath, InstrRev(FilePath, "\") + 1)	
		dim strName		: strName	= Mid(FileName, 1, Instr(FileName, ".") - 1)	
		dim strExt			: strExt	= Mid(FileName, Instr(FileName, ".") + 1)		

		'FileName = strName & "." & strExt 
		'strName	= Mid(FileName, 1, Instr(FileName, ".") - 1)
		if LCase(strExt) = "asp" Or LCase(strExt) = "aspx" Or LCase(strExt) = "html" Or LCase(strExt) = "htm" Or LCase(strExt) = "exe" Or LCase(strExt) = "bat" Or LCase(strExt) = "com" Or LCase(strExt) = "msi" then
			strExt = "xxx"
			FileName = year(date) & month(date) & day(date) & Hour(Now) & Minute(Now) & Second(Now) & "." & strExt 
		else
			FileName = year(date) & month(date) & day(date) & Hour(Now) & Minute(Now) & Second(Now) & "." & strExt 
		end if
		strName	= Mid(FileName, 1, Instr(FileName, ".") - 1)

		dim fExist : fExist = True

		dim strFileNameTemp		: strFileNameTemp	= bo_path & FileName
		dim CountFileName		: CountFileName		= ""

		Do While fExist = True
			If fs.FileExists(strFileNameTemp) = True Then
				CountFileName = CountFileName  + "1"       
				strFileNameTemp =  bo_path & strName & CountFileName & "." & strExt 
				FileName = strName & CountFileName & "." & strExt 
			else  
				FileName = FileName
				fExist = False
			End If
		loop

		SaveFile = FileName

		Set fs =  Nothing
		Exit Function

	End Function

	' 목표일을 strFormat에 맞춰서 출력
	' strFormat 예약 키워드: {m}월, {d}일, {n}출력월, {w}주차
	Function PrintMonWeek(currDate, strFormat)
		Dim outMonth, monWeek, beginDay, lastDay, ocurrDate
	 
		' 목표일을 그주의 목요일로 변경
		ocurrDate = currDate
	'	If 5 > WeekDay(ocurrDate) Then 
	'		ocurrDate = DateAdd("d", 5 - WeekDay(ocurrDate), ocurrDate)
	'	ElseIf 5 < WeekDay(ocurrDate) Then 
	'		ocurrDate = DateAdd("d", (WeekDay(ocurrDate) - 5) * -1, ocurrDate)
	'	End If 

	'	Response.Write "ocurrDate : " & ocurrDate & "<br>"

		' 목표일 몇주차인지 구함
		monWeek = GetMonWeek(ocurrDate)

		if 0 < monWeek then
			' 목표일의 출력월을 구함
			outMonth = Month(ocurrDate)

			' 이전 달 마지막 날 기준으로 출력연도을 구함
			outYear = Year(ocurrDate)
		else
			' 0 = monWeek : 이전 달의 몇 주차에 속하는 것을 의미
	  
			' 현재 달의 시작일을 구함
			beginDay = CDate(Year(ocurrDate) & "-" & Month(ocurrDate) & "-01")
	  
			' 이전 달의 마지막 주 일요일을 구함
			lastDay = DateAdd("d", -1, beginDay)

			' 이전 달 마지막날이 몇주차인지 구함
			monWeek = GetMonWeek(lastDay)
	  
			' 이전 달 마지막 날 기준으로 출력월을 구함
			outMonth = Month(lastDay)

			' 이전 달 마지막 날 기준으로 출력연도을 구함
			outYear = Year(lastDay)
		end if

		' 출력 문자열 가공
		strFormat = Replace(strFormat, "{y}", Year(ocurrDate))
		strFormat = Replace(strFormat, "{m}", Month(ocurrDate))
		strFormat = Replace(strFormat, "{d}", Day(ocurrDate))
		strFormat = Replace(strFormat, "{u}", outYear)
		strFormat = Replace(strFormat, "{n}", outMonth)
		strFormat = Replace(strFormat, "{w}", monWeek)
	 
		' 출력 문자열 리턴 
		PrintMonWeek = strFormat
	End Function

	' 목표일이 몇주차인지 리턴
	' currDate: 목표일
	' 목요일을 기준으로 주를 나눔.
	Function GetMonWeek(currDate)
		Dim beginDay, beginWeek, currWeek

		' 현재 달의 시작일을 구함
		beginDay = CDate(Year(currDate) & "-" & Month(currDate) & "-01")

		' 시작일이 목요일을 지났으면 시작일을 다음주 목요일로 변경
		if 5 < WeekDay(beginDay) Then 
			beginDay = DateAdd("d", 12 - WeekDay(beginDay), beginDay)
		ElseIf 5 > WeekDay(beginDay) Then 
			beginDay = DateAdd("d", 5 - WeekDay(beginDay) , beginDay)
		End If 
	   
	'    ' 시작일이 목요일이 아니면 시작일을 첫번째 목요일로 변경
	'	If 5 > WeekDay(beginDay) Then 
	'		beginDay = DateAdd("d", 5 - WeekDay(beginDay), beginDay)
	'	ElseIf 5 < WeekDay(beginDay) Then 
	'		beginDay = DateAdd("d", (WeekDay(beginDay) - 5) * -1, beginDay)
	'	End If 
	 
		' 시작일의 올해 주 번호 구함
		beginWeek = DatePart("ww", beginDay)

		' 목표일의 올해 주 번호 구함
		currWeek = DatePart("ww", currDate)

		' 목표일이 이번 달 몇번째 주 인지 리턴
		GetMonWeek = (currWeek - beginWeek + 1)
	End Function

	' 목표주차가 몇일인지 리턴
	' currWeek: 목표주차 (2015-01-1) 2015년 1월 1주차의 목요일은 몇일
	Function GetMonDay(currWeek)
		Dim beginDay, beginWeek, currDay
		
		' 날짜 쪼개기
		aCurrWeek = Split(currWeek, "-")
		
		' 현재 달의 시작일을 구함
		beginDay = CDate(aCurrWeek(0) & "-" & aCurrWeek(1) & "-01")

		' 시작일이 목요일을 지났으면 시작일을 다음주 목요일로 변경
		if 5 < WeekDay(beginDay) Then 
			beginDay = DateAdd("d", 12 - WeekDay(beginDay), beginDay)
		ElseIf 5 > WeekDay(beginDay) Then 
			beginDay = DateAdd("d", 5 - WeekDay(beginDay) , beginDay)
		End If 
	 
		' 목표주차의 목요일 날짜 구하기
		currDay = DateAdd("d", (CInt(aCurrWeek(2)) - 1) * 7 , beginDay)

		GetMonDay = currDay
	End Function

    Sub GoToPageDirectly(page, Pagecount, gopage)
	
        Dim myBlodkEnd 
        Dim endNum : endNum = Right(page, 1)
    
        ' 현재 자신의 페이지 블럭에서 마지막 페이지 구하기.
        If (page Mod 10) = 0 Then
            myBlodkEnd = page
        Else
            myBlodkEnd = (Int(page) + 10) - Int(endNum)   '13 + 10 - 3  / 23 + 10 -3
        End If

	    ' 처음으로 가기
	    'if int(page) <> 1 then
	    '	response.write " <a href='./" & gopage & "page=1'>처음</a> "
	    'end if

        '이전 10개 기능 적용
        MainPage = ""
        If Int(myBlodkEnd) > 10 Then
            MainPage = MainPage & "<li class='page-item'>"
            MainPage = MainPage & "     <a class='page-link' href='#Page=" & myBlodkEnd-19 & "' tabindex='-1'>이전</a>"
            MainPage = MainPage & "</li>"
        Else
            MainPage = MainPage & "<li class='page-item disabled'>"
            MainPage = MainPage & "     이전"
            MainPage = MainPage & "</li>"
        End If

        '이전 페이지로 가기 기능 적용
        'if int(right(page,1)) = 1 then
        '    Response.Write " <a href='./" & gopage & "page=" & page - 1 & "'>이전</a> " 
        'end if

	    '페이지리스트 
        Dim i, endNumOfLoop
        If Int(pagecount) > Int(myBlodkEnd) Then
		    endNumOfLoop = myBlodkEnd
	    else
		    endNumOfLoop = Int(pagecount)
	    end if
	    
        For i = myBlodkEnd - 9 To endNumOfLoop
		    if i = int(page) then 
                MainPage = MainPage & "<li class='page-item disabled'>" & i & "</li>"
		    else
                MainPage = MainPage & "<li class='page-item'><a class='page-link' href='#'>" & i & "</a></li>"
		    end if
        Next
    
        '다음 페이지로 가기 기능 적용
        'if int(page) = endNumOfLoop then
        '    Response.Write " &nbsp;<a href='" & gopage & "page=" & page + 1 & "'>다음</a> " 
        'end if

        '다음 10개 기능 적용
        If Int(pagecount) > Int(myBlodkEnd) Then
            MainPage = MainPage & "<li class='page-item'>"
            MainPage = MainPage & "     <a class='page-link' href='#Page=" & myBlodkEnd+1 & "'>다음</a>"
            MainPage = MainPage & "</li>"
        Else
            MainPage = MainPage & "<li class='page-item disabled'>"
            MainPage = MainPage & "     다음"
            MainPage = MainPage & "</li>"
        End If

	    ' 마지막으로 가기
	    'if int(page) <> int(pagecount) and int(pagecount) <> 0 then
	    '	response.write " <a href='./" & gopage & "page=" & pagecount & "'>마지막</a> "
	    'end if


        '// 화면 구성하기
        Html = ""
        Html = Html & "<nav aria-label='Page navigation example'>"
        Html = Html & "     <ul class='pagination justify-content-center'>"
        Html = Html & MainPage      
        Html = Html & "     </ul>"
        Html = Html & "</nav>"

        Response.Write Html

    End Sub

%>
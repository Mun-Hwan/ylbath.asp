	<!-- Bootstrap core JavaScript-->
    <script src="/assets/vendor/jquery/jquery.min.js"></script>
    <script src="/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="/assets/vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Page level plugin JavaScript-->
    <script src="/assets/vendor/datatables/jquery.dataTables.js"></script>
    <script src="/assets/vendor/datatables/dataTables.bootstrap4.js"></script>
	<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
	<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/responsive/2.2.2/js/dataTables.responsive.min.js"></script>
	<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
	<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/locales/bootstrap-datepicker.kr.min.js"></script>
	<!-- <script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script> -->
    <!-- Ajax file Upload -->
	<script src="/assets/js/jquery.form.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="/assets/js/sb-admin.min.js"></script>
    <!-- Custom scripts for this page-->
    <script src="/assets/js/sb-admin-datatables.min.js"></script>
    <!-- Daum 우편번호 서비스 -->
	<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
	<!-- Younglim plugin JavaScript -->
	<script src="/share/js/jquery.ylbath.script.js?<%=year(date) & month(date) & day(date) & hour(now()) & minute(now()) & second(now())%>"></script>

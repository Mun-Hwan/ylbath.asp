<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title><%=SITE_TITLE%></title>
  <!-- Bootstrap core CSS-->
  <link href="/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="/assets/vendor/bootstrap/css/bootstrap.callout.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="/assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Page level plugin CSS-->
  <link href="/assets/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
  <link href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css" rel="stylesheet">
  <link href="https://cdn.datatables.net/responsive/2.2.2/css/responsive.dataTables.min.css" rel="stylesheet">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.standalone.css" rel="stylesheet"/>
  <!-- Custom styles for this template-->
  <link href="/assets/css/sb-admin.min.css" rel="stylesheet">

  <style type="text/css">
		/*.modal:nth-of-type(even) {
			z-index: 1042 !important;
		}
		.modal-backdrop.in:nth-of-type(even) {
			z-index: 1041 !important;
		}*/
  </style>
</head>


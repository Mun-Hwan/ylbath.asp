
jQuery(document).ready(function() {
	jQuery(document).on('show.bs.modal', '.modal', function()
	{
		var maxZ = parseInt(jQuery('.modal-backdrop').css('z-index')) || 1040;

		jQuery('.modal:visible').each(function()
		{
			maxZ = Math.max(parseInt(jQuery(this).css('z-index')), maxZ);
		});

		jQuery('.modal-backdrop').css('z-index', maxZ);
		jQuery(this).css("z-index", maxZ + 1);
		jQuery('.modal-dialog', this).css("z-index", maxZ + 2);
	});

	jQuery(document).on('hidden.bs.modal', '.modal', function () 
	{
		if (jQuery('.modal:visible').length)
		{
			jQuery(document.body).addClass('modal-open');

		   var maxZ = 1040;

		   jQuery('.modal:visible').each(function()
		   {
			   maxZ = Math.max(parseInt(jQuery(this).css('z-index')), maxZ);
		   });

		   jQuery('.modal-backdrop').css('z-index', maxZ-1);
	   }
	});
});

function execDaumPostcode(objZipCode, objAddr1, objAddr2) {
	new daum.Postcode({
		oncomplete: function(data) {
			// 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

			// 각 주소의 노출 규칙에 따라 주소를 조합한다.
			// 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
			var fullAddr = ''; // 최종 주소 변수
			var extraAddr = ''; // 조합형 주소 변수

			// 사용자가 선택한 주소 타입에 따라 해당 주소 값을 가져온다.
			if (data.userSelectedType === 'R') { // 사용자가 도로명 주소를 선택했을 경우
				fullAddr = data.roadAddress;

			} else { // 사용자가 지번 주소를 선택했을 경우(J)
				fullAddr = data.jibunAddress;
			}

			// 사용자가 선택한 주소가 도로명 타입일때 조합한다.
			if(data.userSelectedType === 'R'){
				//법정동명이 있을 경우 추가한다.
				if(data.bname !== ''){
					extraAddr += data.bname;
				}
				// 건물명이 있을 경우 추가한다.
				if(data.buildingName !== ''){
					extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
				}
				// 조합형주소의 유무에 따라 양쪽에 괄호를 추가하여 최종 주소를 만든다.
				fullAddr += (extraAddr !== '' ? ' ('+ extraAddr +')' : '');
			}

			// 우편번호와 주소 정보를 해당 필드에 넣는다.
			document.getElementById(objZipCode).value = data.zonecode; //5자리 새우편번호 사용
			document.getElementById(objAddr1).value = fullAddr;

			// 커서를 상세주소 필드로 이동한다.
			document.getElementById(objAddr2).focus();
		}
	}).open();
}

function foldDaumPostcode(objWrap) {
	// iframe을 넣은 element를 안보이게 한다.
	document.getElementById(objWrap).style.display = 'none';
}

function execDaumPostcode_Inline(objWrap, objZipCode, objAddr1, objAddr2) {
	// 현재 scroll 위치를 저장해놓는다.
	var currentScroll = Math.max(document.body.scrollTop, document.documentElement.scrollTop);
	new daum.Postcode({
		oncomplete: function(data) {
			// 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

			// 각 주소의 노출 규칙에 따라 주소를 조합한다.
			// 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
			var fullAddr = data.address; // 최종 주소 변수
			var extraAddr = ''; // 조합형 주소 변수

			// 기본 주소가 도로명 타입일때 조합한다.
			if(data.addressType === 'R'){
				//법정동명이 있을 경우 추가한다.
				if(data.bname !== ''){
					extraAddr += data.bname;
				}
				// 건물명이 있을 경우 추가한다.
				if(data.buildingName !== ''){
					extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
				}
				// 조합형주소의 유무에 따라 양쪽에 괄호를 추가하여 최종 주소를 만든다.
				fullAddr += (extraAddr !== '' ? ' ('+ extraAddr +')' : '');
			}

			// 우편번호와 주소 정보를 해당 필드에 넣는다.
			if (objZipCode != null)
				document.getElementById(objZipCode).value = data.zonecode; //5자리 새우편번호 사용
			
			if (objAddr1 != null)
				document.getElementById(objAddr1).value = fullAddr;

			if (objAddr2 != null)
				document.getElementById(objAddr2).focus();

			// 커서를 상세주소 필드로 이동한다.

			// iframe을 넣은 element를 안보이게 한다.
			// (autoClose:false 기능을 이용한다면, 아래 코드를 제거해야 화면에서 사라지지 않는다.)
			document.getElementById(objWrap).style.display = 'none';

			// 우편번호 찾기 화면이 보이기 이전으로 scroll 위치를 되돌린다.
			document.body.scrollTop = currentScroll;
		},
		// 우편번호 찾기 화면 크기가 조정되었을때 실행할 코드를 작성하는 부분. iframe을 넣은 element의 높이값을 조정한다.
		onresize : function(size) {
			document.getElementById(objWrap).style.height = size.height+'px';
		},
		width : '100%',
		height : '100%'
	}).embed(document.getElementById(objWrap));

	// iframe을 넣은 element를 보이게 한다.
	document.getElementById(objWrap).style.display = 'block';
}

addCommas = function (input) {
    if (input == "" || input == null) {
        return "";
    }
  // If the regex doesn't match, `replace` returns the string unmodified
  return (input.toString()).replace(
    // Each parentheses group (or 'capture') in this regex becomes an argument 
    // to the function; in this case, every argument after 'match'
    /^([-+]?)(0?)(\d+)(.?)(\d+)$/g, function(match, sign, zeros, before, decimal, after) {

      // Less obtrusive than adding 'reverse' method on all strings
      var reverseString = function(string) { return string.split('').reverse().join(''); };

      // Insert commas every three characters from the right
      var insertCommas  = function(string) { 

        // Reverse, because it's easier to do things from the left
        var reversed           = reverseString(string);

        // Add commas every three characters
        var reversedWithCommas = reversed.match(/.{1,3}/g).join(',');

        // Reverse again (back to normal)
        return reverseString(reversedWithCommas);
      };

      // If there was no decimal, the last capture grabs the final digit, so
      // we have to put it back together with the 'before' substring
      return sign + (decimal ? insertCommas(before) + decimal + after : insertCommas(before + after));
      }
  );
};

// 목록 페이징
Paging = function (totalCnt, dataSize, pageSize, pageNo) {
    totalCnt = parseInt(totalCnt);// 전체레코드수
    dataSize = parseInt(dataSize);       // 페이지당 보여줄 데이타수
    pageSize = parseInt(pageSize);     // 페이지 그룹 범위       1 2 3 5 6 7 8 9 10
    pageNo = parseInt(pageNo);        // 현재페이지

    var html = new Array();
    if (totalCnt == 0) {
        return "";
    }

    // 페이지 카운트
    var pageCnt = totalCnt % dataSize;
    if (pageCnt == 0) {
        pageCnt = parseInt(totalCnt / dataSize);
    } else {
        pageCnt = parseInt(totalCnt / dataSize) + 1;
    }

    var pRCnt = parseInt(pageNo / pageSize);
    if (pageNo % pageSize == 0) {
        pRCnt = pRCnt - 1;
    }

    // 시작 DIV
    html.push("<nav aria-label='Page navigation example'>");
    html.push("     <ul class='pagination justify-content-center'>");

    //이전 화살표
    if (pRCnt > 0) {
        html.push("<li class='page-item'>");
        html.push("<a class='page-link' name='pageGoto' target-page='" + ((parseInt(pRCnt - 1) * pageSize) + 1)  + "' tabindex='-1'>이전</a>");
        html.push("</li>");
    }else {
        html.push("<li class='page-item disabled'>");
        html.push("<a class='page-link' tabindex='-1'>이전</a>");
        html.push("</li>");
    }

    //paging Bar
    for (var index = pRCnt * pageSize + 1; index < (pRCnt + 1) * pageSize + 1 && index <= pageCnt; index++) {
        if (index == pageNo) {
            html.push("<li class='page-item disabled'><a class='page-link' tabindex='-1'>" + index + "</a></li>");
        } else {
            html.push("<li class='page-item'><a class='page-link' name='pageGoto' target-page='" + index + "' tabindex='-1'>" + index + "</a></li>");
        }
    }

    //다음 화살표
    if (pageCnt > (pRCnt + 1) * pageSize) {
        html.push("<li class='page-item'>");
        html.push("<a class='page-link' name='pageGoto' target-page='" + (parseInt((pRCnt + 1) * pageSize) + 1) + "' tabindex='-1'>다음</a>");
        html.push("</li>");
    }else {
        html.push("<li class='page-item disabled'>");
        html.push("<a class='page-link' tabindex='-1'>다음</a>");
        html.push("</li>");
    }

    // 종료 DIV
    html.push("     </ul>");
    html.push("</nav>");


    return html.join("");
}
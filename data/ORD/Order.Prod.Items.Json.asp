<%@Language="VBScript" CODEPAGE="65001"%>
<!-- #include virtual = "/share/include/DbInfo.asp" -->
<!-- #include virtual = "/share/include/Injection.asp" -->
<!-- #include virtual = "/share/include/Function.asp" -->
<!-- #include virtual = "/share/include/JSON/JSON_2.0.4.asp" -->
<!-- #include virtual = "/share/include/JSON/JSON_UTIL_0.1.1.asp" -->
<%

	ProdInfo = str_chk(Request("ProdInfo"))
	UseSite = str_chk(Request("UseSite"))
	ProdOpt = str_chk(Request("ProdOpt"))

	sql = ""
	sql = sql & vbCrlf & "      select  a.code_opt,			"
	sql = sql & vbCrlf & "    	            a.prod_code,		"
	sql = sql & vbCrlf & "    	            b.prod_name,		"
	sql = sql & vbCrlf & "    			    a.topcode,			"
	sql = sql & vbCrlf & "    			    a.cnt,			"
	sql = sql & vbCrlf & "    			    a.site,		"
	sql = sql & vbCrlf & "    			    a.status,		"
	sql = sql & vbCrlf & "    			    a.worry_yn,		"
	sql = sql & vbCrlf & "    			    b.prod_img,		"
	sql = sql & vbCrlf & "    			    a.prod_max_cnt,		"
	sql = sql & vbCrlf & "    			    dbo.brain_CalCost('P', a.prod_code, a.topcode, a.site) as pcost_p,		"
	sql = sql & vbCrlf & "    			    dbo.brain_CalCost('S', a.prod_code, a.topcode, a.site) as scost_p		"
	sql = sql & vbCrlf & "    	   from  sales_prod a with(nolock),		"
	sql = sql & vbCrlf & "    	            sales_prod_info b with(nolock)		"
	sql = sql & vbCrlf & "    	  where  a.prod_code = b.prod_code		"
	sql = sql & vbCrlf & "    	     and  b.usetype = 'Y'		"
	sql = sql & vbCrlf & "    	  	 and  a.site = '" & UseSite & "'		"
	sql = sql & vbCrlf & "    	  	 and  a.code_opt in ('장코드', 'SET', '대표코드')		"

	If ProdInfo <> "" Then 
		sql = sql & vbCrlf & "    	  	 and  (		"
		sql = sql & vbCrlf & "    	  	               a.prod_code like '%" & ProdInfo & "%'		"
		sql = sql & vbCrlf & "    	  	          or  b.prod_name like '%" & ProdInfo & "%'		"
		sql = sql & vbCrlf & "    	  		     )		"
	End If 

	If ProdOpt <> "" Then 
		sql = sql & vbCrlf & "    	  	 and  b.opt = '" & ProdOpt & "'		"
	End If 

	sql = sql & vbCrlf & "  order by   b.opt,  b.prod_name					"
	'response.write sql

	QueryToJSON(db, sql).Flush

%>
<!-- #include virtual = "/share/include/DbClose.asp" -->

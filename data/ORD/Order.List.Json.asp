<%@Language="VBScript" CODEPAGE="65001"%>
<!-- #include virtual = "/share/include/DbInfo.asp" -->
<!-- #include virtual = "/share/include/Injection.asp" -->
<!-- #include virtual = "/share/include/Function.asp" -->
<!-- #include virtual = "/share/include/JSON/JSON_2.0.4.asp" -->
<!-- #include virtual = "/share/include/JSON/JSON_UTIL_0.1.1.asp" -->
<%
	startDate = str_chk(Request("startDate"))
	endDate = str_chk(Request("endDate"))
	MemId = str_chk(Request("MemId"))
	OrderCode = str_chk(Request("OrderCode"))
	Page = str_chk(Request("Page"))
	PageSize = str_chk(Request("PageSize"))


	nPageSize = CInt("0" & PageSize)
	If page < "1" Then
		page = 1
	End If

	startNum = ((page - 1) * nPageSize) + 1
	endNum = ((page - 1) * nPageSize) + nPageSize 

	sql = ""
	sql = sql & vbCrlf & "                select   row_number() over (order by odate desc, order_code desc) as rownum,    "
	sql = sql & vbCrlf & "                           a.order_code,    "
	sql = sql & vbCrlf & "                           a.order_opt,    "
	sql = sql & vbCrlf & "                           ( select  z.name  from  sales_customer z with(nolock) where  z.cus_code = a.cus_code ) as cus_name,       "
	sql = sql & vbCrlf & "                           ( select  z.name  from  sales_member z with(nolock) where  z.id = a.mem_id ) as mem_name,       "
	sql = sql & vbCrlf & "                           ( select  z.name  from  sales_member z with(nolock) where  z.id = a.emp_id ) as emp_name,       "
	sql = sql & vbCrlf & "                           a.b_add1,     "
	sql = sql & vbCrlf & "                           a.b_add2,     "
	sql = sql & vbCrlf & "                           ( select  z.t_name  from  sales_order_type z with(nolock) where  z.t_code = a.order_type ) as order_type_nm,       "
	sql = sql & vbCrlf & "                           a.odate,     "
	sql = sql & vbCrlf & "                           a.insu_man,     "
	sql = sql & vbCrlf & "                           a.insu_tel,       "
	sql = sql & vbCrlf & "                           ( select  sum(z.prod_price * z.prod_count) from sales_order_item z with(nolock) where z.order_code = a.order_code and z.ver = a.ver  ) as scost_p,       "
	sql = sql & vbCrlf & "                           ( select  sum(z.prod_pprice * z.prod_count) from sales_order_item z with(nolock) where z.order_code = a.order_code and z.ver = a.ver  ) as pcost_p       "
	sql = sql & vbCrlf & "                 from   sales_order a with(nolock)  "
	sql = sql & vbCrlf & "                where  a.del_chk != 'Y'  "
	sql = sql & vbCrlf & "                   and  a.ver = a.now_ver  "
	sql = sql & vbCrlf & "                   and  a.odate >= '" & startDate & "'  "
	sql = sql & vbCrlf & "                   and  a.odate < '" & DateAdd("d", 1, endDate) & "'  "

	If MemId <> "" Then sql = sql & vbCrlf & "                   and  a.mem_id = '" & MemId & "'   "
	If OrderCode <> "" Then sql = sql & vbCrlf & "                   and  a.order_code like '%" & OrderCode & "%'   "

	If request.cookies("msite") <> "younglim" Then sql = sql & vbCrlf & "                   and  a.mem_id = '" & request.cookies("id") & "'   "


	sql2 = ""
	sql2 = sql2 & "      select  ta.*,  "
	sql2 = sql2 & "                ( select  count(*)   "
	sql2 = sql2 & "                	 from  sales_order z with(nolock)    "
	sql2 = sql2 & "                 where  z.del_chk != 'Y'  "
	sql2 = sql2 & "                    and  z.ver = z.now_ver  "
	sql2 = sql2 & "                    and  z.odate >= '" & startDate & "'  "
	sql2 = sql2 & "                    and  z.odate < '" & DateAdd("d", 1, endDate) & "'  "

	If MemId <> "" Then sql2 = sql2 & "                   and  z.mem_id = '" & MemId & "'   "
	If OrderCode <> "" Then sql2 = sql2 & "                   and  z.order_code like '%" & OrderCode & "%'   "

	If request.cookies("msite") <> "younglim" Then sql = sql & vbCrlf & "                   and  z.mem_id = '" & request.cookies("id") & "'   "

	sql2 = sql2 & "                ) as max_cnt       "
	sql2 = sql2 & "        from   (  "
	sql2 = sql2 & "	 					 " & sql
	sql2 = sql2 & "                   ) ta  "
	sql2 = sql2 & "      where   ta.rownum between " & startNum & " and " & endNum & "			"
	sql2 = sql2 & " order by 	 rownum  	"

	'Response.Write sql2

	QueryToJSON(db, sql2).Flush

%>
<!-- #include virtual = "/share/include/DbClose.asp" -->

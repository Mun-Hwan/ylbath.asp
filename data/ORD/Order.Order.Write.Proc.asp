<%@Language="VBScript" CODEPAGE="65001"%>
<!-- #include virtual = "/share/include/DbInfo.asp" -->
<!-- #include virtual = "/share/include/Injection.asp" -->
<!-- #include virtual = "/share/include/Function.asp" -->
<!-- #include virtual = "/share/include/JSON/JSON2.asp" -->
<%

    OrderData = Request("OrderData")
    Set JsonData = JSON.parse(OrderData)


	'************************************************************
	' 기준정보 조회
	'************************************************************
	'// 등록자 사이트
	sql = ""
	sql = sql & vbCrlf & "  select  site   "
	sql = sql & vbCrlf & "    from  sales_member with(nolock)  "
	sql = sql & vbCrlf & "   where  id='" & JsonData.MemId & "' "
	'response.write sql & "<br>"
	Set rs2 = db.execute(sql)
	If rs2.eof Then
		sMSite = request.cookies("msite")
	else
		sMSite = Trim(rs2("site"))
	End If
	rs2.close
	Set rs2 = Nothing
	
	'// 오더 구분자
	sql = ""
	sql = sql & vbCrlf & "  select  c_head   "
	sql = sql & vbCrlf & "    from  sales_order_type with(nolock)  "
	sql = sql & vbCrlf & "   where  t_code = '" & JsonData.OrderType & "' "
	'response.write sql & "<br>"
	Set rs2 = db.execute(sql)
	If rs2.eof Then
		c_head = "OD"
	else
		c_head = Trim(rs2(0))
	End If
	rs2.close
	Set rs2 = Nothing


	'************************************************************
	' 신규추가이므로 오더번호를 새로 딴다.
	'************************************************************
	sql = ""
	sql = sql & vbCrlf & "     select  order_code   "
	sql = sql & vbCrlf & "       from  sales_order with(updlock) "
	sql = sql & vbCrlf & "     where  substring(order_code,4,2) = right(datepart(year, getdate()),2) "
	sql = sql & vbCrlf & "        and  substring(order_code,6,2) = datepart(month, getdate()) "
	sql = sql & vbCrlf & "        and  ver = now_ver "
	sql = sql & vbCrlf & " order by  right(order_code, 8) desc "
	'Response.Write sql
	Set rs2 = db.execute(sql)
	If rs2.eof Then
		order_code = c_head & "-" & Mid(date, 3, 2) & Mid(date, 6, 2) & "0001"
	else
		num = cdbl(Right(Trim(rs2(0)), 4)) + 1
		num_str = ""
		For ii = 1 To 4 - Len(num)
			num_str = "0" & num_str
		Next
		order_code = c_head & "-" & Mid(date, 3, 2) & Mid(date, 6, 2) & num_str & num
	End If
	rs2.close
	Set rs2 = Nothing


	'************************************************************
	' 오더번호 유무 체크
	'************************************************************
	bBool = true
	Do While (bBool)
		sql = " select * from sales_order with(nolock) where order_code like '%" & Right(order_code, 8) & "' "
		'response.write sql
		Set rs2 = db.execute(sql)
		If rs2.eof Then
			bBool = false
		else
			num = cdbl(Right(Trim(order_code), 4)) + 1
			num_str = ""
			For ii = 1 To 4 - Len(num)
				num_str = "0" & num_str
			Next
			order_code = c_head & "-" & Mid(date, 3, 2) & Mid(date, 6, 2) & num_str & num
		End If
		rs2.close
		Set rs2 = Nothing
	Loop

	


	'************************************************************
	' 오더 저장
	'************************************************************
	'On Error Resume Next 
		'If Err.Number <> 0 Then 
			'tot_rate = 0
			'tot_rate_p = 0
		'End If 
	'On Error GoTo 0

	db.begintrans()

	'// 오더 정보
	sql = ""
	sql = sql & vbCrlf & " insert into  sales_order ( "
	sql = sql & vbCrlf & "										   order_code,  "
	sql = sql & vbCrlf & "										   cus_code,  "
	sql = sql & vbCrlf & "										   mem_id,  "
	sql = sql & vbCrlf & "										   emp_id,  "
	sql = sql & vbCrlf & "										   wdate,  "
	sql = sql & vbCrlf & "										   order_opt,  "
	sql = sql & vbCrlf & "										   b_zipcode,  "
	sql = sql & vbCrlf & "										   b_add1,  "
	sql = sql & vbCrlf & "										   b_add2,  "
	sql = sql & vbCrlf & "										   order_type,  "
	sql = sql & vbCrlf & "										   odate,  "
	sql = sql & vbCrlf & "										   otime,  "
	sql = sql & vbCrlf & "										   sdate,  "
	sql = sql & vbCrlf & "										   sigong_type,  "
	sql = sql & vbCrlf & "										   imsi_code,  "
	sql = sql & vbCrlf & "										   inhouse,  "
	sql = sql & vbCrlf & "										   ins_id,  "
	sql = sql & vbCrlf & "										   out_memo,  "
	sql = sql & vbCrlf & "										   ver,  "
	sql = sql & vbCrlf & "										   now_ver,  "
	sql = sql & vbCrlf & "										   insu_man,  "
	sql = sql & vbCrlf & "										   insu_tel,  "
	sql = sql & vbCrlf & "										   sigong_day,  "
	sql = sql & vbCrlf & "										   vat_type  "
	sql = sql & vbCrlf & "				           ) values (  "
	sql = sql & vbCrlf & "										   '" & order_code & "',  "
	sql = sql & vbCrlf & "										   '" & JsonData.CusCode & "',  "
	sql = sql & vbCrlf & "										   '" & JsonData.MemId & "',  "
	sql = sql & vbCrlf & "										   '" & JsonData.EmpId & "',  "
	sql = sql & vbCrlf & "										   getdate(),  "
	sql = sql & vbCrlf & "										   'frder',  "
	sql = sql & vbCrlf & "										   '" & JsonData.ZipCode & "',  "
	sql = sql & vbCrlf & "										   '" & JsonData.Addr1 & "',  "
	sql = sql & vbCrlf & "										   '" & JsonData.Addr2 & "',  "
	sql = sql & vbCrlf & "										   '" & JsonData.OrderType & "',  "
	sql = sql & vbCrlf & "										   '" & JsonData.ODate & "',  "
	sql = sql & vbCrlf & "										   '" & JsonData.OTime & "',  "
	sql = sql & vbCrlf & "										   '" & JsonData.SDate & "',  "
	sql = sql & vbCrlf & "										   '" & JsonData.SigongType & "',  "
	sql = sql & vbCrlf & "										   '" & JsonData.ImsiCode & "',  "
	sql = sql & vbCrlf & "										   '" & JsonData.Inhouse & "',  "
	sql = sql & vbCrlf & "										   '" & request.cookies("id") & "',  "
	sql = sql & vbCrlf & "										   '" & JsonData.OutMemo & "',  "
	sql = sql & vbCrlf & "										   0,  "
	sql = sql & vbCrlf & "										   0,  "
	sql = sql & vbCrlf & "										   '" & JsonData.InsuMan & "',  "
	sql = sql & vbCrlf & "										   '" & JsonData.InsuTel & "',  "
	sql = sql & vbCrlf & "										   '" & JsonData.SigongDay & "',  "
	sql = sql & vbCrlf & "										   '" & JsonData.VatType & "'  "
	sql = sql & vbCrlf & "				           )   "
	If sql <> "" Then 
		db.execute sql, , adCmdText + adExecuteNoRecords
		If Err.Number <> 0 Then 
			db.rollbacktrans()
			db.close
			response.write "ERROR|오더 저장 중 에러발생<br><br><font color='red'>"&Err.Description&"</font>"
			Err.close()
			Response.End
		End If 
	End If 


	'// 품목 정보
    Set JsonItems = JSON.parse("[" & JsonData.ItemList & "]")
    For i = 0 To JsonItems.length - 1
		sql = ""
		sql = sql & vbCrlf & " insert into  sales_order_item ( "
		sql = sql & vbCrlf & "										   order_code,  "
		sql = sql & vbCrlf & "										   prod_code,  "
		sql = sql & vbCrlf & "										   prod_name,  "
		sql = sql & vbCrlf & "										   prod_count,  "
		sql = sql & vbCrlf & "										   prod_price,  "
		sql = sql & vbCrlf & "										   set_info,  "
		sql = sql & vbCrlf & "										   set_opt,  "
		sql = sql & vbCrlf & "										   cost_ori,  "
		sql = sql & vbCrlf & "										   code_index,  "
		sql = sql & vbCrlf & "										   memo,  "
		sql = sql & vbCrlf & "										   prod_pprice,  "
		sql = sql & vbCrlf & "										   ver,  "
		sql = sql & vbCrlf & "										   pcost_ori  "
		sql = sql & vbCrlf & "				           ) values (  "
		sql = sql & vbCrlf & "										   '" & order_code & "',  "
		sql = sql & vbCrlf & "										   '" & JsonItems.Get(i).ProdCode & "',  "
		sql = sql & vbCrlf & "										   '" & JsonItems.Get(i).ProdName & "',  "
		sql = sql & vbCrlf & "										   '" & JsonItems.Get(i).ProdCount & "',  "
		sql = sql & vbCrlf & "										   '" & JsonItems.Get(i).ProdPrice & "',  "
		sql = sql & vbCrlf & "										   '" & JsonItems.Get(i).SetInfo & "',  "
		sql = sql & vbCrlf & "										   '" & JsonItems.Get(i).SetOpt & "',  "
		sql = sql & vbCrlf & "										   '" & JsonItems.Get(i).CostOri & "',  "
		sql = sql & vbCrlf & "										   " & i & ",  "
		sql = sql & vbCrlf & "										   '" & JsonItems.Get(i).Memo & "',  "
		sql = sql & vbCrlf & "										   '" & JsonItems.Get(i).ProdPPrice & "',  "
		sql = sql & vbCrlf & "										   0,  "
		sql = sql & vbCrlf & "										   '" & JsonItems.Get(i).PCostOri & "'  "
		sql = sql & vbCrlf & "				           )   "
		'Response.Write sql
		If sql <> "" Then 
			db.execute sql, , adCmdText + adExecuteNoRecords
			If Err.Number <> 0 Then 
				db.rollbacktrans()
				db.close
				Response.Write "ERROR|품목 Item 저장 중 에러발생<br><br><font color='red'>"&Err.Description&"</font>"
				Err.close()
				Response.End
			End If 
		End If 	
    Next 


	'// 품목 상세 정보
	DnSeq = 0
    For i = 0 To JsonItems.length - 1
		If JsonItems.Get(i).SetInfo = "장코드" Then 
			sql = ""
			sql = sql & vbCrlf & "    select  a.code_opt, "
			sql = sql & vbCrlf & "				  a.prod_code,  "
			sql = sql & vbCrlf & "				  b.prod_name,  "
			sql = sql & vbCrlf & "				  a.cnt,  "
			sql = sql & vbCrlf & "				  a.pcost_p,  "
			sql = sql & vbCrlf & "				  a.scost_p  "
			sql = sql & vbCrlf & "     from  sales_prod a with(nolock), "
			sql = sql & vbCrlf & "              sales_prod_info b with(nolock) "
			sql = sql & vbCrlf & "   where  a.prod_code = b.prod_code  "
			sql = sql & vbCrlf & "      and  b.usetype = 'Y' "
			sql = sql & vbCrlf & "      and  a.code_opt = '종속' "
			sql = sql & vbCrlf & "      and  a.topcode = '" & JsonItems.Get(i).ProdCode & "' "
			sql = sql & vbCrlf & "      and  a.site = '" & sMSite & "' "
			'Response.Write sql
			Set rs2 = Server.CreateObject("adodb.RecordSet")
			rs2.open sql, db, 1, 1
			
			'// 소비자가 할인률 계산
			DcSCostRate = CDbl(JsonItems.Get(i).ProdPrice) / CDbl(JsonItems.Get(i).CostOri)
			DcPCostRate = CDbl(JsonItems.Get(i).ProdPPrice) / CDbl(JsonItems.Get(i).PCostOri)

			TotCost = CLng(JsonItems.Get(i).ProdPrice) * CLng(JsonItems.Get(i).ProdCount)
			TotPCost = CLng(JsonItems.Get(i).ProdPPrice) * CLng(JsonItems.Get(i).ProdCount)
			TotDetailCost = 0
			TotDetailPCost = 0

			Do Until rs2.bof Or rs2.eof 
				DnSeq = DnSeq + 1
				
				ProdCount = CInt(JsonItems.Get(i).ProdCount) * CInt(rs2("cnt"))
				If Err.Number <> 0 Then ProdCount = 0

				SCost = CDbl(rs2("scost_p")) * DcSCostRate
				SCost = CLng(SCost)

				PCost = CDbl(rs2("pcost_p")) * DcPCostRate
				PCost = CLng(PCost)

				TotDetailCost = TotDetailCost + (SCost * ProdCount)
				TotDetailPCost = TotDetailPCost + (PCost * ProdCount)

				sql = ""
				sql = sql & vbCrlf & " insert into  sales_order_item_detail ( "
				sql = sql & vbCrlf & "										   order_code,  "
				sql = sql & vbCrlf & "										   prod_code,  "
				sql = sql & vbCrlf & "										   prod_name,  "
				sql = sql & vbCrlf & "										   dn_seq,  "
				sql = sql & vbCrlf & "										   cnt,  "
				sql = sql & vbCrlf & "										   price,  "
				sql = sql & vbCrlf & "										   set_info,  "
				sql = sql & vbCrlf & "										   set_opt,  "
				sql = sql & vbCrlf & "										   cost_ori,  "
				sql = sql & vbCrlf & "										   code_index,  "
				sql = sql & vbCrlf & "										   prod_pprice,  "
				sql = sql & vbCrlf & "										   memo,  "
				sql = sql & vbCrlf & "										   ver,  "
				sql = sql & vbCrlf & "										   pcost_ori  "
				sql = sql & vbCrlf & "				           ) values (  "
				sql = sql & vbCrlf & "										   '" & order_code & "',  "
				sql = sql & vbCrlf & "										   '" & rs2("prod_code") & "',  "
				sql = sql & vbCrlf & "										   '" & rs2("prod_name") & "',  "
				sql = sql & vbCrlf & "										   " & DnSeq & ",  "
				sql = sql & vbCrlf & "										   '" & ProdCount & "',  "
				sql = sql & vbCrlf & "										   '" & SCost & "',  "
				sql = sql & vbCrlf & "										   '" & rs2("code_opt") & "',  "
				sql = sql & vbCrlf & "										   '" & JsonItems.Get(i).SetOpt & "',  "
				sql = sql & vbCrlf & "										   '" & rs2("scost_p") & "',  "
				sql = sql & vbCrlf & "										   " & i & ",  "
				sql = sql & vbCrlf & "										   '" & PCost & "',  "
				sql = sql & vbCrlf & "										   '" & JsonItems.Get(i).Memo & "',  "
				sql = sql & vbCrlf & "										   0,  "
				sql = sql & vbCrlf & "										   '" & rs2("pcost_p") & "'  "
				sql = sql & vbCrlf & "				           )   "
				'Response.Write sql
				If sql <> "" Then 
					db.execute sql, , adCmdText + adExecuteNoRecords
					If Err.Number <> 0 Then 
						db.rollbacktrans()
						db.close
						Response.Write "ERROR|품목 ItemDetail(장코드) 저장 중 에러발생<br><br><font color='red'>"&Err.Description&"</font>"
						Err.close()
						Response.End
					End If 
				End If 	

				rs2.movenext
			Loop 
			rs2.close
			Set rs2 = Nothing

			DiffCost = TotCost - TotDetailCost
			DiffPCost = TotPCost - TotDetailPCost

			If (DiffCost + DiffPCost) <> 0 Then 
				DCCost = DiffCost / ProdCount

				sql = ""
				sql = sql & vbCrlf & "     ;with ByOrderItemDetail			"
				sql = sql & vbCrlf & "     as (			"
				sql = sql & vbCrlf & "           select  idx,  rank() over (order by price desc, cnt asc) rnk			"
				sql = sql & vbCrlf & "            from  sales_order_item_detail			"
				sql = sql & vbCrlf & "          where   order_code = '" & order_code & "'    "
				sql = sql & vbCrlf & "             and   code_index = '" & i & "'    "
				sql = sql & vbCrlf & "             and   ver = 0    "
				sql = sql & vbCrlf & "     )			"
				sql = sql & vbCrlf & "     update  sales_order_item_detail     "
				sql = sql & vbCrlf & "           set   price = price + ( " & DiffCost & " / cnt),   "
				sql = sql & vbCrlf & "                  prod_pprice = prod_pprice + ( " & DiffPCost & " / cnt)   "
				sql = sql & vbCrlf & "       where   idx = (select  idx  from  ByOrderItemDetail where rnk = 1)  			"
				'Response.Write sql
				If sql <> "" Then 
					db.execute sql, , adCmdText + adExecuteNoRecords
					If Err.Number <> 0 Then 
						db.rollbacktrans()
						db.close
						Response.Write "ERROR|품목 ItemDetail(자투리금액) 저장 중 에러발생<br><br><font color='red'>"&Err.Description&"</font>"
						Err.close()
						Response.End
					End If 
				End If 	
			End If 


		Else 
			DnSeq = DnSeq + 1

			sql = ""
			sql = sql & vbCrlf & " insert into  sales_order_item_detail ( "
			sql = sql & vbCrlf & "										   order_code,  "
			sql = sql & vbCrlf & "										   prod_code,  "
			sql = sql & vbCrlf & "										   prod_name,  "
			sql = sql & vbCrlf & "										   dn_seq,  "
			sql = sql & vbCrlf & "										   cnt,  "
			sql = sql & vbCrlf & "										   price,  "
			sql = sql & vbCrlf & "										   set_info,  "
			sql = sql & vbCrlf & "										   set_opt,  "
			sql = sql & vbCrlf & "										   cost_ori,  "
			sql = sql & vbCrlf & "										   code_index,  "
			sql = sql & vbCrlf & "										   prod_pprice,  "
			sql = sql & vbCrlf & "										   memo,  "
			sql = sql & vbCrlf & "										   ver,  "
			sql = sql & vbCrlf & "										   pcost_ori  "
			sql = sql & vbCrlf & "				           ) values (  "
			sql = sql & vbCrlf & "										   '" & order_code & "',  "
			sql = sql & vbCrlf & "										   '" & JsonItems.Get(i).ProdCode & "',  "
			sql = sql & vbCrlf & "										   '" & JsonItems.Get(i).ProdName & "',  "
			sql = sql & vbCrlf & "										   " & DnSeq & ",  "
			sql = sql & vbCrlf & "										   '" & JsonItems.Get(i).ProdCount & "',  "
			sql = sql & vbCrlf & "										   '" & JsonItems.Get(i).ProdPrice & "',  "
			sql = sql & vbCrlf & "										   '" & JsonItems.Get(i).SetInfo & "',  "
			sql = sql & vbCrlf & "										   '" & JsonItems.Get(i).SetOpt & "',  "
			sql = sql & vbCrlf & "										   '" & JsonItems.Get(i).CostOri & "',  "
			sql = sql & vbCrlf & "										   " & i & ",  "
			sql = sql & vbCrlf & "										   '" & JsonItems.Get(i).ProdPPrice & "',  "
			sql = sql & vbCrlf & "										   '" & JsonItems.Get(i).Memo & "',  "
			sql = sql & vbCrlf & "										   0,  "
			sql = sql & vbCrlf & "										   '" & JsonItems.Get(i).PCostOri & "'  "
			sql = sql & vbCrlf & "				           )   "
			If sql <> "" Then 
				db.execute sql, , adCmdText + adExecuteNoRecords
				If Err.Number <> 0 Then 
					db.rollbacktrans()
					db.close
					Response.Write "ERROR|품목 ItemDetail(단일상품) 저장 중 에러발생<br><br><font color='red'>"&Err.Description&"</font>"
					Err.close()
					Response.End
				End If 
			End If 	

		End If 
    Next 




		db.committrans()
		Response.Write "OK|" & order_code

	'End If 

%>
<!-- #include virtual = "/share/include/DbClose.asp" -->

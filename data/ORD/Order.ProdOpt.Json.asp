<%@Language="VBScript" CODEPAGE="65001"%>
<!-- #include virtual = "/share/include/DbInfo.asp" -->
<!-- #include virtual = "/share/include/Injection.asp" -->
<!-- #include virtual = "/share/include/Function.asp" -->
<!-- #include virtual = "/share/include/JSON/JSON_2.0.4.asp" -->
<!-- #include virtual = "/share/include/JSON/JSON_UTIL_0.1.1.asp" -->
<%
	UseSite = str_chk(Request("UseSite"))
	OrderType = str_chk(Request("OrderType"))

	sql = ""
	sql = sql & vbCrlf & "       	select   idx,					"
	sql = sql & vbCrlf & "       				title,					"
	sql = sql & vbCrlf & "       				(select title from sales_category with(nolock) where idx=a.up_idx) as gp,					"
	sql = sql & vbCrlf & "       				row_number() over (order by up_idx, idx) as sort2,					"
	sql = sql & vbCrlf & "       				'SET' as tp,					"
	sql = sql & vbCrlf & "       				a.set_code as sc,					"
	sql = sql & vbCrlf & "       				'1' as sort					"
	sql = sql & vbCrlf & "       	  from   sales_category a with (nolock)					"
	sql = sql & vbCrlf & "          where   charindex('" & UseSite & "', site) > 0					"
	sql = sql & vbCrlf & "       	   and   charindex('" & OrderType & "', t_code) > 0					"
	sql = sql & vbCrlf & "       	   and   step=3					"
	sql = sql & vbCrlf & "       	   and   isnull(set_code,'') != ''					"
	sql = sql & vbCrlf & "       union all					"
	sql = sql & vbCrlf & "       	select   idx,					"
	sql = sql & vbCrlf & "       				title,					"
	sql = sql & vbCrlf & "       				'Item' as gp,					"
	sql = sql & vbCrlf & "       				case when title = '타일' then 1					"
	sql = sql & vbCrlf & "       						when title = '위생도기' then 2					"
	sql = sql & vbCrlf & "       						when title = '욕실장' then 3						"
	sql = sql & vbCrlf & "       						when title = '인조대리석' then 4					"
	sql = sql & vbCrlf & "       						when title = 'ACC' then 5					"
	sql = sql & vbCrlf & "       						when title = '거울' then 6					"
	sql = sql & vbCrlf & "       						when title = '샤워부스/파티션' then 7					"
	sql = sql & vbCrlf & "       						when title = '욕조' then 8					"
	sql = sql & vbCrlf & "       						when title = 'SMC천장재' then 9					"
	sql = sql & vbCrlf & "       						when title = '기기' then 10					"
	sql = sql & vbCrlf & "       						when title = '수전금구' then 11					"
	sql = sql & vbCrlf & "       						when title = '조명기구' then 12					"
	sql = sql & vbCrlf & "       						when title = '부자재' then 13					"
	sql = sql & vbCrlf & "       						when title = '대표코드' then 14					"
	sql = sql & vbCrlf & "       						else 100					"
	sql = sql & vbCrlf & "       				 end as sort2,					"
	sql = sql & vbCrlf & "       				 'UNIT' as tp,					"
	sql = sql & vbCrlf & "       				 '' as sc,					"
	sql = sql & vbCrlf & "       				 '2' as sort						"
	sql = sql & vbCrlf & "       	   from   sales_category b with (nolock)					"
	sql = sql & vbCrlf & "       	where   charindex('" & mt & "', site) > 0					"
	sql = sql & vbCrlf & "       		and   charindex('" & ot & "', t_code) > 0					"
	sql = sql & vbCrlf & "       		and   step=2					"
	sql = sql & vbCrlf & "       		and   up_idx=2					"
	sql = sql & vbCrlf & "       order by    sort,					"
	sql = sql & vbCrlf & "       				    sort2					"
	'response.write sql

	QueryToJSON(db, sql).Flush

%>
<!-- #include virtual = "/share/include/DbClose.asp" -->

<%@Language="VBScript" CODEPAGE="65001"%>
<!-- #include virtual = "/share/include/DbInfo.asp" -->
<!-- #include virtual = "/share/include/Injection.asp" -->
<!-- #include virtual = "/share/include/Function.asp" -->
<!-- #include virtual = "/share/include/JSON/JSON_2.0.4.asp" -->
<!-- #include virtual = "/share/include/JSON/JSON_UTIL_0.1.1.asp" -->
<%

	UseSite = str_chk(Request("UseSite"))
	OrderType = str_chk(Request("OrderType"))

	sql = ""
	sql = sql & vbCrlf & "       	select  a.idx,					"
	sql = sql & vbCrlf & "       			   a.title,					"
	sql = sql & vbCrlf & "       				case when a.title = '타일' then 1					"
	sql = sql & vbCrlf & "       						when a.title = '위생도기' then 2					"
	sql = sql & vbCrlf & "       						when a.title = '욕실장' then 3						"
	sql = sql & vbCrlf & "       						when a.title = '인조대리석' then 4					"
	sql = sql & vbCrlf & "       						when a.title = 'ACC' then 5					"
	sql = sql & vbCrlf & "       						when a.title = '거울' then 6					"
	sql = sql & vbCrlf & "       						when a.title = '샤워부스/파티션' then 7					"
	sql = sql & vbCrlf & "       						when a.title = '욕조' then 8					"
	sql = sql & vbCrlf & "       						when a.title = 'SMC천장재' then 9					"
	sql = sql & vbCrlf & "       						when a.title = '기기' then 10					"
	sql = sql & vbCrlf & "       						when a.title = '수전금구' then 11					"
	sql = sql & vbCrlf & "       						when a.title = '조명기구' then 12					"
	sql = sql & vbCrlf & "       						when a.title = '부자재' then 13					"
	sql = sql & vbCrlf & "       						when a.title = '대표코드' then 14					"
	sql = sql & vbCrlf & "       						else 100					"
	sql = sql & vbCrlf & "       				 end as sort2					"
	sql = sql & vbCrlf & "       	   from   sales_category a with (nolock)					"
	sql = sql & vbCrlf & "       	where   charindex('" & UseSite & "', a.site) > 0					"
	sql = sql & vbCrlf & "       		and   charindex('" & OrderType & "', a.t_code) > 0					"
	sql = sql & vbCrlf & "       		and   a.step = 2					"
	sql = sql & vbCrlf & "       		and   a.up_idx = 2					"
	sql = sql & vbCrlf & "       order by   sort2, a.sort					"
	'response.write sql

	QueryToJSON(db, sql).Flush

%>
<!-- #include virtual = "/share/include/DbClose.asp" -->

<%@Language="VBScript" CODEPAGE="65001"%>
<!-- #include virtual = "/share/include/DbInfo.asp" -->
<!-- #include virtual = "/share/include/Injection.asp" -->
<!-- #include virtual = "/share/include/Function.asp" -->
<!-- #include virtual = "/share/include/JSON/JSON_2.0.4.asp" -->
<!-- #include virtual = "/share/include/JSON/JSON_UTIL_0.1.1.asp" -->
<%

	UseSite = str_chk(Request("UseSite"))
	OrderType = str_chk(Request("OrderType"))
	CategoryIdx = str_chk(Request("CategoryIdx"))

	sql = ""
	sql = sql & vbCrlf & "           select   a.idx,					"
	sql = sql & vbCrlf & "       				  a.title,					"
	sql = sql & vbCrlf & "       				  a.set_code					"
	sql = sql & vbCrlf & "            from   sales_category a with (nolock)					"
	sql = sql & vbCrlf & "          where   charindex('" & UseSite & "', a.site) > 0					"
	sql = sql & vbCrlf & "       	     and   charindex('" & OrderType & "', a.t_code) > 0					"
	sql = sql & vbCrlf & "       	     and   a.step = 3					"
	sql = sql & vbCrlf & "       	     and   a.up_idx = '" & CategoryIdx & "'					"
	sql = sql & vbCrlf & "       	     and   isnull(a.set_code,'') != ''					"
	sql = sql & vbCrlf & "      order by   a.sort, a.sort2					"
	'response.write sql

	QueryToJSON(db, sql).Flush

%>
<!-- #include virtual = "/share/include/DbClose.asp" -->

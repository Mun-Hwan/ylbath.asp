<%@Language="VBScript" CODEPAGE="65001"%>
<!-- #include virtual = "/share/include/DbInfo.asp" -->
<!-- #include virtual = "/share/include/Injection.asp" -->
<!-- #include virtual = "/share/include/Function.asp" -->
<!-- #include virtual = "/share/include/JSON/JSON_2.0.4.asp" -->
<!-- #include virtual = "/share/include/JSON/JSON_UTIL_0.1.1.asp" -->
<%
	Site = str_chk(Request("Site"))
	MemInfo = str_chk(Request("MemInfo"))

	sql = ""
	sql = sql & vbCrlf & "                select  a.idx,  a.id,  a.name,  a.hp,  "
	sql = sql & vbCrlf & "                          a.add1,  a.add2,  a.site       "
	sql = sql & vbCrlf & "                 from   sales_member a with(nolock)  "
	sql = sql & vbCrlf & "                where  del_chk != 'Y'  "

	If Site <> "" Then sql = sql & vbCrlf & "                   and  a.site = '" & Site & "'   "
	If MemInfo <> "" Then 
		sql = sql & vbCrlf & "                   and  (        "
		sql = sql & vbCrlf & "                                  a.name like '%" & MemInfo & "%'   "
		sql = sql & vbCrlf & "                             or  a.hp like '%" & MemInfo & "%'   "
		sql = sql & vbCrlf & "                             or  a.add1 like '%" & MemInfo & "%'   "
		sql = sql & vbCrlf & "                             or  a.add2 like '%" & MemInfo & "%'    "
		sql = sql & vbCrlf & "                           )        "
	End If 

	sql = sql & vbCrlf & "           order by  a.name, a.id  "

	'Response.Write sql

	QueryToJSON(db, sql).Flush

%>
<!-- #include virtual = "/share/include/DbClose.asp" -->

<%@Language="VBScript" CODEPAGE="65001"%>
<!-- #include virtual = "/share/include/DbInfo.asp" -->
<!-- #include virtual = "/share/include/Injection.asp" -->
<!-- #include virtual = "/share/include/Function.asp" -->
<!-- #include virtual = "/share/include/JSON/JSON2.asp" -->
<%
	ProdCodes = Request("ProdCodes")
	Set JsonData = JSON.parse(ProdCodes)

	If JsonData.Length = 0 Then 
		OutDay = 2
	Else 
		strProds = ""
		For i = 0 To JsonData.length - 1
			With JsonData.Get(i)
				strProds = strProds & ",'" & .ProdCode & "'"
			End With 
		Next 

		sql = ""
		sql = sql & vbCrlf & "   select  max(out_day) as out_day   "
		sql = sql & vbCrlf & "    from  sales_prod_info with(nolock)   "
		sql = sql & vbCrlf & "  where  prod_code in ('abcd12345zyxw'   "
		sql = sql & vbCrlf & "   " & strProds
		sql = sql & vbCrlf & "  )   "
		'Response.Write sql
		If sql <> "" Then 
			Set rs = db.execute(sql)

			If Not rs.bof Then 
				OutDay = CInt("0" & rs("out_day"))
			End If 

			rs.close
			Set rs = Nothing 
		End If 

	End If

	sql = ""
	sql = sql & vbCrlf & "  select  a.hmonth,  a.hday  "
	sql = sql & vbCrlf & "   from   holyday a with(nolock)  "
	Set rs = db.execute(sql)

	CurDate = Date()

	'// 오후 6시를 넘어가면 하루 추가
	If Hour(Now()) >= 18 Then 
		CurDate = DateAdd("d", 1, CurDate)
	End If 
	
	EndDate = DateAdd("d", OutDay, CurDate)

	'// 주문일의 다음날 부터 체크함.
	CurDate = DateAdd("d", 1, CurDate)
	Do While CurDate <= EndDate

		If DatePart("w", CurDate) = "1" Then 
			EndDate = DateAdd("d", 1, EndDate)
		End If 

		rs.movefirst
		Do Until rs.bof Or rs.eof
			YY = Year(Date())
			MM = rs("hmonth")
			If Len(MM) = 1 Then MM = "0" & MM
			DD = rs("hday")
			If Len(DD) = 1 Then DD = "0" & DD

			HoliDay = YY & "-" & MM & "-" & DD
			If CDate(CurDate) = CDate(HoliDay) Then 
				EndDate = DateAdd("d", 1, EndDate)
				Exit Do 
			End If 

			rs.movenext
		Loop 

		CurDate = DateAdd("d", 1, CurDate)
	Loop 

	rs.close
	Set rs = Nothing 

	Response.Write EndDate
%>
<!-- #include virtual = "/share/include/DbClose.asp" -->

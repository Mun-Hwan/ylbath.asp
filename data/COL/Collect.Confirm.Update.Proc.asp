<%@Language="VBScript" CODEPAGE="65001"%>
<!-- #include virtual = "/share/include/DbInfo.asp" -->
<!-- #include virtual = "/share/include/Injection.asp" -->
<!-- #include virtual = "/share/include/Function.asp" -->
<%

	Idx = str_chk(Request("Idx"))
	ConfirmYn = str_chk(Request("ConfirmYn"))

	YesCost = 0
	MemId = ""

	'// 확정 금액 알아오기
	sql = ""
	sql = sql & vbCrlf & "    select   isnull(a.yes_cost, 0) as yes_cost,    "
	sql = sql & vbCrlf & "               a.id    "
	sql = sql & vbCrlf & "      from  sales_will_history a with(nolock)   "
	sql = sql & vbCrlf & "    where  a.idx = '" & Idx & "'   "
	sql = sql & vbCrlf & "       and  a.confirm_yn != '" & ConfirmYn & "'   "
	'Response.Write sql
	Set rs2 = db.execute(sql)
	If Not rs2.eof Then
		YesCost = CLng("0" & rs2("yes_cost"))
		MemId = rs2("id")
	End If
	rs2.close
	Set rs2 = Nothing
	
	If YesCost  = 0 Then 
		Response.Write "OK|"
		Response.End
	End If 


	'On Error Resume Next 
		'If Err.Number <> 0 Then 
			'tot_rate = 0
			'tot_rate_p = 0
		'End If 
	'On Error GoTo 0


	'// 트랜잭션 시작
	db.begintrans()

	sql = ""
	sql = sql & vbCrlf & "    update  sales_will_history  "
	sql = sql & vbCrlf & "          set  confirm_yn = '" & ConfirmYn & "' "
	sql = sql & vbCrlf & "      where  idx = '" & idx & "'  "
	'Response.Write sql 

	If sql <> "" Then 
		db.execute sql, , adCmdText + adExecuteNoRecords
		If Err.Number <> 0 Then 
			db.rollbacktrans()
			db.close
			Response.Write "ERROR|수금 확정 중 에러발생\n\n" & Err.Description
			Err.close()
			Response.End
		End If 
	End If 

	Select Case ConfirmYn
		Case "Y"
			sql = ""
			sql = sql & vbCrlf & "    update  sales_ys  "
			sql = sql & vbCrlf & "          set  use_ys = use_ys - " & YesCost & ",  "
			sql = sql & vbCrlf & "                ok_ys = full_ys - (use_ys - " & YesCost & ")  "
			sql = sql & vbCrlf & "      where  mem_id = '" & MemId & "'  "

		Case "N"
			sql = ""
			sql = sql & vbCrlf & "    update  sales_ys  "
			sql = sql & vbCrlf & "          set  use_ys = use_ys + " & YesCost & ",  "
			sql = sql & vbCrlf & "                ok_ys = full_ys - (use_ys + " & YesCost & ")  "
			sql = sql & vbCrlf & "      where  mem_id = '" & MemId & "'  "

	End Select 

	'Response.Write sql 
	If sql <> "" Then 
		db.execute sql, , adCmdText + adExecuteNoRecords
		If Err.Number <> 0 Then 
			db.rollbacktrans()
			db.close
			Response.Write "ERROR|여신 계산 중 에러발생\n\n" & Err.Description
			Err.close()
			Response.End
		End If 
	End If 


	db.committrans()
	Response.Write "OK|"


%>
<!-- #include virtual = "/share/include/DbClose.asp" -->

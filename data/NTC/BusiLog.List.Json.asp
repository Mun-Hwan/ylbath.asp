<%@Language="VBScript" CODEPAGE="65001"%>
<!-- #include virtual = "/share/include/DbInfo.asp" -->
<!-- #include virtual = "/share/include/Injection.asp" -->
<!-- #include virtual = "/share/include/Function.asp" -->
<!-- #include virtual = "/share/include/JSON/JSON_2.0.4.asp" -->
<!-- #include virtual = "/share/include/JSON/JSON_UTIL_0.1.1.asp" -->
<%
	BusiDate_Start = str_chk(Request("param1"))
	If BusiDate_Start = "" Then BusiDate_Start = "1900-01-01"

	BusiDate_End = str_chk(Request("param2"))
	If BusiDate_End = "" Then BusiDate_End = "1900-01-01"
	BusiDate_End = DateAdd("d", 1, BusiDate_End)

	Mem_Id = str_chk(Request("param3"))


	sql = ""
	sql = sql & vbCrlf & "         select  row_number() over(order by BusiDate, Mem_id, VisitTime) as rownum,   "
	sql = sql & vbCrlf & "                    a.Idx,  a.Mem_Id,  a.VisitTime,  a.ReturnVisit,  "
	sql = sql & vbCrlf & "                    a.Shop_Id,  a.Addr1, a.Addr2,  a.ProdType,       "
	sql = sql & vbCrlf & "                    a.Remark,  substring(a.Addr1, 0, 5) as ShortAddr,  a.BusiDate  "
	sql = sql & vbCrlf & "          from   sales_busilog a with(nolock)  "
	sql = sql & vbCrlf & "         where  a.BusiDate >= '" & BusiDate_Start& "'   "
	sql = sql & vbCrlf & "            and  a.BusiDate < '" & BusiDate_End & "'   "
	sql = sql & vbCrlf & "            and  a.DelBit = 0   "
	If Mem_Id <> "" Then  sql = sql & vbCrlf & "            and  a.Mem_id = '" & Mem_Id & "'   "
	'Response.Write sql

	QueryToJSON(db, sql).Flush

%>
<!-- #include virtual = "/share/include/DbClose.asp" -->

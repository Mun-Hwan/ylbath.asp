<%@Language="VBScript" CODEPAGE="65001"%>
<!-- #include virtual = "/share/include/DbInfo.asp" -->
<!-- #include virtual = "/share/include/Injection.asp" -->
<!-- #include virtual = "/share/include/Function.asp" -->
<!-- #include virtual = "/share/include/JSON/JSON_2.0.4.asp" -->
<!-- #include virtual = "/share/include/JSON/JSON_UTIL_0.1.1.asp" -->
<%
	CusInfo = str_chk(Request("CusInfo"))
	Page = str_chk(Request("Page"))
	PageSize = str_chk(Request("PageSize"))


	strWhere = ""
	If CusInfo <> "" Then 
		strWhere = strWhere & vbCrlf & "            and  (       "
		strWhere = strWhere & vbCrlf & "                                  a.name like '%" & CusInfo & "%'  "
		strWhere = strWhere & vbCrlf & "                             or  a.hp like '%" & CusInfo & "%'  "
		strWhere = strWhere & vbCrlf & "                             or  a.add1 like '%" & CusInfo & "%'  "
		strWhere = strWhere & vbCrlf & "                             or  a.add2 like '%" & CusInfo & "%'  "
		strWhere = strWhere & vbCrlf & "                    )  "
	End If 


	RecordCount = 0
	sql = ""
	sql = sql & vbCrlf & "                select   count(*) as Cnt     "
	sql = sql & vbCrlf & "                 from   sales_customer a with(nolock)  "
	sql = sql & vbCrlf & "               where   a.del_chk != 'Y'  "
	sql = sql & vbCrlf & strWhere
	'Response.Write sql
    Set rs = db.Execute(sql)
	If Not rs.bof And Not rs.eof Then  RecordCount = CInt(rs("Cnt"))

	nPageSize = CInt("0" & PageSize)
	If page < "1" Then
		page = 1
	End If

	startNum = ((page - 1) * nPageSize) + 1
	endNum = ((page - 1) * nPageSize) + nPageSize 

	sql = ""
	sql = sql & vbCrlf & "                select   row_number() over (order by a.name asc, a.idx desc) as rownum,    "
	sql = sql & vbCrlf & "                           a.idx,    "
	sql = sql & vbCrlf & "                           a.name,    "
	sql = sql & vbCrlf & "                           a.hp,     "
	sql = sql & vbCrlf & "                           a.zipcode,     "
	sql = sql & vbCrlf & "                           a.add1,     "
	sql = sql & vbCrlf & "                           a.add2     "
	sql = sql & vbCrlf & "                 from   sales_customer a with(nolock)  "
	sql = sql & vbCrlf & "               where  a.del_chk != 'Y'  "
	sql = sql & vbCrlf & strWhere


	sql2 = ""
	sql2 = sql2 & "      select  ta.*,  "
	sql2 = sql2 & "                " & RecordCount & " as max_cnt       "
	sql2 = sql2 & "        from   (  "
	sql2 = sql2 & "	 					 " & sql
	sql2 = sql2 & "                   ) ta  "
	sql2 = sql2 & "      where   ta.rownum between " & startNum & " and " & endNum & "			"
	sql2 = sql2 & " order by 	rownum  	"
	'Response.Write sql2

	QueryToJSON(db, sql2).Flush
%>
<!-- #include virtual = "/share/include/DbClose.asp" -->

<%@Language="VBScript" CODEPAGE="65001"%>
<!-- #include virtual = "/share/include/DbInfo.asp" -->
<!-- #include virtual = "/share/include/Injection.asp" -->
<!-- #include virtual = "/share/include/Function.asp" -->
<!-- #include virtual = "/share/include/JSON/JSON_2.0.4.asp" -->
<!-- #include virtual = "/share/include/JSON/JSON_UTIL_0.1.1.asp" -->
<%
	OutStatus = Request("OutStatus")
	OrderOpt = str_chk(Request("OrderOpt"))
	OrderCode = str_chk(Request("OrderCode"))
	EmpId = str_chk(Request("EmpId"))
	OrderType = str_chk(Request("OrderType"))
	startDate = str_chk(Request("startDate"))
	endDate = str_chk(Request("endDate"))
	ProdInfo = str_chk(Request("ProdInfo"))
	Page = str_chk(Request("Page"))
	PageSize = str_chk(Request("PageSize"))
	
	
	strWhere = ""
	strWhere = strWhere & vbCrlf & "                   and  a.odate >= '" & startDate & "'  "
	strWhere = strWhere & vbCrlf & "                   and  a.odate < '" & DateAdd("d", 1, endDate) & "'  "
	If OutStatus <> "" Then 
		strWhere = strWhere & vbCrlf & "                   and  a.out_status in ('zzzzz'   "
		aOutStatus = Split(OutStatus, ",")
		For i = 0 To uBound(aOutStatus)
			strWhere = strWhere & vbCrlf & "                   , '" & Trim(aOutStatus(i)) & "'   "
		Next 
		strWhere = strWhere & vbCrlf & "							)   "
	End If 
	If OrderOpt <> "" Then strWhere = strWhere & vbCrlf & "                   and  a.order_opt = '" & OrderOpt & "'   "
	If OrderCode <> "" Then strWhere = strWhere & vbCrlf & "                   and  a.order_code like '%" & OrderCode & "%'   "
	If EmpId <> "" Then strWhere = strWhere & vbCrlf & "                   and  a.emp_id = '" & EmpId & "'   "
	If OrderType <> "" Then strWhere = strWhere & vbCrlf & "                   and  a.order_type = '" & OrderType & "'   "
	If ProdInfo <> "" Then 
		strWhere = strWhere & vbCrlf & "                   and  exists (        select  *    "
		strWhere = strWhere & vbCrlf & "                                              from  sales_order_item z with(nolock)      "
		strWhere = strWhere & vbCrlf & "                                            where  z.order_code = a.order_code      "
		strWhere = strWhere & vbCrlf & "                                               and  z.ver = a.ver        "
		strWhere = strWhere & vbCrlf & "                                               and  (        z.prod_code like '%" & ProdInfo & "%'   "
		strWhere = strWhere & vbCrlf & "                                                          or  z.prod_name like '%" & ProdInfo & "%'  )  "
	End If 
	If request.cookies("msite") <> "younglim" Then strWhere = strWhere & vbCrlf & "                   and  a.mem_id = '" & request.cookies("id") & "'   "


	RecordCount = 0
	sql = ""
	sql = sql & vbCrlf & "                select   count(*) as Cnt     "
	sql = sql & vbCrlf & "                 from   sales_order a with(nolock)  "
	sql = sql & vbCrlf & "                where  a.del_chk != 'Y'  "
	sql = sql & vbCrlf & "                   and  a.ver = a.now_ver  "
	sql = sql & vbCrlf & strWhere
	'Response.Write sql
    Set rs = db.Execute(sql)
	If Not rs.bof And Not rs.eof Then  RecordCount = CInt(rs("Cnt"))


	nPageSize = CInt("0" & PageSize)
	If page < "1" Then
		page = 1
	End If

	startNum = ((page - 1) * nPageSize) + 1
	endNum = ((page - 1) * nPageSize) + nPageSize 

	sql = ""
	sql = sql & vbCrlf & "                select   row_number() over (order by odate desc, order_code desc) as rownum,    "
	sql = sql & vbCrlf & "                           a.order_code,    "
	sql = sql & vbCrlf & "                           a.order_opt,    "
	sql = sql & vbCrlf & "                           ( select  z.t_name  from  sales_order_type z with(nolock) where  z.t_code = a.order_type ) as order_type_nm,       "
	sql = sql & vbCrlf & "                           a.b_add1,     "
	sql = sql & vbCrlf & "                           a.b_add2,     "
	sql = sql & vbCrlf & "   						  dbo.brain_ProductAmount('P', 'M', a.order_code) as order_pamt,  /* 품목만 공급가 - 부가세별도 */		"
	sql = sql & vbCrlf & "   						  dbo.brain_DeliveryAmount('P', 'P', a.order_code) as order_delivery,  /* 배송비 - 부가세포함 */		"
	sql = sql & vbCrlf & "                           a.otime,     "
	sql = sql & vbCrlf & "                           ( select  z.name  from  sales_member z with(nolock) where  z.id = a.mem_id ) as mem_name,       "
	sql = sql & vbCrlf & "                           a.odate,     "
	sql = sql & vbCrlf & "                           a.out_status     "
	sql = sql & vbCrlf & "                 from   sales_order a with(nolock)  "
	sql = sql & vbCrlf & "                where  a.del_chk != 'Y'  "
	sql = sql & vbCrlf & "                   and  a.ver = a.now_ver  "
	sql = sql & vbCrlf & strWhere


	sql2 = ""
	sql2 = sql2 & "      select  ta.*,  "
	sql2 = sql2 & "                " & RecordCount & " as max_cnt       "
	sql2 = sql2 & "        from   (  "
	sql2 = sql2 & "	 					 " & sql
	sql2 = sql2 & "                   ) ta  "
	sql2 = sql2 & "      where   ta.rownum between " & startNum & " and " & endNum & "			"
	sql2 = sql2 & " order by 	 rownum  	"
	'Response.Write sql2

	QueryToJSON(db, sql2).Flush

%>
<!-- #include virtual = "/share/include/DbClose.asp" -->

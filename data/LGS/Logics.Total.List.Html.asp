<%@Language="VBScript" CODEPAGE="65001"%>
<!-- #include virtual = "/share/include/DbInfo.asp" -->
<!-- #include virtual = "/share/include/Injection.asp" -->
<!-- #include virtual = "/share/include/Function.asp" -->
<%
	OrderOpt = str_chk(Request("OrderOpt"))
	OrderCode = str_chk(Request("OrderCode"))
	EmpId = str_chk(Request("EmpId"))
	OrderType = str_chk(Request("OrderType"))
	startDate = str_chk(Request("startDate"))
	endDate = str_chk(Request("endDate"))
	ProdInfo = str_chk(Request("ProdInfo"))
	
	
	strWhere = ""
	strWhere = strWhere & vbCrlf & "                   and  a.odate >= '" & startDate & "'  "
	strWhere = strWhere & vbCrlf & "                   and  a.odate < '" & DateAdd("d", 1, endDate) & "'  "
	If OrderOpt <> "" Then strWhere = strWhere & vbCrlf & "                   and  a.order_opt = '" & OrderOpt & "'   "
	If OrderCode <> "" Then strWhere = strWhere & vbCrlf & "                   and  a.order_code like '%" & OrderCode & "%'   "
	If EmpId <> "" Then strWhere = strWhere & vbCrlf & "                   and  a.emp_id = '" & EmpId & "'   "
	If OrderType <> "" Then strWhere = strWhere & vbCrlf & "                   and  a.order_type = '" & OrderType & "'   "
	If ProdInfo <> "" Then 
		strWhere = strWhere & vbCrlf & "                                 and  (        b.prod_code like '%" & ProdInfo & "%'   "
		strWhere = strWhere & vbCrlf & "                                            or  b.prod_name like '%" & ProdInfo & "%'  )  "
	End If 
	If request.cookies("msite") <> "younglim" Then strWhere = strWhere & vbCrlf & "                   and  a.mem_id = '" & request.cookies("id") & "'   "


	sql = ""
	sql = sql & vbCrlf & "                select   b.prod_code,     "
	sql = sql & vbCrlf & "                           b.prod_name,     "
	
	CurDate = startDate
	Do While CDate(CurDate) <= CDate(endDate)
		sql = sql & vbCrlf & "                           sum(case when a.odate = '" & CurDate & "' then (case when a.order_type = 'i33' or a.order_type = 'i41' then (b.cnt * -1) else  b.cnt end) else 0 end) as """ & CurDate & """,     "
		CurDate = DateAdd("d", 1, CurDate)
	Loop 
	sql = sql & vbCrlf & "                           sum(case when a.order_type = 'i33' or a.order_type = 'i41' then (b.cnt * -1) else  b.cnt end) as TotCount     "
	sql = sql & vbCrlf & "                 from   sales_order a with(nolock),  "
	sql = sql & vbCrlf & "                           sales_order_item_detail b with(nolock)  "
	sql = sql & vbCrlf & "                where  a.del_chk != 'Y'  "
	sql = sql & vbCrlf & "                   and  a.ver = a.now_ver  "
	sql = sql & vbCrlf & "                   and  a.order_code = b.order_code  "
	sql = sql & vbCrlf & "                   and  a.ver = b.ver  "
	sql = sql & vbCrlf & strWhere
	sql = sql & vbCrlf & "           group by  b.prod_code,  "
	sql = sql & vbCrlf & "                          b.prod_name  "
	'Response.Write sql
	Set rs = db.execute (sql)
%>

	<table class="table table-hover table-bordered nowrap" style="font-size: 8pt;" id="dataTable" width="100%" cellspacing="0">
		<thead>
			<tr>
				<th scope="col">제품코드</th>
				<th scope="col">제품명</th>
				<%
					aColName = Array()
					For Each col In rs.Fields
						Select Case col.Name
							Case "prod_code", "prod_name", "TotCount"
							Case Else
								Response.Write vbCrlf & "<th scope='col' class='text-center'>" & Month(CDate(col.Name)) & "/" & Day(CDate(col.Name)) & "</th>"
						End Select 

						ReDim Preserve aColName(ubound(aColName) + 1)
						aColName(ubound(aColName)) = col.Name
					Next 
				%>
				<th scope="col" class='text-center'>합&nbsp;&nbsp;계</th>
			</tr>
		</thead>
		<tbody>
			<%
				nTotSum = 0
				aSumCount = Array()
				For i = 0 To ubound(aColName)
					ReDim Preserve aSumCount(i)
					aSumCount(i) = 0
				Next 

				Do Until rs.Bof Or rs.Eof 
					Response.Write vbCrlf & "<tr>"
					Response.Write vbCrlf & "	<td>" & rs("prod_code") & "</td>"
					Response.Write vbCrlf & "	<td>" & rs("prod_name") & "</td>"
					For i = 0 To ubound(aColName)
						Select Case aColName(i)
							Case "prod_code", "prod_name"
							Case "TotCount"
								nTotSum = nTotSum + CInt(rs("TotCount"))
							Case Else 
								Response.Write vbCrlf & "<td class='text-center'>" & rs(aColName(i)) & "</td>"
								aSumCount(i) = aSumCount(i) + CInt(rs(aColName(i)))
						End Select 
					Next 
					Response.Write vbCrlf & "	<td class='text-center'>" & rs("TotCount") & "</td>"
					Response.Write vbCrlf & "</tr>"

					rs.movenext
				Loop 
			%>
		</tbody>
		<tfoot>
			<tr style='font-weight: bold;'>
				<td colspan="2" class='text-center'>합&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;계</td>
				<%
					For i = 0 To ubound(aColName)
						Select Case aColName(i)
							Case "prod_code", "prod_name"
							Case "TotCount"
								Response.Write "<td class='text-center'>" & nTotSum & "</td>"
							Case Else 
								Response.Write "<td class='text-center'>" & aSumCount(i) & "</td>"
						End Select 
					Next 
				%>
			</tr>
		</tfoot>
	 </table>
 <%
	rs.close
	Set rs = Nothing 
 %>
<!-- #include virtual = "/share/include/DbClose.asp" -->

<%@Language="VBScript" CODEPAGE="65001"%>
<!-- #include virtual = "/share/include/DbInfo.asp" -->
<!-- #include virtual = "/share/include/Injection.asp" -->
<!-- #include virtual = "/share/include/Function.asp" -->
<%
	OrderOpt = str_chk(Request("OrderOpt"))
	OrderCode = str_chk(Request("OrderCode"))
	EmpId = str_chk(Request("EmpId"))
	OrderType = str_chk(Request("OrderType"))
	startDate = str_chk(Request("startDate"))
	endDate = str_chk(Request("endDate"))
	ProdInfo = str_chk(Request("ProdInfo"))
	
	
	strWhere = ""
	strWhere = strWhere & vbCrlf & "                   and  a.odate >= '" & startDate & "'  "
	strWhere = strWhere & vbCrlf & "                   and  a.odate < '" & DateAdd("d", 1, endDate) & "'  "
	If OrderOpt <> "" Then strWhere = strWhere & vbCrlf & "                   and  a.order_opt = '" & OrderOpt & "'   "
	If OrderCode <> "" Then strWhere = strWhere & vbCrlf & "                   and  a.order_code like '%" & OrderCode & "%'   "
	If EmpId <> "" Then strWhere = strWhere & vbCrlf & "                   and  a.emp_id = '" & EmpId & "'   "
	If OrderType <> "" Then strWhere = strWhere & vbCrlf & "                   and  a.order_type = '" & OrderType & "'   "
	If ProdInfo <> "" Then 
		strWhere = strWhere & vbCrlf & "                                 and  (        b.prod_code like '%" & ProdInfo & "%'   "
		strWhere = strWhere & vbCrlf & "                                            or  b.prod_name like '%" & ProdInfo & "%'  )  "
	End If 
	If request.cookies("msite") <> "younglim" Then strWhere = strWhere & vbCrlf & "                   and  a.mem_id = '" & request.cookies("id") & "'   "


	sql = ""
	sql = sql & vbCrlf & "                select   b.prod_code,     "
	sql = sql & vbCrlf & "                           b.prod_name,     "
	
	CurDate = startDate
	Do While CDate(CurDate) <= CDate(endDate)
		sql = sql & vbCrlf & "                           sum(case when a.odate = '" & CurDate & "' then (case when a.order_type = 'i33' or a.order_type = 'i41' then (b.cnt * -1) else  b.cnt end) else 0 end) as """ & CurDate & """,     "
		CurDate = DateAdd("d", 1, CurDate)
	Loop 
	sql = sql & vbCrlf & "                           sum(case when a.order_type = 'i33' or a.order_type = 'i41' then (b.cnt * -1) else  b.cnt end) as TotCount     "
	sql = sql & vbCrlf & "                 from   sales_order a with(nolock),  "
	sql = sql & vbCrlf & "                           sales_order_item_detail b with(nolock)  "
	sql = sql & vbCrlf & "                where  a.del_chk != 'Y'  "
	sql = sql & vbCrlf & "                   and  a.ver = a.now_ver  "
	sql = sql & vbCrlf & "                   and  a.order_code = b.order_code  "
	sql = sql & vbCrlf & "                   and  a.ver = b.ver  "
	sql = sql & vbCrlf & strWhere
	sql = sql & vbCrlf & "           group by  b.prod_code,  "
	sql = sql & vbCrlf & "                          b.prod_name  "
	'Response.Write sql
	Set rs = db.execute (sql)


	xml_str = ""
	xml_str = xml_str & vbCrlf & "<?xml version=""1.0""?>"
	xml_str = xml_str & vbCrlf & "<?mso-application progid=""Excel.Sheet""?>"
	xml_str = xml_str & vbCrlf & "<Workbook xmlns=""urn:schemas-microsoft-com:office:spreadsheet"""
	xml_str = xml_str & vbCrlf & " xmlns:o=""urn:schemas-microsoft-com:office:office"""
	xml_str = xml_str & vbCrlf & " xmlns:x=""urn:schemas-microsoft-com:office:excel"""
	xml_str = xml_str & vbCrlf & " xmlns:ss=""urn:schemas-microsoft-com:office:spreadsheet"""
	xml_str = xml_str & vbCrlf & " xmlns:html=""http://www.w3.org/TR/REC-html40"">"
	xml_str = xml_str & vbCrlf & " <DocumentProperties xmlns=""urn:schemas-microsoft-com:office:office"">"
	xml_str = xml_str & vbCrlf & "  <Author></Author>"
	xml_str = xml_str & vbCrlf & "  <LastAuthor></LastAuthor>"
	xml_str = xml_str & vbCrlf & "  <LastPrinted></LastPrinted>"
	xml_str = xml_str & vbCrlf & "  <Created></Created>"
	xml_str = xml_str & vbCrlf & "  <LastSaved></LastSaved>"
	xml_str = xml_str & vbCrlf & "  <Version>16.00</Version>"
	xml_str = xml_str & vbCrlf & " </DocumentProperties>"
	xml_str = xml_str & vbCrlf & " <OfficeDocumentSettings xmlns=""urn:schemas-microsoft-com:office:office"">"
	xml_str = xml_str & vbCrlf & "  <AllowPNG/>"
	xml_str = xml_str & vbCrlf & " </OfficeDocumentSettings>"
	xml_str = xml_str & vbCrlf & " <ExcelWorkbook xmlns=""urn:schemas-microsoft-com:office:excel"">"
	xml_str = xml_str & vbCrlf & "  <WindowHeight>12285</WindowHeight>"
	xml_str = xml_str & vbCrlf & "  <WindowWidth>28800</WindowWidth>"
	xml_str = xml_str & vbCrlf & "  <WindowTopX>0</WindowTopX>"
	xml_str = xml_str & vbCrlf & "  <WindowTopY>0</WindowTopY>"
	xml_str = xml_str & vbCrlf & "  <ProtectStructure>False</ProtectStructure>"
	xml_str = xml_str & vbCrlf & "  <ProtectWindows>False</ProtectWindows>"
	xml_str = xml_str & vbCrlf & " </ExcelWorkbook>"
	xml_str = xml_str & vbCrlf & " <Styles>"
	xml_str = xml_str & vbCrlf & "  <Style ss:ID=""Default"" ss:Name=""Normal"">"
	xml_str = xml_str & vbCrlf & "   <Alignment ss:Vertical=""Center""/>"
	xml_str = xml_str & vbCrlf & "   <Borders/>"
	xml_str = xml_str & vbCrlf & "   <Font ss:FontName=""맑은 고딕"" x:CharSet=""129"" x:Family=""Modern"" ss:Size=""11"""
	xml_str = xml_str & vbCrlf & "    ss:Color=""#000000""/>"
	xml_str = xml_str & vbCrlf & "   <Interior/>"
	xml_str = xml_str & vbCrlf & "   <NumberFormat/>"
	xml_str = xml_str & vbCrlf & "   <Protection/>"
	xml_str = xml_str & vbCrlf & "  </Style>"
	xml_str = xml_str & vbCrlf & "  <Style ss:ID=""s62"">"
	xml_str = xml_str & vbCrlf & "   <Alignment ss:Horizontal=""Center"" ss:Vertical=""Center""/>"
	xml_str = xml_str & vbCrlf & "   <Interior ss:Color=""#C9C9C9"" ss:Pattern=""Solid""/>"
	xml_str = xml_str & vbCrlf & "  </Style>"
	xml_str = xml_str & vbCrlf & "  <Style ss:ID=""s64"">"
	xml_str = xml_str & vbCrlf & "   <Alignment ss:Horizontal=""Center"" ss:Vertical=""Center"" ss:WrapText=""1""/>"
	xml_str = xml_str & vbCrlf & "   <Borders>"
	xml_str = xml_str & vbCrlf & "    <Border ss:Position=""Bottom"" ss:LineStyle=""Continuous"" ss:Weight=""1"""
	xml_str = xml_str & vbCrlf & "     ss:Color=""#000000""/>"
	xml_str = xml_str & vbCrlf & "    <Border ss:Position=""Left"" ss:LineStyle=""Continuous"" ss:Weight=""1"""
	xml_str = xml_str & vbCrlf & "     ss:Color=""#000000""/>"
	xml_str = xml_str & vbCrlf & "    <Border ss:Position=""Right"" ss:LineStyle=""Continuous"" ss:Weight=""1"""
	xml_str = xml_str & vbCrlf & "     ss:Color=""#000000""/>"
	xml_str = xml_str & vbCrlf & "    <Border ss:Position=""Top"" ss:LineStyle=""Continuous"" ss:Weight=""1"""
	xml_str = xml_str & vbCrlf & "     ss:Color=""#000000""/>"
	xml_str = xml_str & vbCrlf & "   </Borders>"
	xml_str = xml_str & vbCrlf & "   <Font ss:FontName=""맑은 고딕"" x:CharSet=""129"" x:Family=""Modern"" ss:Color=""#000000"""
	xml_str = xml_str & vbCrlf & "    ss:Bold=""1""/>"
	xml_str = xml_str & vbCrlf & "   <Interior ss:Color=""#C9C9C9"" ss:Pattern=""Solid""/>"
	xml_str = xml_str & vbCrlf & "  </Style>"
	xml_str = xml_str & vbCrlf & "  <Style ss:ID=""s68"">"
	xml_str = xml_str & vbCrlf & "   <Alignment ss:Horizontal=""Center"" ss:Vertical=""Center"" ss:WrapText=""1""/>"
	xml_str = xml_str & vbCrlf & "   <Borders>"
	xml_str = xml_str & vbCrlf & "    <Border ss:Position=""Bottom"" ss:LineStyle=""Continuous"" ss:Weight=""1"""
	xml_str = xml_str & vbCrlf & "     ss:Color=""#000000""/>"
	xml_str = xml_str & vbCrlf & "    <Border ss:Position=""Left"" ss:LineStyle=""Continuous"" ss:Weight=""1"""
	xml_str = xml_str & vbCrlf & "     ss:Color=""#000000""/>"
	xml_str = xml_str & vbCrlf & "    <Border ss:Position=""Right"" ss:LineStyle=""Continuous"" ss:Weight=""1"""
	xml_str = xml_str & vbCrlf & "     ss:Color=""#000000""/>"
	xml_str = xml_str & vbCrlf & "    <Border ss:Position=""Top"" ss:LineStyle=""Continuous"" ss:Weight=""1"""
	xml_str = xml_str & vbCrlf & "     ss:Color=""#000000""/>"
	xml_str = xml_str & vbCrlf & "   </Borders>"
	xml_str = xml_str & vbCrlf & "  </Style>"
	xml_str = xml_str & vbCrlf & "  <Style ss:ID=""s70"">"
	xml_str = xml_str & vbCrlf & "   <Alignment ss:Vertical=""Center"" ss:WrapText=""1""/>"
	xml_str = xml_str & vbCrlf & "   <Borders>"
	xml_str = xml_str & vbCrlf & "    <Border ss:Position=""Bottom"" ss:LineStyle=""Continuous"" ss:Weight=""1"""
	xml_str = xml_str & vbCrlf & "     ss:Color=""#000000""/>"
	xml_str = xml_str & vbCrlf & "    <Border ss:Position=""Left"" ss:LineStyle=""Continuous"" ss:Weight=""1"""
	xml_str = xml_str & vbCrlf & "     ss:Color=""#000000""/>"
	xml_str = xml_str & vbCrlf & "    <Border ss:Position=""Right"" ss:LineStyle=""Continuous"" ss:Weight=""1"""
	xml_str = xml_str & vbCrlf & "     ss:Color=""#000000""/>"
	xml_str = xml_str & vbCrlf & "    <Border ss:Position=""Top"" ss:LineStyle=""Continuous"" ss:Weight=""1"""
	xml_str = xml_str & vbCrlf & "     ss:Color=""#000000""/>"
	xml_str = xml_str & vbCrlf & "   </Borders>"
	xml_str = xml_str & vbCrlf & "   <Font ss:FontName=""맑은 고딕"" x:CharSet=""129"" x:Family=""Modern"" ss:Color=""#000000""/>"
	xml_str = xml_str & vbCrlf & "  </Style>"
	xml_str = xml_str & vbCrlf & "  <Style ss:ID=""s71"">"
	xml_str = xml_str & vbCrlf & "   <Alignment ss:Horizontal=""Center"" ss:Vertical=""Center"" ss:WrapText=""1""/>"
	xml_str = xml_str & vbCrlf & "   <Borders>"
	xml_str = xml_str & vbCrlf & "    <Border ss:Position=""Bottom"" ss:LineStyle=""Continuous"" ss:Weight=""1"""
	xml_str = xml_str & vbCrlf & "     ss:Color=""#000000""/>"
	xml_str = xml_str & vbCrlf & "    <Border ss:Position=""Left"" ss:LineStyle=""Continuous"" ss:Weight=""1"""
	xml_str = xml_str & vbCrlf & "     ss:Color=""#000000""/>"
	xml_str = xml_str & vbCrlf & "    <Border ss:Position=""Right"" ss:LineStyle=""Continuous"" ss:Weight=""1"""
	xml_str = xml_str & vbCrlf & "     ss:Color=""#000000""/>"
	xml_str = xml_str & vbCrlf & "    <Border ss:Position=""Top"" ss:LineStyle=""Continuous"" ss:Weight=""1"""
	xml_str = xml_str & vbCrlf & "     ss:Color=""#000000""/>"
	xml_str = xml_str & vbCrlf & "   </Borders>"
	xml_str = xml_str & vbCrlf & "   <Font ss:FontName=""맑은 고딕"" x:CharSet=""129"" x:Family=""Modern"" ss:Color=""#000000""/>"
	xml_str = xml_str & vbCrlf & "  </Style>"
	xml_str = xml_str & vbCrlf & "  <Style ss:ID=""s72"">"
	xml_str = xml_str & vbCrlf & "   <Alignment ss:Horizontal=""Center"" ss:Vertical=""Center"" ss:WrapText=""1""/>"
	xml_str = xml_str & vbCrlf & "   <Borders>"
	xml_str = xml_str & vbCrlf & "    <Border ss:Position=""Bottom"" ss:LineStyle=""Continuous"" ss:Weight=""1"""
	xml_str = xml_str & vbCrlf & "     ss:Color=""#000000""/>"
	xml_str = xml_str & vbCrlf & "    <Border ss:Position=""Left"" ss:LineStyle=""Continuous"" ss:Weight=""1"""
	xml_str = xml_str & vbCrlf & "     ss:Color=""#000000""/>"
	xml_str = xml_str & vbCrlf & "    <Border ss:Position=""Right"" ss:LineStyle=""Continuous"" ss:Weight=""1"""
	xml_str = xml_str & vbCrlf & "     ss:Color=""#000000""/>"
	xml_str = xml_str & vbCrlf & "    <Border ss:Position=""Top"" ss:LineStyle=""Continuous"" ss:Weight=""1"""
	xml_str = xml_str & vbCrlf & "     ss:Color=""#000000""/>"
	xml_str = xml_str & vbCrlf & "   </Borders>"
	xml_str = xml_str & vbCrlf & "   <Font ss:FontName=""맑은 고딕"" x:CharSet=""129"" x:Family=""Modern"" ss:Color=""#000000"""
	xml_str = xml_str & vbCrlf & "    ss:Bold=""1""/>"
	xml_str = xml_str & vbCrlf & "  </Style>"
	xml_str = xml_str & vbCrlf & "  <Style ss:ID=""s73"">"
	xml_str = xml_str & vbCrlf & "   <Alignment ss:Vertical=""Center"" ss:WrapText=""1""/>"
	xml_str = xml_str & vbCrlf & "   <Borders>"
	xml_str = xml_str & vbCrlf & "    <Border ss:Position=""Bottom"" ss:LineStyle=""Continuous"" ss:Weight=""1"""
	xml_str = xml_str & vbCrlf & "     ss:Color=""#000000""/>"
	xml_str = xml_str & vbCrlf & "    <Border ss:Position=""Left"" ss:LineStyle=""Continuous"" ss:Weight=""1"""
	xml_str = xml_str & vbCrlf & "     ss:Color=""#000000""/>"
	xml_str = xml_str & vbCrlf & "    <Border ss:Position=""Right"" ss:LineStyle=""Continuous"" ss:Weight=""1"""
	xml_str = xml_str & vbCrlf & "     ss:Color=""#000000""/>"
	xml_str = xml_str & vbCrlf & "    <Border ss:Position=""Top"" ss:LineStyle=""Continuous"" ss:Weight=""1"""
	xml_str = xml_str & vbCrlf & "     ss:Color=""#000000""/>"
	xml_str = xml_str & vbCrlf & "   </Borders>"
	xml_str = xml_str & vbCrlf & "   <Font ss:FontName=""맑은 고딕"" x:CharSet=""129"" x:Family=""Modern"" ss:Size=""11"""
	xml_str = xml_str & vbCrlf & "    ss:Color=""#000000"" ss:Bold=""1""/>"
	xml_str = xml_str & vbCrlf & "  </Style>"
	xml_str = xml_str & vbCrlf & "  <Style ss:ID=""s74"">"
	xml_str = xml_str & vbCrlf & "   <Font ss:FontName=""맑은 고딕"" x:CharSet=""129"" x:Family=""Modern"" ss:Size=""11"""
	xml_str = xml_str & vbCrlf & "    ss:Color=""#000000"" ss:Bold=""1""/>"
	xml_str = xml_str & vbCrlf & "  </Style>"
	xml_str = xml_str & vbCrlf & "  <Style ss:ID=""s76"">"
	xml_str = xml_str & vbCrlf & "   <Alignment ss:Horizontal=""Center"" ss:Vertical=""Center"" ss:WrapText=""1""/>"
	xml_str = xml_str & vbCrlf & "   <Borders>"
	xml_str = xml_str & vbCrlf & "    <Border ss:Position=""Bottom"" ss:LineStyle=""Continuous"" ss:Weight=""1"""
	xml_str = xml_str & vbCrlf & "     ss:Color=""#000000""/>"
	xml_str = xml_str & vbCrlf & "    <Border ss:Position=""Left"" ss:LineStyle=""Continuous"" ss:Weight=""1"""
	xml_str = xml_str & vbCrlf & "     ss:Color=""#000000""/>"
	xml_str = xml_str & vbCrlf & "    <Border ss:Position=""Right"" ss:LineStyle=""Continuous"" ss:Weight=""1"""
	xml_str = xml_str & vbCrlf & "     ss:Color=""#000000""/>"
	xml_str = xml_str & vbCrlf & "    <Border ss:Position=""Top"" ss:LineStyle=""Continuous"" ss:Weight=""1"""
	xml_str = xml_str & vbCrlf & "     ss:Color=""#000000""/>"
	xml_str = xml_str & vbCrlf & "   </Borders>"
	xml_str = xml_str & vbCrlf & "   <Font ss:FontName=""맑은 고딕"" x:CharSet=""129"" x:Family=""Modern"" ss:Color=""#000000"""
	xml_str = xml_str & vbCrlf & "    ss:Bold=""1""/>"
	xml_str = xml_str & vbCrlf & "   <Interior ss:Color=""#C9C9C9"" ss:Pattern=""Solid""/>"
	xml_str = xml_str & vbCrlf & "   <NumberFormat ss:Format=""m&quot;/&quot;d;@""/>"
	xml_str = xml_str & vbCrlf & "  </Style>"
	xml_str = xml_str & vbCrlf & "  <Style ss:ID=""s87"">"
	xml_str = xml_str & vbCrlf & "   <Alignment ss:Horizontal=""Center"" ss:Vertical=""Center""/>"
	xml_str = xml_str & vbCrlf & "   <Borders>"
	xml_str = xml_str & vbCrlf & "    <Border ss:Position=""Bottom"" ss:LineStyle=""Continuous"" ss:Weight=""1"""
	xml_str = xml_str & vbCrlf & "     ss:Color=""#000000""/>"
	xml_str = xml_str & vbCrlf & "   </Borders>"
	xml_str = xml_str & vbCrlf & "   <Font ss:FontName=""맑은 고딕"" x:CharSet=""129"" x:Family=""Modern"" ss:Size=""14"""
	xml_str = xml_str & vbCrlf & "    ss:Color=""#000000"" ss:Bold=""1""/>"
	xml_str = xml_str & vbCrlf & "  </Style>"
	xml_str = xml_str & vbCrlf & " </Styles>"


	xml_str = xml_str & vbCrlf & " <Worksheet ss:Name=""출고집계현황조회"">"
	xml_str = xml_str & vbCrlf & "  <Names>"
	xml_str = xml_str & vbCrlf & "   <NamedRange ss:Name=""Print_Titles"" ss:RefersTo=""=출고집계현황조회!R1:R2""/>"
	xml_str = xml_str & vbCrlf & "  </Names>"
	xml_str = xml_str & vbCrlf & "  <Table ss:ExpandedColumnCount=""9999"" ss:ExpandedRowCount=""99999"" x:FullColumns=""1"""
	xml_str = xml_str & vbCrlf & "   x:FullRows=""1"" ss:DefaultColumnWidth=""54"" ss:DefaultRowHeight=""16.5"">"
	xml_str = xml_str & vbCrlf & "   <Column ss:Width=""82.5""/>"
	xml_str = xml_str & vbCrlf & "   <Column ss:Width=""249.75""/>"
	xml_str = xml_str & vbCrlf & "   <Column ss:AutoFitWidth=""0"" ss:Width=""32.25"" ss:Span=""#ColDateSize#""/>"
	xml_str = xml_str & vbCrlf & "   <Column ss:Index=""#TitleTotalIndex#"" ss:StyleID=""s74"" ss:AutoFitWidth=""0"" ss:Width=""40.5""/>"
	xml_str = xml_str & vbCrlf & "   <Row ss:AutoFitHeight=""0"" ss:Height=""35.25"">"

	xml_str = xml_str & vbCrlf & "    <Cell ss:MergeAcross=""#TitleMerge#"" ss:StyleID=""s87""><Data ss:Type=""String"">출고집계현황 [ " & startDate & " ~ " & endDate & " ]</Data><NamedCell"
	xml_str = xml_str & vbCrlf & "      ss:Name=""Print_Titles""/></Cell>"
	xml_str = xml_str & vbCrlf & "   </Row>"
	xml_str = xml_str & vbCrlf & "   <Row ss:AutoFitHeight=""0"" ss:Height=""22.5"" ss:StyleID=""s62"">"
	xml_str = xml_str & vbCrlf & "    <Cell ss:StyleID=""s64""><Data ss:Type=""String"">제품코드</Data><NamedCell"
	xml_str = xml_str & vbCrlf & "      ss:Name=""Print_Titles""/></Cell>"
	xml_str = xml_str & vbCrlf & "    <Cell ss:StyleID=""s64""><Data ss:Type=""String"">제품명</Data><NamedCell"
	xml_str = xml_str & vbCrlf & "      ss:Name=""Print_Titles""/></Cell>"

	aColName = Array()
	For Each col In rs.Fields
		Select Case col.Name
			Case "prod_code", "prod_name", "TotCount"
			Case Else
				xml_str = xml_str & vbCrlf & "    <Cell ss:StyleID=""s76""><Data ss:Type=""DateTime"">" & col.Name & "</Data><NamedCell"
				xml_str = xml_str & vbCrlf & "      ss:Name=""Print_Titles""/></Cell>"
		End Select 

		ReDim Preserve aColName(ubound(aColName) + 1)
		aColName(ubound(aColName)) = col.Name
	Next 

	xml_str = xml_str & vbCrlf & "    <Cell ss:StyleID=""s64""><Data ss:Type=""String"">계</Data><NamedCell"
	xml_str = xml_str & vbCrlf & "      ss:Name=""Print_Titles""/></Cell>"
	xml_str = xml_str & vbCrlf & "   </Row>"


	nTotSum = 0
	aSumCount = Array()
	For i = 0 To ubound(aColName)
		ReDim Preserve aSumCount(i)
		aSumCount(i) = 0
	Next 

	Do Until rs.Bof Or rs.Eof 
		xml_str = xml_str & vbCrlf & "   <Row ss:AutoFitHeight=""1"" ss:Height=""22"">"
		xml_str = xml_str & vbCrlf & "    <Cell ss:StyleID=""s70""><Data ss:Type=""String"">" & rs("prod_code") & "</Data></Cell>"
		xml_str = xml_str & vbCrlf & "    <Cell ss:StyleID=""s70""><Data ss:Type=""String"">&#160;" & rs("prod_name") & "</Data></Cell>"

		For i = 0 To ubound(aColName)
			Select Case aColName(i)
				Case "prod_code", "prod_name"
				Case "TotCount"
					nTotSum = nTotSum + CInt(rs("TotCount"))
				Case Else 
					xml_str = xml_str & vbCrlf & "    <Cell ss:StyleID=""s71""><Data ss:Type=""Number"">" & rs(aColName(i)) & "</Data></Cell>"
					aSumCount(i) = aSumCount(i) + CInt(rs(aColName(i)))
			End Select 
		Next 

		xml_str = xml_str & vbCrlf & "    <Cell ss:StyleID=""s72""><Data ss:Type=""Number"">" & rs("TotCount") & "</Data></Cell>"
		xml_str = xml_str & vbCrlf & "   </Row>"

		rs.movenext
	Loop 

	xml_str = xml_str & vbCrlf & "   <Row ss:AutoFitHeight=""0"" ss:Height=""21.75"" ss:StyleID=""s74"">"
	xml_str = xml_str & vbCrlf & "    <Cell ss:StyleID=""s72""><Data ss:Type=""String"">합&#160;&#160;&#160;계</Data></Cell>"
	xml_str = xml_str & vbCrlf & "    <Cell ss:StyleID=""s73""/>"

	For i = 0 To ubound(aColName)
		Select Case aColName(i)
			Case "prod_code", "prod_name"
			Case "TotCount"
				xml_str = xml_str & vbCrlf & "    <Cell ss:StyleID=""s72""><Data ss:Type=""Number"">" & nTotSum & "</Data></Cell>"
			Case Else 
				xml_str = xml_str & vbCrlf & "    <Cell ss:StyleID=""s72""><Data ss:Type=""Number"">" & aSumCount(i) & "</Data></Cell>"
		End Select 
	Next 

	xml_str = xml_str & vbCrlf & "   </Row>"
	xml_str = xml_str & vbCrlf & "  </Table>"
	xml_str = xml_str & vbCrlf & "  <WorksheetOptions xmlns=""urn:schemas-microsoft-com:office:excel"">"
	xml_str = xml_str & vbCrlf & "   <PageSetup>"
	xml_str = xml_str & vbCrlf & "    <Layout x:Orientation=""Landscape""/>"
	xml_str = xml_str & vbCrlf & "    <Header x:Margin=""0""/>"
	xml_str = xml_str & vbCrlf & "    <Footer x:Margin=""0.39370078740157483"" x:Data=""&amp;C&amp;P / &amp;N""/>"
	xml_str = xml_str & vbCrlf & "    <PageMargins x:Bottom=""0.39370078740157483"" x:Left=""0.39370078740157483"""
	xml_str = xml_str & vbCrlf & "     x:Right=""0.39370078740157483"" x:Top=""0.39370078740157483""/>"
	xml_str = xml_str & vbCrlf & "   </PageSetup>"
	xml_str = xml_str & vbCrlf & "   <FitToPage/>"
	xml_str = xml_str & vbCrlf & "   <Print>"
	xml_str = xml_str & vbCrlf & "    <FitHeight>0</FitHeight>"
	xml_str = xml_str & vbCrlf & "    <ValidPrinterInfo/>"
	xml_str = xml_str & vbCrlf & "    <PaperSizeIndex>9</PaperSizeIndex>"
	xml_str = xml_str & vbCrlf & "    <Scale>75</Scale>"
	xml_str = xml_str & vbCrlf & "    <HorizontalResolution>600</HorizontalResolution>"
	xml_str = xml_str & vbCrlf & "    <VerticalResolution>600</VerticalResolution>"
	xml_str = xml_str & vbCrlf & "   </Print>"
	xml_str = xml_str & vbCrlf & "   <Selected/>"
	xml_str = xml_str & vbCrlf & "   <FreezePanes/>"
	xml_str = xml_str & vbCrlf & "   <FrozenNoSplit/>"
	xml_str = xml_str & vbCrlf & "   <SplitHorizontal>2</SplitHorizontal>"
	xml_str = xml_str & vbCrlf & "   <TopRowBottomPane>2</TopRowBottomPane>"
	xml_str = xml_str & vbCrlf & "   <SplitVertical>2</SplitVertical>"
	xml_str = xml_str & vbCrlf & "   <LeftColumnRightPane>2</LeftColumnRightPane>"
	xml_str = xml_str & vbCrlf & "   <ActivePane>0</ActivePane>"
	xml_str = xml_str & vbCrlf & "   <Panes>"
	xml_str = xml_str & vbCrlf & "    <Pane>"
	xml_str = xml_str & vbCrlf & "     <Number>3</Number>"
	xml_str = xml_str & vbCrlf & "    </Pane>"
	xml_str = xml_str & vbCrlf & "    <Pane>"
	xml_str = xml_str & vbCrlf & "     <Number>1</Number>"
	xml_str = xml_str & vbCrlf & "    </Pane>"
	xml_str = xml_str & vbCrlf & "    <Pane>"
	xml_str = xml_str & vbCrlf & "     <Number>2</Number>"
	xml_str = xml_str & vbCrlf & "    </Pane>"
	xml_str = xml_str & vbCrlf & "    <Pane>"
	xml_str = xml_str & vbCrlf & "     <Number>0</Number>"
	xml_str = xml_str & vbCrlf & "     <ActiveRow>0</ActiveRow>"
	xml_str = xml_str & vbCrlf & "     <ActiveCol>0</ActiveCol>"
	xml_str = xml_str & vbCrlf & "    </Pane>"
	xml_str = xml_str & vbCrlf & "   </Panes>"
	xml_str = xml_str & vbCrlf & "   <ProtectObjects>False</ProtectObjects>"
	xml_str = xml_str & vbCrlf & "   <ProtectScenarios>False</ProtectScenarios>"
	xml_str = xml_str & vbCrlf & "  </WorksheetOptions>"
	xml_str = xml_str & vbCrlf & " </Worksheet>"
	xml_str = xml_str & vbCrlf & "</Workbook>"

	nDateDiff = DateDiff("d", startDate, endDate)
	xml_str = Replace(xml_str, "#ColDateSize#", nDateDiff - 1)
	xml_str = Replace(xml_str, "#TitleTotalIndex#", ubound(aColName))
	xml_str = Replace(xml_str, "#TitleMerge#", ubound(aColName))

	response.write xml_str

	filename = "출고집계현황" & "[" & Replace(startDate, "-", "") & "-" & Replace(endDate, "-", "") & "]" & ".xls"


	response.expires = 0
	response.buffer = true
	'Response.ContentType  = "application/x-msexcel" ' xls
	Response.ContentType  = "application/vnd.ms-excel" ' xlsx				'// 이거 사용시 파일을 읽을 수 없습니다. 오류가 종종 발생함.
	'Response.ContentType  = "application/vnd.xls" ' xlsx
	Response.CacheControl = "public"
	Response.AddHeader  "Content-Disposition" , "attachment; filename=" & filename & ".xls"
%>

<!-- #include virtual = "/share/include/DbClose.asp" -->
